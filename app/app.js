var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');
var data = require('./routes/data');
var rootfiles = require('./routes/rootfiles');
var asciifiles = require('./routes/asciifiles');
var app = express();

// Authentication module. 
var auth = require('http-auth');
var basic = auth.basic({
    realm: "TPX3 Visualization Area.",
    file: __dirname + "/.htpasswd",
    skipUser: true
});

app.use(auth.connect(basic));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Global settings
app.use(function(req, res, next){
  res.setHeader('Access-Control-Allow-Origin', '*');
  next();
});

app.use('/', routes);
app.use('/data', data);
app.use('/rootfiles', rootfiles);
app.use('/asciifiles', asciifiles)

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// error logger - writes error message into console
app.use(function(err, req, res, next) {
  console.error(err.message);
  next(err);
})

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
