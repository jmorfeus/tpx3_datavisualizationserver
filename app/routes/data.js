var express = require('express');
var router = express.Router();
var exec = require('child_process').exec;

var tpx3data = require('../cpp/build/Release/tpx3data');

/* GET data. */
router.get('/', function(req, res, next) {
	res.send("Server is up and running!");
});

router.param('file', function(req, res, next, file){
	next();
})

/* GET data. */
router.get('/interval/:startTime/:timeSlice/:file?', function(req, res, next) {
	var startTime = req.params.startTime;
	var timeSlice = req.params.timeSlice;
	var file = req.params.file;
	var command = 'find /data/tpx3-visualization-data/ROOT/ -name "' + file + '"';
	exec(command, function(error, stdout, stderr){
		file = stdout.trim();

		if (isNaN(startTime) || isNaN(timeSlice)){
			res.status(404).send('Wrong parameters.');
		} else {
			if (file && file != undefined && file != ""){
				tpx3data.getIntervalAsync(Number(startTime), Number(timeSlice), file, function(data){
					res.send(data);
				});
			} else {
				res.send(tpx3data.getInterval(Number(startTime), Number(timeSlice)));
			}
		}

	});
	return;
});


module.exports = router;
