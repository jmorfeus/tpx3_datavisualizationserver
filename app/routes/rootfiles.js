var express = require('express');
var router = express.Router();
var exec = require('child_process').exec;
var fs = require('fs'); 


function validateParams(req){
	req.params.folder = req.params.folder ? req.params.folder + "/" : "";
	req.params.subfolder = req.params.subfolder ? req.params.subfolder + "/" : "";
	req.params.subsubfolder = req.params.subsubfolder ? req.params.subsubfolder + "/" : "";
	req.params.subsubsubfolder = req.params.subsubsubfolder ? req.params.subsubsubfolder + "/" : "";
};


/* GET /rootfiles */
router.get('/files/:folder?/:subfolder?/:subsubfolder?/:subsubsubfolder?', function(req, res, next) {
	validateParams(req);

	var dir = '/data/tpx3-visualization-data/ROOT/' + req.params.folder + req.params.subfolder + req.params.subsubfolder;
	var files = {};
	files["Files"] = [];

	try {
        stat = fs.statSync(dir);
		var filenames = fs.readdirSync(dir);
		filenames.forEach(function(filename){
				var stats = fs.statSync(dir + filename);
				if(stats.isFile() && filename != "readme.txt" && filename.match(/^(.*\.(root)$)/igm)){
						files["Files"].push(filename);
					}
			});
		var jsonres = JSON.stringify(files);
		res.send(jsonres); 

	} catch (err) {
        res.send(JSON.stringify(files));
    }

});

router.get('/folder-list/:folder?/:subfolder?/:subsubfolder?/:subsubsubfolder?', function(req, res, next){
	validateParams(req);
	var dir = '/data/tpx3-visualization-data/ROOT/' + req.params.folder + req.params.subfolder + req.params.subsubfolder + req.params.subsubsubfolder;
	var folders = {};
	folders["Folders"] = [];
	try {
        stat = fs.statSync(dir);
		var foldernames = fs.readdirSync(dir);
		foldernames.forEach(function(foldername){
				var stats = fs.statSync(dir + foldername);
				if(stats.isDirectory() && foldername != "previews"){
						folders["Folders"].push(foldername);
					}
			});
		if (req.params.folder){
			folders["Folders"].push("..");
		}
		folders["Folders"].push(".");
		var jsonres = JSON.stringify(folders);
		res.send(jsonres);

    } catch (err) {
        res.send(JSON.stringify(folders));
    }

});

router.get('/preview/:file', function(req, res){
	if (req.params.file){
		var command = "find /data/tpx3-visualization-data/ROOT/ -name " + req.params.file;
		exec(command, function(error, stdout, stderr){
			file = stdout.trim();

			if (file && file != undefined && file != ""){
				res.sendFile(file);
			} else {
				res.send("");
				return;
			}
		});
	}
});


module.exports = router;
