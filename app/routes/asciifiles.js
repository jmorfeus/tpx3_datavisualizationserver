var express = require('express');
var router = express.Router();
var exec = require('child_process').exec;
var tpx3data = require('../cpp/build/Release/tpx3data');
var multer = require('multer');
var sanitize = require("sanitize-filename");
var fs = require('fs'); 


var storage = multer.diskStorage({
  destination: function (req, file, cb) {
  	validateParams(req);
	var dest = '/data/tpx3-visualization-data/RAW/' + req.params.folder + req.params.subfolder + req.params.subsubfolder;
	try {
        stat = fs.statSync(dest);
    } catch (err) {
        fs.mkdirSync(dest);
    }
    cb(null, dest);
  },
  filename: function (req, file, cb) {
    cb(null,  'wu-' + sanitize(encodeURI(file.originalname.replace(/[^A-Za-z0-9]+/g, "_"))) + Date.now());
  }
});

var upload = multer({storage: storage}).single('asciiFile');

/**
* Checks the input parameters, specifying which folder to upload to. If new folder is specified, sanitizes the name to be able to create safe new folder. 
* Converts all undefined parameters to empty string.
*/
function validateParams(req){
	if (req.body.newFolderName){
		req.body.newFolderName = sanitize(encodeURI(req.body.newFolderName.replace(/[^A-Za-z0-9]+/g, "_")));
		if (!req.params.folder) req.params.folder = req.body.newFolderName;
		else if (!req.params.subfolder) req.params.subfolder = req.body.newFolderName;
		else if (!req.params.subsubfolder) req.params.subsubfolder = req.body.newFolderName;
	}
	req.params.folder = req.params.folder ? req.params.folder + "/" : "";
	req.params.subfolder = req.params.subfolder ? req.params.subfolder + "/" : "";
	req.params.subsubfolder = req.params.subsubfolder ? req.params.subsubfolder + "/" : "";
};

/** GET /asciifiles 
* Gets list of ASCII files on the server, within specified directory/subdirectory
*/
router.get('/files/:folder?/:subfolder?/:subsubfolder?', function(req, res, next) {
	validateParams(req);
	var dir = '/data/tpx3-visualization-data/RAW/' + req.params.folder + req.params.subfolder + req.params.subsubfolder;
	var files = {};
	files["Files"] = [];
	var filenames = fs.readdirSync(dir);
	filenames.forEach(function(filename){
			var stats = fs.statSync(dir + filename);
			if(stats.isFile() && filename != "readme.txt"){
					files["Files"].push(filename);
				}
		});
	var jsonres = JSON.stringify(files);
	res.send(jsonres);
});


router.get('/convert/:filename/:folder?/:subfolder?/:subsubfolder?', function(req, res, next){
	var file = req.params.filename;
	validateParams(req);

	var dest = req.params.folder;
	try {
        stat = fs.statSync('/data/tpx3-visualization-data/ROOT/' + dest);
    } catch (err) {
        fs.mkdirSync('/data/tpx3-visualization-data/ROOT/' + dest);
    }
	dest += req.params.subfolder;
	try {
        stat = fs.statSync('/data/tpx3-visualization-data/ROOT/' + dest);
    } catch (err) {
        fs.mkdirSync('/data/tpx3-visualization-data/ROOT/' + dest);
    }
	dest += req.params.subsubfolder;
	try {
        stat = fs.statSync('/data/tpx3-visualization-data/ROOT/' + dest);
    } catch (err) {
        fs.mkdirSync('/data/tpx3-visualization-data/ROOT/' + dest);
    }

	if (typeof file === 'string' || file instanceof String){
		res.send(tpx3data.convert(dest + file, dest + file));	
	} else {
		var err = new Error("Wrong parameter");
		err.status = 404;
		throw err;
	}
});

router.get('/upload/:folder?/:subfolder?/:subsubfolder?', function (req, res, next) {
	validateParams(req);
	var folderName = req.params.folder + req.params.subfolder + req.params.subsubfolder;
	res.render('upload', {folder: folderName});
});

router.post('/upload/:folder?/:subfolder?/:subsubfolder?', function(req, res, next){
	upload(req, res, function (err) {
	    if (err){
			console.log("ERROR " + err.message);
			res.redirect('/asciifiles/upload-failure/' + req.params.folder + req.params.subfolder + req.params.subsubfolder);
		} else {
			res.redirect('/asciifiles/upload-success/' + req.params.folder + req.params.subfolder + req.params.subsubfolder);
		}
    });
});

router.get('/upload-failure/:folder?/:subfolder?/:subsubfolder?', function(req, res, next){
	validateParams(req);
	var folderName = req.params.folder + req.params.subfolder + req.params.subsubfolder;
	res.render('upload', {message: "There has been error the uploading the file. Permission denied."});
});

router.get('/upload-success/:folder?/:subfolder?/:subsubfolder?', function(req, res, next){
	validateParams(req);
	var folderName = req.params.folder + req.params.subfolder + req.params.subsubfolder;
	res.render('upload', {message: "File has been successfully uploaded.", folder: folderName});
});

router.get('/convert-failure/:folder?/:subfolder?/:subsubfolder?', function(req, res, next){
	validateParams(req);
	var folderName = req.params.folder + req.params.subfolder + req.params.subsubfolder;
	res.render('upload', {message: "There has been error the converting the file. Possibly wrong format.", folder: folderName});
});

router.get('/convert-success/:folder?/:subfolder?/:subsubfolder?', function(req, res, next){
	validateParams(req);
	var folderName = req.params.folder + req.params.subfolder + req.params.subsubfolder;
	res.render('upload', {message: "File has been successfully converted!", folder: folderName});
});

router.get('/folder-list/:folder?/:subfolder?/:subsubfolder?', function(req, res, next){
	validateParams(req);
	var dir = '/data/tpx3-visualization-data/RAW/' + req.params.folder + req.params.subfolder + req.params.subsubfolder;
	var folders = {};
	folders["Folders"] = [];
	var foldernames = fs.readdirSync(dir);
	foldernames.forEach(function(foldername){
			var stats = fs.statSync(dir + foldername);
			if(stats.isDirectory()){
					folders["Folders"].push(foldername);
				}
		});
	if (req.params.folder){
		folders["Folders"].push("..");
	}
	var jsonres = JSON.stringify(folders);
	res.send(jsonres);
});

module.exports = router;
