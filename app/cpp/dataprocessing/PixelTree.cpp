#include "PixelTree.h"

const std::string PixelTree::defaultDataTreeName = "Datatree";

TTree * PixelTree::GetTree(){
	return this->t_data;
}

Int_t PixelTree::Write(const char * name, Int_t option){
	return this->t_data->Write(name, option, 0);
}

void PixelTree::InitializeBranches(){
	this->t_data->Branch("index", &this->m_index, "index/l");
	this->t_data->Branch("PixX", &this->m_PixX, "PixX/S");
	this->t_data->Branch("PixY", &this->m_PixY, "PixY/S");
	this->t_data->Branch("ToA", &this->m_ToA, "ToA/D");
	this->t_data->Branch("ToT", &this->m_ToT, "ToT/I");
	this->t_data->Branch("sToA", &this->m_sToA, "sToA/l");
	this->t_data->Branch("fToA", &this->m_fToA, "fToA/i");
	this->t_data->Branch("triggerNo", &this->m_triggerNo, "triggerNo/l");
	this->t_data->Branch("ToT_hd", &this->m_ToT_hd, "ToT_hd/D");
	this->t_data->Branch("ToT_KeV", &this->m_ToT_KeV, "ToT_KeV/D");
	this->t_data->Branch("ToT_e", &this->m_ToT_e, "ToT_e/D");
	this->t_data->Branch("ToA_tw", &this->m_ToA_tw, "ToA_tw/D");
}

void PixelTree::SetBranchesAddresses(){
	if (this->t_data->GetBranch("index")){
		this->t_data->SetBranchAddress("index", &this->m_index);
	}
	if (this->t_data->GetBranch("PixX")){
		this->t_data->SetBranchAddress("PixX", &this->m_PixX);
	}
	if (this->t_data->GetBranch("PixY")){
		this->t_data->SetBranchAddress("PixY", &this->m_PixY);
	}
	if (this->t_data->GetBranch("ToA")){
		this->t_data->SetBranchAddress("ToA", &this->m_ToA);
	}
	if (this->t_data->GetBranch("ToT")){
		this->t_data->SetBranchAddress("ToT", &this->m_ToT);
	}
	if (this->t_data->GetBranch("sToA")){
		this->t_data->SetBranchAddress("sToA", &this->m_sToA);
	}
	if (this->t_data->GetBranch("fToA")){
		this->t_data->SetBranchAddress("fToA", &this->m_fToA);
	}
	if (this->t_data->GetBranch("triggerNo")){
		this->t_data->SetBranchAddress("triggerNo", &this->m_triggerNo);
	}
	if (this->t_data->GetBranch("ToT_hd")){
		this->t_data->SetBranchAddress("ToT_hd", &this->m_ToT_hd);
	}
	if (this->t_data->GetBranch("ToT_KeV")){
		this->t_data->SetBranchAddress("ToT_KeV", &this->m_ToT_KeV);
	}
	if (this->t_data->GetBranch("ToT_e")){
		this->t_data->SetBranchAddress("ToT_e", &this->m_ToT_e);
	}
	if (this->t_data->GetBranch("ToA_tw")){
		this->t_data->SetBranchAddress("ToA_tw", &this->m_ToA_tw);
	}
}

bool PixelTree::SavePixel(SinglePixel * currentPixel){
	// arbitrary event index
	this->m_index = currentPixel->GetIndex();
	// pixel X coordinate
	this->m_PixX = currentPixel->GetPixX();
	// pixel Y coordinate
	this->m_PixY = currentPixel->GetPixY();
	// real Time of Acquisition
	this->m_ToA = currentPixel->GetToA();
	// energy value
	this->m_ToT = currentPixel->GetToT();
	// trigger number
	this->m_triggerNo = currentPixel->GetTriggerNo();
	// slow Time of Acquisition
	this->m_sToA = currentPixel->GetSToA();
	// fast Time of Acquisition
	this->m_fToA = currentPixel->GetFToA();
	// ToT hd value
	this->m_ToT_hd = currentPixel->GetToT_hd();
	// ToT e- value
	this->m_ToT_e = currentPixel->GetToT_e();
	// ToT KeV value
	this->m_ToT_KeV = currentPixel->GetToT_KeV();
	// ToA timewalk correction value
	this->m_ToA_tw = currentPixel->GetToA_tw();

	// save current data into TTree
	this->t_data->Fill();

	return true;
}

SinglePixel * PixelTree::GetPixelAt(ULong64_t index)
{
	this->SetBranchesAddresses();
	this->t_data->GetEntry(index);
	return new SinglePixel(this->m_index, this->m_PixX, this->m_PixY, this->m_ToA, this->m_ToT, this->m_triggerNo, this->m_sToA, this->m_fToA, this->m_ToT_hd, this->m_ToT_e, this->m_ToT_KeV, this->m_ToA_tw);
}

