/// <reference path="tools.ts" />

namespace tpx3.unitdisplayer{

	export class UnitDisplayer extends BaseObject{
		public static GIGA:number = 0.000000001;
		public static MEGA:number = 0.000001;
		public static KILO:number = 0.001;
		public static BASE:number = 1;
		public static MILLI:number = 1000;
		public static MICRO:number = 1000000;
		public static NANO:number = 1000000000;

		protected _currentUnits: number;

		/** Trivial */
		public static nanoTobase = function(nanoValue: number) : number {
			return nanoValue / UnitDisplayer.NANO;
		}

		/** Trivial */
		public static baseToNano = function(secValue: number) : number {
			return secValue * UnitDisplayer.NANO;
		}

		/** Trivial */
		public static baseToMilli = function(secValue: number) : number {
			return secValue * UnitDisplayer.MILLI;
		}

		public static milliToBase = function(milliValue: number) : number {
			return milliValue / UnitDisplayer.MILLI;
		}

		constructor(){
			super("UnitDisplayer");
			this._currentUnits = UnitDisplayer.NANO;
		}

		getCurrentUnits(){
			return this._currentUnits;
		}

		setCurrentUnits(newUnits: number){
			this._currentUnits = newUnits;
		}

		parseValueWithUnits(inputString){
			let arr = inputString.split(" ");
			let units = arr[1].slice(0,-1);

			switch (units) {
				case "n":
					this.setCurrentUnits(UnitDisplayer.NANO);
					break;
				case "μ":
					this.setCurrentUnits(UnitDisplayer.MICRO);
					break;
				case "m":
					this.setCurrentUnits(UnitDisplayer.MILLI);
					break;
				case "":
					this.setCurrentUnits(UnitDisplayer.BASE);
					break;
			}
			return (+arr[0]);
		}

		getUnitName(inputUnits:number = 0){
			if (inputUnits == 0){
				inputUnits = this.getCurrentUnits();
			}
			switch (inputUnits) {
				case UnitDisplayer.NANO:
					return "nano";
				case UnitDisplayer.MICRO:
					return "micro";
				case UnitDisplayer.MILLI:
					return "milli";
				case UnitDisplayer.BASE:
					return "";
				default:
					return "";
			}
		}

		getUnitShortcut(inputUnits:number = 0){
			if (inputUnits == 0){
				inputUnits = this.getCurrentUnits();
			}
			switch (inputUnits) {
				case UnitDisplayer.NANO:
					return "n";
				case UnitDisplayer.MICRO:
					return "μ";
				case UnitDisplayer.MILLI:
					return "m";
				case UnitDisplayer.BASE:
					return "";
				default:
					return "";
			}
		}

		getBestUnits(inputValue:number, inputFormat:number):number{
			let value = this.convert(inputValue, inputFormat, UnitDisplayer.NANO);
			let negative = false;
			if (value < 0){
				value = Math.abs(value);
				negative = true;
			}
			this._currentUnits = UnitDisplayer.NANO;
			if (value > 99){
				value /= 1000;
				this._currentUnits = UnitDisplayer.MICRO;
				if (value > 99){
					value /= 1000;
					this._currentUnits = UnitDisplayer.MILLI;
					if (value > 99){
						value /= 1000;
						this._currentUnits = UnitDisplayer.BASE;
						// if (value > 99){
						// 	value /= 1000;
						// 	this._currentUnits = UnitDisplayer.KILO;
						// 	if (value > 99){
						// 		value /= 1000;
						// 		this._currentUnits = UnitDisplayer.MEGA;
						// 		if (value > 99){
						// 			value /= 1000;
						// 			this._currentUnits = UnitDisplayer.GIGA;
						// 		}
						// 	}
						// }
					}
				}
			}
			if (negative){
				value = 0 - value;
			}
			return value;
		}

		toSeconds(inputNumber:number, inputFormat:number = UnitDisplayer.NANO): number{
			return this.convert(inputNumber, inputFormat, UnitDisplayer.BASE);
		}

		convert(inputNumber:number, inputFormat:number, outputFormat:number):number{
			return inputNumber * outputFormat / inputFormat;
		}

	}
}