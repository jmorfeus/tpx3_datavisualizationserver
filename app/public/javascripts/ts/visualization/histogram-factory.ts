/// <reference path="histogram-canvas.ts" />

import ToAHistogram  = tpx3.histogram.ToAHistogramCanvas;
import ToTHistogram = tpx3.histogram.ToTHistogramCanvas;
import HitsHistogram = tpx3.histogram.HitsHistogramCanvas;
import HistogramCanvas = tpx3.histogram.HistogramCanvas;

namespace tpx3.histogramfactory{

	/** Factory class used for creating new instances of individual histograms and placing them correctly on the HTML page */
	export class HistogramFactory extends BaseObject {
		
		constructor(name = "") {
		 super(name);
		}

		/** Creates and returns new instance of histogram for displaying ToA values */
		createToAHistogram(parent: Sensor, id: string, data: any = null) : HistogramCanvas {
			return new ToAHistogram(parent, id, data);
		}

		/** Creates and returns new instance of histogram for displaying ToT values */
		createToTHistogram(parent: Sensor, id: string, data: any = null) : HistogramCanvas {
			return new ToTHistogram(parent, id, data);
		}

		/** Creates and returns new instance of histogram for displaying #hits */
		createHitsHistogram(parent: Sensor, id: string, data: any = null) : HistogramCanvas {
			return new HitsHistogram(parent, id, data);
		}

		/** Puts all the contents required for drawing the histogram into the HTML page */
		putHistogramHtmlContent(id: string, containerId: string, values: string, header: string){
			let jContainer = $('#' + containerId);
			jContainer.append(
				"<div class=\"col-md-4 hist-col\">\
                  " + header + "\
                    <div>\
                      <div class=\"canvas-wrapper\">\
                        <div class=\"canvas-inner-wrapper\">\
                          <canvas class=\"sensor-canvas\" id=\"" + id + "\">\
                          </canvas>\
                        </div>\
                      </div>\
                    </div>\
                  \
                  <canvas class=\"sensor-colormap-histogram\" id=\"" + id + "-colormap-histogram\">\
                  </canvas>\
                  <div class=\"sensor-scale-histogram\" id=\"" + id + "-scale-histogram\">\
                  </div>\
                  <div class=\"sensor-labels\">\
                  <p class=\"canvas-labels\"><span id=\"" + id + "-lock\" class=\"glyphicon glyphicon-lock\" style=\"display:none\"> </span>  X: <span id=\"" + id + "-pixX\">0</span> Y: <span id=\"" + id + "-pixY\">0</span> " + values + "</p>\
                  </div>\
                </div>");
		}

		/** Puts all the contents required for drawing the histogram in its collapsed state into the HTML page */
		putCollapsedHistogramContent(id: string, sensorName: string, values: string, header: string){
			let jNavContainer = $("#" + sensorName + "-tab-nav");
			let jContentContainer = $("#" + sensorName + "-tab-content");
			jNavContainer.prepend("<li class=\"control-tab-toa active\" id=\"" + id + "-nav-tab\" ><a data-toggle=\"tab\" href=\"#" + id + "-tab\">" + header + "</a></li>");
			jContentContainer.append(
				"<div id=\"" + id + "-tab\" class=\"tab-pane active\">\
                  <div class=\"canvas-wrapper\">\
                    <div class=\"canvas-inner-wrapper\">\
                      <canvas class=\"sensor-canvas\" id=\"" + id + "\">\
                      </canvas>\
                    </div>\
                  </div>\
\
                  <canvas class=\"sensor-colormap-histogram\" id=\"" + id + "-colormap-histogram\">\
                  </canvas>\
\
                  <div class=\"sensor-scale-histogram\" id=\"" + id + "-scale-histogram\">\
                  </div>\
                  \
                  <div class=\"sensor-labels\">\
                    <p class=\"canvas-labels\"><span id=\"" + id + "-lock\" class=\"glyphicon glyphicon-lock\" style=\"display:none\"> </span> X: <span id=\"" + id + "-pixX\">0</span> Y: <span id=\"" + id + "-pixY\">0</span> " + values + "</p>\
                  </div>\
                  </div>");
		}

		/** Adds a new histogram in the HTML page and return its created instance for manipulation */
		addToAHistogram(parent: Sensor, id: string, containerId: string){
			let header = "<h3 class=\"sensor-header\" data-toggle=\"tooltip\" title=\"Time of aquisition in nanoseconds\">ToA [ns]</h3>";
			let values = "ToA: <span id=\"" + id + "-toa\">0</span> sToA: <span id=\"" + id + "-stoa\">0</span> fToA: <span id=\"" + id + "-ftoa\">0</span>";
			this.putHistogramHtmlContent(id, containerId, values, header);
			return this.createToAHistogram(parent, id);
		}

		/** Adds a new histogram in the HTML page and return its created instance for manipulation */
		addToTHistogram(parent: Sensor, id: string, containerId: string){
			let header = "<h3 class=\"sensor-header\" data-toggle=\"tooltip\" title=\"Energy of the particle in clock counts\">ToT [cts]</h3>";
			let values = "ToT: <span id=\"" + id + "-tot\">0</span> ToT_KeV:  <span id=\"" + id + "-tot_KeV\">0</span>";
			this.putHistogramHtmlContent(id, containerId, values, header);
			return this.createToTHistogram(parent, id);
		}

		/** Adds a new histogram in the HTML page and return its created instance for manipulation */
		addHitsHistogram(parent: Sensor, id: string, containerId: string){
			let header = "<h3 class=\"sensor-header\" data-toggle=\"tooltip\" title=\"Number of hits of each pixel\">No. pixel hits</h3>";
			let values = "#hits: <span id=\"" + id + "-hits\">0</span>";
			this.putHistogramHtmlContent(id, containerId, values, header);
			return this.createHitsHistogram(parent, id);
		}

		/** Adds a new histogram in the HTML page in the collapsed state and return its created instance for manipulation */
		addCollapsedToAHistogram(parent: Sensor, id: string, sensorName: string){
			let values = "ToA: <span id=\"" + id + "-toa\">0</span> sToA: <span id=\"" + id + "-stoa\">0</span> fToA: <span id=\"" + id + "-ftoa\">0</span>";
			let header = "ToA";
			this.putCollapsedHistogramContent(id, sensorName, values, header);
			return this.createToAHistogram(parent, id);
		}

		/** Adds a new histogram in the HTML page in the collapsed state and return its created instance for manipulation */
		addCollapsedToTHistogram(parent: Sensor, id: string, sensorName: string){
			let values = "ToT: <span id=\"" + id + "-tot\">0</span> ToT_KeV:  <span id=\"" + id + "-tot_KeV\">0</span>";
			let header = "ToT";
			this.putCollapsedHistogramContent(id, sensorName, values, header);
			return this.createToTHistogram(parent, id);
		}

		/** Adds a new histogram in the HTML page in the collapsed state and return its created instance for manipulation */
		addCollapsedHitsHistogram(parent: Sensor, id: string, sensorName: string){
			let values = "#hits: <span id=\"" + id + "-hits\">0</span>";
			let header = "#hits";
			this.putCollapsedHistogramContent(id, sensorName, values, header);
			return this.createHitsHistogram(parent, id);
		}

	}

}

