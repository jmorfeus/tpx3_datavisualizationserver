/// <reference path="mockup_data.ts" />
/// <reference path="baseobject.ts" />
/// <reference path="data-filter.ts" />

import mockup_pixels = tpx3.mockupdata.mockup_pixels;
import DataFilter = tpx3.datafilter.DataFilter;

namespace tpx3.dataaccess {

/**
Class responsible for handling the data. Receiving from server, providing to other classes, filtering, etc.
*/
	export class DataAccess extends BaseObject{
		/** The pixel data */
		private _data: any[];
		/** Time offsets (timestamp to be added to each ToA) for each of the root file sources */
		private _offsets: number[];
		/** Current root files sources */
		private _rootFiles: string[];
		/** Parameter of the desired data interval -- it's starting time */
		private _startTime: number;
		/** Parameter of the desired data interval -- it's size */
		private _timeSlice: number;
		/** Address of the server providing the API for getting the data */
		private _serverAddress: string;
		/** collection of filters, based on which the proper data will be filtered and returned */
		private _filters: Array<DataFilter>;
		/** user-specified expression, based on which the data will be filtered */
		private _ownFilterString: string;
		/** Limitation of from where in the interval to start taking the data */
		private _startIndex: number[];
		/** Limitation of from where in the interval to end taking the data */
		private _endIndex: number[];
		/** allowed tags in the user-specified filter expression */
		private _allowedFilterTags = [];
		/** Marks how many sources were queried for the data within one accumulated call */
		private _calledSources = 0;
		/** Handle of currently running request */
		private _currentRequest = null;

		/** create DataAccess object and bind it to a data source (server address) */
		constructor(serverAddress: string = "") {
			super("DataAccess-" + serverAddress);
			console.log("Server address is " + serverAddress);
			if (serverAddress){
				this._serverAddress = "http://" + serverAddress;
			} else {
				this._serverAddress = "http://localhost:3000";
			}
			this._data = [];
			this._offsets = [];
			this._filters = [];
			this._startIndex = [];
			this._endIndex =[];
			this._ownFilterString = "";
			this._rootFiles = [];
			this._allowedFilterTags = ["TriggerNo", "ToA", "ToT", "fToA", "sToA", "+", "-", "*", "/", "%", "==", "!=", "<", ">", ">=", "<=", "&&", "||", "(", ")"];
		}

		/**
		Queries the server with new request for new set of data.
		*/
		getNewData(startTime: number, timeSlice: number, source: string, callback) {
			this._startTime = startTime;
			this._timeSlice = timeSlice;
			if (!callback){
				return mockup_pixels;
			}
			if (this._serverAddress === "http://test:3000"){
				this.setData("mockup", mockup_pixels);
				callback(mockup_pixels);
				return;
			}

			if (source === ""){
				this.callForAllData(startTime, timeSlice, callback);
				return;
			} else {
				this.callForOneData(startTime, timeSlice, source, callback);
			}
		}

		/** Asynchronous call for data from single (rootFile) source of data */
		callForOneData(startTime: number, timeSlice: number, source: string, callback){
			let daObj = this;
			//console.log("Ajax call for data from " + source + " started");
			let offset: number = 0;
			if (this._offsets[source] != undefined){
				offset = this._offsets[source];
			}

			offset = +offset;
			startTime = +startTime;
			startTime -= offset;

			this._currentRequest = $.ajax({
				url: this._serverAddress + "/data/interval/" + startTime + "/" + timeSlice + "/" + source
			}).done(function(data){
				//console.log("Ajax call for data from " + source + " done");
				daObj.setData(source, JSON.parse(data));
				daObj.checkDataForErrors(source);
				callback(daObj.getData(source));
			}).fail(function(err){
				//console.log("Ajax call for data from " + source + " error");
				daObj.setData(source, null);
				daObj.addError(source + ": There was an error contacting the server. No data received.");
				callback(null);
			});
		}

		/** Calls periodically for new data from all current source files */
		callForAllData(startTime: number, timeSlice: number, callback){
			let daObj = this;
			this._calledSources = 0;
			//console.log("Ajax call for data from all sources started");
			for (let oneFile of this._rootFiles){
				this.callForOneData(startTime, timeSlice, oneFile, function(){
					daObj._calledSources++;
					if (daObj._calledSources == daObj._rootFiles.length){
						callback();
					}
				});
			}
		}

		/** Queries the server with request for list of all root files currently stored on the server */
		getRootFilesList(folders, callback){
			console.log("Ajax call for file list started");

			let daObj = this;
			this._currentRequest = $.ajax({
				url: this._serverAddress + "/rootfiles/files/" + folders
			}).done(function(data){
				console.log("Ajax call for file list done");
				data = JSON.parse(data);
				callback(data);
			}).fail(function(err){
				console.log("Ajax call for file list error");
				daObj.addError("There was an error contacting the server. No data received.");
				callback(null);
			});
		}

		callForFolderList(folder = "", callback){
			this._currentRequest = $.ajax({
				url: this._serverAddress + "/rootfiles/folder-list/" + folder
			}).done(function(data){
				console.log("Ajax call for ascii folder list done");
				callback(JSON.parse(data));
			}).fail(function(err){
				console.log("Ajax call for ascii folder list error");
				callback(null);
			});
		}

		/** Specifies current root files on the server, from which to retrieve the data */
		setRootFile(filename){
			if (this._rootFiles.indexOf(filename) == -1){
				this._rootFiles.push(filename);
			}
		}

		/** Kills the currently running request on the server */
		abortCurrentRequest(){
			this._currentRequest.abort();
		}

		getCurrentRequest(){
			return this._currentRequest;
		}

		removeRootFile(filename){
			let index = this._rootFiles.indexOf(filename);
			if (index > -1){
				this._rootFiles.splice(index, 1);
			}
		}

		/** Saves the received data into the class attribute */
		setData(source: string, data){
			if (!data) {
				return;
			}
			this.setRootFile(source);
			this._data[source] = data;
			this._startIndex[source] = 0;
			this._endIndex[source] = data.Size;
		}

		hasFilter(){
			if (this._filters.length > 0 || this._ownFilterString != ""){
				return true;
			} else {
				return false;
			}
		}
		

		/** Checks the specified data for any possible errors or inaccuracies and writes down errors/warnings */
		checkDataForErrors(sourceName: string){

			let multipleTriggers: boolean = false;
			let negativeValues: number = 0;
			if (this._data[sourceName].TriggerCount > 1){
				multipleTriggers = true;
			}
			for (let i = 0; i < this._data[sourceName].Size; i++){
				if (this._data[sourceName].Pixels[i].ToA < 0){
					negativeValues++;
				}
			}
			if (multipleTriggers){
				let currTime: string = this.getCurrentTime();
				this.addWarning(currTime + " Selected interval contains data from multiple triggers. Possible inaccuracies in aquisition time.");
			}
			if (negativeValues > 0){
				let currTime: string = this.getCurrentTime();
				this.addWarning(currTime + " There are " + negativeValues + " pixels with negative ToA values. Possible error in sensor. Hover pixels to get more info.");
			}

		}


		/** Gets the correct data, checks for possible errors, and returns them. */
		getData(source: string = "", original: boolean = false){
			if (source == ""){
				source = this._rootFiles[0];
			}
			let data;
			if (original){
				data = this._data[source];
			} else{
				data = this.getFilteredData(source);
			}
			
			if (!data){
				return null;
			}
			
			if (data.Offset == undefined){
				data.Offset = 0;
			}
			if (this._offsets[source] != undefined){
				data.Offset = this._offsets[source];
			}

			return data;
		}

		getStartTime(){
			return this._startTime;
		}

		setStartTime(newStartTime: number){
			this._startTime = newStartTime;
		}

		getTimeSlice(){
			return this._timeSlice;
		}

		setTimeSlice(newTimeSlice: number){
			this._timeSlice = newTimeSlice;
		}

		getAllowedFilterTags(){
			return this._allowedFilterTags;
		}

		/** Resets all the filters that have been set up before */
		reset(){
			this._filters = [];
			this._filters.length = 0;
			this._ownFilterString = "";
		}

		/** Sets the interval from getting the data from the _data attribute*/
		setInterval(startIndex: number, endIndex: number, source: string){
			if (!this._data[source]){
				console.log("No data for given source.");
				return;
			}
			if (startIndex < 0){
				startIndex = 0;
			} 
			if (endIndex > this._data[source].Size){
				endIndex = this._data[source].Size;
			}
			this._startIndex[source] = startIndex;
			this._endIndex[source] = endIndex;
		}

		/**
		Makes sure the specified string is valid expression based on which the data can be filtered. Allows usage only of pre-defined allowed tags.
		Sets the filtering string to filter the data, if it is correct.
		*/
		ownFilter(filterString: string){
			let termsArray = filterString.split( /\s\s*/ );
			for (let i = 0; i < termsArray.length; i++){
				termsArray[i] = termsArray[i].replace(new RegExp('[()]', 'g'), "");
				if (this._allowedFilterTags.indexOf(termsArray[i]) == -1 && isNaN(+termsArray[i]) ){
					this.addError("Term \"" + termsArray[i] + "\" is not allowed for filtering.");
					return false;
				}
			}

			let pixel = this._data[this._rootFiles[0]].Pixels[100];
			let ToA = pixel.ToA;
			let TriggerNo = pixel.TriggerNo;
			let ToT = pixel.ToT;
			let sToA = pixel.sToA;
			let fToA = pixel.fToA;
			try{
				eval(filterString);
				this._ownFilterString = filterString;
			} catch(e){
				console.log(e.message);
				return false;
			}
			return true;
		}
		
		/**
		Adds a new filter to the collection of filters, based on which the data will be filtered, before they are returned.
		*/
		addFilter(what: string, valueFrom: number, valueTo: number){
			if (isNaN(+valueFrom) || isNaN(+valueTo) || (valueFrom.toString() === "" && valueTo.toString() === "")) {
				console.log("Values are not correctly set, filtering won't occur");
				return;
			}
			if (valueFrom.toString() == ""){
				valueFrom = -Infinity;
			}
			if (valueTo.toString() == ""){
				valueTo = Infinity;
			}
			
			this._filters[what] = new DataFilter(what, valueFrom, valueTo);
			this._filters.length++;

		}

		/**
		Getting and returning the current data, taking all the filtering that should take place into consideration.
		*/
		getFilteredData(source: string) {
			if (!this._data[source]){
				return null;
			}
			if (this._data[source].Pixels.length == 0){
				return this._data[source];
			}
			let newData: any = {};
			newData.Pixels = [];
			newData.Size = 0;
			newData.SumEnergy = 0;
			newData.TriggerCount = 0;
			newData.StartTime = this._data[source].StartTime;
			let firstTrigger: number = this._data[source].Pixels[0].TriggerNo;
			let lastTrigger: number;
			let add: boolean;

			for (let i = this._startIndex[source]; i < this._endIndex[source]; i++) {
				add = true;
				for (let j in this._filters){
					if (this._data[source].Pixels[i][this._filters[j]._valueName] < this._filters[j]._minimum || this._data[source].Pixels[i][this._filters[j]._valueName] >= this._filters[j]._maximum) {
						add = false;
					}
				}
				if (this._ownFilterString !== ""){
					let pixel = this._data[source].Pixels[i];
					let ToA = pixel.ToA;
					let TriggerNo = pixel.TriggerNo;
					let ToT = pixel.ToT;
					let sToA = pixel.sToA;
					let fToA = pixel.fToA;
					if (!eval(this._ownFilterString)){
						add = false;
					}
				}
				if (add){
					newData.Pixels.push(this._data[source].Pixels[i]);
					newData.Size++;
					newData.SumEnergy += this._data[source].Pixels[i].ToT;
					lastTrigger = this._data[source].Pixels[i].TriggerNo;
				}
			}
			newData.TriggerCount = lastTrigger - firstTrigger + 1;
		  	return newData;
		}

		setOffset(offset: number = 0, source: string = ""){
			if (!offset || isNaN(offset)){
				offset = 0;
				this.addWarning("Wrong offset value, setting 0.");
			}
			console.log("Setting offset of " + offset);
			//offset = Tools.secondsToNano(offset);
			this._offsets[source] = +offset;
		}
	}
}
