/// <reference path="../../typings/jqueryui/jqueryui.d.ts" />

namespace tpx3.baseobject{
	/**
	Class representing any object of the visualization application that can be named and has any functionality, which can produce errors. 
	*/
	export abstract class BaseObject{
		/** name of the object */
		_name: string;

		/** element for writing the errors */
		protected _errorSpan;
		/** element for writing the number of errors */
		protected _errorCountSpan;
		/** element for writin the number of warnings */
		protected _warningCountSpan;
		
		/** Sets up the HTML elements for displaying errors and warnings */
		constructor(name: string = ""){
			if (name === ""){
				console.log("Name not set for object: ");	
				console.log(this);		
			} else {
				this._name = name;
			}
			this._errorSpan = $("#errors-inner");
			this._errorCountSpan = $("#error-count");
			this._warningCountSpan = $("#warning-count");
		}

		/** Returns the actuall current day time in HH:MM:SS format, e.g. 9:13:09*/
		getCurrentTime(){
			let date = new Date();
			return date.getHours().toString() + ":" + (date.getMinutes() < 10 ? "0" : "") + date.getMinutes().toString() + ":" + (date.getSeconds() < 10 ? "0" : "") + date.getSeconds().toString();
		}

		/** Prints error into the pre-specified error span element */
		addError(message: string){
			let errorCount: number = this._errorCountSpan.html();
			errorCount++;
			this._errorCountSpan.html(errorCount.toString());
			this._errorSpan.prepend("<p><span class=\"label label-danger\">Error</span> " + message + "</p>");
		}

		/** Prints a warning into the pre-specified error span element */
		addWarning(message: string) {
			let warningCount: number = this._warningCountSpan.html();
			warningCount++;
			this._warningCountSpan.html(warningCount.toString());
			this._errorSpan.prepend("<p><span class=\"label label-warning\">Warning</span> " + message + "</p>");
		}
	}
}
