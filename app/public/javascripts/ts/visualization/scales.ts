/// <reference path="../../typings/jquery/jquery.d.ts" />

namespace tpx3.scales {

	/**
	Abstract class representing any scale. Children should implement the required functionality.
	*/
	export abstract class Scale {
		constructor() {

		}
		/** Get the specific value that would be placed in a given proportion in the specified min-max interval */
		abstract getProportionValue(value: number, min: number, max: number);
		/** Get the correct scaled value between 0 and 100, according to positionof the given value within given min-max interval */
		abstract getScaledValue(value : number, min : number, max : number);
	}

	/** 
	Standard linear scale with equal difference in return value for equal differences in input values.
	*/
	export class LinearScale extends Scale{
		constructor(){
			super();
		}

		getProportionValue(value: number, min: number, max: number){
			return (min + max) * value;
		}

		getScaledValue(value : number, min : number, max : number){
			if (max - min == 0){
				return "0";
			}
			return ((value - min) / (max - min) * 100).toFixed(0);
		}
	}
	/**
	Logarithmic scale, with changes in return value according to changes in orders of magnitude of input values.
	*/
	export class LogarithmicScale extends Scale {
		constructor(){
			super();
		}

		getProportionValue(value: number, min: number, max: number){
			if (min == 0){
				min = 1;
			}
			let logMid = (Math.log(max) + Math.log(min)) * value;
			return Math.pow(Math.E, logMid);
		}

		getScaledValue(value : number, min : number, max : number){
			if (max - min == 0){
				return "0";
			}
			let logMax = Math.log(max);
			let logMin = Math.log(min);
			if (min == 0){
				logMin = -1;
				if (value == 0){
					return value.toFixed(0);
				}
			}
			let logVal = Math.log(value);
			return ((logVal - logMin) / (logMax - logMin) * 100).toFixed(0);
		}
	}
}
