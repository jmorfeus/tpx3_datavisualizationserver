/// <reference path="sensor.ts" />
/// <reference path="timeline.ts" />
/// <reference path="statistics.ts" />
/// <reference path="redrawer.ts" />
/// <reference path="unit-displayer.ts" />

import GlobalControls = tpx3.globalcontrols.GlobalControls;
import Sensor = tpx3.sensor.Sensor;
import Timeline = tpx3.timeline.Timeline;
import WhiteDecorator = tpx3.colormap.WhiteBackgroundDecorator;
import Statistics = tpx3.statistics.Statistics;
import CubehelixMap = tpx3.colormap.CubehelixMap;
import Redrawer = tpx3.redrawer.Redrawer;
import TimelineRedrawer = tpx3.redrawer.TimelineRedrawer;
import UnitDisplayer = tpx3.unitdisplayer.UnitDisplayer;


// ====================== DECLARATION, INITIALIZATION =======================
var globalServerAddress;
let white = false;
let animating: boolean = false;
let dataAccess = new DataAccess(globalServerAddress);
let pixelsData;
let redrawer = new Redrawer("HistogramsRedrawer");
let timelineRedrawer = new TimelineRedrawer("TimelineRedrawer");
let scale = new LinearScale();
let logScale = new LogarithmicScale();
let colormap: IColorMap;
let greymap: IColorMap = new GreyscaleMap();
let jetmap: IColorMap = new JetMap();
let hotmap: IColorMap = new HotMap();
let cubehelixmap: IColorMap = new CubehelixMap();
let globalControls = new GlobalControls("GlobalControls");
let currFolder = "";
// ===================== END DECLARATION, INITIALIZATION ======================

// set the default initial colormap to all the histograms
colormap = hotmap;

globalControls.setStartTime(0);
globalControls.setTimeSlice(20000000);
globalControls.setColorMap(colormap);


let timeStartUnits = new UnitDisplayer();
timeStartUnits.setCurrentUnits(UnitDisplayer.BASE);
let timeSliceUnits = new UnitDisplayer();
timeSliceUnits.setCurrentUnits(UnitDisplayer.BASE);
let binSizeUnits = new UnitDisplayer();
binSizeUnits.setCurrentUnits(UnitDisplayer.BASE);
let offsetUnits = new UnitDisplayer();
offsetUnits.setCurrentUnits(UnitDisplayer.BASE);

let sensor1: Sensor = new Sensor("sensor_neutrons01", "histograms-row", "neutrons01.root", dataAccess, globalControls, Sensor.STATE_COLLAPSED);

/* 
Register all observers to a Redrawer object, so they will react on a change in the currently-to-be-displayed data
*/
redrawer.subscribeObserver(sensor1, "sensor_neutrons01");

timelineRedrawer.subscribeObserver(sensor1.getTimeline(), "sensor_neutrons01");

globalControls.addSensor(sensor1, "sensor_neutrons01");

globalControls.setBinSize(0);


let folderName = "";


// get the initial data and draw them
dataAccess.getNewData(globalControls.getStartTime(), globalControls.getTimeSlice(), sensor1.getSource(), function(data){
		redrawer.redrawAll(dataAccess);
	});


// draw previews of the colormaps
greymap.preview("greyscale-canvas");
hotmap.preview("hot-canvas");
jetmap.preview("jet-canvas");
cubehelixmap.preview("cubehelix-canvas");


// display the default values in correct fields
$('#time-start-input').val(timeStartUnits.convert(globalControls.getStartTime(), UnitDisplayer.NANO, timeStartUnits.getCurrentUnits()));
$('#time-slice-input').val(timeSliceUnits.convert(globalControls.getTimeSlice(), UnitDisplayer.NANO, timeSliceUnits.getCurrentUnits()));
$('#bin-size-input').val(binSizeUnits.convert(globalControls.getBinSize(), UnitDisplayer.NANO, binSizeUnits.getCurrentUnits()));
$('.allowed-tags').append(dataAccess.getAllowedFilterTags().join(" "));




function updateRootListByDate(newDate=null){
	let folders = newDate.format("YYYY") + "/" + newDate.format("MM") + "/" + newDate.format("DD") + "/" + newDate.format("HH");
	// get the data about available root files on the server
	dataAccess.getRootFilesList(folders, function(data){
		$("#rootfiles").html("");
		// do something with the data
		if (data && data.Files){
			for (let i = 0; i < data.Files.length; i++){
				let name = "sensor_" + data.Files[i].split(".root")[0];
				name = name.replace(/\./g, '_'); 
				$("#rootfiles").prepend("<li class=\"root-select-li\"><a class=\"root-select-a\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"" + data.Files[i] + "\" filename=\"" + data.Files[i] + "\" href=\"#\"><input class=\"root-select-check\"  filename=\"" + data.Files[i] + "\" type=\"checkbox\" id=\"" + name + "-checkbox\"></input> " + data.Files[i] + "</a></li>");
				if (redrawer.has(name)){
					$('#' + name + '-checkbox').prop("checked", "checked");				
				}
			}
		}
	});
}

function updateRootListByFolder(folders=null){
	// get the data about available root files on the server
	dataAccess.getRootFilesList(folders, function(data){
		$("#rootfiles").html("");
		// do something with the data
		if (data && data.Files){
			for (let i = 0; i < data.Files.length; i++){
				let name = "sensor_" + data.Files[i].split(".root")[0];
				name = name.replace(/\./g, '_'); 
				$("#rootfiles").prepend("<li class=\"root-select-li\"><a class=\"root-select-a\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"" + data.Files[i] + "\" filename=\"" + data.Files[i] + "\" href=\"#\"><input class=\"root-select-check\"  filename=\"" + data.Files[i] + "\" type=\"checkbox\" id=\"" + name + "-checkbox\"></input> " + data.Files[i] + "</a></li>");
				if (redrawer.has(name)){
					$('#' + name + '-checkbox').prop("checked", "checked");				
				}
			}
		}
	});
}

// ===================== START REGISTER ALL EVENT CALLBACKS =========================================
//                        (let's make things interactive)


$(document).on('click', '.root-select-a', function(){
	let filename:string = $(this).attr("filename");
	$('.ajax-loading').fadeIn(50);

	let name = ("sensor_" + filename.split(".root")[0]);
	name = name.replace(/\./g, "_");


	if (redrawer.has(name)){
		<Sensor>(redrawer.get(name)).softDelete();
		redrawer.unsubscribeObserver(name);
		timelineRedrawer.unsubscribeObserver(name);
		globalControls.removeSensor(name);
		dataAccess.removeRootFile(filename);
		$('.ajax-loading').fadeOut(50);
		$('#' + name + "-checkbox").removeProp("checked");
		return;
	}
	let newSensor = new Sensor(name, "histograms-row", filename, dataAccess, globalControls, Sensor.STATE_COLLAPSED);
	redrawer.subscribeObserver(newSensor, name);
	timelineRedrawer.subscribeObserver(newSensor.getTimeline(), name);
	globalControls.addSensor(newSensor, name);
	$('#' + name + "-checkbox").prop("checked", "checked");
	dataAccess.getNewData(globalControls.getStartTime(), globalControls.getTimeSlice(), filename, function(data) {
		redrawer.redrawAll(dataAccess);
		$('.ajax-loading').fadeOut(50);
	});
});


$('.theme-select').on('click', function(){
	$('.dropdown-menu input:radio').removeProp("checked");
	let name = $(this).attr("filename");
	$('#css-style').attr("href", "stylesheets/css/" + name + ".css");
	$('#' + name + "-check").prop("checked", "checked");
});

$('#remove-all-a').on('click', function(){

});

$(document).on('click', '.sensor-expand', function(){
	let name = $(this).attr("sensorName");
	let filename = (<Sensor>(redrawer.get(name))).getSource();
	<Sensor>(redrawer.get(name)).softDelete();
	redrawer.unsubscribeObserver(name);
	timelineRedrawer.unsubscribeObserver(name);
	globalControls.removeSensor(name);
	let newSensor = new Sensor(name, "histograms-row", filename, dataAccess, globalControls, Sensor.STATE_EXPANDED);
	redrawer.subscribeObserver(newSensor, name);
	timelineRedrawer.subscribeObserver(newSensor.getTimeline(), name);
	globalControls.addSensor(newSensor, name);
	newSensor.redraw(dataAccess);
	return false;
});

$(document).on('click', '.sensor-fullscreen', function(){
	let name = $(this).attr("sensorName");
	let filename = (<Sensor>(redrawer.get(name))).getSource();
	<Sensor>(redrawer.get(name)).softDelete();
	redrawer.unsubscribeObserver(name);
	timelineRedrawer.unsubscribeObserver(name);
	globalControls.removeSensor(name);
	let newSensor = new Sensor(name, "histograms-row", filename, dataAccess, globalControls, Sensor.STATE_FULLSCREEN);
	redrawer.subscribeObserver(newSensor, name);
	timelineRedrawer.subscribeObserver(newSensor.getTimeline(), name);
	globalControls.addSensor(newSensor, name);
	newSensor.redraw(dataAccess);
	return false;
});

$(document).on('click', '.sensor-collapse', function(){
	let name = $(this).attr("sensorName");
	let filename = (<Sensor>(redrawer.get(name))).getSource();
	<Sensor>(redrawer.get(name)).softDelete();
	redrawer.unsubscribeObserver(name);
	timelineRedrawer.unsubscribeObserver(name);
	globalControls.removeSensor(name);
	let newSensor = new Sensor(name, "histograms-row", filename, dataAccess, globalControls, Sensor.STATE_COLLAPSED);
	redrawer.subscribeObserver(newSensor, name);
	globalControls.addSensor(newSensor, name);
	newSensor.redraw(dataAccess);
	return false;
});

$(document).keydown(function(e){
	switch (e.which) {
		case 37:
			$('#timeline-prev-button').click();
			break;
		case 38:
			$('#intensity-up').click();
			break;
		case 39:
			$('#timeline-next-button').click();
			break;
		case 40:
			$('#intensity-down').click();
			break;
		default:
			return;
	}
	e.preventDefault();
})

$(document).on("swipeleft", function(){
	$('#timeline-next-button').click();
})


$('#masking-row').on('click', function() {
	globalControls.toggleMaskingOn();
	redrawer.redrawAll();
	$('#masking-radio').prop("checked", "checked");
	$('#zooming-radio').removeProp("checked")
	$('#masking-row').addClass("tool-selected");
	$('#zooming-row').removeClass("tool-selected");
	return false;
});

$('#masking-checkbox').on('click', function(){
	globalControls.toggleShowMaskedPixels();
	redrawer.redrawAll();
});

$('#cursor-checkbox').on('click', function(){
	globalControls.toggleShowCursor();
});

$('#zooming-row').on('click', function() {
	globalControls.toggleMaskingOff();
	redrawer.redrawAll();
	$('#zooming-radio').prop("checked", "checked")
	$('#masking-radio').removeProp("checked");
	$('#zooming-row').addClass("tool-selected");
	$('#masking-row').removeClass("tool-selected");
	return false;
});

$('#values-relative').on('click', function(){
	globalControls.toggleShowRelativeValuesOn();
	$('#values-absolute').removeClass("active-link");
	$(this).addClass("active-link");
	redrawer.redrawAll(dataAccess);
	return false;
});

$('#values-absolute').on('click', function(){
	globalControls.toggleShowRelativeValuesOff();
	$('#values-relative').removeClass("active-link");
	$(this).addClass("active-link");
	redrawer.redrawAll(dataAccess);
	return false;
});

$('#scale-linear').on('click', function() {
	globalControls.changeScale(scale);
	$(this).addClass("active-link");
	$('#scale-logarithmic').removeClass("active-link");
	redrawer.redrawAll();
	return false;
});

$('#scale-logarithmic').on('click', function() {
	globalControls.changeScale(logScale);
	$(this).addClass("active-link");
	$('#scale-linear').removeClass("active-link");
	redrawer.redrawAll();
	return false;
});

$('#intensity-up').on('click', function() {
	colormap.boost();
	redrawer.redrawAll();
	return false;
});

$('#intensity-down').on('click', function() {
	colormap.toneDown();
	redrawer.redrawAll();
	return false;
});

$('#intensity-reset').on('click', function() {
	colormap.boost(0);
	redrawer.redrawAll();
	return false;
});

$('#jet-canvas').on('click', function() {
	white = false;
	$("#colormap-white-bckg").removeClass("active-link");
	colormap = jetmap;
	globalControls.changeColorMap(colormap);
	redrawer.redrawAll();
	return false;
});

$('#cubehelix-canvas').on('click', function() {
	white = false;
	$("#colormap-white-bckg").removeClass("active-link");
	colormap = cubehelixmap;
	globalControls.changeColorMap(colormap);
	redrawer.redrawAll();
	return false;
});

$('#hot-canvas').on('click', function() {
	white = false;
	$("#colormap-white-bckg").removeClass("active-link");
	colormap = hotmap;
	globalControls.changeColorMap(colormap);
	redrawer.redrawAll();
	return false;
});

$('#greyscale-canvas').on('click', function() {
	white = false;
	$("#colormap-white-bckg").removeClass("active-link");
	colormap = greymap;
	globalControls.changeColorMap(colormap);
	redrawer.redrawAll();
	return false;
});

$('#colormap-white-bckg').on('click', function() {
	if (!white){
		$(this).addClass("active-link");
		white = true;
		let whiteColormap = new WhiteDecorator(colormap);
		globalControls.changeColorMap(whiteColormap);
		redrawer.redrawAll();
	} else {
		$(this).removeClass("active-link");
		white = false;
		globalControls.changeColorMap(colormap);
		redrawer.redrawAll();
	}
	return false;
});

$('#time-start-input').on('change', function(){
	let numValue: number = +this.value;
	let setUnits = false;
	if (isNaN(numValue)){
		numValue = timeStartUnits.parseValueWithUnits(this.value);
		setUnits = true;
	}

	globalControls.setTimeSlice(globalControls.getTimeSlice());
	globalControls.setStartTime(timeStartUnits.convert(numValue, timeStartUnits.getCurrentUnits(), UnitDisplayer.NANO));
	if (!setUnits){
		this.value = timeStartUnits.getBestUnits(globalControls.getStartTime(), UnitDisplayer.NANO);
	} else {
		this.value = timeStartUnits.convert(globalControls.getStartTime(), UnitDisplayer.NANO, timeStartUnits.getCurrentUnits());
	}
	$('#time-start-units').html('[' + timeStartUnits.getUnitShortcut() + 's]');
});

$('#time-slice-input').on('change', function() {
	let numValue: number = +this.value;
	let setUnits = false;
	if (isNaN(numValue)){
		numValue = timeSliceUnits.parseValueWithUnits(this.value);
		setUnits = true;
	}
	
	globalControls.setStartTime(globalControls.getStartTime());
	globalControls.setTimeSlice(timeSliceUnits.convert(numValue, timeSliceUnits.getCurrentUnits(), UnitDisplayer.NANO));
	if (!setUnits){
		this.value = timeSliceUnits.getBestUnits(globalControls.getTimeSlice(), UnitDisplayer.NANO);
	} else {
		this.value = timeSliceUnits.convert(globalControls.getTimeSlice(), UnitDisplayer.NANO, timeSliceUnits.getCurrentUnits());
	}
	$('#time-slice-units').html('[' + timeSliceUnits.getUnitShortcut() + 's]');
});


$('#bin-size-input').on('change', function(){
	let numValue: number = +this.value;
	let setUnits = false;
	if (isNaN(numValue)){
		numValue = binSizeUnits.parseValueWithUnits(this.value);
		setUnits = true;
	}
	globalControls.setBinSize(binSizeUnits.convert(numValue, binSizeUnits.getCurrentUnits(), UnitDisplayer.NANO));
	if (!setUnits){
		this.value = binSizeUnits.getBestUnits(globalControls.getBinSize(), UnitDisplayer.NANO);
	} else {
		this.value = binSizeUnits.convert(globalControls.getBinSize(), UnitDisplayer.NANO, binSizeUnits.getCurrentUnits());
	}
	$('#bin-size-units').html('[' + binSizeUnits.getUnitShortcut() + 's]');
	timelineRedrawer.redrawAll(dataAccess);
});


$(document).on('change', '.offset-input', function(){
	let numValue = +this.value;
	let setUnits = false;
	if (isNaN(numValue)){
		numValue = offsetUnits.parseValueWithUnits(this.value);
		setUnits = true;
	}
	let nanoValue = offsetUnits.convert(numValue, offsetUnits.getCurrentUnits(), UnitDisplayer.NANO);
	dataAccess.setOffset(nanoValue, $(this).attr("filename"));
	if (!setUnits){
		this.value = offsetUnits.getBestUnits(nanoValue, UnitDisplayer.NANO);
	} else {
		this.value = offsetUnits.convert(nanoValue, UnitDisplayer.NANO, offsetUnits.getCurrentUnits());
	}
	$('#offset-units').html('[' + offsetUnits.getUnitShortcut() + 's]');
});

$('#timeline-snap-toggle').on('click', function(){
	globalControls.toggleSnapping();
});


$('#timeline-prev-button').on('click', function(){
	$('.ajax-loading').fadeIn(50);
	$('#time-start-input').val(timeStartUnits.convert(+globalControls.getStartTime(), UnitDisplayer.NANO, timeStartUnits.getCurrentUnits()) - timeSliceUnits.convert(+$('#time-slice-input').val(), timeSliceUnits.getCurrentUnits(), timeStartUnits.getCurrentUnits())).change();
	dataAccess.getNewData(globalControls.getStartTime(), globalControls.getTimeSlice(), "", function(data){
		redrawer.redrawAll(dataAccess);
		$('.ajax-loading').fadeOut(50);
	});
	return false;
});

$('#timeline-next-button').on('click', function() {
	$('.ajax-loading').fadeIn(50);
	$('#time-start-input').val(timeStartUnits.convert(+globalControls.getStartTime(), UnitDisplayer.NANO, timeStartUnits.getCurrentUnits()) + timeSliceUnits.convert(+$('#time-slice-input').val(), timeSliceUnits.getCurrentUnits(), timeStartUnits.getCurrentUnits())).change();
	dataAccess.getNewData(globalControls.getStartTime(), globalControls.getTimeSlice(), "", function(data){
		redrawer.redrawAll(dataAccess);
		$('.ajax-loading').fadeOut(50);
	});
	return false;
});

$('#timeline-update-button').on('click', function() {
	$('.ajax-loading').fadeIn(50);
	let longRequest = true;

	setTimeout(function(){
		if (longRequest){
			$('#abort-request-span').fadeIn(50);
		}
	}, 2500);

	dataAccess.getNewData(globalControls.getStartTime(), globalControls.getTimeSlice(), "", function(data){
		if (dataAccess.getCurrentRequest().statusText == "abort") {
			$('#abort-request-span').fadeOut(50);
			$('.ajax-loading').fadeOut(50);
			return false;
		}
		redrawer.redrawAll(dataAccess);
		longRequest = false;
		$("#bin-size-input").val(0);
		$('#bin-size-input').change();
		$('#abort-request-span').fadeOut(50);
		$('.ajax-loading').fadeOut(50);
	});
	return false;
});

$('#abort-request').on('click', function(){
	globalControls.undoTimeSettings();
	$('#time-start-input').val(timeStartUnits.convert(globalControls.getStartTime(), UnitDisplayer.NANO, timeStartUnits.getCurrentUnits()));
	$('#time-slice-input').val(timeSliceUnits.convert(globalControls.getTimeSlice(), UnitDisplayer.NANO, timeSliceUnits.getCurrentUnits()));
	dataAccess.abortCurrentRequest();
	$('.ajax-loading').fadeOut(50);
	$('#abort-request-span').fadeOut(50);
})

$('#timeline-zoom-button').on('click', function() {
	globalControls.setStartTime(+$('#timeline-startTime').attr("trueValue") + globalControls.getStartTime());
	globalControls.setTimeSlice(+$('#timeline-deltaTime').attr("trueValue"));
	$('#time-start-input').val(timeStartUnits.convert(globalControls.getStartTime(), UnitDisplayer.NANO, timeStartUnits.getCurrentUnits()));
	$('#time-slice-input').val(timeSliceUnits.convert(globalControls.getTimeSlice(), UnitDisplayer.NANO, timeSliceUnits.getCurrentUnits()));
	dataAccess.getNewData(globalControls.getStartTime(), globalControls.getTimeSlice(), "", function(data){
		redrawer.redrawAll(dataAccess);
		$("#bin-size-input").val(0);
		$('#bin-size-input').change();
		$('.ajax-loading').fadeOut(50);
	});
	return false;
});

$('#timeline-undo-button').on('click', function() {
	if (!globalControls.undoTimeSettings()){
		return;
	}
	$('#time-start-input').val(timeStartUnits.convert(globalControls.getStartTime(), UnitDisplayer.NANO, timeStartUnits.getCurrentUnits()));
	$('#time-slice-input').val(timeSliceUnits.convert(globalControls.getTimeSlice(), UnitDisplayer.NANO, timeSliceUnits.getCurrentUnits()));
	dataAccess.getNewData(globalControls.getStartTime(), globalControls.getTimeSlice(), "", function(data){
		redrawer.redrawAll(dataAccess);
		$('#bin-size-input').change();
		$('.ajax-loading').fadeOut(50);
	});
	return false;
});

function animate() {
	if (!animating){
		return;
	}
	let shown = false;
	let animationDelay = Tools.secondsToMilli($('#animation-input').val());

	setTimeout(function(){
		if (!shown){
			$('.ajax-loading').fadeIn(50);
		}
	}, 300);

	$('#time-start-input').val(timeStartUnits.convert(+globalControls.getStartTime(), UnitDisplayer.NANO, timeStartUnits.getCurrentUnits()) + timeSliceUnits.convert(+$('#time-slice-input').val(), timeSliceUnits.getCurrentUnits(), timeStartUnits.getCurrentUnits())).change();
	dataAccess.getNewData(globalControls.getStartTime(), globalControls.getTimeSlice(), "", function(data) {
		redrawer.redrawAll(dataAccess);
		shown = true;
		$('.ajax-loading').fadeOut(50);
		setTimeout(animate, animationDelay);
	});
}

$('#timeline-animate-button').on('click', function(){
	animating = !animating;
	if (animating){
		$('#animating-glyphicon').removeClass('glyphicon-play');
		$('#animating-glyphicon').addClass('glyphicon-pause');
	} else {
		$('#animating-glyphicon').removeClass('glyphicon-pause');
		$('#animating-glyphicon').addClass('glyphicon-play');
	}
	animate();
});

$('#triggers-filter-button').on('click', function(){
	dataAccess.addFilter("TriggerNo", $('#triggers-from').val(), $('#triggers-to').val());
	redrawer.redrawAll(dataAccess);
});

$('#toa-filter-button').on('click', function() {
	dataAccess.addFilter("ToA", $('#toa-from').val(), $('#toa-to').val());
	redrawer.redrawAll(dataAccess);
});

$('#tot-filter-button').on('click', function() {
	dataAccess.addFilter("ToT", $('#tot-from').val(), $('#tot-to').val());
	redrawer.redrawAll(dataAccess);
});

$('#ftoa-filter-button').on('click', function() {
	dataAccess.addFilter("fToA", $('#ftoa-from').val(), $('#ftoa-to').val());
	redrawer.redrawAll(dataAccess);
});

$('#stoa-filter-button').on('click', function() {
	dataAccess.addFilter("sToA", $('#stoa-from').val(), $('#stoa-to').val());
	redrawer.redrawAll(dataAccess);
});

$('#own-filter-button').on('click', function() {
	if(!dataAccess.ownFilter($("#own-filter").val())){
		return;
	}
	redrawer.redrawAll(dataAccess);
});

$('#reset-filters-button').on('click', function() {
	dataAccess.reset();
	$('.filter-input').val("");
	redrawer.redrawAll(dataAccess);
});

$('#clear-log-button').on('click', function() {
	$('#errors-inner').html("");
	$('#warning-count').html("0");
	$('#error-count').html("0");
	return false;
});

$('#datetimepicker12').on('dp.change', function(e){
	updateRootListByDate(e.date);
});

$(document).on('click', '.folder-change', function(e){
	folderName = $(this).attr('folderName') + "/";
	console.log("Folder " + folderName + " clicked.");
	currFolder += folderName;
	updateRootListByFolder(currFolder);
	dataAccess.callForFolderList(currFolder, processFolderList);
	return false;
});
// ==================== END EVENT CALLBACKS ===============================


$('#bin-size-input').change();
$('#time-slice-input').change();
$('#time-start-input').change();

// ==================== AUTOCOMPLETE of the "OWN FILTTER" field ============
function split( val ) {
		return val.split( /\s\s*/ );
    }
function extractLast( term ) {
  	return split( term ).pop();
}

$("#own-filter")// don't navigate away from the field on tab when selecting an item
      .bind( "keydown", function( event ) {
        if ( event.keyCode === $.ui.keyCode.TAB &&
            $( this ).autocomplete( "instance" ).menu.active ) {
          		event.preventDefault();
        		}
      })
      .autocomplete({
        minLength: 0,
        source: function( request, response ) {
          // delegate back to autocomplete, but extract the last term
          response( $.ui.autocomplete.filter(
            dataAccess.getAllowedFilterTags(), extractLast( request.term ) ) );
        },
        focus: function() {
          // prevent value inserted on focus
          return false;
        },
        select: function( event, ui ) {
          var terms = split( this.value );
          // remove the current input
          terms.pop();
          // add the selected item
          terms.push( ui.item.value );
          // add placeholder to get the comma-and-space at the end
          terms.push( "" );
          this.value = terms.join( " " );
          return false;
        }
      });
// ====================== END AUTOCOMPLETE ===============================




$(document).tooltip();


$('#datetimepicker12').datetimepicker({
	inline: true,
	sideBySide: true,
	format: "MM/YYYY dd HH",
	defaultDate: Date.now()
});


function processFolderList(data){
	if (data == null) return;
	$('#folder-list').html("");
	let folderList = data.Folders;
	for (let i = 0; i < folderList.length; i++){
		if(data.Folders[i] == ".."){
			$('#folder-list').append(' <a class="folder-change" folderName="' + data.Folders[i] + '" href="#"><span class="glyphicon glyphicon-level-up"></span></a> ');
		} else if (data.Folders[i] == "."){
			$('#folder-list').append(' <a class="folder-change" id="folder-update" folderName="' + data.Folders[i] + '" href="#"><span class="glyphicon glyphicon-refresh"></span></a> ');
		} else { 	
			$('#folder-list').append(' <a class="folder-change" folderName="' + data.Folders[i] + '" href="#">' + data.Folders[i] + '</a> ');
		}
	}
}

function updateFolders(){
	$('#folder-update').click();
	setTimeout(updateFolders, 5000);
}

$(document).on('ready', function(){
	updateRootListByFolder(currFolder);
	dataAccess.callForFolderList(currFolder, processFolderList);
	updateFolders();
});



