/// <reference path="baseobject.ts" />
/// <reference path="color-map.ts" />

import BaseObject = tpx3.baseobject.BaseObject;
import ColorMap = tpx3.colormap.ColorMap;


namespace tpx3.redrawer {
	/**
	Interface of all the object that can be redrawn. These are "observers" and can be registered to a Redrawer class and be later notified upon some event happening.
	*/
	export interface IRedrawable{
		/** Redraw the visualization */
		redraw();
		/** Redraw the visualization with specified new data */
		redraw(newData: DataAccess);
		drawNoData();
		updateSize();
	}

	/**
	Class responsible for keeping a list of redrawable observers and notifying all of them to redraw all the data at once.
	*/
	export class Redrawer extends BaseObject {
		/** List of subscribed observers to notify to redraw later */
		protected _observerList: IRedrawable[];
		
		/** Initializes empty list of observers/subscribers */
		constructor(name = "") {
			super(name);
			this._observerList = [];
		}

		/** Subscribe/add new observer to the list of observers/subscribers */
		subscribeObserver(newObserver: IRedrawable, observerName: string = ""){
			if (observerName != ""){
				this._observerList[observerName] = newObserver;
			} else {
				this._observerList.push(newObserver);
			}
		}

		/** Redraw all the subscribed observers/subscribers, calling their redraw() method */
		redrawAll(newData: DataAccess = null){
			for (let i in this._observerList){
				if (this._observerList[i] != null){
					this._observerList[i].redraw(newData);
				}
			}
		}

		/** Checks if an observer with a given name is subscribed to this redrawer */
		has(name: string){
			if (this._observerList[name] !== undefined && this._observerList[name] !== null){
				return true;
			} else {
				return false;
			}
		}

		/** Returns an observer with given name, if such observer exists, false otherwise */
		get(name: string){
			if (this.has(name)){
				return this._observerList[name];
			} else {
				return false;
			}
		}

		/** Removes observer with specified name from the observers list */
		unsubscribeObserver(observerName: string){
			if (this.has(observerName)){
				delete this._observerList[observerName];
			}
		}
	}

	/** Class responsible for keeping a list and for redrawing Timeline objects */
	export class TimelineRedrawer extends Redrawer{
		constructor(name){
			super(name);
		}

		/** Redraws filtered data of all subscribed timelines */
		redrawAllFiltered(newDataSource: DataAccess = null){
			for(let i in this._observerList){
				(<Timeline>this._observerList[i]).redrawFiltered(newDataSource);
			}
		}

	}
}