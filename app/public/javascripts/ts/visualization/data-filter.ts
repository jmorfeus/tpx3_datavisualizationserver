namespace tpx3.datafilter {

	/** 
	Class representing a single filter on the data. Specifies the attribute to be filtered, its desired minimum value and maximum value.
	*/
	export class DataFilter {
		/** Quantity, of which to filter */
		_valueName: string;
		/** Minimum required value of the quantity */
		_minimum: number;
		/** Maximum required value of the quantity */
		_maximum: number;

		/** Creates a new filter for the data. Specifies the desired quantity on which to filter, it's minimum and maximum values. */
		constructor(valueName: string, minimum: number, maximum: number) {
			this._valueName = valueName;
			this._minimum = minimum;
			this._maximum = maximum;
		}

		/** Checks if the inputted value passes this filter's criteria */
		passed(value: number){
			return (value >= this._minimum && value < this._maximum) ? true : false;
		}
		
	}

}
