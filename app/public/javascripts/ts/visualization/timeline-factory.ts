/// <reference path="timeline.ts" />

import Tline = tpx3.timeline.Timeline;

namespace tpx3.timelinefactory{
	/** Factory class used for creating new instances of Timeline class and placing the timelines correctly on the HTML page */
	export class TimelineFactory extends BaseObject{

		constructor(name = "") {
			super(name);
		}

		/** Creates new Timeline object */
		protected createTimeline(parent: Sensor, name: string, dataAccess: DataAccess, globalControls: GlobalControls, startTime: number, timeSlice: number, binSize: number = 0){
			return new Tline(parent, name, dataAccess, globalControls, startTime, timeSlice, binSize);
		}

		/** Puts the necessary HTML content onto the page */
		protected putTimelineHtmlContent(container: string, name: string){
			let jContainer = $("." + container);
			jContainer.prepend("\
				<div class=\"row timeline-jumbo\"  id=\"" + name + "-wrapper\">\
	                <canvas id=\"" + name + "-scale-vertical\" class=\"timeline-canvas-scale-vertical\">\
	                </canvas>\
					<div class=\"timeline-canvas-wrapper\">\
		                <canvas id=\"" + name + "\" class=\"timeline-canvas\">\
		                </canvas>\
		                <div class=\"timeline-slider\" id=\"" + name + "-slider-left\">\
		                </div>\
		                <div class=\"timeline-slider\" id=\"" + name + "-slider-right\">\
		                </div>\
		                <canvas id=\"" + name + "-scale\" class=\"timeline-canvas-scale\">\
		                </canvas>\
		        	</div>\
		        </div>\
	        	");
		}

		/** Complete function for adding a new Timeline into the visualization. Creates the HTML content and returns a reference to object representing this timeline, which is used for all the manipulation */
		addTimeline(container: string, parent: Sensor, name: string, dataAccess: DataAccess, globalControls: GlobalControls, startTime: number, timeSlice: number, binSize: number = 0){
			this.putTimelineHtmlContent(container, name);
			return this.createTimeline(parent, name, dataAccess, globalControls, startTime, timeSlice, binSize);
		}

	}

}
