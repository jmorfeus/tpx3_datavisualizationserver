/// <reference path="color-map.ts" />
/// <reference path="timeline.ts" />

import IColorMap = tpx3.colormap.IColorMap;
import GreyscaleMap = tpx3.colormap.GreyscaleMap;
import JetMap = tpx3.colormap.JetMap;
import HotMap = tpx3.colormap.HotMap;

namespace tpx3.histogram {

	/**
	Abstract class representing the interactive 2D square histogram visualization of the sensor. 
	*/
	export abstract class HistogramCanvas extends CanvasVisualization implements IRedrawable{

		/** Parent element, specifying which sensor this single histogram belongs to */
		protected _parent: Sensor;
		/** ID of an element where to display the X coordinate label for the pixel */
		protected _labelXid: string;
		/** ID of an element where to display the Y coordinate label for the pixel */
		protected _labelYid: string;
		/** ID of an element where to display the value label for the pixel */
		protected _labelValId: string[];
		/** ID of an element where to draw the preview of currently used scale and colormap */
		protected _previewScaleId: string;
		/** Number of pixels to be displayed on the histogram */
		protected _pixelCount: number;
		/** Maximum possible number of pixels to be displayed on the histogram (the real size of the pixel's matrix) when absolutely un-zoomed */
		protected _maxPixelCount: number;
		/** Start time of the data - starting time of the hits received */
		protected _startTime: number;
		/** Zooming ratio */
		protected _zoom: number;
		/** X coordinate of the starting pixel to be displayed on the histogram */
		protected _startX: number;
		/** Y coordinate of the starting pixel to be displayed on the histogram */
		protected _startY: number;
		/** Maximum value of all the pixels displayed */
		protected _maxValue: number;
		/** Minimum value of all the pixels displayed */
		protected _minValue: number;
		/** Currently selected color map */
		protected _colorMap: IColorMap;
		/** Indicator, if some pixel was selected */
		protected _selectedPixel: boolean;
		/** Indicator, if user is currently selecting masked pixels */
		protected _masking: boolean;
		/** Indicator, if user wants to see the masked pixels */
		protected _showMasked: boolean;
		/** Indicator, if user is currently zooming */
		protected _zooming: boolean;
		/** Starting X coordinate for the zooming */
		protected _zoomStartX: number;
		/** Starting Y coordinate for the zooming */
		protected _zoomStartY: number;
		/** Size of the zooming */
		protected _zoomWidth: number;
		/** Size of the zooming */
		protected _zoomHeight: number;
		/** Matrix of masked pixels that will be ignored in the visualization (always shown as 0 value) */
		protected _maskedMatrix;
		protected _maskedPixelsColor: string;
		protected _maskingColor: string;
		protected _offset;
		protected _startMouseX: number;
		protected _startMouseY: number;
		protected _unitDisplayer: UnitDisplayer;
		protected _precision: number;

		/** Set name for this visualization, and get the visualization data from the given pixels data */
		constructor(parent: Sensor, name: string, data: any = null) {
			super(name);
			if (parent){
				this._parent = parent;
			}
			this.initParameters();
			if (data){
				this._data = this.getCanvasData(data);
			} else {
				this.drawNoData();
			}
			this.setupLabelIds();
			this.setupHandlers();
		}

		
		/** Initialize the basic parameters, like size of the matrix, default scale and color map, etc. */
		initParameters(){
			this._precision = 2;
			this._showMasked = false;
			this._zooming = false;
			this._pixelCount = 256;
			this._maxPixelCount = 256;
			this._zoom = 1;
			this._startX = 0;
			this._startY = 0;
			this._maxValue = 0;
			this._minValue = 0;
			this._startTime = 0;
			this._scale = new LinearScale();
			this.changeColormap(new GreyscaleMap());
			this.initMaskedMatrix();
			this._maskingColor = "#f68735";
			this._maskedPixelsColor = "#3f547f";
			this._offset = 0;
			this._unitDisplayer = new UnitDisplayer();
		}

		/** Process pixel's values and save the value of the pixel to be displayed */
		abstract processPixel(pixel);
		/** Get the proper color for this pixel, how it should be shown on the histogram */
		abstract getPixelColor(pixel);
		/** Displays a preview of a scale with values */
		abstract previewScale(divId: string);


		/** Initializes the matrix of masked pixels. 
		Uses masked matrix of its parent, to be able to mask across all the histograms of a single sensor.  */
		initMaskedMatrix(){
			this._maskedMatrix = this._parent._maskedMatrix;
		}

		/**
		Initializes the _data array (2D array of the visualization) and process all the pixel data from the data source to store the correct visualization values.
		*/
		getCanvasData(data: any) {
			this._minValue = 0;
			this._maxValue = 0;
			this._data = [];
			this._offset = data.Offset;
			for (let i = 0; i <  this._pixelCount; i++){
				this._data[i] = [];
				for (let j = 0; j < this._pixelCount; j++){
					this._data[i][j] = [];
					this._data[i][j][0] = 0;
					this._data[i][j][1] = 0;
				}
			}
			for (let  i = 0; i < data.Size; i++){
				this.processPixel(data.Pixels[i]);
	    }
			return this._data;
		}

		/** Shows the data attributes of the pixel on certain coordinate on the canvas */
		updateLabels(mouseX: number, mouseY: number){
			let posX: number = this.canvasToSensorCoordX(mouseX);
			let posY: number = this.canvasToSensorCoordY(mouseY);

			if (posX < this._startX || posY < this._startY || posX > this._startX + this._pixelCount || posY > this._startY + this._pixelCount) {
				console.log("Out of boundaries.");
				return;
			}
			for (let i: number = 0; i < this._labelValId.length; i++){
				$("#" + this._labelValId[i]).html(this._data[posX][posY][i]);
				if (this._data[posX][posY][i] == undefined){
					$("#" + this._labelValId[i]).html("0");
				}
			}
			$("#" + this._labelXid).html(posX.toString());
			$("#" + this._labelYid).html(posY.toString());
		}

		/** Finds out for which value would currently selected scale show it is the 50% value */
		getMidValue(){
			return this._scale.getProportionValue(1/2, this._minValue, this._maxValue);
		}

		/** Finds out for which value would currently selected scale show it is the 25% value */
		getQuarterValue(){
			return this._scale.getProportionValue(1/4, this._minValue, this._maxValue);
		}

		/** Updates the current zooming coordinates according to input 2 points on the canvas. The coordinates will always represent a square and will never exceed the canvas' boundaries. 
		Also it always determines the top-most, left-most point of the resulting square as the start coordinate and then the size of the square.
		*/
		updateZoomingCoordinates(startX: number, startY: number, endX: number, endY: number) {
			let sizeX: number = Math.max(Math.abs(endX - startX), Math.abs(endY - startY));
			let sizeY: number = sizeX;

			if (startX > endX) {
				sizeX = -sizeX;
			}

			if (startY > endY) {
				sizeY = -sizeY;
			}

			if (startX + sizeX >= this._canvasWidth) {
				sizeX = this._canvasWidth - startX;
				if (sizeY < 0) {
					sizeY = - sizeX;
				} else {
					sizeY = sizeX;
				}
			}
			if (startY + sizeY >= this._canvasHeight) {
				sizeY = this._canvasHeight - startY;
				if (sizeX < 0) {
					sizeX = - sizeY;
				} else {
					sizeX = sizeY;
				}
			}
			if (startX + sizeX <= 0) {
				sizeX = -startX;
				if (sizeY < 0) {
					sizeY = sizeX;
				} else {
					sizeY = - sizeX;
				}
			}
			if (startY + sizeY <= 0) {
				sizeY = -startY;
				if (sizeX < 0) {
					sizeX = sizeY;
				} else {
					sizeX = - sizeY;
				}
			}

			this._zoomStartX = startX;
			this._zoomStartY = startY;
			this._zoomWidth = sizeX;
			this._zoomHeight = sizeY;
		}

		/** Draw a semi-transparent square on current zooming coordinates position. Makes the square in proper color to be always clearly visible on the canvas. */
		drawZoomSquare(){
			if (!this._tempCanvas || !this._tempContext){
				console.log("No temporary canvas created. Aborting.");
				return;
			}
			let lowColorValue: Color = this._colorMap.getColor(this._scale.getScaledValue(0, 0, 100));
			if (this._masking){
				this._tempContext.fillStyle = this.getMaskingColor();
			} else {
				this._tempContext.fillStyle = "rgba(" + (255 - lowColorValue.r()) + "," + (255 - lowColorValue.r()) + "," + (255 - lowColorValue.r()) + "," + 0.5 + ")";
			}
			this._tempContext.fillRect(this._zoomStartX, this._canvasHeight - this._zoomStartY, this._zoomWidth, -this._zoomHeight);
		}

		/** Updates the visualization to only show values from the selected (zoomed) area */
		updateViewport(startPixelX: number, startPixelY: number, zoomSize: number){
			if (zoomSize == 0) {
				this._pixelCount = this._maxPixelCount;
				this._startX = 0;
				this._startY = 0;
			} else {
				this._startX = startPixelX;
				this._startY = startPixelY;
				this._pixelCount = zoomSize;
			}
		}

		/** Indicates, if some pixel was selected to display its values in labels, or if labels should update on mousemove */
		togglePixelSelection(){
			this._selectedPixel = !this._selectedPixel;
			$('#' + this._name + "-lock").toggle();
		}

		/** Marks pixels on given coordinates as masked */
		maskPixels(startPixelX: number, startPixelY: number, zoomSize: number){
			if (zoomSize == 0) zoomSize = 1;
			for (let i = startPixelX; i < startPixelX + zoomSize; i++){
				for (let j = startPixelY; j < startPixelY + zoomSize; j++){
					this._maskedMatrix[i][j] = !this._maskedMatrix[i][j];
				}
			}
		}

		/** Resets the masking matrix, to set all pixels not to be maskes */
		unmaskAll(){
			for (let i = 0; i < 256; i++){
				for (let j = 0; j < 256; j++){
					this._maskedMatrix[i][j] = 0;
				}
			}
		}

		/** Converts a coordinate from the screen coordinate system to corresponding coordinate on the sensor's pixel matrix. */
		canvasToSensorCoordX(coordinate: number){
			return this._startX + Math.floor(coordinate * this._pixelCount / this._canvasWidth);
		}

		/** Converts a coordinate from the screen coordinate system to corresponding coordinate on the sensor's pixel matrix. */
		canvasToSensorCoordY(coordinate: number){
			return this._startY + Math.floor(coordinate * this._pixelCount / this._canvasHeight);
		}

		/** Gets real coordinates of the mouse, respective to the histogram canvas, from the global (screen) coordinates of the mouse. Top left corner of the canvas is 0 0 */
		getRealCanvasCoordX(coordinate: number, jCanvas){
			let newCoordinate = coordinate - $(jCanvas).offset().left;
			return newCoordinate;
		}

		/** Gets real coordinates of the mouse, respective to the histogram canvas, from the global (screen) coordinates of the mouse. Top left corner of the canvas is 0 0 */
		getRealCanvasCoordY(coordinate: number, jCanvas){
			let newCoordinate = this._canvasHeight - (coordinate - $(jCanvas).offset().top);
			return newCoordinate;
		}

		mouseUpHandler(e, jCanvas){
				if (!this._data || this._data.Size == 0){
					return;
				}
				if (!this._zooming){
					return;
				}
				this._zooming = false;
				let endMouseX: number =  this.getRealCanvasCoordX(e.pageX, jCanvas);
				let endMouseY: number =  this.getRealCanvasCoordY(e.pageY, jCanvas);

				this.updateZoomingCoordinates(this._startMouseX, this._startMouseY, endMouseX, endMouseY);

				let startPixelX = this.canvasToSensorCoordX(Math.min(this._zoomStartX, this._zoomStartX + this._zoomWidth));
				let startPixelY = this.canvasToSensorCoordY(Math.min(this._zoomStartY, this._zoomStartY + this._zoomHeight));
				let endPixelX = this.canvasToSensorCoordX(Math.max(this._zoomStartX, this._zoomStartX + this._zoomWidth));

				let zoomSize: number = Math.abs(endPixelX - startPixelX);

				if (zoomSize == 0 && !this._masking){
					//this.togglePixelSelection();
					return;
				}

				if (this._masking){
					this.maskPixels(startPixelX, startPixelY, zoomSize);
				} else {
					this._parent.updateViewport(startPixelX, startPixelY, zoomSize);
				}

				this.clearTempCanvas();
				this._parent.redrawHistograms();
		}

		/** Sets up all necessary event callbacks to provide the interactivity for the histogram. */
		setupHandlers ( ){

			let sensorObj = this;

			this._jTempCanvas.mousedown(function(e) {
				if (!sensorObj._data || sensorObj._data.Size == 0){
					return;
				}
				sensorObj._startMouseX = sensorObj.getRealCanvasCoordX(e.pageX, this);
				sensorObj._startMouseY = sensorObj.getRealCanvasCoordY(e.pageY, this);
				sensorObj._zooming = true;
			});

			this._jTempCanvas.dblclick(function(e){
				e.preventDefault();
				try{
					window.getSelection().empty();
				} catch (e){

				}
				if (sensorObj._masking) {
					sensorObj.unmaskAll();
					sensorObj._parent.redraw();
					return;
				}
				sensorObj._parent.updateViewport(0,0,256);
				sensorObj._parent.redrawHistograms();
			});

			this._jTempCanvas.contextmenu(function(e){
				e.preventDefault();
				sensorObj.togglePixelSelection();
			});

			this._jTempCanvas.mouseup(function(e){
				sensorObj.mouseUpHandler(e, this);
			});

			this._jTempCanvas.mouseleave(function(e) {
				e.preventDefault();
				try{
					window.getSelection().empty();
				} catch (e){

				}
				sensorObj.mouseUpHandler(e, this);
			});

			this._jTempCanvas.mousemove(function(e){
				if (!sensorObj._data || sensorObj._data.Size == 0){
					console.log("data null");
					return;
				}
				let currentMouseX : number =  sensorObj.getRealCanvasCoordX(e.pageX, this);
				let currentMouseY : number =  sensorObj.getRealCanvasCoordY(e.pageY, this);

				if (sensorObj._zooming){
					sensorObj.clearTempCanvas();
					sensorObj.updateZoomingCoordinates(sensorObj._startMouseX, sensorObj._startMouseY, currentMouseX, currentMouseY);
					sensorObj.drawZoomSquare();
				}

				if (!sensorObj._selectedPixel){
					sensorObj.updateLabels(currentMouseX, currentMouseY);
				}
			});

		}

		/** Gets the corresponding color for input value, according to currently selected scale and color map */
		getScaledColor(value : number){
			let scaledValue : number = this._scale.getScaledValue(value, 0, this._maxValue);
			if (scaledValue == 0 && value != 0){
				scaledValue += 1;
			}
			let color : string = this._colorMap.getColor(scaledValue).css();

			return color;
		}

		/** Returns a color in which all the masked (ignored) bits will be displayed */
		getMaskedPixelsColor(){
			return this._maskedPixelsColor;
		}

		/** Returns a color in which the masked and currently masked pixels will be shown in masking mode */
		getMaskingColor(){
			return this._maskingColor;
		}

		/** Draws a square on specified location. Square is the size of canvas width / number of pixels to be displayed. */
		drawSquare(x_coord : number, y_coord : number){
			y_coord = this._pixelCount - 1 - y_coord;
			let point : number = this._canvasWidth / this._pixelCount;
			this._context.fillRect(x_coord * point, y_coord * point, point, point);
		}

		/** Redraws all the data on the visualization.  */
		redraw(newDataSource: DataAccess = null){
			this.updateSize();
			if (newDataSource){
				this.updateViewport(0,0,0);
				this._startTime = newDataSource.getStartTime();
				this.getCanvasData(newDataSource.getData(this._parent.getSource()));
			}
			this.drawAllPixels();
			this.previewScale(this._previewScaleId);
		}

		/** Draws all the pixels currently saved in the _data array on the canvas. */
		drawAllPixels(){
			if (!this._data || this._data.Size == 0){
				return;
			}

			this.updateSize();
			this._context.fillStyle = this.getPixelColor(0);
			this._context.fillRect(0,0,this._canvasWidth, this._canvasHeight);
			for (let i = 0; i < this._pixelCount; i++) {
				for (let j = 0; j < this._pixelCount; j++) {
					/** if this current pixel was masked */
					if (this._maskedMatrix[this._startX + i][this._startY + j] == 1){
						if (this._masking){
							this._context.fillStyle = this.getMaskingColor();
						} else if (this._showMasked){
							this._context.fillStyle = this.getMaskedPixelsColor();
						} else {
							this._context.fillStyle = this.getPixelColor(0);
						}
						this.drawSquare(i, j);
						continue;
					}

					let value = this._data[this._startX+i][this._startY+j][0];
					if (value != 0){
						this._context.fillStyle = this.getPixelColor(value);
	        			this.drawSquare(i, j);
        			}
				}
			}
		}

		/** Update the maximum value currently present in the visualization */
		updateMaxValue(value: number){
			if (value > this._maxValue) {
				this._maxValue = value;
			}
		}

		/** Shows all currently masked pixels */
		toggleShowMaskedPixels(){
			this._showMasked = !this._showMasked;
		}

		/** Switches to the masking mode, if the user is currently selecting pixels to be masked, or no */
		toggleMaskingOn(){
			this._masking = true;
		}

		/** Switches off the masking mode */
		toggleMaskingOff(){
			this._masking = false;
		}

		/** Log some info about the canvas into console */
		logHistogram(){
			console.log(this._name);
			console.log(this._canvas);
			console.log(this._data);
			console.log(this._maxValue);
			console.log(this._colorMap);
		}

		/** Set up elements, where to draw the labels and info about the pixels */
		setupLabelIds(){
			this._labelXid = this._name + "-pixX";
			this._labelYid = this._name + "-pixY";
			this._previewScaleId = this._name + "-scale-histogram";
		}

		changeColormap(colormap: IColorMap){
			this._colorMap = colormap;
			colormap.preview(this._name + "-colormap-histogram", true);
		}

		showRelativeValues(){
			return this._parent.showRelativeValues();
		}

	}

	/**
	Histogram displaying the ToT value of the pixel.
	*/
	export class ToTHistogramCanvas extends HistogramCanvas{
		constructor(parent: Sensor, name : string, data : any = null){
			super(parent, name, data);
			this._labelValId = [this._name + "-tot", this._name + "-tot_KeV"];
		}

		processPixel(pixel) {
			this._data[pixel.pixX][pixel.pixY][0] += pixel.ToT;
			this._data[pixel.pixX][pixel.pixY][1] += pixel.ToT_KeV;
			this.updateMaxValue(this._data[pixel.pixX][pixel.pixY][0]);	
		}
		getPixelColor(pixel) {
			return this.getScaledColor(pixel);
		}

		/** Previews the color scale used, with the proper corresponding values to show, which displayed color means what. */
		previewScale(divId: string){
			let jPreviewCanvas = $("#" + divId);
			jPreviewCanvas.html("");
			jPreviewCanvas.append("<span class=\"scale-min-value\">" + this._minValue.toFixed(0) + "</span>");
			jPreviewCanvas.append("<span class=\"scale-quarter-value\">" + this.getQuarterValue().toFixed(0) + "</span>");
			jPreviewCanvas.append("<span class=\"scale-mid-value\">" + this.getMidValue().toFixed(0) + "</span>");
			jPreviewCanvas.append("<span class=\"scale-max-value\">" + this._maxValue.toFixed(0) + " [cts]</span>");
		}
	}
	/**
	Histogram displaying the ToA value of the pixel + original raw ToA(as sToA) and fToA
	*/
	export class ToAHistogramCanvas extends HistogramCanvas{
		constructor(parent: Sensor, name : string, data : any = null){
			super(parent, name, data);
			this._labelValId = [this._name + "-toa", this._name + "-stoa", this._name + "-ftoa"];
		}

		processPixel(pixel){
			this._data[pixel.pixX][pixel.pixY][0] = pixel.ToA + this._offset;
			if (this.showRelativeValues()){
				this._data[pixel.pixX][pixel.pixY][0] -= this._startTime;
			}
			this._data[pixel.pixX][pixel.pixY][1] = pixel.sToA;
			this._data[pixel.pixX][pixel.pixY][2] = pixel.fToA;
			this.updateMaxValue(this._data[pixel.pixX][pixel.pixY][0]);
		}

		getPixelColor(pixel){
			return this.getScaledColor(pixel);
		}

		/** Previews the color scale used, with the proper corresponding values to show, which displayed color means what. */
		previewScale(divId: string){
			let jPreviewCanvas = $("#" + divId);
			jPreviewCanvas.html("");
			jPreviewCanvas.append("<span class=\"scale-min-value\">" + this._minValue.toFixed(0) + "</span>");
			jPreviewCanvas.append("<span class=\"scale-quarter-value\">" + this._unitDisplayer.getBestUnits(this.getQuarterValue(), UnitDisplayer.NANO).toFixed(this._precision) + "</span>");
			jPreviewCanvas.append("<span class=\"scale-mid-value\">" + this._unitDisplayer.convert(this.getMidValue(), UnitDisplayer.NANO, this._unitDisplayer.getCurrentUnits()).toFixed(this._precision) + "</span>");
			jPreviewCanvas.append("<span class=\"scale-max-value\">" + this._unitDisplayer.convert(this._maxValue, UnitDisplayer.NANO, this._unitDisplayer.getCurrentUnits()).toFixed(this._precision) + " [" + this._unitDisplayer.getUnitShortcut() + "s]</span>");
		}

	}
	/**
	Histogram displaying number of pixel hits on each coordinate on the sensor's matrix
	*/
	export class HitsHistogramCanvas extends HistogramCanvas{
		constructor(parent: Sensor, name: string, data : any = null){
			super(parent, name, data);
			this._labelValId = [this._name + "-hits"];
		}

		processPixel(pixel){
			this._data[pixel.pixX][pixel.pixY][0] += 1;
			this.updateMaxValue(this._data[pixel.pixX][pixel.pixY][0]);
		}

		getPixelColor(pixel) {
			return this.getScaledColor(pixel);
		}

		/** Previews the color scale used, with the proper corresponding values to show, which displayed color means what. */
		previewScale(divId: string){
			let jPreviewCanvas = $("#" + divId);
			jPreviewCanvas.html("");
			jPreviewCanvas.append("<span class=\"scale-min-value\">" + this._minValue.toFixed(0) + "</span>");
			jPreviewCanvas.append("<span class=\"scale-quarter-value\">" + this.getQuarterValue().toFixed(0) + "</span>");
			jPreviewCanvas.append("<span class=\"scale-mid-value\">" + this.getMidValue().toFixed(0) + "</span>");
			jPreviewCanvas.append("<span class=\"scale-max-value\">" + this._maxValue.toFixed(0) + " [#hits]</span>");
		}
	}

}
