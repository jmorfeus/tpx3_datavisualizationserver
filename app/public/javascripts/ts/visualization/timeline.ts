/// <reference path="data-access.ts" />
/// <reference path="canvas-visualization.ts" />
/// <reference path="tools.ts" />

import CanvasVisualization = tpx3.canvasvisualization.CanvasVisualization;
import DataAccess = tpx3.dataaccess.DataAccess;
import Tools = tpx3.tools.TPX3Tools;

namespace tpx3.timeline {

	/**
	Class representing the interactive 1D timeline visualization. 
	*/
	export class Timeline extends CanvasVisualization implements IRedrawable{
		/** Parent element, specifying which sensor this single histogram belongs to */
		protected _parent: Sensor;
		/** Name/ID of the scale canvas element */
		protected _scaleName: string;
		/** Name/ID of the horizontal scale canvas element */
		protected _scaleNameVertical: string;
		/** ID of the element where to show label about start time */
		protected _startTimeLabelId: string;
		/** ID of the element where to show label about end time */
		protected _endTimeLabelId: string;
		/** ID of the element where to show label about difference between start and end time */
		protected _deltaTimeLabelId: string;
		/** ID of the element where to show the value of the current bin */
		protected _binValueLabelId: string;
		/** ID of the element where to show the current position of the timeline cursor */
		protected _cursorTimeLabelId: string;
		/** Array of filtered data to be drawn on the timeline */
		protected _filteredData: any;
		/** Size of the bins */
		protected _binSize: number;
		/** Maximum value of all the bins */
		protected _maxBin: number;
		/** Number of bins displayed on the timeline */
		protected _numOfBins: number;
		/** Size of the interval displayed */
		protected _timeSlice: number;
		/** Starting time of the displayed interval */
		protected _startTime: number;
		/** Trigger, on which the displayed data start */
		protected _startTrigger: number;
		/** Toggle, if snapping to non-zero values is selected */
		protected _snapToNonZero: boolean;
		/** Reference to DataAccess object, to be able to filter the data displayed */
		protected _dataAccess: DataAccess;
		/** On which bin the leftmost handle is placed */
		protected _leftBin: number;
		/** On which bin the rightmost handle is placed */
		protected _rightBin: number;
		/** Starting indexes of pixels for each of the displayed bins */
		protected _pixelIndexes = [];
		/** Color, in which to show the un-filtered data */
		protected _dataColor: string;
		/** Color, in which to show the filtered data */
		protected _filteredDataColor: string;
		protected _triggerColor: string;
		/** Precision of numbers shown as timeline labels and values in form inputs */
		protected _precision: number;
		protected _currentHoveredBin: number;
		/** the jQuery element of 3rd visualization layer canvas */
		protected _jOverlayCanvas;
		/** the HTML5 Canvas element of the 3rd layer */
		protected _overlayCanvas: HTMLCanvasElement;
		/** the drawing context for the 3rd layer */
		protected _overlayContext: CanvasRenderingContext2D;

		protected _globalControls: GlobalControls;

		protected _unitDisplayer: UnitDisplayer;

		/** Creates new timeline, attaches all the dependencies, as DataAccess for getting and filtering the pixel data and parent Sensor object for updating the other components of its visualization */
		constructor(parent: Sensor, name: string, dataAccess: DataAccess, globalControls: GlobalControls, startTime: number, timeSlice: number, binSize: number = 0) {
			super(name);
			this._globalControls = globalControls;
			this.createOverlayCanvas();
			this._parent = parent;
			this._maxBin = 1;
			this._scaleName = this._name + "-scale";
			this._scaleNameVertical = this._name + "-scale-vertical";
			this._timeSlice = timeSlice;
			this._startTime = startTime;
			this._dataAccess = dataAccess;
			this.initParameters();
			
			let data = dataAccess.getData();

			if (data) {
				//this.getCanvasData(data);
			} else {
				this.drawNoData();
			}

			this._leftBin = 0;
			this._rightBin = this._numOfBins - 1;
			this.setupHandlers();
			this.setupLabelIds();
			this.updateSize();
		}

		/** Sets up the basic parameters to default values */
		initParameters(){
			this._unitDisplayer = new UnitDisplayer();
			this._scale = new LinearScale();
			this._snapToNonZero = this._globalControls.getTimelineSnapping();
			this.setBinSize(this._globalControls.getBinSize());
			this._dataColor = "#333";
			this._filteredDataColor = "#f68735"
			this._triggerColor = "#f68735";
			this._precision = 2;
			this._currentHoveredBin = -1;
		}

		getBinSize():number{
			return this._binSize;
		}

		/** Sets the bin size, but checks if the value is not too large, or too low to display */
		setBinSize(newSize: number){
			if (isNaN(+newSize)){
				console.log("Invalid value for bin size.");
				return;
			}
			let currTime = this.getCurrentTime();
			if (this._timeSlice / newSize > this._canvasWidth || newSize < 1.5625) {
				this.addWarning(currTime + " Wrong value for bin size selected(" + newSize + ") - too small for screen or detector resolution. Value rounded up.");
				this._binSize = this._timeSlice / this._canvasWidth;
				if (this._binSize < 1.5625){
					this._binSize = 1.5625;
				}
			} else if (this._timeSlice / newSize < 10) {
				this.addWarning(currTime + " Wrong value for bin size selected(" + newSize + ") - too large for distinct bins. Value rounded down.");
				this._binSize = this._timeSlice / 10;
			} else {
				this._binSize = newSize;
			}
		}

		/** Initializes the ID of the labels - specifies, where the values should be displayed */
		setupLabelIds(){
			this._startTimeLabelId = "timeline-startTime";
			this._endTimeLabelId = "timeline-endTime";
			this._deltaTimeLabelId = "timeline-deltaTime";
			this._binValueLabelId = "timeline-binValue";
			this._cursorTimeLabelId = "timeline-cursorTime";
		}

		/** Returns the number of the bin, which is present on a coordinate specified by the input parameter */
		getCurrentBinNumber(input: number){
			input += 3;
			let binWidth = this._canvasWidth / this._data.length;
			let binNumber = input / binWidth;
			return Math.floor(binNumber);
		}

		/** For particular bin number, returns the accurate position where it is drawn on the canvas, in canvas coordinates */
		getPositionForBinNumber(binNumber: number){
			let binWidth = this._canvasWidth / this._data.length;
			let position = binNumber * binWidth;
			return position - 3 + binWidth / 2; // move cursor's center (3px from left of cursor) to the middle of bin (bin width/2)
		}

		/** Returns how many pixels are contained in specified bin */
		getBinValue(binNumber: number){
			let binDataValue: number = this._data[Math.floor(binNumber)];
			return Math.abs(binDataValue);
		}

		/** Find first non-empty bin */
		findClosestNonEmpty(binNumber: number){
			for (let i = Math.floor(binNumber); i < this._data.length; i++){
				if (this._data[i] != 0){
					return i;
				}
			}
			console.log("Nothing to snap on.");
			return binNumber;
		}

		/** Updates the labes and filteres all the data in the DataAccess class, based on current position of the draggable handles (that specify the specified interval)*/
		updateData(){
			if (this._leftBin == undefined){
				this._leftBin = 0;
			} 
			if (this._rightBin == undefined){
				this._rightBin = this._numOfBins - 1;
			}
			let minBin: number = Math.min(this._leftBin, this._rightBin);
			let maxBin: number = Math.max(this._leftBin, this._rightBin);

			let pixCount = 0;
			for (let i = minBin; i <= maxBin; i++){
				pixCount += Math.abs(this._data[i]);
			}

			maxBin += 1;

			$("#" + this._binValueLabelId).html(pixCount.toString());
			$("#" + this._deltaTimeLabelId).attr("trueValue", maxBin * this.getBinSize() - minBin * this.getBinSize());

			$("#" + this._deltaTimeLabelId).html(this._unitDisplayer.convert(maxBin * this._binSize - minBin * this._binSize, UnitDisplayer.NANO, this._unitDisplayer.getCurrentUnits()).toFixed(this._precision));
			
			$("#" + this._endTimeLabelId).html(this._unitDisplayer.convert(maxBin * this._binSize, UnitDisplayer.NANO, this._unitDisplayer.getCurrentUnits()).toFixed(this._precision));
			$("#" + this._startTimeLabelId).attr("trueValue", minBin * this.getBinSize());
			$("#" + this._startTimeLabelId).html(this._unitDisplayer.convert(minBin * this._binSize, UnitDisplayer.NANO, this._unitDisplayer.getCurrentUnits()).toFixed(this._precision));
			this._dataAccess.setInterval(this._pixelIndexes[minBin], this._pixelIndexes[maxBin], this._parent.getSource());
		}

		/** For a given jQuery handle, determines its position, determines the current bin and updates(filters) the data based on the position of the handle */
		handleStop(jHandle: JQuery, which: string){
			if (!this._data || this._data.Size == 0){
				return;
			}
			let posString = jHandle.css("left");
			let posNumber: number = +posString.substr(0, posString.length - 2); // remove "px" from end of "left" css attribute
			let binNumber = this.getCurrentBinNumber(+posNumber);
			let binValue = this.getBinValue(binNumber);
			if (this._snapToNonZero && binValue == 0) {
				binNumber = this.findClosestNonEmpty(binNumber);
				let newPos = this.getPositionForBinNumber(binNumber);
				jHandle.css("left", newPos);
				binValue = this.getBinValue(binNumber);
			}

			if (which === "left"){
				this._leftBin = binNumber;
			} else {
				this._rightBin = binNumber;
			}

			this.updateData();
			this._parent.redrawHistograms(this._dataAccess);
		}

		/** Handler for stop event of the left handle */
		rightHandleStop(jHandle: JQuery){
			this.handleStop(jHandle, "right");
		}

		/** Handler for stop event of the right handle */
		leftHandleStop(jHandle: JQuery){
			this.handleStop(jHandle, "left");
		}

		/** Gets real coordinates of the mouse, respective to the histogram canvas, from the global (screen) coordinates of the mouse. Top left corner of the canvas is 0 0 */
		getRealCanvasCoordX(coordinate: number, jCanvas){
			let newCoordinate = coordinate - jCanvas.parentNode.offsetLeft;
			return newCoordinate;
		}

		/** Creates another layer */
		createOverlayCanvas() {
			let container = this._jCanvas[0].parentNode;
			this._overlayCanvas = document.createElement('canvas');
			this._overlayCanvas.id = this._name + "-over";
			this._overlayCanvas.width = this._canvasWidth;
			this._overlayCanvas.height = this._canvasHeight;
			container.appendChild(this._overlayCanvas);
			this._overlayContext = this._overlayCanvas.getContext('2d');
			this._jOverlayCanvas = $('#' + this._overlayCanvas.id);
			this._jOverlayCanvas.addClass("temp-canvas");
			this._jOverlayCanvas.attr("height", this._canvasHeight);
			this._jOverlayCanvas.attr("width", this._canvasWidth);
			this._jOverlayCanvas.css("max-height", this._canvasHeight);
		}

		/** Registers all the callback functions for interactive elements on the timeline. */
		setupHandlers(){
			let timelineObj = this;

		    $( "#" + this._name + "-slider-left").draggable({
					scroll: false,
					axis: "x",
					containment: ".timeline-canvas-wrapper",
					drag: function(){},
					stop: function() {
						timelineObj.leftHandleStop($(this));
					}
		    });
			$("#" + this._name + "-slider-right").draggable({
					scroll: false,
					axis: "x",
					containment: ".timeline-canvas-wrapper",
					drag: function(){},
					stop: function() {
						timelineObj.rightHandleStop($(this));
					}
		    });
		    this._jOverlayCanvas.mousemove(function(e){
		    	if (!timelineObj._globalControls.showCursor()){
		    		return;
		    	}
		    	let coord = timelineObj.getRealCanvasCoordX(e.pageX, this);
		    	let currBin = timelineObj.getCurrentBinNumber(coord);
		    	if (currBin != timelineObj._currentHoveredBin){
		    		timelineObj._globalControls.highlightBin(currBin);
		    		timelineObj._currentHoveredBin = currBin;
		    	}
				$("#" + timelineObj._cursorTimeLabelId).html(timelineObj._unitDisplayer.convert(currBin * timelineObj._binSize, UnitDisplayer.NANO, timelineObj._unitDisplayer.getCurrentUnits()).toFixed(timelineObj._precision + 1) + " [" + timelineObj._unitDisplayer.getUnitShortcut() + "s]");
		    });

		}

		highlightBin(binNumber: number){
			this._overlayContext.clearRect(0,0, this._canvasWidth, this._canvasHeight);
			this._overlayContext.fillStyle = "rgba(255,23,23,0.5)";
			this._overlayContext.fillRect(binNumber * this._canvasWidth / this._numOfBins, 0, this._canvasWidth / this._numOfBins, this._canvasHeight);
		}

		/** Sets the "snap to non-zero" functionality to OFF */
		toggleSnappingOff(){
			this._snapToNonZero = false;
		}

		/** Sets the "snap to non-zero" functionality to ON */
		toggleSnappingOn(){
			this._snapToNonZero = true;
		}

		/** Redraws the data on the Timeline */
		redraw(newDataSource: DataAccess = null){
			this.drawFilteredData();
			let data = null;
			if (newDataSource){
				data = newDataSource.getData(this._parent.getSource(), true);
				this._startTime = newDataSource.getStartTime();
				this._timeSlice = newDataSource.getTimeSlice();
				this.setBinSize(this._globalControls.getBinSize());
			}
			if (data){
				if (data == undefined || data == 0) {
					this.drawNoData();
					return;
				}
				this.getCanvasData(data);
			}

			this.clearTempCanvas();
			this.updateSize();
			this.drawData();
			if (newDataSource && newDataSource.hasFilter()){
				this.redrawFiltered(newDataSource);
			}

			$("#" + this._name + "-slider-left").css("left", 0);
			$("#" + this._name + "-slider-right").css("left", "99.6%");
			this._leftBin = 0;
			this._rightBin = this._numOfBins - 1;

			this.previewScales();
			this.updateData();
		}

		/** Redraws only second layer of the visualization, to show current filtered data, while keeping the original for comparison. */
		redrawFiltered(newDataSource: DataAccess = null){
			let data = null;
			if (newDataSource){
				data = newDataSource.getData(this._parent.getSource());
			}
			if (data && data != undefined && data != null){
				this.clearTempCanvas();
				this.getFilteredData(data);
				this.drawFilteredData();
			} 
		}

		/** Drawing all the bins and the trigger lines, where the measurement registered a trigger. */
		drawData(){
			if (!this._data){
				this.drawNoData();
				return;
			}
			let triggerWidth: number = 1;
			let binCount: number = this._data.length;
			let triggerCount: number = 0;
			this._context.fillStyle = this._dataColor;

			for (let i = 0; i < binCount; i++){
				let currentData = this._data[i];
				let coord = this._canvasHeight - this._canvasHeight * this._scale.getScaledValue(Math.abs(currentData), 0, this._maxBin) / 100;
				this._context.fillRect(i * this._canvasWidth / this._numOfBins, this._canvasHeight - this._canvasHeight * this._scale.getScaledValue(Math.abs(currentData), 0, this._maxBin) / 100, this._canvasWidth / this._numOfBins, this._canvasHeight);

				if (currentData < 0) {
					this._context.fillStyle = this._triggerColor;
					let curTrigger = this._startTrigger + triggerCount + 1;
					this._context.fillText("Trigger " + curTrigger, (i+1) * this._canvasWidth / binCount + 4, curTrigger % 2 == 0 ? 15 : 30);
					this._context.fillRect(-triggerWidth + (i+1) * this._canvasWidth / binCount, 0, triggerWidth, this._canvasHeight);
					this._context.fillStyle = this._dataColor;
					triggerCount++;
				}
			}

		}

		/** Draws only second layer of the visualization, to show current filtered data, while keeping the original for comparison. */
		drawFilteredData(){
			if (!this._filteredData) {
				return;
			}

			this._tempContext.fillStyle = this._filteredDataColor;
			for (let i = 0; i < this._numOfBins; i++){
				let currentData = this._filteredData[i];
				this._tempContext.fillRect(i * this._canvasWidth / this._numOfBins, this._canvasHeight - this._canvasHeight * this._scale.getScaledValue(Math.abs(currentData), 0, this._maxBin) / 100, this._canvasWidth / this._numOfBins, this._canvasHeight);
			}
		}

		/** Draws both horizontal and vertical scales of the timeline */
		previewScales(){
			this.previewHorizontalScale();
			this.previewVerticalScale();
		}

		/** Displays the scale under the timeline, to describe the X-axis. To show, how long time period represents a certain distance (number of bins) on the timeline */
		previewHorizontalScale(){
			if (!this._data){
				return;
			}
			$('#' + this._scaleName).attr("height", 25);
			$('#' + this._scaleName).attr("width", this._canvasWidth);

			let canvas = document.getElementById(this._scaleName) as HTMLCanvasElement;
			let context = canvas.getContext("2d");
			let minWidth = this._canvasWidth / 20;
			let rightPosition = this._canvasWidth / this._data.length;
			let origRightPosition = rightPosition;
			let numberOfBins = 1;
			context.fillStyle = "rgba(255,255,255,0.4)";
			context.fillRect(0, 0, this._canvasWidth, 25);
			while(rightPosition < minWidth){
				rightPosition += origRightPosition;
				numberOfBins++;
			}
			origRightPosition = rightPosition;
				context.fillStyle = "#000000";
				context.fillRect(1, 0, 0.5, 100);
				context.font = "15px Georgia";
				context.fillText("0", 4, 18);
			let whichBin = 1;
			let chosenUnits = false;
			while(rightPosition < this._canvasWidth){
				context.fillRect(rightPosition, 0, 0.5, 100);
				if (!chosenUnits){
					this._unitDisplayer.getBestUnits(numberOfBins * whichBin * this._binSize, UnitDisplayer.NANO);
					chosenUnits = true;
				}
				let correctNumber = this._unitDisplayer.convert(numberOfBins * whichBin * this._binSize, UnitDisplayer.NANO, this._unitDisplayer.getCurrentUnits());

				context.fillText(correctNumber.toFixed(this._precision), rightPosition + 3, 18, origRightPosition-6);
				rightPosition += origRightPosition;
				whichBin++;
			}
			rightPosition = this._canvasWidth - 30;
			context.fillStyle = "rgba(255,255,255,0.4)";
			context.clearRect(rightPosition, 0, 30, 100);
			context.fillRect(rightPosition, 0, 30, 100);
			context.fillStyle = "#000000";
			context.fillText("[" + this._unitDisplayer.getUnitShortcut() + "s]", rightPosition + 3, 18);
		}
		/** Displays the scale next to the timeline, to describe the Y-axis. To show number of hits within bins */
		previewVerticalScale(){
			$('#' + this._scaleNameVertical).attr("height", this._canvasHeight);
			$('#' + this._scaleNameVertical).attr("width", this._canvasWidth / 50);
			let width = this._canvasWidth / 50;
			let canvas = document.getElementById(this._scaleNameVertical) as HTMLCanvasElement;
			let context = canvas.getContext("2d");
			context.fillStyle = "rgba(255,255,255,0.8)";
			context.fillRect(0, 0, 50, this._canvasHeight);
			context.fillStyle = "#000000";
			context.font = "20px Georgia";
			context.textAlign = "center";
			context.fillText("#", width/2, 59);
			context.fillText("h", width/2, 79);
			context.fillText("i", width/2, 94);
			context.fillText("t", width/2, 109);
			context.fillText("s", width/2, 123);
		}


		/** Takes the new data from argument and saves them into _filteredArray, to be later drawn over the un-filtered, original data */
		getFilteredData(data: any){
			this._filteredData = [];
			let baseTime = data.StartTime;
			let binHighBound = this._startTime;
			let j = 0;
			let currTrigger = this._startTrigger;

			for (let i = 0; i < this._numOfBins; i++){
				this._filteredData[i] = 0;
				binHighBound += this._binSize;

				while (j < data.Size && (baseTime + data.Pixels[j].ToA) < binHighBound){
					if (data.Pixels[j].TriggerNo != currTrigger){
						currTrigger = data.Pixels[j].TriggerNo;
						baseTime += data.Pixels[j-1].ToA;
						break;
					}
					this._filteredData[i]++;
					j++;
				}
			}

		}

		/** Gets the data and assigns them to bins, according to their ToA values and considering the triggers. Also saves starting pixel index for each bin and maximum bin value, for scaling purposes. */
		getCanvasData(data: any) {
			this._binSize = +this._binSize;
			this._numOfBins = Math.floor(this._timeSlice / this._binSize);

			let baseTime = data.StartTime + data.Offset;
			let binLowBound = this._startTime;
			let binHighBound = binLowBound;
			let j = 0;
			this._data = [];
			this._maxBin = 0;
			let currTrigger

			// can be left undefined if there are no data, as following loop is for < data.Size
			if (data.Size > 0){
				currTrigger = data.Pixels[0].TriggerNo;
				this._startTrigger = currTrigger;
			}

			// for each bin (fill)
			for (let i = 0; i < this._numOfBins; i++){
			 	this._data[i] = 0;
			 	binHighBound += this._binSize;

				// Save index of first pixel in this bin
				this._pixelIndexes[i] = j;

				// Loop through pixels in data
			 	while (j < data.Size && (baseTime + data.Pixels[j].ToA) < binHighBound){
			 		// If trigger was found in the data
			 		if (data.Pixels[j].TriggerNo != currTrigger){
			 			currTrigger = data.Pixels[j].TriggerNo;
			 			baseTime += data.Pixels[j-1].ToA;
			 			this._data[i] = -this._data[i];
			 			break;
			 		}

			 		// Add current pixel into current bin, go to next pixel
					this._data[i]++;
					j++;
				}

				// Mark trigger in data by negative value
				if (Math.abs(this._data[i]) > this._maxBin){
					this._maxBin = Math.abs(this._data[i]);
				}
			}

			// Last non-existing bin has no first pixel
			this._pixelIndexes[this._numOfBins] = Infinity;
		}

		changeColormap(colormap: IColorMap){
			
		}

	}
}
