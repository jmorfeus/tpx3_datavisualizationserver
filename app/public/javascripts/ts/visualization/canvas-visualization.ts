/// <reference path="scales.ts" />
/// <reference path="redrawer.ts" />

import Scale = tpx3.scales.Scale;
import LinearScale = tpx3.scales.LinearScale;
import LogarithmicScale = tpx3.scales.LogarithmicScale;
import IRedrawable = tpx3.redrawer.IRedrawable;

namespace tpx3.canvasvisualization{
	/**
	Abstract class representing a two-layered visualization feature on the screen, based on HTML5 canvas element.
	*/
	export abstract class CanvasVisualization extends BaseObject{
		/** the jQuery element of this visualization canvas */
		protected _jCanvas;
		/** the HTML5 Canvas element */
		protected _canvas: HTMLCanvasElement;
		/** the drawing context for the canvas */
		protected _context: CanvasRenderingContext2D;
		/** the jQuery element of 2nd visualization layer canvas */
		protected _jTempCanvas;
		/** the HTML5 Canvas element of the 2nd layer */
		protected _tempCanvas: HTMLCanvasElement;
		/** the drawing context for the 2nd layer */
		protected _tempContext: CanvasRenderingContext2D;
		/** height of the canvas */
		protected _canvasHeight: number;
		/** width of the canvas */
		protected _canvasWidth: number;
		/** data to display on the visualization */
		protected _data: any;
		/** the currently selected scale */
		protected _scale: Scale;

		/** Initializes the object with its name, initializes the canvas, creates the second canvas visualization layer */
		constructor(name: string = ""){
			super(name);
			this._canvas = document.getElementById(name) as HTMLCanvasElement;
			this._jCanvas = $("#" + this._name);
			this._context = this._canvas.getContext("2d");
			this._canvasHeight = this._jCanvas.height();
			this._canvasWidth = this._jCanvas.width();

			this.createTempCanvas();
		}
		/** Each concrete visualization class should implement its own drawing method. */
		abstract redraw();

		/** Updates the size of the visualization according to the current size of the HTML element (handy if it changes with screen size) */
		updateSize() {
			$("#" + this._name).attr("height", this._canvasHeight);
			$("#" + this._name).attr("width", this._canvasWidth);
		}

		/** When there are no data to be drawn, displays the information on the visualization canvas. */
		drawNoData(){
			this.updateSize();
			this._context.fillStyle = "#ffffff";
			this._context.fillRect(0, 0, this._canvasWidth, this._canvasHeight);
			this._context.fillStyle = "#000000";
			this._context.font = "20px Georgia";
			this._context.fillText("No data.", this._canvasWidth / 2, this._canvasHeight / 2);
		}

		/** Creates the second layer of the visualization, directly on top the base layer, with the exact same size. */
		createTempCanvas() {
			let container = this._jCanvas[0].parentNode;
			this._tempCanvas = document.createElement('canvas');
			this._tempCanvas.id = this._name + "-temp";
			this._tempCanvas.width = this._canvasWidth;
			this._tempCanvas.height = this._canvasHeight;
			container.appendChild(this._tempCanvas);
			this._tempContext = this._tempCanvas.getContext('2d');
			this._jTempCanvas = $('#' + this._tempCanvas.id);
			this._jTempCanvas.addClass("temp-canvas");
			this._jTempCanvas.attr("height", this._canvasHeight);
			this._jTempCanvas.attr("width", this._canvasWidth);
			this._jTempCanvas.css("max-height", this._canvasHeight);
		}

		/** Disposes of the created second visualization layer canvas, that was created */
		destroyTempCanvas() {
			this._tempCanvas.remove();
			this._tempContext = null;
		}

		/** Clears everything that was drawn into the canvas of the second layer of the visualization */
		clearTempCanvas() {
			if (!this._tempCanvas || !this._tempContext) {
				console.log("No temporary canvas created. Aborting.");
				return;
			}
			this._tempContext.clearRect(0, 0, this._canvasWidth, this._canvasHeight);
		}

		changeScale(scale: Scale){
			this._scale = scale;
		}
	}
}