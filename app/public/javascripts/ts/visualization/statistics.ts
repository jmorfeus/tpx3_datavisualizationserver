namespace tpx3.statistics {
  /**
  Class representing the display of the statistic information about the data on the screen. Can be redrawn in event of change of the data.
  */
  export class Statistics extends BaseObject implements IRedrawable{
    /** The pixel data about which the statistics are */
    protected _data: any;
    /** Parent element, specifying which sensor this single histogram belongs to */
    protected _parent: Sensor;

    /** Get the pixel data */
    constructor(name: string, parent: Sensor, data: any = null){
      super(name);
      this._parent = parent;
      this._data = data;
      this.putHtmlContent();
    }

    /** Show the statistics about the correct actual data */
    redraw(newDataSource: DataAccess = null){
        if (newDataSource){
          let data = newDataSource.getData( this._parent.getSource() );
          if (data){
            this._data = data;
          }
        }
        if (!this._data){
          this.drawNoData();
          return;
        }
      	$("#" + this._parent._name + "-statistics-size").html(this._data.Size.toString());
      	$("#" + this._parent._name + "-statistics-triggerCnt").html(this._data.TriggerCount.toString());
      	$("#" + this._parent._name + "-statistics-sumEnergy").html(this._data.SumEnergy.toString());
        $("#" + this._parent._name + "-statistics-offset").html(this._data.Offset.toString());
    }

    /** Shows all zeroes. Usually when no data were received from the server. */
    drawNoData(){
      $("#" + this._parent._name + "-statistics-size").html("0");
      $("#" + this._parent._name + "-statistics-triggerCnt").html("0");
      $("#" + this._parent._name + "-statistics-sumEnergy").html("0");
      $("#" + this._parent._name + "-statistics-offset").html("0");
    }

    /** Do nothing, as resizing has no influence on showing statistics. */
    updateSize(){
      
    }

    /** Puts the necessary HTML content on the page to show the statistics */
    putHtmlContent(){
      let jContainer = $('#statistics');
      let filename = this._parent.getSource().substr(0,this._parent.getSource().length-5) + ".png";
      jContainer.prepend("\
        <table id=\"" + this._name + "\">\
          <tbody>\
            <tr>\
              <td class=\"td-left\" colspan=\"2\">Stats for: " + this._parent._name + "</td>\
            </tr>\
            <tr>\
              <td># of pixel hits</td>\
              <td id=\"" + this._parent._name + "-statistics-size\">0</td>\
            </tr>\
            <tr>\
              <td>Trigger count</td>\
              <td id=\"" + this._parent._name + "-statistics-triggerCnt\">0</td>\
            </tr>\
            <tr>\
              <td>Summed energy</td>\
              <td id=\"" + this._parent._name + "-statistics-sumEnergy\">0</td>\
            </tr>\
            <tr>\
              <td>Offset</td>\
              <td><input id=\"" + this._parent._name + "-statistics-offset\" filename=\"" + this._parent.getSource() + "\" class=\"offset-input\" type=\"text\" value=\"0\"></input><span id=\"offset-units\">[s]</span></td>\
            </tr>\
            <tr>\
              <td>Preview</td>\
              <td><a target=\"_blank\" href=\"rootfiles/preview/" + filename + "\"><img width=\"100%\" src=\"rootfiles/preview/" + filename + "\" /></a></td>\
            </tr>\
          </tbody>\
        </table>");
    }

    changeColormap(colormap: IColorMap){
      
    }
  }
}
