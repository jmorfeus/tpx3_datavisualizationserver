/// <reference path="sensor.ts" />
/// <reference path="timeline.ts" />
/// <reference path="statistics.ts" />

namespace tpx3.globalcontrols{
	/** Class responsible for handling and delegating global settings of the visualization, which is needed to for all the individual parts.
	Contains common settings and controls, like bin size for all timelines, color map for all the histograms, etc. */
	export class GlobalControls extends BaseObject{
		/** List of sensors currently shown on the visualization */
		protected _sensorsList: Sensor[];
		/** Current start time of the displayed data */
		protected _startTime: number;
		/** Stack of previous values of _startTime, for UNDO function */
		protected _startTimeHistory: number[];
		/** Current time slice of the displayed data */
		protected _timeSlice: number;
		/** Stack of previous values of _timeSlice, for UNDO function */
		protected _timeSliceHistory: number[];
		/** Current bin size for all the timelines */
		protected _binSize: number;
		/** Determines the visibility of the coincidency cursor on the timelines */
		protected _showCursor: boolean;
		/** Boolean determining if relative ToA values (from the start of current time frame) should be shown, or absolute (raw stored ToA) */
		protected _showRelativeValues;
		/** Toggles the "Snap to non zero values" functionality on the timelines */
		protected _snapToNonZero: boolean;
		/** Number of possible UNDO steps */
		protected _historyLimit: number;
		/** Currently globally selected color map */
		protected _colorMap: IColorMap;
		/** Currently globally selected scale */
		protected _scale: Scale;
		/** Global unit displayer, setting how are units displayed on the Time start, Time slice, Bin size, and possibly other inputs on the visualization screen */
		protected _unitDisplayer: UnitDisplayer;

		constructor(name = ""){
			super(name);
			this._sensorsList = [];
			this._showCursor = true;
			this._showRelativeValues = true;
			this._binSize = 0;
			this._snapToNonZero = false;
			this._timeSliceHistory = [];
			this._startTimeHistory = [];
			this._historyLimit = 10;
			this._unitDisplayer = new UnitDisplayer();
			this._unitDisplayer.setCurrentUnits(UnitDisplayer.BASE);
		}

		/** Adds a new sensor which was added to the visualization */
		addSensor(newSensor: Sensor, name: string){
			this._sensorsList[name] = newSensor;
		}

		/** Checks if sensor with specified name is present */
		has(name: string): boolean{
			if (this._sensorsList[name] !== undefined && this._sensorsList[name] !== null){
				return true;
			} else {
				return false;
			}
		}

		/** Removes sensor from list of current active sensors */
		removeSensor(name: string){
			if (this.has(name)){
				delete this._sensorsList[name];
			}
		}

		/** Toggles masking tool for all the sensors of the visualization */
		toggleMaskingOn(){
			for (let i in this._sensorsList){
				this._sensorsList[i].toggleMaskingOn();
			}	
		}

		/** Toggles masking tool for all the sensors of the visualization */
		toggleMaskingOff(){
			for (let i in this._sensorsList){
				this._sensorsList[i].toggleMaskingOff();
			}
		}

		/** Toggles the visibility of the coincidency cursor */
		toggleShowCursor(){
			this._showCursor = !this._showCursor;
			if (!this._showCursor){
				this.highlightBin(-1);
			}
		}

		/** Getter for _showCursor attribute */
		showCursor(){
			return this._showCursor;
		}

		/** Getter for _showRelativeValues attribute*/
		showRelativeValues(){
			return this._showRelativeValues;
		}

		/** Changes scale for all the sensors of the visualization */ 
		changeScale(newScale: Scale){
			this.setScale(newScale);
			for (let i in this._sensorsList){
				this._sensorsList[i].changeScale(newScale);
			}
		}

		/** Changes color map for all the sensors of the visualization */ 
		changeColorMap(newColorMap: IColorMap){
			this.setColorMap(newColorMap);
			for (let i in this._sensorsList){
				this._sensorsList[i].changeColormap(newColorMap);
			}
		}

		setStartTime(newTime: number){
			if (this._startTimeHistory.length >= this._historyLimit){
				this._startTimeHistory.shift();
			}
			this._startTimeHistory.push(this._startTime);
			this._startTime = newTime;
		}

		getStartTime(): number{
			return this._startTime;
		}

		setTimeSlice(newSlice: number){
			if (this._timeSliceHistory.length >= this._historyLimit){
				this._timeSliceHistory.shift();
			}
			this._timeSliceHistory.push(this._timeSlice);
			this._timeSlice = newSlice;
		}

		/** Returns settings of _starTime and _timeSlice to previousl values */
		undoTimeSettings(){
			if (this._startTimeHistory.length == 1 || this._timeSliceHistory.length == 1){
				return false;
			}
			this._startTime = this._startTimeHistory.pop();
			this._timeSlice = this._timeSliceHistory.pop();
			return true;
		}

		getTimeSlice(): number{
			return this._timeSlice;
		}

		setBinSize(newBinSize: number){
			this._binSize = newBinSize;
			for (let i in this._sensorsList){
				this._sensorsList[i].getTimeline().setBinSize(this._binSize);
				if (this._binSize != this._sensorsList[i].getTimeline().getBinSize()){
					this._binSize = this._sensorsList[i].getTimeline().getBinSize();
				}
			}

			$('#bin-size-input').val(this.getBinSize());
		}

		getBinSize(): number{
			return this._binSize;
		}

		getUnitDisplayer():UnitDisplayer{
			return this._unitDisplayer;
		}

		/** Highlights a single bin on all the timelines */
		highlightBin(binNumber: number){
			for (let i in this._sensorsList){
				this._sensorsList[i].getTimeline().highlightBin(binNumber);
			}
		}

		/** Gets current state of timelines' snapping functionality */
		getTimelineSnapping(){
			return this._snapToNonZero;
		}


		toggleSnapping(){
			this._snapToNonZero = !this._snapToNonZero;
			if (this._snapToNonZero){
				this.toggleSnappingOn();
			} else {
				this.toggleSnappingOff();
			}
		}

		/** Switches off snapping to non-zeroes values functionality for all the timelines */
		toggleSnappingOff(){
			this._snapToNonZero = false;
			for (let i in this._sensorsList){
				this._sensorsList[i].getTimeline().toggleSnappingOff();
			}
		}

		/** Switches on the snapping functionality */
		toggleSnappingOn(){
			this._snapToNonZero = true;
			for (let i in this._sensorsList){
				this._sensorsList[i].getTimeline().toggleSnappingOn();
			}
		}

		/** Selects the option to show the masked pixels for all the sensors */
		toggleShowMaskedPixels(){
			for (let i in this._sensorsList){
				this._sensorsList[i].toggleShowMaskedPixels();
			}
		}

		toggleShowRelativeValuesOn(){
			this._showRelativeValues = true;
		}


		toggleShowRelativeValuesOff(){
			this._showRelativeValues = false;
		}

		exportSettings(){

		}

		importSettings(){

		}

		setColorMap(newMap: IColorMap){
			this._colorMap = newMap;
		}

		getColorMap(): IColorMap{
			return this._colorMap;
		}

		setScale(newScale: Scale){
			this._scale = newScale;
		}

	}
}