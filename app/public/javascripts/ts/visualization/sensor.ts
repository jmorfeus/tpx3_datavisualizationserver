/// <reference path="histogram-factory.ts" />
/// <reference path="timeline-factory.ts" />

import HistogramFactory = tpx3.histogramfactory.HistogramFactory;
import TimelineFactory = tpx3.timelinefactory.TimelineFactory;

namespace tpx3.sensor{
	/** Class representing (data from) one single sensor/detector - contains a collection of separate histogram for different modes/values saved by the sensor */
	export class Sensor extends BaseObject implements IRedrawable{

		/** List of attached histograms for each of the modes */
		protected _histograms: HistogramCanvas[];
		/** Statistics object with info about this sensor */
		protected _statistics: Statistics;
		/** Timeline object diplaying the data of this sensor */
		protected _timeline: Timeline;
		/** Name of the root file containing the data for this sensor */
		protected _dataSource: string;
		/** Represents collapsed state of the sensor */
		public static STATE_COLLAPSED: number = 0;
		/** Represents expanded state of the sensor */
		public static STATE_EXPANDED: number = 1;
		/** Represents fullscreen state of the sensor */
		public static STATE_FULLSCREEN: number = 2;
		/** Matrix of masked pixels, which will be ignored in the visualization in each of the histograms */
		public _maskedMatrix;
		protected _currentState = 0;

		protected _globalControls: GlobalControls;

		/** Creates new sensor with its 3 Histograms (for 3 modes), its Timeline and its own Statistics */
		constructor(name: string, container: string, source: string, dataAccess: DataAccess, globalControls: GlobalControls, strategy: number = Sensor.STATE_EXPANDED){
			super(name);
			this._currentState = strategy;
			this._globalControls = globalControls;
			this._dataSource = source;
			let hFactory = new HistogramFactory("HistogramFactory");
			let tFactory = new TimelineFactory("TimelineFactory");
			this.initMaskedMatrix();
			this._histograms = [];
			if (strategy == Sensor.STATE_EXPANDED){
				let sContainer = this.createExpandedContainer(name, container);
				this._histograms[0] = hFactory.addToAHistogram(this, this._name + "-toa", sContainer);
				this._histograms[1] = hFactory.addToTHistogram(this, this._name + "-tot", sContainer);
				this._histograms[2] = hFactory.addHitsHistogram(this, this._name + "-hits", sContainer);
			} else {
				// collapsed state of the sensor histograms
				this.createTabFrame(name, container);
				this._histograms[0] = hFactory.addCollapsedHitsHistogram(this, this._name + "-hits", this._name);
				this._histograms[1] = hFactory.addCollapsedToTHistogram(this, this._name + "-tot", this._name);
				this._histograms[2] = hFactory.addCollapsedToAHistogram(this, this._name + "-toa", this._name);
				$('#' + name + "-tot-nav-tab").removeClass("active");
				$('#' + name + "-tot-tab").removeClass("active");
				$('#' + name + "-hits-nav-tab").removeClass("active");
				$('#' + name + "-hits-tab").removeClass("active");
			}
			this._statistics = new Statistics(name + "-stats", this);
			this._timeline = tFactory.addTimeline("timeline-container", this, this._name + "-timeline", dataAccess, globalControls, 0, 20000000);
			this.changeColormap(globalControls.getColorMap());
		}

		/** Initializes the matrix of masked pixels to 0 value (none of the pixels are masked) */
		initMaskedMatrix(){
			this._maskedMatrix = [];
			for (let i = 0; i < 256; i++){
				this._maskedMatrix[i] = [];
				for (let j = 0; j < 256; j++){
					this._maskedMatrix[i][j] = 0;
				}
			}
		}

		/** Getter for the source parameter (name of the file from which the data are taken) */
		getSource(){
			return this._dataSource;
		}

		/** Sets the file source for data to display */
		setSource(filename: string){
			this._dataSource = filename;
		}

		/** Creates a frame needed for putting all the histograms next to each other in expanded state, and adds it to the HTML page */
		createExpandedContainer(name: string, container: string){
			let jContainer = $('#' + container);
			let sContainer = name + "-container";
			jContainer.prepend(
				"<div class=\"histogram-container\" id=\"" + sContainer + "\">\
				<h4>" + name + "<a href=\"#\" class=\"sensor-small-link sensor-collapse\" sensorName=\"" + name + "\"><span class=\"glyphicon glyphicon-resize-small\"></span> collapse</a>\
                </h4>\
				</div>\
				");
			return sContainer;
		}

		getTimeline(): Timeline{
			return this._timeline;
		}

		/** Creates a frame needed for putting all the histograms underneath each other in the collapsed state, and adds it to the HTML page */
		createTabFrame(name: string, container: string){
			let jContainer = $('#' + container);
			let size: string = "col-md-4";
			let link: string = "fullscreen";
			let glyphicon: string = "fullscreen";
			let expandLink: string = "<a href=\"#\" class=\"sensor-small-link sensor-expand\" sensorName=\"" + name + "\"> <span class=\"glyphicon glyphicon-new-window\"></span> expand</a></h4>";
			if (this._currentState == Sensor.STATE_FULLSCREEN){
				size = "col-md-12";
				link = "collapse";
				glyphicon = "resize-small";
				expandLink = "";
			}
			jContainer.prepend(
			"<div class=\"" + size + " histogram-container\" id=\"" + name + "-container\">\
                <h4>" + name + expandLink +
                
                "<h4><a href=\"#\" class=\"sensor-small-link sensor-" + link + "\" sensorName=\"" + name + "\"><span class=\"glyphicon glyphicon-" + glyphicon + "\"></span> " + link + "</a>\
                </h4>\
                  <ul class=\"nav nav-tabs control-tab\" id=\"" + name + "-tab-nav\">\
                  </ul>\
                  <div class=\"tab-content\" id=\"" + name + "-tab-content\">\
                  </div>\
             </div>");
		}

		redraw(newDataSource: DataAccess = null){
			for (let i=0; i < this._histograms.length; i++){
				this._histograms[i].redraw(newDataSource);
			}
			this._statistics.redraw(newDataSource);
			this._timeline.redraw(newDataSource);
		}

		/** Redraws the histograms, without redrawing the Timeline */
		redrawHistograms(newDataSource: DataAccess = null){
			for (let i=0; i < this._histograms.length; i++){
				this._histograms[i].redraw(newDataSource);
			}
			this._statistics.redraw(newDataSource);
		}

		/** Changes colormap for each histogram */
		changeColormap(colormap: IColorMap){
			for (let i = 0; i < this._histograms.length; i++){
				this._histograms[i].changeColormap(colormap);
			}
		}

		drawNoData(){
			for (let i = 0; i < this._histograms.length; i++){
				this._histograms[i].drawNoData();
			}
		}

		updateSize(){
			for (let i = 0; i < this._histograms.length; i++){
				this._histograms[i].drawNoData();
			}
		}

		updateViewport(startPixelX: number, startPixelY: number, zoomSize: number){
			for (let i = 0; i < this._histograms.length; i++){
				this._histograms[i].updateViewport(startPixelX, startPixelY, zoomSize);
			}
		}

		changeScale(scale: Scale){
			for (let i = 0; i < this._histograms.length; i++){
				this._histograms[i].changeScale(scale);
			}
		}

		toggleMaskingOn(){
			for (let i = 0; i < this._histograms.length; i++){
				this._histograms[i].toggleMaskingOn();
			}
		}

		toggleMaskingOff(){
			for (let i = 0; i < this._histograms.length; i++){
				this._histograms[i].toggleMaskingOff();
			}
		}

		toggleShowMaskedPixels(){
			for (let i = 0; i < this._histograms.length; i++){
				this._histograms[i].toggleShowMaskedPixels();
			}
		}

		/** Returns whether relative values should be shown (TRUE) or absolute (FALSE) */
		showRelativeValues(): boolean{
			return this._globalControls.showRelativeValues();
		}

		/** Removes the HTML elements connected to this instance of sensor from the page and returns NULL. 
			Does NOT effectively guarantee the destruction of the object! */
		softDelete(){
			$("#" + this._name + "-container").remove();
			$("#" + this._name + "-stats").remove();
			$("#" + this._name + "-timeline-wrapper").remove();
			this._histograms = [];
			this._statistics = null;
			this._timeline = null;
			return null;
		}

	}
}