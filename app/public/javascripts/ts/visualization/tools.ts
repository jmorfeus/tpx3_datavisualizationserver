namespace tpx3.tools{


/** Class with some useful tools and utilities to be used across the program */
export class TPX3Tools{
	constructor(){
	}

  /** Trivial */
  public static nanoToSeconds = function(nanoValue: number) : number{
    return nanoValue / 1000000000;
  }

  /** Trivial */
  public static secondsToNano = function(secValue: number) : number {
    return secValue * 1000000000;
  }

  /** Trivial */
  public static secondsToMilli = function(secValue: number){
    return secValue * 1000;
  }

  /** Converts number to scientific exponential form (e) with set precision */
  public static toExponentialPrecision(value: number, precision: number){
  	if (value === 0.0){
  		return value.toString();
  	}
  	let correctNumber = value.toExponential().toString();
  	let numArray = correctNumber.split(".");
  	let intPart = numArray[0];
  	if (numArray.length <= 1) {
  		  return numArray[0];
  	  }
  	numArray = numArray[1].split("e");
  	let ePart = numArray[1];
  	let decPart = numArray[0].substr(0,precision);
  	correctNumber = intPart + "." + decPart + "e" + ePart; 
  	return correctNumber;
  }
  
}

}
