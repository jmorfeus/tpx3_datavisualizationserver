/// <reference path="../../typings/jquery/jquery.d.ts" />
/// <reference path="color.ts" />
import Color = ieap.colormaps.Color;

namespace tpx3.colormap {

	/**
	Interface defining the required functionality of a color map. 
	*/
	export interface IColorMap{
		/** Shifts all the color values for a flat amount to make them brighter */
		boost();
		/** Shifts all the color values for a specified amount */
		boost(value: number);
		/** Shifts all the color values to lower values to make them less bright */
		toneDown();
		/** Get the appropriate color for the given scaled value */
		getColor(value: number) : Color;
		/** Draw a preview of the colormap into specified canvas element */
		preview(name:string);
		/** Draw a preview of the colormap, with markers for proportional values, for labelling */
		preview(name: string, withScale: boolean);
	}
	/**
	Abstract class, representing the color map and defining the basic common functionality
	*/
	export abstract class ColorMap implements IColorMap{
		/** Amount of boosting */
		_boostValue: number;
		/** Step by which the boosting happens */
		_boostStep: number;
		
		/** Initializes the basic parameters of the class. For example boost step. */
		constructor() {
			this._boostStep = 10;
			this._boostValue = 0;
		}
		/** Each of the concrete color maps should implement its own method on which color to return based on input number. */
		protected abstract getRealColor(value: number) : Color;

		/** Shifts all values of colors by a flat amount. Makes everything appear brighter. */
		boost(value : number = null){
			if (value === null) value = this._boostValue + this._boostStep;
			this._boostValue = value;
		}
		/** Opposite of boost(). Makes everything less bringht. */
		toneDown(){
			this._boostValue -= this._boostStep;
		}

		/** Checks if the values is within 0-100 range, if not, trims accordingly */
		checkValue(value : number){
			value = +value;

			if (value !== 0){
				value += this._boostValue;
			}
			if (value > 100){
				return 100;
			}
			if (value < 0){
				return 0;
			}
			return value;
		}
		/** Checks the value and then calls the implementation of getRealColor() method */
		getColor(value: number) : Color {
			let trimValue : number = this.checkValue(value);
			return this.getRealColor(trimValue);
		}

		/** Draws a preview of the colormap on a specified canvas HTML5 element */
		preview(name: string, withScale: boolean = false){
			let canvas: HTMLCanvasElement = document.getElementById(name) as HTMLCanvasElement;
			// Acquire required variables.
			let ctx = canvas.getContext('2d');
			// Clear everything.
			ctx.clearRect(0, 0, canvas.width, canvas.height);

			// Fill the color map.
			ctx.save();
			for (let x = 0; x < canvas.width; ++x) {
				let color = this.getColor(x/canvas.width * 100);
				if (withScale && (x == 1 || x == (canvas.width/2) || x == (canvas.width/4) ||x == canvas.width - 2)){
					let subtrValue = 255;
					if (color.r() > 110 && color.r() < 140 && color.g() > 110 && color.g() < 140 && color.b() > 110 && color.b() < 140){
						subtrValue += 110;
					}
					color = new Color((subtrValue-color.r()), (subtrValue-color.g()), (subtrValue-color.b()));
				}
				ctx.fillStyle = color.css();
				ctx.fillRect(x, 0, 1, canvas.height);
			}
			ctx.restore();
		}
	}

	/**
	Simple greyscale colormap, 0: black, 100: white.
	*/
	export class GreyscaleMap extends ColorMap{
		constructor(){
			super();
		}
		protected getRealColor(value: number) : Color {
			let numValue: number;
			if (value <= 0){
				numValue = 0;
			} else {
				numValue = value * 2.55;
			}

			return new Color(numValue, numValue, numValue);
		}
	}

	/**
	"Jet" colormap from the MATLAB implementation.
	*/
	export class JetMap extends ColorMap {

		constructor() {
			super();
		}

		protected getRealColor(value: number) : Color {
			let zeroValue = value / 100;
			
			var interpolate = function (val, y0, x0, y1, x1) {
	            return (val-x0)*(y1-y0)/(x1-x0) + y0;
		    };

	        var base = function (val) {
		        if ( val <= -0.75 ) return 0;
		        else if ( val <= -0.25 ) return interpolate( val, 0.0, -0.75, 1.0, -0.25 );
		        else if ( val <= 0.25 ) return 1.0;
		        else if ( val <= 0.75 ) return interpolate( val, 1.0, 0.25, 0.0, 0.75 );
		        else return 0.0;
	        };

	        var red = function (gray) {
	            return base( gray - 0.5 );
	        };

	        var green = function (gray) {
	            return base( gray );
	        };

	        var blue = function (gray) {
	            return base( gray + 0.5 );
	        };

	        var translated = zeroValue * 2 - 1;
	        var r = red(translated) * 255;
	        var g = green(translated) * 255;
	        var b = blue(translated) * 255;

			return new Color(r, g, b);
		}

	}

	/**
	"Hot" colormap from the MATLAB implementation.
	*/
	export class HotMap extends ColorMap {
		constructor(){
			super();
		}
		protected getRealColor(value: number) : Color {
			value = value / 100;
	        var ratio = 3/8;
			      var red = function (val) {
		           if (val > ratio) return 1;
	            return val / ratio;
	        };

	        var green = function (val) {
	            if (val <= ratio) return 0;
	            if (val > 2 * ratio) return 1;
	            return (val - ratio) / ratio;
	        };

	        var blue = function (val) {
	            if (val <= 2 * ratio) return 0;
	            return (val - 2 * ratio) / (1 - 2 * ratio);
	        };

	        var r = red(value) * 255;
	        var g = green(value) * 255;
	        var b = blue(value) * 255;
			return new Color(r, g, b);
		}
	}

	/**
	"Cubehelix" colormap
	# Sat Jun 18 2016 06:03:04 GMT+0200 (CEST)
	# ---------------------------------------------
	# R/G/B cubehelix colour scheme 
	#
	# see http://www.mrao.cam.ac.uk/~dag/CUBEHELIX/
	#----------------------------------------------
	# see Green (2011), BASI, 39, 289. 
	#
	# start............: 0.5
	# rotations........: -1.5
	# hue..............: 1.0
	# gamma............: 1.0
	# number of levels.: 101
	#----------------------------------------------
	# Dave Green: dag @ mrao.cam.ac.uk 
	#----------------------------------------------
	# faction and R/G/B values
	#
	*/
	export class CubehelixMap extends ColorMap {
		
		constructor() {
			super();
		}

		protected getRealColor(value: number) : Color {
			value = value / 100;
			let strValue = value.toFixed(2);

			let lookup={
			"0.00": "#000000",
			"0.01": "#040104",
			"0.02": "#080308",
			"0.03": "#0c050d",
			"0.04": "#0f0612",
			"0.05": "#120817",
			"0.06": "#140a1c",
			"0.07": "#160d21",
			"0.08": "#180f26",
			"0.09": "#19122b",
			"0.10": "#1a1530",
			"0.11": "#1a1835",
			"0.12": "#1b1c39",
			"0.13": "#1a1f3d",
			"0.14": "#1a2341",
			"0.15": "#1a2744",
			"0.16": "#192b47",
			"0.17": "#182f4a",
			"0.18": "#17344b",
			"0.19": "#17384d",
			"0.20": "#163d4e",
			"0.21": "#15414e",
			"0.22": "#15464e",
			"0.23": "#154a4e",
			"0.24": "#154f4d",
			"0.25": "#16534c",
			"0.26": "#17574a",
			"0.27": "#185b48",
			"0.28": "#1a5f46",
			"0.29": "#1c6244",
			"0.30": "#1f6642",
			"0.31": "#22693f",
			"0.32": "#266c3c",
			"0.33": "#2a6f3a",
			"0.34": "#2f7137",
			"0.35": "#347335",
			"0.36": "#397533",
			"0.37": "#407632",
			"0.38": "#467830",
			"0.39": "#4d792f",
			"0.40": "#54792f",
			"0.41": "#5b7a2f",
			"0.42": "#637a2f",
			"0.43": "#6b7b31",
			"0.44": "#737b32",
			"0.45": "#7a7a35",
			"0.46": "#827a37",
			"0.47": "#8a7a3b",
			"0.48": "#927a3f",
			"0.49": "#997944",
			"0.50": "#a07949",
			"0.51": "#a7794f",
			"0.52": "#ad7955",
			"0.53": "#b3795c",
			"0.54": "#b97963",
			"0.55": "#be796a",
			"0.56": "#c37a72",
			"0.57": "#c77b7a",
			"0.58": "#ca7c82",
			"0.59": "#cd7d8a",
			"0.60": "#d07e93",
			"0.61": "#d2809b",
			"0.62": "#d382a3",
			"0.63": "#d485ab",
			"0.64": "#d487b3",
			"0.65": "#d48aba",
			"0.66": "#d48dc1",
			"0.67": "#d391c8",
			"0.68": "#d294ce",
			"0.69": "#d198d4",
			"0.70": "#cf9cda",
			"0.71": "#cea1df",
			"0.72": "#cca5e3",
			"0.73": "#caaae7",
			"0.74": "#c9aeea",
			"0.75": "#c7b3ed",
			"0.76": "#c5b8ef",
			"0.77": "#c4bcf1",
			"0.78": "#c3c1f2",
			"0.79": "#c2c6f3",
			"0.80": "#c1caf3",
			"0.81": "#c1cef3",
			"0.82": "#c2d3f3",
			"0.83": "#c2d7f3",
			"0.84": "#c3dbf2",
			"0.85": "#c5def2",
			"0.86": "#c7e2f1",
			"0.87": "#c9e5f0",
			"0.88": "#cce8f0",
			"0.89": "#cfebef",
			"0.90": "#d2eeef",
			"0.91": "#d6f0ef",
			"0.92": "#daf2ef",
			"0.93": "#def4ef",
			"0.94": "#e3f6f0",
			"0.95": "#e8f8f2",
			"0.96": "#ecf9f3",
			"0.97": "#f1fbf6",
			"0.98": "#f6fcf8",
			"0.99": "#fafefb",
			"1.00": "ffffff"
			}
			
			return Color.fromHex(lookup[strValue]);
		}

	}


	/**
	Decorator over the color map class. Keeps the functionality of decorated color map, only changes color for the 0 value always to white. 
	Implements the IColorMap interface, therefore can be used in exact same way as any color map.
	*/
	export class WhiteBackgroundDecorator implements IColorMap{
		private _colorMap: IColorMap;
		constructor(colormap: IColorMap) {
			this._colorMap = colormap;
		}

		getColor(value: number) : Color{
			if (value == 0){
				return new Color(255, 255, 255);
			} else {
				return this._colorMap.getColor(value);
			}
		}

		preview(name: string){
			this._colorMap.preview(name);
		}

		boost(value: number = null){
			this._colorMap.boost(value);
		}

		toneDown(){
			this._colorMap.toneDown();
		}

	}


}


/*
Parts of this code are attributed to Bc. Petr Manek according to MIT license:

The MIT License (MIT)
Copyright (c) 2016 Petr Mánek
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/