namespace ieap.colormaps {
    /**
     * Color which can be displayed on the screen.


        This piece of code is attributed to Bc. Petr Manek according to MIT license:

        The MIT License (MIT)

        Copyright (c) 2016 Petr Mánek
        
        Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
        The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
        
     */

    export class Color {
        private _red: number;
        private _green: number;
        private _blue: number;
        private _alpha: number;

        public constructor(red: number, green: number, blue: number, alpha: number = 255) {
            this._red = red;
            this._green = green;
            this._blue = blue;
            this._alpha = alpha;
        }

        public r = () => this._red;
        public g = () => this._green;
        public b = () => this._blue;
        public a = () => this._alpha;

        public css = () => {
            var r = Math.round(this._red).toString();
            var g = Math.round(this._green).toString();
            var b = Math.round(this._blue).toString();
            var a = (this._alpha / 255).toFixed(3);
            return 'rgba(' + r + ', ' + g + ', ' + b + ', ' + a + ')';
        };

        public setImageData = (imgData: ImageData, position: number) => {
            // The data are stored sequentially in the order: R, G, B, A.
            imgData.data[position + 0] = Math.round(this._red);
            imgData.data[position + 1] = Math.round(this._green);
            imgData.data[position + 2] = Math.round(this._blue);
            imgData.data[position + 3] = Math.round(this._alpha);
        };

        public static fromHex = (hex: string, alpha: number = 255) => {
            // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
            var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
            hex = hex.replace(shorthandRegex, function(m, r, g, b) {
                return r + r + g + g + b + b;
            });

            var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
            return result ? new Color(parseInt(result[1], 16), parseInt(result[2], 16), parseInt(result[3], 16), alpha) : null;
        };

        public static fromRgba = (red: number, green: number, blue: number, alpha: number = 255) => {
            return new Color(red, green, blue, alpha);
        };

        public static fromWhite = (white: number, alpha: number = 255) => {
            return new Color(white, white, white, alpha);
        };

        // Some common colors.
        public static red = Color.fromHex('#f00');
        public static green = Color.fromHex('#0f0');
        public static blue = Color.fromHex('#00f');
        public static white = Color.fromHex('#fff');
        public static black = Color.fromHex('#000');
        public static clear = Color.fromRgba(0, 0, 0, 0);
    }

}