/// <reference path="../../typings/jquery/jquery.d.ts" />
$(document).on('ready', function(){
	let ok: boolean = false;

	$(".non-empty").on('change focus', function(){
		if ($(this).val() == ''){
			ok = false;
			$(this).parent().addClass('has-error');
			$(this).parent().removeClass('has-success');
		} else {
			ok = true;
			$(this).parent().addClass('has-success');
			$(this).parent().removeClass('has-error');
		}
	});

	$(".submit-validate").on('click', function(e){
		if (!ok){
			e.preventDefault();
			$(".non-empty").change();
			alert("Please choose a file to upload.");
		}
	});

	
});