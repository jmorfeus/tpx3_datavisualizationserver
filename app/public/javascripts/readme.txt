
This is the Frontend part of the visualization application for data from the Timepix3 detectors. It provides an interactive HTML5/JavaScript application with tools for examining the data. It consists of 1D and 2D visualization elements, filters, statistics, etc.

The visualization can work with different sources of data. The data come from the backend part of the application, an API RESTful server providing the data. Server can be run locally, or remotely.

In this documentation of the TypeScript source, you can find description of all the used classes and their APIs. Majority of the documented classes providing the functionality are placed in the <a href="./modules/tpx3.html">tpx3</a> module.
<h2>Manual</h2>

<h3>Run</h3>
All the files are located in the <code>/app</code> directory. All necessary files for distribution of the project are <code>index.html</code>, <code>config.js</code>, <code>javascripts/jquery-1.12.2.min.js</code>, <code>javascripts/tsc.js</code>, <code>stylesheets/css/screen.css</code> and directories <code>bootstrap</code>, <code>jquery-ui-1.11.4</code> and <code>images</code>.

<ul>
<li>Specify the server address in the config file <code>config.js</code>:
<ul>
<li>Default server address is <code>localhost</code> - the server is running on local machine</li>
<li>specify any other IP address of remote TPX3 Data Server,</li>
<li>put <code>test</code> for accessing local data stored on disk, for testing purposes. Note that all functionality using querying the server for new data, will not be functional!</li>
</ul>
</li>
<li>Open the <code>index.html</code> file.</li>


</ul>

<h3>Build</h3>
All the files are located in the <code>/app</code> directory
<ul>
<li>Install prerequisites: <a href="http://sass-lang.com/">SASS</a>, <a href="http://compass-style.org/">Compass</a> and <a href="http://www.typescriptlang.org/">TypeScript</a></li>
<li>Compile and watch the TypeScript sources by running the <code>tsc -w</code> command within the <code>javascripts/ts</code> directory</li>
<li>Compile and watch the SASS sources by running the <code>compass watch</code> command within the <code>stylesheets/sass</code> directory</li>
<li>If needed, edit the server address in the config file <code>config.js</code></li>
<li>Open the <code>index.html</code> file.</li>
</ul>

<h2>Structure</h2>
<ul>
<li><code>index.html</code> - main page of the application</li>
<li><code>config.js</code> - configuration file specifying the source of data</li>
<li><code>javascripts</code> - folder with compiled JavaScripts
	<ul>
	<li><code>ts</code> - folder with TypeScript sources</li>
	</ul>
</li>
<li><code>stylesheets</code>
	<ul>
	<li><code>sass</code> - folder with SASS sources</li>
	<li><code>css</code> - folder with compiled CSS stylesheets</li>
	</ul>
</li>
<li><code>bootstrap</code> - folder with the used <a href="http://getbootstrap.com">Twitter Bootstrap </a>framework</li>
<li><code>jquery-ui-1.11.4</code> - folder with the used <a href="http://jqueryui.com">jQueryUI</a> libraries </li>
<li><code>images</code> - folder with images </li>
</ul>

<h2>Copyright and license</h2>

The MIT License (MIT)

Copyright (c) 2016 Jiri Vycpalek

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
