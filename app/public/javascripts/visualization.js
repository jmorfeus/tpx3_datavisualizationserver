var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/// <reference path="../../typings/jqueryui/jqueryui.d.ts" />
var tpx3;
(function (tpx3) {
    var baseobject;
    (function (baseobject) {
        /**
        Class representing any object of the visualization application that can be named and has any functionality, which can produce errors.
        */
        var BaseObject = (function () {
            /** Sets up the HTML elements for displaying errors and warnings */
            function BaseObject(name) {
                if (name === void 0) { name = ""; }
                if (name === "") {
                    console.log("Name not set for object: ");
                    console.log(this);
                }
                else {
                    this._name = name;
                }
                this._errorSpan = $("#errors-inner");
                this._errorCountSpan = $("#error-count");
                this._warningCountSpan = $("#warning-count");
            }
            /** Returns the actuall current day time in HH:MM:SS format, e.g. 9:13:09*/
            BaseObject.prototype.getCurrentTime = function () {
                var date = new Date();
                return date.getHours().toString() + ":" + (date.getMinutes() < 10 ? "0" : "") + date.getMinutes().toString() + ":" + (date.getSeconds() < 10 ? "0" : "") + date.getSeconds().toString();
            };
            /** Prints error into the pre-specified error span element */
            BaseObject.prototype.addError = function (message) {
                var errorCount = this._errorCountSpan.html();
                errorCount++;
                this._errorCountSpan.html(errorCount.toString());
                this._errorSpan.prepend("<p><span class=\"label label-danger\">Error</span> " + message + "</p>");
            };
            /** Prints a warning into the pre-specified error span element */
            BaseObject.prototype.addWarning = function (message) {
                var warningCount = this._warningCountSpan.html();
                warningCount++;
                this._warningCountSpan.html(warningCount.toString());
                this._errorSpan.prepend("<p><span class=\"label label-warning\">Warning</span> " + message + "</p>");
            };
            return BaseObject;
        }());
        baseobject.BaseObject = BaseObject;
    })(baseobject = tpx3.baseobject || (tpx3.baseobject = {}));
})(tpx3 || (tpx3 = {}));
/// <reference path="../../typings/jquery/jquery.d.ts" />
var tpx3;
(function (tpx3) {
    var scales;
    (function (scales) {
        /**
        Abstract class representing any scale. Children should implement the required functionality.
        */
        var Scale = (function () {
            function Scale() {
            }
            return Scale;
        }());
        scales.Scale = Scale;
        /**
        Standard linear scale with equal difference in return value for equal differences in input values.
        */
        var LinearScale = (function (_super) {
            __extends(LinearScale, _super);
            function LinearScale() {
                _super.call(this);
            }
            LinearScale.prototype.getProportionValue = function (value, min, max) {
                return (min + max) * value;
            };
            LinearScale.prototype.getScaledValue = function (value, min, max) {
                if (max - min == 0) {
                    return "0";
                }
                return ((value - min) / (max - min) * 100).toFixed(0);
            };
            return LinearScale;
        }(Scale));
        scales.LinearScale = LinearScale;
        /**
        Logarithmic scale, with changes in return value according to changes in orders of magnitude of input values.
        */
        var LogarithmicScale = (function (_super) {
            __extends(LogarithmicScale, _super);
            function LogarithmicScale() {
                _super.call(this);
            }
            LogarithmicScale.prototype.getProportionValue = function (value, min, max) {
                if (min == 0) {
                    min = 1;
                }
                var logMid = (Math.log(max) + Math.log(min)) * value;
                return Math.pow(Math.E, logMid);
            };
            LogarithmicScale.prototype.getScaledValue = function (value, min, max) {
                if (max - min == 0) {
                    return "0";
                }
                var logMax = Math.log(max);
                var logMin = Math.log(min);
                if (min == 0) {
                    logMin = -1;
                    if (value == 0) {
                        return value.toFixed(0);
                    }
                }
                var logVal = Math.log(value);
                return ((logVal - logMin) / (logMax - logMin) * 100).toFixed(0);
            };
            return LogarithmicScale;
        }(Scale));
        scales.LogarithmicScale = LogarithmicScale;
    })(scales = tpx3.scales || (tpx3.scales = {}));
})(tpx3 || (tpx3 = {}));
var ieap;
(function (ieap) {
    var colormaps;
    (function (colormaps) {
        /**
         * Color which can be displayed on the screen.
    
    
            This piece of code is attributed to Bc. Petr Manek according to MIT license:
    
            The MIT License (MIT)
    
            Copyright (c) 2016 Petr Mánek
            
            Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
            The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
            THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
            
         */
        var Color = (function () {
            function Color(red, green, blue, alpha) {
                var _this = this;
                if (alpha === void 0) { alpha = 255; }
                this.r = function () { return _this._red; };
                this.g = function () { return _this._green; };
                this.b = function () { return _this._blue; };
                this.a = function () { return _this._alpha; };
                this.css = function () {
                    var r = Math.round(_this._red).toString();
                    var g = Math.round(_this._green).toString();
                    var b = Math.round(_this._blue).toString();
                    var a = (_this._alpha / 255).toFixed(3);
                    return 'rgba(' + r + ', ' + g + ', ' + b + ', ' + a + ')';
                };
                this.setImageData = function (imgData, position) {
                    // The data are stored sequentially in the order: R, G, B, A.
                    imgData.data[position + 0] = Math.round(_this._red);
                    imgData.data[position + 1] = Math.round(_this._green);
                    imgData.data[position + 2] = Math.round(_this._blue);
                    imgData.data[position + 3] = Math.round(_this._alpha);
                };
                this._red = red;
                this._green = green;
                this._blue = blue;
                this._alpha = alpha;
            }
            Color.fromHex = function (hex, alpha) {
                if (alpha === void 0) { alpha = 255; }
                // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
                var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
                hex = hex.replace(shorthandRegex, function (m, r, g, b) {
                    return r + r + g + g + b + b;
                });
                var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
                return result ? new Color(parseInt(result[1], 16), parseInt(result[2], 16), parseInt(result[3], 16), alpha) : null;
            };
            Color.fromRgba = function (red, green, blue, alpha) {
                if (alpha === void 0) { alpha = 255; }
                return new Color(red, green, blue, alpha);
            };
            Color.fromWhite = function (white, alpha) {
                if (alpha === void 0) { alpha = 255; }
                return new Color(white, white, white, alpha);
            };
            // Some common colors.
            Color.red = Color.fromHex('#f00');
            Color.green = Color.fromHex('#0f0');
            Color.blue = Color.fromHex('#00f');
            Color.white = Color.fromHex('#fff');
            Color.black = Color.fromHex('#000');
            Color.clear = Color.fromRgba(0, 0, 0, 0);
            return Color;
        }());
        colormaps.Color = Color;
    })(colormaps = ieap.colormaps || (ieap.colormaps = {}));
})(ieap || (ieap = {}));
/// <reference path="../../typings/jquery/jquery.d.ts" />
/// <reference path="color.ts" />
var Color = ieap.colormaps.Color;
var tpx3;
(function (tpx3) {
    var colormap;
    (function (colormap_1) {
        /**
        Abstract class, representing the color map and defining the basic common functionality
        */
        var ColorMap = (function () {
            /** Initializes the basic parameters of the class. For example boost step. */
            function ColorMap() {
                this._boostStep = 10;
                this._boostValue = 0;
            }
            /** Shifts all values of colors by a flat amount. Makes everything appear brighter. */
            ColorMap.prototype.boost = function (value) {
                if (value === void 0) { value = null; }
                if (value === null)
                    value = this._boostValue + this._boostStep;
                this._boostValue = value;
            };
            /** Opposite of boost(). Makes everything less bringht. */
            ColorMap.prototype.toneDown = function () {
                this._boostValue -= this._boostStep;
            };
            /** Checks if the values is within 0-100 range, if not, trims accordingly */
            ColorMap.prototype.checkValue = function (value) {
                value = +value;
                if (value !== 0) {
                    value += this._boostValue;
                }
                if (value > 100) {
                    return 100;
                }
                if (value < 0) {
                    return 0;
                }
                return value;
            };
            /** Checks the value and then calls the implementation of getRealColor() method */
            ColorMap.prototype.getColor = function (value) {
                var trimValue = this.checkValue(value);
                return this.getRealColor(trimValue);
            };
            /** Draws a preview of the colormap on a specified canvas HTML5 element */
            ColorMap.prototype.preview = function (name, withScale) {
                if (withScale === void 0) { withScale = false; }
                var canvas = document.getElementById(name);
                // Acquire required variables.
                var ctx = canvas.getContext('2d');
                // Clear everything.
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                // Fill the color map.
                ctx.save();
                for (var x = 0; x < canvas.width; ++x) {
                    var color = this.getColor(x / canvas.width * 100);
                    if (withScale && (x == 1 || x == (canvas.width / 2) || x == (canvas.width / 4) || x == canvas.width - 2)) {
                        var subtrValue = 255;
                        if (color.r() > 110 && color.r() < 140 && color.g() > 110 && color.g() < 140 && color.b() > 110 && color.b() < 140) {
                            subtrValue += 110;
                        }
                        color = new Color((subtrValue - color.r()), (subtrValue - color.g()), (subtrValue - color.b()));
                    }
                    ctx.fillStyle = color.css();
                    ctx.fillRect(x, 0, 1, canvas.height);
                }
                ctx.restore();
            };
            return ColorMap;
        }());
        colormap_1.ColorMap = ColorMap;
        /**
        Simple greyscale colormap, 0: black, 100: white.
        */
        var GreyscaleMap = (function (_super) {
            __extends(GreyscaleMap, _super);
            function GreyscaleMap() {
                _super.call(this);
            }
            GreyscaleMap.prototype.getRealColor = function (value) {
                var numValue;
                if (value <= 0) {
                    numValue = 0;
                }
                else {
                    numValue = value * 2.55;
                }
                return new Color(numValue, numValue, numValue);
            };
            return GreyscaleMap;
        }(ColorMap));
        colormap_1.GreyscaleMap = GreyscaleMap;
        /**
        "Jet" colormap from the MATLAB implementation.
        */
        var JetMap = (function (_super) {
            __extends(JetMap, _super);
            function JetMap() {
                _super.call(this);
            }
            JetMap.prototype.getRealColor = function (value) {
                var zeroValue = value / 100;
                var interpolate = function (val, y0, x0, y1, x1) {
                    return (val - x0) * (y1 - y0) / (x1 - x0) + y0;
                };
                var base = function (val) {
                    if (val <= -0.75)
                        return 0;
                    else if (val <= -0.25)
                        return interpolate(val, 0.0, -0.75, 1.0, -0.25);
                    else if (val <= 0.25)
                        return 1.0;
                    else if (val <= 0.75)
                        return interpolate(val, 1.0, 0.25, 0.0, 0.75);
                    else
                        return 0.0;
                };
                var red = function (gray) {
                    return base(gray - 0.5);
                };
                var green = function (gray) {
                    return base(gray);
                };
                var blue = function (gray) {
                    return base(gray + 0.5);
                };
                var translated = zeroValue * 2 - 1;
                var r = red(translated) * 255;
                var g = green(translated) * 255;
                var b = blue(translated) * 255;
                return new Color(r, g, b);
            };
            return JetMap;
        }(ColorMap));
        colormap_1.JetMap = JetMap;
        /**
        "Hot" colormap from the MATLAB implementation.
        */
        var HotMap = (function (_super) {
            __extends(HotMap, _super);
            function HotMap() {
                _super.call(this);
            }
            HotMap.prototype.getRealColor = function (value) {
                value = value / 100;
                var ratio = 3 / 8;
                var red = function (val) {
                    if (val > ratio)
                        return 1;
                    return val / ratio;
                };
                var green = function (val) {
                    if (val <= ratio)
                        return 0;
                    if (val > 2 * ratio)
                        return 1;
                    return (val - ratio) / ratio;
                };
                var blue = function (val) {
                    if (val <= 2 * ratio)
                        return 0;
                    return (val - 2 * ratio) / (1 - 2 * ratio);
                };
                var r = red(value) * 255;
                var g = green(value) * 255;
                var b = blue(value) * 255;
                return new Color(r, g, b);
            };
            return HotMap;
        }(ColorMap));
        colormap_1.HotMap = HotMap;
        /**
        "Cubehelix" colormap
        # Sat Jun 18 2016 06:03:04 GMT+0200 (CEST)
        # ---------------------------------------------
        # R/G/B cubehelix colour scheme
        #
        # see http://www.mrao.cam.ac.uk/~dag/CUBEHELIX/
        #----------------------------------------------
        # see Green (2011), BASI, 39, 289.
        #
        # start............: 0.5
        # rotations........: -1.5
        # hue..............: 1.0
        # gamma............: 1.0
        # number of levels.: 101
        #----------------------------------------------
        # Dave Green: dag @ mrao.cam.ac.uk
        #----------------------------------------------
        # faction and R/G/B values
        #
        */
        var CubehelixMap = (function (_super) {
            __extends(CubehelixMap, _super);
            function CubehelixMap() {
                _super.call(this);
            }
            CubehelixMap.prototype.getRealColor = function (value) {
                value = value / 100;
                var strValue = value.toFixed(2);
                var lookup = {
                    "0.00": "#000000",
                    "0.01": "#040104",
                    "0.02": "#080308",
                    "0.03": "#0c050d",
                    "0.04": "#0f0612",
                    "0.05": "#120817",
                    "0.06": "#140a1c",
                    "0.07": "#160d21",
                    "0.08": "#180f26",
                    "0.09": "#19122b",
                    "0.10": "#1a1530",
                    "0.11": "#1a1835",
                    "0.12": "#1b1c39",
                    "0.13": "#1a1f3d",
                    "0.14": "#1a2341",
                    "0.15": "#1a2744",
                    "0.16": "#192b47",
                    "0.17": "#182f4a",
                    "0.18": "#17344b",
                    "0.19": "#17384d",
                    "0.20": "#163d4e",
                    "0.21": "#15414e",
                    "0.22": "#15464e",
                    "0.23": "#154a4e",
                    "0.24": "#154f4d",
                    "0.25": "#16534c",
                    "0.26": "#17574a",
                    "0.27": "#185b48",
                    "0.28": "#1a5f46",
                    "0.29": "#1c6244",
                    "0.30": "#1f6642",
                    "0.31": "#22693f",
                    "0.32": "#266c3c",
                    "0.33": "#2a6f3a",
                    "0.34": "#2f7137",
                    "0.35": "#347335",
                    "0.36": "#397533",
                    "0.37": "#407632",
                    "0.38": "#467830",
                    "0.39": "#4d792f",
                    "0.40": "#54792f",
                    "0.41": "#5b7a2f",
                    "0.42": "#637a2f",
                    "0.43": "#6b7b31",
                    "0.44": "#737b32",
                    "0.45": "#7a7a35",
                    "0.46": "#827a37",
                    "0.47": "#8a7a3b",
                    "0.48": "#927a3f",
                    "0.49": "#997944",
                    "0.50": "#a07949",
                    "0.51": "#a7794f",
                    "0.52": "#ad7955",
                    "0.53": "#b3795c",
                    "0.54": "#b97963",
                    "0.55": "#be796a",
                    "0.56": "#c37a72",
                    "0.57": "#c77b7a",
                    "0.58": "#ca7c82",
                    "0.59": "#cd7d8a",
                    "0.60": "#d07e93",
                    "0.61": "#d2809b",
                    "0.62": "#d382a3",
                    "0.63": "#d485ab",
                    "0.64": "#d487b3",
                    "0.65": "#d48aba",
                    "0.66": "#d48dc1",
                    "0.67": "#d391c8",
                    "0.68": "#d294ce",
                    "0.69": "#d198d4",
                    "0.70": "#cf9cda",
                    "0.71": "#cea1df",
                    "0.72": "#cca5e3",
                    "0.73": "#caaae7",
                    "0.74": "#c9aeea",
                    "0.75": "#c7b3ed",
                    "0.76": "#c5b8ef",
                    "0.77": "#c4bcf1",
                    "0.78": "#c3c1f2",
                    "0.79": "#c2c6f3",
                    "0.80": "#c1caf3",
                    "0.81": "#c1cef3",
                    "0.82": "#c2d3f3",
                    "0.83": "#c2d7f3",
                    "0.84": "#c3dbf2",
                    "0.85": "#c5def2",
                    "0.86": "#c7e2f1",
                    "0.87": "#c9e5f0",
                    "0.88": "#cce8f0",
                    "0.89": "#cfebef",
                    "0.90": "#d2eeef",
                    "0.91": "#d6f0ef",
                    "0.92": "#daf2ef",
                    "0.93": "#def4ef",
                    "0.94": "#e3f6f0",
                    "0.95": "#e8f8f2",
                    "0.96": "#ecf9f3",
                    "0.97": "#f1fbf6",
                    "0.98": "#f6fcf8",
                    "0.99": "#fafefb",
                    "1.00": "ffffff"
                };
                return Color.fromHex(lookup[strValue]);
            };
            return CubehelixMap;
        }(ColorMap));
        colormap_1.CubehelixMap = CubehelixMap;
        /**
        Decorator over the color map class. Keeps the functionality of decorated color map, only changes color for the 0 value always to white.
        Implements the IColorMap interface, therefore can be used in exact same way as any color map.
        */
        var WhiteBackgroundDecorator = (function () {
            function WhiteBackgroundDecorator(colormap) {
                this._colorMap = colormap;
            }
            WhiteBackgroundDecorator.prototype.getColor = function (value) {
                if (value == 0) {
                    return new Color(255, 255, 255);
                }
                else {
                    return this._colorMap.getColor(value);
                }
            };
            WhiteBackgroundDecorator.prototype.preview = function (name) {
                this._colorMap.preview(name);
            };
            WhiteBackgroundDecorator.prototype.boost = function (value) {
                if (value === void 0) { value = null; }
                this._colorMap.boost(value);
            };
            WhiteBackgroundDecorator.prototype.toneDown = function () {
                this._colorMap.toneDown();
            };
            return WhiteBackgroundDecorator;
        }());
        colormap_1.WhiteBackgroundDecorator = WhiteBackgroundDecorator;
    })(colormap = tpx3.colormap || (tpx3.colormap = {}));
})(tpx3 || (tpx3 = {}));
/*
Parts of this code are attributed to Bc. Petr Manek according to MIT license:

The MIT License (MIT)
Copyright (c) 2016 Petr Mánek
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/ 
/// <reference path="baseobject.ts" />
/// <reference path="color-map.ts" />
var BaseObject = tpx3.baseobject.BaseObject;
var ColorMap = tpx3.colormap.ColorMap;
var tpx3;
(function (tpx3) {
    var redrawer;
    (function (redrawer) {
        /**
        Class responsible for keeping a list of redrawable observers and notifying all of them to redraw all the data at once.
        */
        var Redrawer = (function (_super) {
            __extends(Redrawer, _super);
            /** Initializes empty list of observers/subscribers */
            function Redrawer(name) {
                if (name === void 0) { name = ""; }
                _super.call(this, name);
                this._observerList = [];
            }
            /** Subscribe/add new observer to the list of observers/subscribers */
            Redrawer.prototype.subscribeObserver = function (newObserver, observerName) {
                if (observerName === void 0) { observerName = ""; }
                if (observerName != "") {
                    this._observerList[observerName] = newObserver;
                }
                else {
                    this._observerList.push(newObserver);
                }
            };
            /** Redraw all the subscribed observers/subscribers, calling their redraw() method */
            Redrawer.prototype.redrawAll = function (newData) {
                if (newData === void 0) { newData = null; }
                for (var i in this._observerList) {
                    if (this._observerList[i] != null) {
                        this._observerList[i].redraw(newData);
                    }
                }
            };
            /** Checks if an observer with a given name is subscribed to this redrawer */
            Redrawer.prototype.has = function (name) {
                if (this._observerList[name] !== undefined && this._observerList[name] !== null) {
                    return true;
                }
                else {
                    return false;
                }
            };
            /** Returns an observer with given name, if such observer exists, false otherwise */
            Redrawer.prototype.get = function (name) {
                if (this.has(name)) {
                    return this._observerList[name];
                }
                else {
                    return false;
                }
            };
            /** Removes observer with specified name from the observers list */
            Redrawer.prototype.unsubscribeObserver = function (observerName) {
                if (this.has(observerName)) {
                    delete this._observerList[observerName];
                }
            };
            return Redrawer;
        }(BaseObject));
        redrawer.Redrawer = Redrawer;
        /** Class responsible for keeping a list and for redrawing Timeline objects */
        var TimelineRedrawer = (function (_super) {
            __extends(TimelineRedrawer, _super);
            function TimelineRedrawer(name) {
                _super.call(this, name);
            }
            /** Redraws filtered data of all subscribed timelines */
            TimelineRedrawer.prototype.redrawAllFiltered = function (newDataSource) {
                if (newDataSource === void 0) { newDataSource = null; }
                for (var i in this._observerList) {
                    this._observerList[i].redrawFiltered(newDataSource);
                }
            };
            return TimelineRedrawer;
        }(Redrawer));
        redrawer.TimelineRedrawer = TimelineRedrawer;
    })(redrawer = tpx3.redrawer || (tpx3.redrawer = {}));
})(tpx3 || (tpx3 = {}));
/// <reference path="scales.ts" />
/// <reference path="redrawer.ts" />
var Scale = tpx3.scales.Scale;
var LinearScale = tpx3.scales.LinearScale;
var LogarithmicScale = tpx3.scales.LogarithmicScale;
var tpx3;
(function (tpx3) {
    var canvasvisualization;
    (function (canvasvisualization) {
        /**
        Abstract class representing a two-layered visualization feature on the screen, based on HTML5 canvas element.
        */
        var CanvasVisualization = (function (_super) {
            __extends(CanvasVisualization, _super);
            /** Initializes the object with its name, initializes the canvas, creates the second canvas visualization layer */
            function CanvasVisualization(name) {
                if (name === void 0) { name = ""; }
                _super.call(this, name);
                this._canvas = document.getElementById(name);
                this._jCanvas = $("#" + this._name);
                this._context = this._canvas.getContext("2d");
                this._canvasHeight = this._jCanvas.height();
                this._canvasWidth = this._jCanvas.width();
                this.createTempCanvas();
            }
            /** Updates the size of the visualization according to the current size of the HTML element (handy if it changes with screen size) */
            CanvasVisualization.prototype.updateSize = function () {
                $("#" + this._name).attr("height", this._canvasHeight);
                $("#" + this._name).attr("width", this._canvasWidth);
            };
            /** When there are no data to be drawn, displays the information on the visualization canvas. */
            CanvasVisualization.prototype.drawNoData = function () {
                this.updateSize();
                this._context.fillStyle = "#ffffff";
                this._context.fillRect(0, 0, this._canvasWidth, this._canvasHeight);
                this._context.fillStyle = "#000000";
                this._context.font = "20px Georgia";
                this._context.fillText("No data.", this._canvasWidth / 2, this._canvasHeight / 2);
            };
            /** Creates the second layer of the visualization, directly on top the base layer, with the exact same size. */
            CanvasVisualization.prototype.createTempCanvas = function () {
                var container = this._jCanvas[0].parentNode;
                this._tempCanvas = document.createElement('canvas');
                this._tempCanvas.id = this._name + "-temp";
                this._tempCanvas.width = this._canvasWidth;
                this._tempCanvas.height = this._canvasHeight;
                container.appendChild(this._tempCanvas);
                this._tempContext = this._tempCanvas.getContext('2d');
                this._jTempCanvas = $('#' + this._tempCanvas.id);
                this._jTempCanvas.addClass("temp-canvas");
                this._jTempCanvas.attr("height", this._canvasHeight);
                this._jTempCanvas.attr("width", this._canvasWidth);
                this._jTempCanvas.css("max-height", this._canvasHeight);
            };
            /** Disposes of the created second visualization layer canvas, that was created */
            CanvasVisualization.prototype.destroyTempCanvas = function () {
                this._tempCanvas.remove();
                this._tempContext = null;
            };
            /** Clears everything that was drawn into the canvas of the second layer of the visualization */
            CanvasVisualization.prototype.clearTempCanvas = function () {
                if (!this._tempCanvas || !this._tempContext) {
                    console.log("No temporary canvas created. Aborting.");
                    return;
                }
                this._tempContext.clearRect(0, 0, this._canvasWidth, this._canvasHeight);
            };
            CanvasVisualization.prototype.changeScale = function (scale) {
                this._scale = scale;
            };
            return CanvasVisualization;
        }(BaseObject));
        canvasvisualization.CanvasVisualization = CanvasVisualization;
    })(canvasvisualization = tpx3.canvasvisualization || (tpx3.canvasvisualization = {}));
})(tpx3 || (tpx3 = {}));
var tpx3;
(function (tpx3) {
    var mockupdata;
    (function (mockupdata) {
        mockupdata.mockup_pixels = { "Size": 2911, "TriggerCount": 3, "SumEnergy": 122659, "Pixels": [
                { "pixel_index": 13, "pixX": 144, "pixY": 131, "ToA": 315434.375, "ToT": 61, "TriggerNo": 0 }, { "pixel_index": 12, "pixX": 144, "pixY": 130, "ToA": 315435.9375, "ToT": 20, "TriggerNo": 0 }, { "pixel_index": 2, "pixX": 144, "pixY": 132, "ToA": 315437.5, "ToT": 16, "TriggerNo": 0 }, { "pixel_index": 8, "pixX": 144, "pixY": 133, "ToA": 315439.0625, "ToT": 16, "TriggerNo": 0 }, { "pixel_index": 11, "pixX": 145, "pixY": 130, "ToA": 315439.0625, "ToT": 13, "TriggerNo": 0 }, { "pixel_index": 9, "pixX": 142, "pixY": 136, "ToA": 315440.625, "ToT": 15, "TriggerNo": 0 }, { "pixel_index": 10, "pixX": 143, "pixY": 134, "ToA": 315440.625, "ToT": 13, "TriggerNo": 0 }, { "pixel_index": 5, "pixX": 143, "pixY": 135, "ToA": 315440.625, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 6, "pixX": 145, "pixY": 129, "ToA": 315442.1875, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 7, "pixX": 146, "pixY": 128, "ToA": 315443.75, "ToT": 20, "TriggerNo": 0 }, { "pixel_index": 1, "pixX": 142, "pixY": 137, "ToA": 315446.875, "ToT": 8, "TriggerNo": 0 }, { "pixel_index": 3, "pixX": 147, "pixY": 127, "ToA": 315448.4375, "ToT": 15, "TriggerNo": 0 }, { "pixel_index": 0, "pixX": 146, "pixY": 129, "ToA": 315450.0, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 4, "pixX": 148, "pixY": 126, "ToA": 315451.5625, "ToT": 34, "TriggerNo": 0 }, { "pixel_index": 15, "pixX": 167, "pixY": 206, "ToA": 1134785.9375, "ToT": 18, "TriggerNo": 0 }, { "pixel_index": 17, "pixX": 166, "pixY": 206, "ToA": 1134785.9375, "ToT": 24, "TriggerNo": 0 }, { "pixel_index": 16, "pixX": 168, "pixY": 206, "ToA": 1134790.625, "ToT": 23, "TriggerNo": 0 }, { "pixel_index": 14, "pixX": 165, "pixY": 206, "ToA": 1134792.1875, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 19, "pixX": 224, "pixY": 183, "ToA": 1199173.4375, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 22, "pixX": 225, "pixY": 183, "ToA": 1199173.4375, "ToT": 19, "TriggerNo": 0 }, { "pixel_index": 20, "pixX": 226, "pixY": 184, "ToA": 1199176.5625, "ToT": 13, "TriggerNo": 0 }, { "pixel_index": 24, "pixX": 228, "pixY": 184, "ToA": 1199176.5625, "ToT": 40, "TriggerNo": 0 }, { "pixel_index": 23, "pixX": 227, "pixY": 184, "ToA": 1199176.5625, "ToT": 34, "TriggerNo": 0 }, { "pixel_index": 18, "pixX": 228, "pixY": 185, "ToA": 1199187.5, "ToT": 3, "TriggerNo": 0 }, { "pixel_index": 21, "pixX": 229, "pixY": 184, "ToA": 1199187.5, "ToT": 33, "TriggerNo": 0 }, { "pixel_index": 28, "pixX": 186, "pixY": 198, "ToA": 2282387.5, "ToT": 17, "TriggerNo": 0 }, { "pixel_index": 25, "pixX": 187, "pixY": 198, "ToA": 2282389.0625, "ToT": 11, "TriggerNo": 0 },
                { "pixel_index": 29, "pixX": 185, "pixY": 198, "ToA": 2282390.625, "ToT": 19, "TriggerNo": 0 }, { "pixel_index": 26, "pixX": 184, "pixY": 198, "ToA": 2282392.1875, "ToT": 17, "TriggerNo": 0 }, { "pixel_index": 30, "pixX": 183, "pixY": 197, "ToA": 2282401.5625, "ToT": 19, "TriggerNo": 0 }, { "pixel_index": 27, "pixX": 182, "pixY": 197, "ToA": 2282409.375, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 31, "pixX": 112, "pixY": 226, "ToA": 3076204.6875, "ToT": 35, "TriggerNo": 0 }, { "pixel_index": 33, "pixX": 83, "pixY": 206, "ToA": 3401157.8125, "ToT": 52, "TriggerNo": 0 }, { "pixel_index": 32, "pixX": 83, "pixY": 207, "ToA": 3401159.375, "ToT": 34, "TriggerNo": 0 }, { "pixel_index": 35, "pixX": 226, "pixY": 19, "ToA": 4221482.8125, "ToT": 59, "TriggerNo": 0 }, { "pixel_index": 34, "pixX": 226, "pixY": 20, "ToA": 4221485.9375, "ToT": 10, "TriggerNo": 0 },
                { "pixel_index": 37, "pixX": 22, "pixY": 87, "ToA": 6582946.875, "ToT": 77, "TriggerNo": 0 }, { "pixel_index": 38, "pixX": 22, "pixY": 89, "ToA": 6582946.875, "ToT": 23, "TriggerNo": 0 }, { "pixel_index": 41, "pixX": 22, "pixY": 88, "ToA": 6582946.875, "ToT": 28, "TriggerNo": 0 }, { "pixel_index": 39, "pixX": 23, "pixY": 89, "ToA": 6582951.5625, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 40, "pixX": 23, "pixY": 90, "ToA": 6582953.125, "ToT": 35, "TriggerNo": 0 }, { "pixel_index": 36, "pixX": 23, "pixY": 91, "ToA": 6582976.5625, "ToT": 7, "TriggerNo": 0 },
                { "pixel_index": 44, "pixX": 35, "pixY": 209, "ToA": 7726850.0, "ToT": 30, "TriggerNo": 0 }, { "pixel_index": 43, "pixX": 35, "pixY": 208, "ToA": 7726859.375, "ToT": 25, "TriggerNo": 0 }, { "pixel_index": 42, "pixX": 35, "pixY": 207, "ToA": 7726885.9375, "ToT": 5, "TriggerNo": 0 },
                { "pixel_index": 45, "pixX": 150, "pixY": 33, "ToA": 9345264.0625, "ToT": 13, "TriggerNo": 0 }, { "pixel_index": 49, "pixX": 115, "pixY": 98, "ToA": 9518712.5, "ToT": 82, "TriggerNo": 0 }, { "pixel_index": 48, "pixX": 115, "pixY": 99, "ToA": 9518715.625, "ToT": 26, "TriggerNo": 0 }, { "pixel_index": 47, "pixX": 116, "pixY": 98, "ToA": 9518717.1875, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 46, "pixX": 115, "pixY": 97, "ToA": 9518734.375, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 50, "pixX": 93, "pixY": 194, "ToA": 11356906.25, "ToT": 55, "TriggerNo": 0 },
                { "pixel_index": 91, "pixX": 68, "pixY": 172, "ToA": 11920681.25, "ToT": 45, "TriggerNo": 0 }, { "pixel_index": 60, "pixX": 69, "pixY": 175, "ToA": 11920682.8125, "ToT": 17, "TriggerNo": 0 }, { "pixel_index": 76, "pixX": 69, "pixY": 168, "ToA": 11920682.8125, "ToT": 22, "TriggerNo": 0 }, { "pixel_index": 82, "pixX": 69, "pixY": 169, "ToA": 11920682.8125, "ToT": 16, "TriggerNo": 0 }, { "pixel_index": 86, "pixX": 69, "pixY": 174, "ToA": 11920682.8125, "ToT": 30, "TriggerNo": 0 }, { "pixel_index": 87, "pixX": 69, "pixY": 170, "ToA": 11920682.8125, "ToT": 19, "TriggerNo": 0 }, { "pixel_index": 75, "pixX": 69, "pixY": 172, "ToA": 11920684.375, "ToT": 20, "TriggerNo": 0 }, { "pixel_index": 63, "pixX": 68, "pixY": 165, "ToA": 11920684.375, "ToT": 14, "TriggerNo": 0 }, { "pixel_index": 77, "pixX": 69, "pixY": 165, "ToA": 11920684.375, "ToT": 15, "TriggerNo": 0 }, { "pixel_index": 83, "pixX": 69, "pixY": 166, "ToA": 11920684.375, "ToT": 17, "TriggerNo": 0 }, { "pixel_index": 88, "pixX": 68, "pixY": 164, "ToA": 11920684.375, "ToT": 24, "TriggerNo": 0 }, { "pixel_index": 81, "pixX": 69, "pixY": 173, "ToA": 11920685.9375, "ToT": 26, "TriggerNo": 0 }, { "pixel_index": 62, "pixX": 69, "pixY": 171, "ToA": 11920687.5, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 84, "pixX": 68, "pixY": 162, "ToA": 11920687.5, "ToT": 24, "TriggerNo": 0 }, { "pixel_index": 89, "pixX": 68, "pixY": 163, "ToA": 11920687.5, "ToT": 15, "TriggerNo": 0 }, { "pixel_index": 78, "pixX": 68, "pixY": 161, "ToA": 11920689.0625, "ToT": 15, "TriggerNo": 0 }, { "pixel_index": 69, "pixX": 68, "pixY": 160, "ToA": 11920690.625, "ToT": 15, "TriggerNo": 0 }, { "pixel_index": 58, "pixX": 67, "pixY": 162, "ToA": 11920690.625, "ToT": 11, "TriggerNo": 0 }, { "pixel_index": 71, "pixX": 68, "pixY": 158, "ToA": 11920692.1875, "ToT": 11, "TriggerNo": 0 }, { "pixel_index": 68, "pixX": 81, "pixY": 149, "ToA": 11920692.1875, "ToT": 16, "TriggerNo": 0 }, { "pixel_index": 90, "pixX": 69, "pixY": 157, "ToA": 11920692.1875, "ToT": 15, "TriggerNo": 0 }, { "pixel_index": 92, "pixX": 69, "pixY": 158, "ToA": 11920692.1875, "ToT": 20, "TriggerNo": 0 }, { "pixel_index": 79, "pixX": 68, "pixY": 159, "ToA": 11920692.1875, "ToT": 13, "TriggerNo": 0 }, { "pixel_index": 59, "pixX": 80, "pixY": 149, "ToA": 11920693.75, "ToT": 14, "TriggerNo": 0 }, { "pixel_index": 67, "pixX": 79, "pixY": 150, "ToA": 11920695.3125, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 74, "pixX": 70, "pixY": 154, "ToA": 11920695.3125, "ToT": 23, "TriggerNo": 0 }, { "pixel_index": 85, "pixX": 69, "pixY": 156, "ToA": 11920695.3125, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 57, "pixX": 78, "pixY": 150, "ToA": 11920695.3125, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 65, "pixX": 71, "pixY": 153, "ToA": 11920696.875, "ToT": 14, "TriggerNo": 0 }, { "pixel_index": 66, "pixX": 74, "pixY": 151, "ToA": 11920696.875, "ToT": 16, "TriggerNo": 0 }, { "pixel_index": 73, "pixX": 77, "pixY": 150, "ToA": 11920696.875, "ToT": 8, "TriggerNo": 0 }, { "pixel_index": 56, "pixX": 75, "pixY": 151, "ToA": 11920696.875, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 64, "pixX": 76, "pixY": 151, "ToA": 11920696.875, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 70, "pixX": 72, "pixY": 152, "ToA": 11920698.4375, "ToT": 13, "TriggerNo": 0 }, { "pixel_index": 55, "pixX": 73, "pixY": 152, "ToA": 11920698.4375, "ToT": 8, "TriggerNo": 0 }, { "pixel_index": 80, "pixX": 69, "pixY": 155, "ToA": 11920698.4375, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 72, "pixX": 69, "pixY": 154, "ToA": 11920700.0, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 54, "pixX": 70, "pixY": 153, "ToA": 11920701.5625, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 61, "pixX": 73, "pixY": 151, "ToA": 11920704.6875, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 52, "pixX": 77, "pixY": 151, "ToA": 11920720.3125, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 53, "pixX": 82, "pixY": 149, "ToA": 11920721.875, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 51, "pixX": 68, "pixY": 176, "ToA": 11920729.6875, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 97, "pixX": 206, "pixY": 84, "ToA": 12826270.3125, "ToT": 50, "TriggerNo": 0 }, { "pixel_index": 93, "pixX": 207, "pixY": 84, "ToA": 12826271.875, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 95, "pixX": 206, "pixY": 83, "ToA": 12826271.875, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 101, "pixX": 207, "pixY": 83, "ToA": 12826271.875, "ToT": 27, "TriggerNo": 0 }, { "pixel_index": 102, "pixX": 206, "pixY": 81, "ToA": 12826271.875, "ToT": 23, "TriggerNo": 0 }, { "pixel_index": 103, "pixX": 206, "pixY": 82, "ToA": 12826271.875, "ToT": 23, "TriggerNo": 0 }, { "pixel_index": 98, "pixX": 207, "pixY": 81, "ToA": 12826271.875, "ToT": 24, "TriggerNo": 0 }, { "pixel_index": 96, "pixX": 208, "pixY": 81, "ToA": 12826273.4375, "ToT": 16, "TriggerNo": 0 }, { "pixel_index": 99, "pixX": 208, "pixY": 82, "ToA": 12826273.4375, "ToT": 33, "TriggerNo": 0 }, { "pixel_index": 100, "pixX": 207, "pixY": 82, "ToA": 12826273.4375, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 94, "pixX": 208, "pixY": 83, "ToA": 12826276.5625, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 104, "pixX": 115, "pixY": 112, "ToA": 13072239.0625, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 106, "pixX": 248, "pixY": 243, "ToA": 13080110.9375, "ToT": 60, "TriggerNo": 0 }, { "pixel_index": 107, "pixX": 247, "pixY": 244, "ToA": 13080110.9375, "ToT": 82, "TriggerNo": 0 }, { "pixel_index": 105, "pixX": 248, "pixY": 244, "ToA": 13080114.0625, "ToT": 21, "TriggerNo": 0 }, { "pixel_index": 109, "pixX": 251, "pixY": 140, "ToA": 13086854.6875, "ToT": 38, "TriggerNo": 0 }, { "pixel_index": 113, "pixX": 10, "pixY": 103, "ToA": 13087267.1875, "ToT": 39, "TriggerNo": 0 }, { "pixel_index": 110, "pixX": 11, "pixY": 103, "ToA": 13087268.75, "ToT": 22, "TriggerNo": 0 }, { "pixel_index": 112, "pixX": 12, "pixY": 102, "ToA": 13087273.4375, "ToT": 14, "TriggerNo": 0 }, { "pixel_index": 108, "pixX": 12, "pixY": 101, "ToA": 13087304.6875, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 111, "pixX": 91, "pixY": 248, "ToA": 13089857.8125, "ToT": 14, "TriggerNo": 0 }, { "pixel_index": 115, "pixX": 14, "pixY": 249, "ToA": 13095259.375, "ToT": 75, "TriggerNo": 0 }, { "pixel_index": 114, "pixX": 13, "pixY": 249, "ToA": 13095270.3125, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 116, "pixX": 195, "pixY": 69, "ToA": 13101059.375, "ToT": 32, "TriggerNo": 0 }, { "pixel_index": 117, "pixX": 237, "pixY": 98, "ToA": 13107148.4375, "ToT": 11, "TriggerNo": 0 }, { "pixel_index": 118, "pixX": 238, "pixY": 99, "ToA": 13107148.4375, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 119, "pixX": 237, "pixY": 99, "ToA": 13107148.4375, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 127, "pixX": 105, "pixY": 132, "ToA": 13114098.4375, "ToT": 27, "TriggerNo": 0 }, { "pixel_index": 130, "pixX": 105, "pixY": 134, "ToA": 13114098.4375, "ToT": 33, "TriggerNo": 0 }, { "pixel_index": 121, "pixX": 104, "pixY": 132, "ToA": 13114100.0, "ToT": 17, "TriggerNo": 0 }, { "pixel_index": 124, "pixX": 103, "pixY": 129, "ToA": 13114100.0, "ToT": 19, "TriggerNo": 0 }, { "pixel_index": 122, "pixX": 104, "pixY": 130, "ToA": 13114101.5625, "ToT": 15, "TriggerNo": 0 }, { "pixel_index": 128, "pixX": 104, "pixY": 131, "ToA": 13114101.5625, "ToT": 20, "TriggerNo": 0 }, { "pixel_index": 120, "pixX": 103, "pixY": 130, "ToA": 13114103.125, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 129, "pixX": 179, "pixY": 54, "ToA": 13115723.4375, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 123, "pixX": 179, "pixY": 55, "ToA": 13115729.6875, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 126, "pixX": 112, "pixY": 67, "ToA": 13115889.0625, "ToT": 23, "TriggerNo": 0 }, { "pixel_index": 125, "pixX": 130, "pixY": 221, "ToA": 13115896.875, "ToT": 48, "TriggerNo": 0 }, { "pixel_index": 134, "pixX": 187, "pixY": 231, "ToA": 13128414.0625, "ToT": 16, "TriggerNo": 0 }, { "pixel_index": 136, "pixX": 188, "pixY": 232, "ToA": 13128414.0625, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 139, "pixX": 188, "pixY": 233, "ToA": 13128414.0625, "ToT": 16, "TriggerNo": 0 }, { "pixel_index": 142, "pixX": 189, "pixY": 234, "ToA": 13128414.0625, "ToT": 39, "TriggerNo": 0 }, { "pixel_index": 132, "pixX": 187, "pixY": 232, "ToA": 13128415.625, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 133, "pixX": 189, "pixY": 233, "ToA": 13128415.625, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 135, "pixX": 189, "pixY": 235, "ToA": 13128425.0, "ToT": 18, "TriggerNo": 0 }, { "pixel_index": 131, "pixX": 189, "pixY": 236, "ToA": 13128434.375, "ToT": 18, "TriggerNo": 0 }, { "pixel_index": 146, "pixX": 185, "pixY": 40, "ToA": 13133943.75, "ToT": 44, "TriggerNo": 0 }, { "pixel_index": 148, "pixX": 185, "pixY": 41, "ToA": 13133943.75, "ToT": 167, "TriggerNo": 0 }, { "pixel_index": 140, "pixX": 184, "pixY": 41, "ToA": 13133945.3125, "ToT": 14, "TriggerNo": 0 }, { "pixel_index": 138, "pixX": 186, "pixY": 41, "ToA": 13133945.3125, "ToT": 11, "TriggerNo": 0 }, { "pixel_index": 143, "pixX": 185, "pixY": 42, "ToA": 13133945.3125, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 137, "pixX": 184, "pixY": 40, "ToA": 13133945.3125, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 145, "pixX": 165, "pixY": 131, "ToA": 13136920.3125, "ToT": 22, "TriggerNo": 0 }, { "pixel_index": 141, "pixX": 164, "pixY": 131, "ToA": 13136920.3125, "ToT": 16, "TriggerNo": 0 }, { "pixel_index": 147, "pixX": 49, "pixY": 164, "ToA": 13139143.75, "ToT": 78, "TriggerNo": 0 }, { "pixel_index": 144, "pixX": 49, "pixY": 163, "ToA": 13139153.125, "ToT": 18, "TriggerNo": 0 }, { "pixel_index": 149, "pixX": 197, "pixY": 130, "ToA": 13142985.9375, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 152, "pixX": 212, "pixY": 97, "ToA": 13146320.3125, "ToT": 35, "TriggerNo": 0 }, { "pixel_index": 161, "pixX": 213, "pixY": 97, "ToA": 13146320.3125, "ToT": 37, "TriggerNo": 0 }, { "pixel_index": 168, "pixX": 142, "pixY": 246, "ToA": 13147067.1875, "ToT": 85, "TriggerNo": 0 }, { "pixel_index": 174, "pixX": 142, "pixY": 245, "ToA": 13147067.1875, "ToT": 165, "TriggerNo": 0 }, { "pixel_index": 166, "pixX": 143, "pixY": 245, "ToA": 13147068.75, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 160, "pixX": 141, "pixY": 245, "ToA": 13147070.3125, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 151, "pixX": 141, "pixY": 246, "ToA": 13147070.3125, "ToT": 3, "TriggerNo": 0 }, { "pixel_index": 150, "pixX": 143, "pixY": 246, "ToA": 13147071.875, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 159, "pixX": 142, "pixY": 244, "ToA": 13147073.4375, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 178, "pixX": 75, "pixY": 247, "ToA": 13148585.9375, "ToT": 43, "TriggerNo": 0 }, { "pixel_index": 180, "pixX": 74, "pixY": 246, "ToA": 13148585.9375, "ToT": 61, "TriggerNo": 0 }, { "pixel_index": 181, "pixX": 74, "pixY": 247, "ToA": 13148585.9375, "ToT": 88, "TriggerNo": 0 }, { "pixel_index": 175, "pixX": 74, "pixY": 248, "ToA": 13148587.5, "ToT": 44, "TriggerNo": 0 }, { "pixel_index": 179, "pixX": 73, "pixY": 247, "ToA": 13148587.5, "ToT": 145, "TriggerNo": 0 }, { "pixel_index": 170, "pixX": 75, "pixY": 248, "ToA": 13148592.1875, "ToT": 21, "TriggerNo": 0 }, { "pixel_index": 171, "pixX": 75, "pixY": 246, "ToA": 13148592.1875, "ToT": 22, "TriggerNo": 0 }, { "pixel_index": 177, "pixX": 73, "pixY": 246, "ToA": 13148592.1875, "ToT": 28, "TriggerNo": 0 }, { "pixel_index": 169, "pixX": 73, "pixY": 248, "ToA": 13148593.75, "ToT": 28, "TriggerNo": 0 }, { "pixel_index": 172, "pixX": 72, "pixY": 247, "ToA": 13148596.875, "ToT": 15, "TriggerNo": 0 }, { "pixel_index": 167, "pixX": 74, "pixY": 245, "ToA": 13148600.0, "ToT": 11, "TriggerNo": 0 }, { "pixel_index": 162, "pixX": 74, "pixY": 249, "ToA": 13148600.0, "ToT": 8, "TriggerNo": 0 }, { "pixel_index": 163, "pixX": 72, "pixY": 248, "ToA": 13148604.6875, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 164, "pixX": 76, "pixY": 247, "ToA": 13148606.25, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 165, "pixX": 72, "pixY": 246, "ToA": 13148607.8125, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 157, "pixX": 73, "pixY": 245, "ToA": 13148610.9375, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 154, "pixX": 73, "pixY": 249, "ToA": 13148614.0625, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 156, "pixX": 75, "pixY": 245, "ToA": 13148615.625, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 158, "pixX": 76, "pixY": 246, "ToA": 13148618.75, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 153, "pixX": 75, "pixY": 249, "ToA": 13148621.875, "ToT": 3, "TriggerNo": 0 }, { "pixel_index": 155, "pixX": 76, "pixY": 248, "ToA": 13148626.5625, "ToT": 3, "TriggerNo": 0 }, { "pixel_index": 176, "pixX": 11, "pixY": 152, "ToA": 13155560.9375, "ToT": 74, "TriggerNo": 0 }, { "pixel_index": 173, "pixX": 11, "pixY": 151, "ToA": 13155565.625, "ToT": 8, "TriggerNo": 0 }, { "pixel_index": 182, "pixX": 73, "pixY": 19, "ToA": 13164185.9375, "ToT": 33, "TriggerNo": 0 }, { "pixel_index": 183, "pixX": 177, "pixY": 160, "ToA": 13165110.9375, "ToT": 40, "TriggerNo": 0 }, { "pixel_index": 190, "pixX": 169, "pixY": 18, "ToA": 13165328.125, "ToT": 30, "TriggerNo": 0 }, { "pixel_index": 195, "pixX": 169, "pixY": 17, "ToA": 13165328.125, "ToT": 39, "TriggerNo": 0 }, { "pixel_index": 185, "pixX": 168, "pixY": 18, "ToA": 13165343.75, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 184, "pixX": 168, "pixY": 17, "ToA": 13165343.75, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 196, "pixX": 64, "pixY": 7, "ToA": 13169553.125, "ToT": 21, "TriggerNo": 0 }, { "pixel_index": 189, "pixX": 61, "pixY": 7, "ToA": 13169554.6875, "ToT": 15, "TriggerNo": 0 }, { "pixel_index": 186, "pixX": 65, "pixY": 7, "ToA": 13169554.6875, "ToT": 11, "TriggerNo": 0 }, { "pixel_index": 191, "pixX": 66, "pixY": 7, "ToA": 13169556.25, "ToT": 18, "TriggerNo": 0 }, { "pixel_index": 194, "pixX": 63, "pixY": 7, "ToA": 13169556.25, "ToT": 11, "TriggerNo": 0 }, { "pixel_index": 187, "pixX": 62, "pixY": 7, "ToA": 13169556.25, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 205, "pixX": 67, "pixY": 8, "ToA": 13169559.375, "ToT": 29, "TriggerNo": 0 }, { "pixel_index": 188, "pixX": 66, "pixY": 8, "ToA": 13169559.375, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 197, "pixX": 67, "pixY": 9, "ToA": 13169573.4375, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 192, "pixX": 217, "pixY": 0, "ToA": 13170489.0625, "ToT": 21, "TriggerNo": 0 }, { "pixel_index": 207, "pixX": 180, "pixY": 143, "ToA": 13171345.3125, "ToT": 44, "TriggerNo": 0 }, { "pixel_index": 212, "pixX": 180, "pixY": 142, "ToA": 13171345.3125, "ToT": 86, "TriggerNo": 0 }, { "pixel_index": 193, "pixX": 181, "pixY": 143, "ToA": 13171351.5625, "ToT": 3, "TriggerNo": 0 }, { "pixel_index": 199, "pixX": 181, "pixY": 142, "ToA": 13171354.6875, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 201, "pixX": 80, "pixY": 84, "ToA": 13173129.6875, "ToT": 20, "TriggerNo": 0 }, { "pixel_index": 208, "pixX": 215, "pixY": 7, "ToA": 13173132.8125, "ToT": 31, "TriggerNo": 0 }, { "pixel_index": 211, "pixX": 215, "pixY": 6, "ToA": 13173132.8125, "ToT": 59, "TriggerNo": 0 }, { "pixel_index": 200, "pixX": 216, "pixY": 6, "ToA": 13173132.8125, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 214, "pixX": 217, "pixY": 6, "ToA": 13173132.8125, "ToT": 24, "TriggerNo": 0 }, { "pixel_index": 198, "pixX": 79, "pixY": 84, "ToA": 13173132.8125, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 217, "pixX": 216, "pixY": 7, "ToA": 13173132.8125, "ToT": 41, "TriggerNo": 0 }, { "pixel_index": 206, "pixX": 78, "pixY": 84, "ToA": 13173134.375, "ToT": 18, "TriggerNo": 0 }, { "pixel_index": 213, "pixX": 217, "pixY": 5, "ToA": 13173134.375, "ToT": 27, "TriggerNo": 0 }, { "pixel_index": 215, "pixX": 217, "pixY": 7, "ToA": 13173135.9375, "ToT": 8, "TriggerNo": 0 }, { "pixel_index": 202, "pixX": 214, "pixY": 6, "ToA": 13173137.5, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 209, "pixX": 217, "pixY": 4, "ToA": 13173139.0625, "ToT": 23, "TriggerNo": 0 }, { "pixel_index": 203, "pixX": 217, "pixY": 3, "ToA": 13173154.6875, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 204, "pixX": 107, "pixY": 114, "ToA": 13174739.0625, "ToT": 11, "TriggerNo": 0 }, { "pixel_index": 210, "pixX": 46, "pixY": 85, "ToA": 13176748.4375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 216, "pixX": 121, "pixY": 36, "ToA": 13183621.875, "ToT": 25, "TriggerNo": 0 }, { "pixel_index": 218, "pixX": 166, "pixY": 81, "ToA": 13185651.5625, "ToT": 20, "TriggerNo": 0 }, { "pixel_index": 221, "pixX": 153, "pixY": 233, "ToA": 13189776.5625, "ToT": 72, "TriggerNo": 0 }, { "pixel_index": 219, "pixX": 153, "pixY": 234, "ToA": 13189778.125, "ToT": 11, "TriggerNo": 0 }, { "pixel_index": 220, "pixX": 154, "pixY": 233, "ToA": 13189779.6875, "ToT": 16, "TriggerNo": 0 }, { "pixel_index": 223, "pixX": 121, "pixY": 136, "ToA": 13192810.9375, "ToT": 64, "TriggerNo": 0 }, { "pixel_index": 222, "pixX": 121, "pixY": 137, "ToA": 13192810.9375, "ToT": 22, "TriggerNo": 0 }, { "pixel_index": 226, "pixX": 168, "pixY": 129, "ToA": 13195650.0, "ToT": 36, "TriggerNo": 0 }, { "pixel_index": 232, "pixX": 86, "pixY": 232, "ToA": 13196607.8125, "ToT": 17, "TriggerNo": 0 }, { "pixel_index": 246, "pixX": 86, "pixY": 234, "ToA": 13196607.8125, "ToT": 67, "TriggerNo": 0 }, { "pixel_index": 247, "pixX": 85, "pixY": 234, "ToA": 13196607.8125, "ToT": 66, "TriggerNo": 0 }, { "pixel_index": 262, "pixX": 86, "pixY": 233, "ToA": 13196607.8125, "ToT": 207, "TriggerNo": 0 }, { "pixel_index": 238, "pixX": 84, "pixY": 233, "ToA": 13196607.8125, "ToT": 13, "TriggerNo": 0 }, { "pixel_index": 261, "pixX": 85, "pixY": 233, "ToA": 13196607.8125, "ToT": 189, "TriggerNo": 0 }, { "pixel_index": 231, "pixX": 85, "pixY": 232, "ToA": 13196609.375, "ToT": 16, "TriggerNo": 0 }, { "pixel_index": 239, "pixX": 87, "pixY": 233, "ToA": 13196609.375, "ToT": 15, "TriggerNo": 0 }, { "pixel_index": 225, "pixX": 87, "pixY": 234, "ToA": 13196610.9375, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 224, "pixX": 84, "pixY": 234, "ToA": 13196612.5, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 227, "pixX": 146, "pixY": 116, "ToA": 13197250.0, "ToT": 31, "TriggerNo": 0 }, { "pixel_index": 228, "pixX": 146, "pixY": 115, "ToA": 13197253.125, "ToT": 13, "TriggerNo": 0 }, { "pixel_index": 260, "pixX": 168, "pixY": 65, "ToA": 13198171.875, "ToT": 39, "TriggerNo": 0 }, { "pixel_index": 240, "pixX": 168, "pixY": 66, "ToA": 13198171.875, "ToT": 21, "TriggerNo": 0 }, { "pixel_index": 248, "pixX": 169, "pixY": 66, "ToA": 13198175.0, "ToT": 24, "TriggerNo": 0 }, { "pixel_index": 234, "pixX": 169, "pixY": 67, "ToA": 13198182.8125, "ToT": 15, "TriggerNo": 0 }, { "pixel_index": 233, "pixX": 170, "pixY": 68, "ToA": 13198189.0625, "ToT": 16, "TriggerNo": 0 }, { "pixel_index": 237, "pixX": 170, "pixY": 67, "ToA": 13198190.625, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 241, "pixX": 171, "pixY": 69, "ToA": 13198192.1875, "ToT": 21, "TriggerNo": 0 }, { "pixel_index": 229, "pixX": 174, "pixY": 124, "ToA": 13198431.25, "ToT": 13, "TriggerNo": 0 }, { "pixel_index": 268, "pixX": 176, "pixY": 124, "ToA": 13198431.25, "ToT": 282, "TriggerNo": 0 }, { "pixel_index": 245, "pixX": 177, "pixY": 123, "ToA": 13198431.25, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 251, "pixX": 176, "pixY": 125, "ToA": 13198431.25, "ToT": 28, "TriggerNo": 0 }, { "pixel_index": 258, "pixX": 176, "pixY": 123, "ToA": 13198431.25, "ToT": 44, "TriggerNo": 0 }, { "pixel_index": 243, "pixX": 177, "pixY": 124, "ToA": 13198431.25, "ToT": 27, "TriggerNo": 0 }, { "pixel_index": 242, "pixX": 175, "pixY": 125, "ToA": 13198431.25, "ToT": 17, "TriggerNo": 0 }, { "pixel_index": 250, "pixX": 175, "pixY": 124, "ToA": 13198431.25, "ToT": 175, "TriggerNo": 0 }, { "pixel_index": 230, "pixX": 177, "pixY": 125, "ToA": 13198432.8125, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 244, "pixX": 175, "pixY": 123, "ToA": 13198432.8125, "ToT": 23, "TriggerNo": 0 }, { "pixel_index": 235, "pixX": 174, "pixY": 123, "ToA": 13198434.375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 236, "pixX": 176, "pixY": 122, "ToA": 13198435.9375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 280, "pixX": 110, "pixY": 8, "ToA": 13203870.3125, "ToT": 44, "TriggerNo": 0 }, { "pixel_index": 295, "pixX": 110, "pixY": 10, "ToA": 13203870.3125, "ToT": 273, "TriggerNo": 0 }, { "pixel_index": 291, "pixX": 110, "pixY": 9, "ToA": 13203870.3125, "ToT": 83, "TriggerNo": 0 }, { "pixel_index": 298, "pixX": 108, "pixY": 8, "ToA": 13203870.3125, "ToT": 295, "TriggerNo": 0 }, { "pixel_index": 302, "pixX": 111, "pixY": 10, "ToA": 13203870.3125, "ToT": 45, "TriggerNo": 0 }, { "pixel_index": 301, "pixX": 109, "pixY": 8, "ToA": 13203870.3125, "ToT": 245, "TriggerNo": 0 }, { "pixel_index": 284, "pixX": 108, "pixY": 9, "ToA": 13203870.3125, "ToT": 142, "TriggerNo": 0 }, { "pixel_index": 273, "pixX": 108, "pixY": 10, "ToA": 13203870.3125, "ToT": 49, "TriggerNo": 0 }, { "pixel_index": 304, "pixX": 109, "pixY": 9, "ToA": 13203870.3125, "ToT": 315, "TriggerNo": 0 }, { "pixel_index": 293, "pixX": 109, "pixY": 10, "ToA": 13203870.3125, "ToT": 215, "TriggerNo": 0 }, { "pixel_index": 278, "pixX": 107, "pixY": 8, "ToA": 13203871.875, "ToT": 48, "TriggerNo": 0 }, { "pixel_index": 289, "pixX": 107, "pixY": 9, "ToA": 13203871.875, "ToT": 26, "TriggerNo": 0 }, { "pixel_index": 287, "pixX": 108, "pixY": 7, "ToA": 13203871.875, "ToT": 51, "TriggerNo": 0 }, { "pixel_index": 276, "pixX": 109, "pixY": 7, "ToA": 13203871.875, "ToT": 43, "TriggerNo": 0 }, { "pixel_index": 267, "pixX": 111, "pixY": 9, "ToA": 13203873.4375, "ToT": 22, "TriggerNo": 0 }, { "pixel_index": 259, "pixX": 110, "pixY": 7, "ToA": 13203875.0, "ToT": 14, "TriggerNo": 0 }, { "pixel_index": 256, "pixX": 107, "pixY": 7, "ToA": 13203875.0, "ToT": 15, "TriggerNo": 0 }, { "pixel_index": 265, "pixX": 107, "pixY": 10, "ToA": 13203876.5625, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 274, "pixX": 111, "pixY": 11, "ToA": 13203878.125, "ToT": 15, "TriggerNo": 0 }, { "pixel_index": 299, "pixX": 110, "pixY": 11, "ToA": 13203878.125, "ToT": 37, "TriggerNo": 0 }, { "pixel_index": 252, "pixX": 108, "pixY": 11, "ToA": 13203879.6875, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 266, "pixX": 109, "pixY": 11, "ToA": 13203881.25, "ToT": 31, "TriggerNo": 0 }, { "pixel_index": 255, "pixX": 111, "pixY": 8, "ToA": 13203882.8125, "ToT": 8, "TriggerNo": 0 }, { "pixel_index": 257, "pixX": 108, "pixY": 6, "ToA": 13203885.9375, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 270, "pixX": 109, "pixY": 6, "ToA": 13203889.0625, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 271, "pixX": 106, "pixY": 8, "ToA": 13203896.875, "ToT": 3, "TriggerNo": 0 }, { "pixel_index": 249, "pixX": 106, "pixY": 9, "ToA": 13203906.25, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 253, "pixX": 112, "pixY": 10, "ToA": 13203923.4375, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 254, "pixX": 104, "pixY": 228, "ToA": 13204889.0625, "ToT": 16, "TriggerNo": 0 }, { "pixel_index": 264, "pixX": 105, "pixY": 228, "ToA": 13204889.0625, "ToT": 21, "TriggerNo": 0 }, { "pixel_index": 279, "pixX": 30, "pixY": 47, "ToA": 13207120.3125, "ToT": 42, "TriggerNo": 0 }, { "pixel_index": 269, "pixX": 30, "pixY": 46, "ToA": 13207125.0, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 272, "pixX": 31, "pixY": 46, "ToA": 13207125.0, "ToT": 11, "TriggerNo": 0 }, { "pixel_index": 263, "pixX": 154, "pixY": 226, "ToA": 13207142.1875, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 282, "pixX": 138, "pixY": 230, "ToA": 13210700.0, "ToT": 56, "TriggerNo": 0 }, { "pixel_index": 277, "pixX": 138, "pixY": 231, "ToA": 13210703.125, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 275, "pixX": 137, "pixY": 230, "ToA": 13210710.9375, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 296, "pixX": 204, "pixY": 61, "ToA": 13212482.8125, "ToT": 65, "TriggerNo": 0 }, { "pixel_index": 297, "pixX": 206, "pixY": 56, "ToA": 13212485.9375, "ToT": 37, "TriggerNo": 0 }, { "pixel_index": 300, "pixX": 206, "pixY": 57, "ToA": 13212485.9375, "ToT": 36, "TriggerNo": 0 }, { "pixel_index": 307, "pixX": 207, "pixY": 57, "ToA": 13212485.9375, "ToT": 32, "TriggerNo": 0 }, { "pixel_index": 306, "pixX": 207, "pixY": 56, "ToA": 13212487.5, "ToT": 25, "TriggerNo": 0 }, { "pixel_index": 294, "pixX": 207, "pixY": 58, "ToA": 13212487.5, "ToT": 14, "TriggerNo": 0 }, { "pixel_index": 288, "pixX": 206, "pixY": 58, "ToA": 13212489.0625, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 303, "pixX": 206, "pixY": 59, "ToA": 13212490.625, "ToT": 51, "TriggerNo": 0 }, { "pixel_index": 285, "pixX": 205, "pixY": 60, "ToA": 13212498.4375, "ToT": 14, "TriggerNo": 0 }, { "pixel_index": 292, "pixX": 204, "pixY": 60, "ToA": 13212498.4375, "ToT": 16, "TriggerNo": 0 }, { "pixel_index": 283, "pixX": 206, "pixY": 60, "ToA": 13212501.5625, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 286, "pixX": 77, "pixY": 171, "ToA": 13212557.8125, "ToT": 11, "TriggerNo": 0 }, { "pixel_index": 281, "pixX": 77, "pixY": 172, "ToA": 13212562.5, "ToT": 18, "TriggerNo": 0 }, { "pixel_index": 290, "pixX": 10, "pixY": 222, "ToA": 13214275.0, "ToT": 21, "TriggerNo": 0 }, { "pixel_index": 305, "pixX": 178, "pixY": 242, "ToA": 13223223.4375, "ToT": 14, "TriggerNo": 0 }, { "pixel_index": 308, "pixX": 109, "pixY": 109, "ToA": 13227803.125, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 310, "pixX": 217, "pixY": 221, "ToA": 13230376.5625, "ToT": 20, "TriggerNo": 0 }, { "pixel_index": 314, "pixX": 217, "pixY": 222, "ToA": 13230376.5625, "ToT": 52, "TriggerNo": 0 }, { "pixel_index": 309, "pixX": 218, "pixY": 221, "ToA": 13230378.125, "ToT": 18, "TriggerNo": 0 }, { "pixel_index": 327, "pixX": 158, "pixY": 21, "ToA": 13232160.9375, "ToT": 23, "TriggerNo": 0 }, { "pixel_index": 326, "pixX": 155, "pixY": 19, "ToA": 13232160.9375, "ToT": 21, "TriggerNo": 0 }, { "pixel_index": 346, "pixX": 159, "pixY": 22, "ToA": 13232160.9375, "ToT": 29, "TriggerNo": 0 }, { "pixel_index": 335, "pixX": 158, "pixY": 22, "ToA": 13232164.0625, "ToT": 11, "TriggerNo": 0 }, { "pixel_index": 315, "pixX": 160, "pixY": 24, "ToA": 13232164.0625, "ToT": 14, "TriggerNo": 0 }, { "pixel_index": 334, "pixX": 161, "pixY": 24, "ToA": 13232164.0625, "ToT": 19, "TriggerNo": 0 }, { "pixel_index": 325, "pixX": 156, "pixY": 19, "ToA": 13232164.0625, "ToT": 17, "TriggerNo": 0 }, { "pixel_index": 319, "pixX": 154, "pixY": 19, "ToA": 13232165.625, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 318, "pixX": 157, "pixY": 20, "ToA": 13232165.625, "ToT": 14, "TriggerNo": 0 }, { "pixel_index": 320, "pixX": 160, "pixY": 23, "ToA": 13232167.1875, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 324, "pixX": 162, "pixY": 25, "ToA": 13232167.1875, "ToT": 52, "TriggerNo": 0 }, { "pixel_index": 316, "pixX": 159, "pixY": 23, "ToA": 13232168.75, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 364, "pixX": 58, "pixY": 55, "ToA": 13232443.75, "ToT": 236, "TriggerNo": 0 }, { "pixel_index": 352, "pixX": 58, "pixY": 56, "ToA": 13232443.75, "ToT": 77, "TriggerNo": 0 }, { "pixel_index": 349, "pixX": 60, "pixY": 56, "ToA": 13232443.75, "ToT": 50, "TriggerNo": 0 }, { "pixel_index": 358, "pixX": 59, "pixY": 55, "ToA": 13232443.75, "ToT": 270, "TriggerNo": 0 }, { "pixel_index": 344, "pixX": 59, "pixY": 57, "ToA": 13232443.75, "ToT": 50, "TriggerNo": 0 }, { "pixel_index": 360, "pixX": 58, "pixY": 54, "ToA": 13232443.75, "ToT": 235, "TriggerNo": 0 }, { "pixel_index": 356, "pixX": 59, "pixY": 56, "ToA": 13232443.75, "ToT": 295, "TriggerNo": 0 }, { "pixel_index": 353, "pixX": 59, "pixY": 54, "ToA": 13232443.75, "ToT": 142, "TriggerNo": 0 }, { "pixel_index": 339, "pixX": 58, "pixY": 57, "ToA": 13232443.75, "ToT": 17, "TriggerNo": 0 }, { "pixel_index": 347, "pixX": 60, "pixY": 55, "ToA": 13232445.3125, "ToT": 48, "TriggerNo": 0 }, { "pixel_index": 341, "pixX": 60, "pixY": 54, "ToA": 13232445.3125, "ToT": 22, "TriggerNo": 0 }, { "pixel_index": 329, "pixX": 60, "pixY": 57, "ToA": 13232445.3125, "ToT": 13, "TriggerNo": 0 }, { "pixel_index": 331, "pixX": 57, "pixY": 56, "ToA": 13232446.875, "ToT": 15, "TriggerNo": 0 }, { "pixel_index": 348, "pixX": 58, "pixY": 53, "ToA": 13232446.875, "ToT": 43, "TriggerNo": 0 }, { "pixel_index": 359, "pixX": 57, "pixY": 55, "ToA": 13232446.875, "ToT": 39, "TriggerNo": 0 }, { "pixel_index": 354, "pixX": 57, "pixY": 54, "ToA": 13232446.875, "ToT": 36, "TriggerNo": 0 }, { "pixel_index": 342, "pixX": 59, "pixY": 53, "ToA": 13232448.4375, "ToT": 21, "TriggerNo": 0 }, { "pixel_index": 350, "pixX": 57, "pixY": 53, "ToA": 13232448.4375, "ToT": 8, "TriggerNo": 0 }, { "pixel_index": 343, "pixX": 56, "pixY": 55, "ToA": 13232450.0, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 337, "pixX": 60, "pixY": 53, "ToA": 13232450.0, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 338, "pixX": 61, "pixY": 56, "ToA": 13232450.0, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 340, "pixX": 57, "pixY": 57, "ToA": 13232450.0, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 355, "pixX": 61, "pixY": 55, "ToA": 13232450.0, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 333, "pixX": 56, "pixY": 54, "ToA": 13232451.5625, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 351, "pixX": 61, "pixY": 54, "ToA": 13232451.5625, "ToT": 3, "TriggerNo": 0 }, { "pixel_index": 332, "pixX": 58, "pixY": 52, "ToA": 13232453.125, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 313, "pixX": 56, "pixY": 56, "ToA": 13232453.125, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 322, "pixX": 59, "pixY": 52, "ToA": 13232454.6875, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 330, "pixX": 59, "pixY": 58, "ToA": 13232456.25, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 317, "pixX": 56, "pixY": 53, "ToA": 13232456.25, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 345, "pixX": 57, "pixY": 52, "ToA": 13232457.8125, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 321, "pixX": 60, "pixY": 52, "ToA": 13232459.375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 311, "pixX": 61, "pixY": 57, "ToA": 13232459.375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 312, "pixX": 58, "pixY": 58, "ToA": 13232460.9375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 336, "pixX": 102, "pixY": 250, "ToA": 13233950.0, "ToT": 81, "TriggerNo": 0 }, { "pixel_index": 328, "pixX": 71, "pixY": 103, "ToA": 13233960.9375, "ToT": 13, "TriggerNo": 0 }, { "pixel_index": 323, "pixX": 102, "pixY": 249, "ToA": 13233981.25, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 361, "pixX": 23, "pixY": 192, "ToA": 13246565.625, "ToT": 30, "TriggerNo": 0 }, { "pixel_index": 368, "pixX": 22, "pixY": 192, "ToA": 13246565.625, "ToT": 52, "TriggerNo": 0 }, { "pixel_index": 363, "pixX": 22, "pixY": 191, "ToA": 13246567.1875, "ToT": 14, "TriggerNo": 0 }, { "pixel_index": 357, "pixX": 23, "pixY": 191, "ToA": 13246568.75, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 362, "pixX": 242, "pixY": 237, "ToA": 13249450.0, "ToT": 3, "TriggerNo": 0 }, { "pixel_index": 379, "pixX": 185, "pixY": 41, "ToA": 13251835.9375, "ToT": 52, "TriggerNo": 0 }, { "pixel_index": 381, "pixX": 38, "pixY": 210, "ToA": 13251839.0625, "ToT": 30, "TriggerNo": 0 }, { "pixel_index": 384, "pixX": 37, "pixY": 209, "ToA": 13251839.0625, "ToT": 27, "TriggerNo": 0 }, { "pixel_index": 370, "pixX": 39, "pixY": 210, "ToA": 13251839.0625, "ToT": 18, "TriggerNo": 0 }, { "pixel_index": 383, "pixX": 38, "pixY": 204, "ToA": 13251839.0625, "ToT": 25, "TriggerNo": 0 }, { "pixel_index": 386, "pixX": 38, "pixY": 205, "ToA": 13251839.0625, "ToT": 25, "TriggerNo": 0 }, { "pixel_index": 371, "pixX": 184, "pixY": 41, "ToA": 13251840.625, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 372, "pixX": 39, "pixY": 207, "ToA": 13251840.625, "ToT": 18, "TriggerNo": 0 }, { "pixel_index": 377, "pixX": 40, "pixY": 209, "ToA": 13251840.625, "ToT": 17, "TriggerNo": 0 }, { "pixel_index": 382, "pixX": 40, "pixY": 206, "ToA": 13251840.625, "ToT": 26, "TriggerNo": 0 }, { "pixel_index": 385, "pixX": 41, "pixY": 206, "ToA": 13251840.625, "ToT": 30, "TriggerNo": 0 }, { "pixel_index": 389, "pixX": 39, "pixY": 206, "ToA": 13251840.625, "ToT": 19, "TriggerNo": 0 }, { "pixel_index": 390, "pixX": 36, "pixY": 208, "ToA": 13251840.625, "ToT": 11, "TriggerNo": 0 }, { "pixel_index": 365, "pixX": 40, "pixY": 208, "ToA": 13251840.625, "ToT": 14, "TriggerNo": 0 }, { "pixel_index": 369, "pixX": 37, "pixY": 207, "ToA": 13251840.625, "ToT": 18, "TriggerNo": 0 }, { "pixel_index": 388, "pixX": 41, "pixY": 205, "ToA": 13251842.1875, "ToT": 72, "TriggerNo": 0 }, { "pixel_index": 387, "pixX": 37, "pixY": 210, "ToA": 13251842.1875, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 374, "pixX": 42, "pixY": 205, "ToA": 13251842.1875, "ToT": 64, "TriggerNo": 0 }, { "pixel_index": 373, "pixX": 40, "pixY": 207, "ToA": 13251843.75, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 366, "pixX": 42, "pixY": 206, "ToA": 13251843.75, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 376, "pixX": 37, "pixY": 208, "ToA": 13251848.4375, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 367, "pixX": 36, "pixY": 209, "ToA": 13251856.25, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 380, "pixX": 11, "pixY": 59, "ToA": 13253628.125, "ToT": 26, "TriggerNo": 0 }, { "pixel_index": 378, "pixX": 12, "pixY": 60, "ToA": 13253632.8125, "ToT": 26, "TriggerNo": 0 }, { "pixel_index": 375, "pixX": 11, "pixY": 60, "ToA": 13253635.9375, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 391, "pixX": 186, "pixY": 217, "ToA": 13261607.8125, "ToT": 112, "TriggerNo": 0 }, { "pixel_index": 392, "pixX": 21, "pixY": 51, "ToA": 13263134.375, "ToT": 22, "TriggerNo": 0 }, { "pixel_index": 393, "pixX": 56, "pixY": 180, "ToA": 13269726.5625, "ToT": 30, "TriggerNo": 0 }, { "pixel_index": 396, "pixX": 89, "pixY": 12, "ToA": 13270414.0625, "ToT": 23, "TriggerNo": 0 }, { "pixel_index": 394, "pixX": 89, "pixY": 13, "ToA": 13270420.3125, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 395, "pixX": 195, "pixY": 183, "ToA": 13272076.5625, "ToT": 13, "TriggerNo": 0 }, { "pixel_index": 398, "pixX": 247, "pixY": 26, "ToA": 13274493.75, "ToT": 35, "TriggerNo": 0 }, { "pixel_index": 399, "pixX": 73, "pixY": 239, "ToA": 13275093.75, "ToT": 76, "TriggerNo": 0 }, { "pixel_index": 397, "pixX": 73, "pixY": 238, "ToA": 13275114.0625, "ToT": 8, "TriggerNo": 0 }, { "pixel_index": 404, "pixX": 123, "pixY": 220, "ToA": 13283093.75, "ToT": 140, "TriggerNo": 0 }, { "pixel_index": 402, "pixX": 123, "pixY": 219, "ToA": 13283106.25, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 400, "pixX": 122, "pixY": 220, "ToA": 13283107.8125, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 401, "pixX": 124, "pixY": 220, "ToA": 13283107.8125, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 403, "pixX": 123, "pixY": 221, "ToA": 13283107.8125, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 405, "pixX": 158, "pixY": 126, "ToA": 13290576.5625, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 412, "pixX": 29, "pixY": 254, "ToA": 13294770.3125, "ToT": 70, "TriggerNo": 0 }, { "pixel_index": 415, "pixX": 31, "pixY": 253, "ToA": 13294771.875, "ToT": 18, "TriggerNo": 0 }, { "pixel_index": 425, "pixX": 31, "pixY": 252, "ToA": 13294771.875, "ToT": 39, "TriggerNo": 0 }, { "pixel_index": 421, "pixX": 30, "pixY": 253, "ToA": 13294771.875, "ToT": 22, "TriggerNo": 0 }, { "pixel_index": 408, "pixX": 32, "pixY": 252, "ToA": 13294773.4375, "ToT": 31, "TriggerNo": 0 }, { "pixel_index": 407, "pixX": 29, "pixY": 253, "ToA": 13294781.25, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 422, "pixX": 30, "pixY": 251, "ToA": 13294781.25, "ToT": 73, "TriggerNo": 0 }, { "pixel_index": 416, "pixX": 30, "pixY": 250, "ToA": 13294781.25, "ToT": 52, "TriggerNo": 0 }, { "pixel_index": 413, "pixX": 31, "pixY": 251, "ToA": 13294781.25, "ToT": 37, "TriggerNo": 0 }, { "pixel_index": 406, "pixX": 30, "pixY": 252, "ToA": 13294782.8125, "ToT": 3, "TriggerNo": 0 }, { "pixel_index": 409, "pixX": 31, "pixY": 250, "ToA": 13294782.8125, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 411, "pixX": 30, "pixY": 254, "ToA": 13294782.8125, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 410, "pixX": 29, "pixY": 251, "ToA": 13294787.5, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 414, "pixX": 21, "pixY": 27, "ToA": 13298103.125, "ToT": 11, "TriggerNo": 0 }, { "pixel_index": 423, "pixX": 38, "pixY": 21, "ToA": 13300637.5, "ToT": 18, "TriggerNo": 0 }, { "pixel_index": 435, "pixX": 37, "pixY": 21, "ToA": 13300639.0625, "ToT": 180, "TriggerNo": 0 }, { "pixel_index": 430, "pixX": 37, "pixY": 20, "ToA": 13300639.0625, "ToT": 67, "TriggerNo": 0 }, { "pixel_index": 426, "pixX": 36, "pixY": 21, "ToA": 13300640.625, "ToT": 15, "TriggerNo": 0 }, { "pixel_index": 419, "pixX": 37, "pixY": 22, "ToA": 13300640.625, "ToT": 16, "TriggerNo": 0 }, { "pixel_index": 420, "pixX": 38, "pixY": 20, "ToA": 13300645.3125, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 424, "pixX": 36, "pixY": 20, "ToA": 13300654.6875, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 417, "pixX": 36, "pixY": 22, "ToA": 13300667.1875, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 418, "pixX": 38, "pixY": 22, "ToA": 13300671.875, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 431, "pixX": 57, "pixY": 195, "ToA": 13309079.6875, "ToT": 42, "TriggerNo": 0 }, { "pixel_index": 429, "pixX": 58, "pixY": 195, "ToA": 13309081.25, "ToT": 29, "TriggerNo": 0 }, { "pixel_index": 434, "pixX": 154, "pixY": 178, "ToA": 13309090.625, "ToT": 14, "TriggerNo": 0 }, { "pixel_index": 428, "pixX": 154, "pixY": 179, "ToA": 13309090.625, "ToT": 13, "TriggerNo": 0 }, { "pixel_index": 432, "pixX": 153, "pixY": 178, "ToA": 13309092.1875, "ToT": 15, "TriggerNo": 0 }, { "pixel_index": 427, "pixX": 153, "pixY": 179, "ToA": 13309098.4375, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 436, "pixX": 24, "pixY": 70, "ToA": 13310868.75, "ToT": 30, "TriggerNo": 0 }, { "pixel_index": 438, "pixX": 23, "pixY": 70, "ToA": 13310875.0, "ToT": 26, "TriggerNo": 0 }, { "pixel_index": 437, "pixX": 22, "pixY": 70, "ToA": 13310882.8125, "ToT": 19, "TriggerNo": 0 }, { "pixel_index": 433, "pixX": 22, "pixY": 69, "ToA": 13310903.125, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 439, "pixX": 46, "pixY": 98, "ToA": 13319812.5, "ToT": 59, "TriggerNo": 0 }, { "pixel_index": 468, "pixX": 17, "pixY": 147, "ToA": 13323679.6875, "ToT": 17, "TriggerNo": 0 }, { "pixel_index": 562, "pixX": 18, "pixY": 148, "ToA": 13323679.6875, "ToT": 29, "TriggerNo": 0 }, { "pixel_index": 508, "pixX": 16, "pixY": 147, "ToA": 13323679.6875, "ToT": 23, "TriggerNo": 0 }, { "pixel_index": 704, "pixX": 18, "pixY": 150, "ToA": 13323679.6875, "ToT": 63, "TriggerNo": 0 }, { "pixel_index": 703, "pixX": 17, "pixY": 149, "ToA": 13323679.6875, "ToT": 286, "TriggerNo": 0 }, { "pixel_index": 760, "pixX": 17, "pixY": 150, "ToA": 13323679.6875, "ToT": 329, "TriggerNo": 0 }, { "pixel_index": 444, "pixX": 16, "pixY": 149, "ToA": 13323679.6875, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 561, "pixX": 16, "pixY": 148, "ToA": 13323679.6875, "ToT": 212, "TriggerNo": 0 }, { "pixel_index": 631, "pixX": 16, "pixY": 150, "ToA": 13323679.6875, "ToT": 338, "TriggerNo": 0 }, { "pixel_index": 632, "pixX": 18, "pixY": 149, "ToA": 13323679.6875, "ToT": 75, "TriggerNo": 0 }, { "pixel_index": 458, "pixX": 17, "pixY": 151, "ToA": 13323679.6875, "ToT": 70, "TriggerNo": 0 }, { "pixel_index": 496, "pixX": 17, "pixY": 148, "ToA": 13323679.6875, "ToT": 109, "TriggerNo": 0 }, { "pixel_index": 445, "pixX": 14, "pixY": 148, "ToA": 13323681.25, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 529, "pixX": 15, "pixY": 151, "ToA": 13323681.25, "ToT": 29, "TriggerNo": 0 }, { "pixel_index": 462, "pixX": 14, "pixY": 150, "ToA": 13323681.25, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 446, "pixX": 15, "pixY": 147, "ToA": 13323681.25, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 456, "pixX": 14, "pixY": 149, "ToA": 13323681.25, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 495, "pixX": 15, "pixY": 149, "ToA": 13323681.25, "ToT": 22, "TriggerNo": 0 }, { "pixel_index": 467, "pixX": 16, "pixY": 151, "ToA": 13323681.25, "ToT": 84, "TriggerNo": 0 }, { "pixel_index": 482, "pixX": 15, "pixY": 148, "ToA": 13323681.25, "ToT": 19, "TriggerNo": 0 }, { "pixel_index": 507, "pixX": 15, "pixY": 150, "ToA": 13323681.25, "ToT": 75, "TriggerNo": 0 }, { "pixel_index": 455, "pixX": 18, "pixY": 151, "ToA": 13323682.8125, "ToT": 22, "TriggerNo": 0 }, { "pixel_index": 470, "pixX": 19, "pixY": 149, "ToA": 13323682.8125, "ToT": 11, "TriggerNo": 0 }, { "pixel_index": 449, "pixX": 18, "pixY": 147, "ToA": 13323682.8125, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 497, "pixX": 19, "pixY": 150, "ToA": 13323685.9375, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 463, "pixX": 19, "pixY": 148, "ToA": 13323687.5, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 441, "pixX": 17, "pixY": 152, "ToA": 13323690.625, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 454, "pixX": 16, "pixY": 152, "ToA": 13323690.625, "ToT": 11, "TriggerNo": 0 }, { "pixel_index": 469, "pixX": 14, "pixY": 151, "ToA": 13323693.75, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 442, "pixX": 15, "pixY": 152, "ToA": 13323696.875, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 440, "pixX": 18, "pixY": 152, "ToA": 13323701.5625, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 443, "pixX": 19, "pixY": 151, "ToA": 13323701.5625, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 450, "pixX": 16, "pixY": 146, "ToA": 13323709.375, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 459, "pixX": 17, "pixY": 146, "ToA": 13323714.0625, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 452, "pixX": 197, "pixY": 223, "ToA": 13325184.375, "ToT": 16, "TriggerNo": 0 }, { "pixel_index": 460, "pixX": 197, "pixY": 222, "ToA": 13325184.375, "ToT": 56, "TriggerNo": 0 }, { "pixel_index": 448, "pixX": 198, "pixY": 223, "ToA": 13325209.375, "ToT": 3, "TriggerNo": 0 }, { "pixel_index": 447, "pixX": 196, "pixY": 222, "ToA": 13325214.0625, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 481, "pixX": 144, "pixY": 233, "ToA": 13327385.9375, "ToT": 50, "TriggerNo": 0 }, { "pixel_index": 457, "pixX": 144, "pixY": 234, "ToA": 13327385.9375, "ToT": 36, "TriggerNo": 0 }, { "pixel_index": 514, "pixX": 144, "pixY": 238, "ToA": 13327387.5, "ToT": 81, "TriggerNo": 0 }, { "pixel_index": 473, "pixX": 145, "pixY": 238, "ToA": 13327389.0625, "ToT": 44, "TriggerNo": 0 }, { "pixel_index": 502, "pixX": 145, "pixY": 237, "ToA": 13327389.0625, "ToT": 60, "TriggerNo": 0 }, { "pixel_index": 466, "pixX": 144, "pixY": 235, "ToA": 13327389.0625, "ToT": 35, "TriggerNo": 0 }, { "pixel_index": 465, "pixX": 145, "pixY": 236, "ToA": 13327390.625, "ToT": 23, "TriggerNo": 0 }, { "pixel_index": 461, "pixX": 143, "pixY": 233, "ToA": 13327393.75, "ToT": 25, "TriggerNo": 0 }, { "pixel_index": 453, "pixX": 143, "pixY": 232, "ToA": 13327401.5625, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 451, "pixX": 144, "pixY": 236, "ToA": 13327407.8125, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 471, "pixX": 122, "pixY": 254, "ToA": 13330329.6875, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 464, "pixX": 122, "pixY": 255, "ToA": 13330337.5, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 494, "pixX": 237, "pixY": 144, "ToA": 13332700.0, "ToT": 11, "TriggerNo": 0 }, { "pixel_index": 472, "pixX": 236, "pixY": 144, "ToA": 13332701.5625, "ToT": 8, "TriggerNo": 0 }, { "pixel_index": 491, "pixX": 61, "pixY": 19, "ToA": 13334125.0, "ToT": 14, "TriggerNo": 0 }, { "pixel_index": 500, "pixX": 60, "pixY": 19, "ToA": 13334126.5625, "ToT": 16, "TriggerNo": 0 }, { "pixel_index": 499, "pixX": 22, "pixY": 218, "ToA": 13334212.5, "ToT": 97, "TriggerNo": 0 }, { "pixel_index": 490, "pixX": 22, "pixY": 219, "ToA": 13334212.5, "ToT": 55, "TriggerNo": 0 }, { "pixel_index": 510, "pixX": 21, "pixY": 218, "ToA": 13334214.0625, "ToT": 20, "TriggerNo": 0 }, { "pixel_index": 532, "pixX": 20, "pixY": 220, "ToA": 13334214.0625, "ToT": 37, "TriggerNo": 0 }, { "pixel_index": 597, "pixX": 21, "pixY": 219, "ToA": 13334214.0625, "ToT": 156, "TriggerNo": 0 }, { "pixel_index": 563, "pixX": 20, "pixY": 219, "ToA": 13334214.0625, "ToT": 133, "TriggerNo": 0 }, { "pixel_index": 498, "pixX": 21, "pixY": 220, "ToA": 13334217.1875, "ToT": 11, "TriggerNo": 0 }, { "pixel_index": 489, "pixX": 20, "pixY": 218, "ToA": 13334217.1875, "ToT": 11, "TriggerNo": 0 }, { "pixel_index": 488, "pixX": 18, "pixY": 219, "ToA": 13334217.1875, "ToT": 13, "TriggerNo": 0 }, { "pixel_index": 531, "pixX": 19, "pixY": 219, "ToA": 13334217.1875, "ToT": 27, "TriggerNo": 0 }, { "pixel_index": 670, "pixX": 19, "pixY": 220, "ToA": 13334218.75, "ToT": 151, "TriggerNo": 0 }, { "pixel_index": 596, "pixX": 18, "pixY": 220, "ToA": 13334218.75, "ToT": 147, "TriggerNo": 0 }, { "pixel_index": 480, "pixX": 19, "pixY": 221, "ToA": 13334218.75, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 509, "pixX": 18, "pixY": 221, "ToA": 13334220.3125, "ToT": 26, "TriggerNo": 0 }, { "pixel_index": 595, "pixX": 17, "pixY": 220, "ToA": 13334221.875, "ToT": 65, "TriggerNo": 0 }, { "pixel_index": 732, "pixX": 17, "pixY": 221, "ToA": 13334221.875, "ToT": 116, "TriggerNo": 0 }, { "pixel_index": 475, "pixX": 20, "pixY": 221, "ToA": 13334223.4375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 487, "pixX": 17, "pixY": 219, "ToA": 13334226.5625, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 669, "pixX": 16, "pixY": 221, "ToA": 13334228.125, "ToT": 79, "TriggerNo": 0 }, { "pixel_index": 474, "pixX": 16, "pixY": 220, "ToA": 13334228.125, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 530, "pixX": 17, "pixY": 222, "ToA": 13334229.6875, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 585, "pixX": 171, "pixY": 224, "ToA": 13334309.375, "ToT": 35, "TriggerNo": 0 }, { "pixel_index": 689, "pixX": 170, "pixY": 224, "ToA": 13334309.375, "ToT": 283, "TriggerNo": 0 }, { "pixel_index": 504, "pixX": 169, "pixY": 224, "ToA": 13334310.9375, "ToT": 138, "TriggerNo": 0 }, { "pixel_index": 523, "pixX": 168, "pixY": 222, "ToA": 13334310.9375, "ToT": 36, "TriggerNo": 0 }, { "pixel_index": 524, "pixX": 171, "pixY": 221, "ToA": 13334310.9375, "ToT": 37, "TriggerNo": 0 }, { "pixel_index": 654, "pixX": 168, "pixY": 221, "ToA": 13334310.9375, "ToT": 49, "TriggerNo": 0 }, { "pixel_index": 828, "pixX": 170, "pixY": 221, "ToA": 13334310.9375, "ToT": 265, "TriggerNo": 0 }, { "pixel_index": 619, "pixX": 171, "pixY": 222, "ToA": 13334310.9375, "ToT": 40, "TriggerNo": 0 }, { "pixel_index": 827, "pixX": 169, "pixY": 222, "ToA": 13334310.9375, "ToT": 311, "TriggerNo": 0 }, { "pixel_index": 874, "pixX": 170, "pixY": 223, "ToA": 13334310.9375, "ToT": 269, "TriggerNo": 0 }, { "pixel_index": 484, "pixX": 168, "pixY": 223, "ToA": 13334310.9375, "ToT": 26, "TriggerNo": 0 }, { "pixel_index": 800, "pixX": 169, "pixY": 221, "ToA": 13334310.9375, "ToT": 315, "TriggerNo": 0 }, { "pixel_index": 584, "pixX": 168, "pixY": 220, "ToA": 13334310.9375, "ToT": 46, "TriggerNo": 0 }, { "pixel_index": 719, "pixX": 169, "pixY": 223, "ToA": 13334310.9375, "ToT": 218, "TriggerNo": 0 }, { "pixel_index": 720, "pixX": 171, "pixY": 223, "ToA": 13334310.9375, "ToT": 38, "TriggerNo": 0 }, { "pixel_index": 853, "pixX": 170, "pixY": 222, "ToA": 13334310.9375, "ToT": 265, "TriggerNo": 0 }, { "pixel_index": 772, "pixX": 170, "pixY": 220, "ToA": 13334310.9375, "ToT": 255, "TriggerNo": 0 }, { "pixel_index": 771, "pixX": 169, "pixY": 220, "ToA": 13334310.9375, "ToT": 355, "TriggerNo": 0 }, { "pixel_index": 485, "pixX": 171, "pixY": 220, "ToA": 13334312.5, "ToT": 29, "TriggerNo": 0 }, { "pixel_index": 477, "pixX": 168, "pixY": 224, "ToA": 13334312.5, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 505, "pixX": 170, "pixY": 225, "ToA": 13334314.0625, "ToT": 21, "TriggerNo": 0 }, { "pixel_index": 506, "pixX": 172, "pixY": 221, "ToA": 13334315.625, "ToT": 3, "TriggerNo": 0 }, { "pixel_index": 583, "pixX": 167, "pixY": 222, "ToA": 13334315.625, "ToT": 3, "TriggerNo": 0 }, { "pixel_index": 688, "pixX": 168, "pixY": 219, "ToA": 13334315.625, "ToT": 22, "TriggerNo": 0 }, { "pixel_index": 503, "pixX": 167, "pixY": 220, "ToA": 13334315.625, "ToT": 3, "TriggerNo": 0 }, { "pixel_index": 748, "pixX": 169, "pixY": 219, "ToA": 13334315.625, "ToT": 186, "TriggerNo": 0 }, { "pixel_index": 801, "pixX": 170, "pixY": 219, "ToA": 13334315.625, "ToT": 98, "TriggerNo": 0 }, { "pixel_index": 550, "pixX": 167, "pixY": 221, "ToA": 13334315.625, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 551, "pixX": 169, "pixY": 218, "ToA": 13334315.625, "ToT": 17, "TriggerNo": 0 }, { "pixel_index": 525, "pixX": 172, "pixY": 222, "ToA": 13334315.625, "ToT": 3, "TriggerNo": 0 }, { "pixel_index": 749, "pixX": 171, "pixY": 219, "ToA": 13334315.625, "ToT": 11, "TriggerNo": 0 }, { "pixel_index": 553, "pixX": 172, "pixY": 223, "ToA": 13334317.1875, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 552, "pixX": 170, "pixY": 218, "ToA": 13334317.1875, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 655, "pixX": 171, "pixY": 218, "ToA": 13334317.1875, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 479, "pixX": 172, "pixY": 220, "ToA": 13334317.1875, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 476, "pixX": 167, "pixY": 223, "ToA": 13334317.1875, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 618, "pixX": 168, "pixY": 218, "ToA": 13334318.75, "ToT": 3, "TriggerNo": 0 }, { "pixel_index": 522, "pixX": 167, "pixY": 219, "ToA": 13334318.75, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 478, "pixX": 171, "pixY": 225, "ToA": 13334318.75, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 486, "pixX": 172, "pixY": 219, "ToA": 13334320.3125, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 493, "pixX": 170, "pixY": 217, "ToA": 13334325.0, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 483, "pixX": 167, "pixY": 218, "ToA": 13334325.0, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 492, "pixX": 169, "pixY": 217, "ToA": 13334326.5625, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 501, "pixX": 131, "pixY": 175, "ToA": 13337043.75, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 1074, "pixX": 146, "pixY": 211, "ToA": 13341360.9375, "ToT": 219, "TriggerNo": 0 }, { "pixel_index": 1039, "pixX": 146, "pixY": 210, "ToA": 13341360.9375, "ToT": 210, "TriggerNo": 0 }, { "pixel_index": 680, "pixX": 144, "pixY": 210, "ToA": 13341360.9375, "ToT": 14, "TriggerNo": 0 }, { "pixel_index": 1001, "pixX": 144, "pixY": 207, "ToA": 13341360.9375, "ToT": 385, "TriggerNo": 0 }, { "pixel_index": 847, "pixX": 145, "pixY": 206, "ToA": 13341360.9375, "ToT": 74, "TriggerNo": 0 }, { "pixel_index": 682, "pixX": 149, "pixY": 217, "ToA": 13341360.9375, "ToT": 26, "TriggerNo": 0 }, { "pixel_index": 685, "pixX": 154, "pixY": 221, "ToA": 13341360.9375, "ToT": 38, "TriggerNo": 0 }, { "pixel_index": 717, "pixX": 154, "pixY": 224, "ToA": 13341360.9375, "ToT": 51, "TriggerNo": 0 }, { "pixel_index": 849, "pixX": 148, "pixY": 213, "ToA": 13341360.9375, "ToT": 195, "TriggerNo": 0 }, { "pixel_index": 850, "pixX": 151, "pixY": 218, "ToA": 13341360.9375, "ToT": 277, "TriggerNo": 0 }, { "pixel_index": 746, "pixX": 157, "pixY": 226, "ToA": 13341360.9375, "ToT": 176, "TriggerNo": 0 }, { "pixel_index": 851, "pixX": 153, "pixY": 222, "ToA": 13341360.9375, "ToT": 212, "TriggerNo": 0 }, { "pixel_index": 867, "pixX": 143, "pixY": 207, "ToA": 13341360.9375, "ToT": 159, "TriggerNo": 0 }, { "pixel_index": 826, "pixX": 156, "pixY": 226, "ToA": 13341360.9375, "ToT": 235, "TriggerNo": 0 }, { "pixel_index": 825, "pixX": 155, "pixY": 224, "ToA": 13341360.9375, "ToT": 309, "TriggerNo": 0 }, { "pixel_index": 824, "pixX": 153, "pixY": 221, "ToA": 13341360.9375, "ToT": 269, "TriggerNo": 0 }, { "pixel_index": 823, "pixX": 150, "pixY": 217, "ToA": 13341360.9375, "ToT": 247, "TriggerNo": 0 }, { "pixel_index": 822, "pixX": 149, "pixY": 215, "ToA": 13341360.9375, "ToT": 254, "TriggerNo": 0 }, { "pixel_index": 821, "pixX": 147, "pixY": 211, "ToA": 13341360.9375, "ToT": 95, "TriggerNo": 0 }, { "pixel_index": 820, "pixX": 144, "pixY": 209, "ToA": 13341360.9375, "ToT": 49, "TriggerNo": 0 }, { "pixel_index": 868, "pixX": 145, "pixY": 208, "ToA": 13341360.9375, "ToT": 139, "TriggerNo": 0 }, { "pixel_index": 869, "pixX": 147, "pixY": 213, "ToA": 13341360.9375, "ToT": 118, "TriggerNo": 0 }, { "pixel_index": 711, "pixX": 142, "pixY": 209, "ToA": 13341360.9375, "ToT": 13, "TriggerNo": 0 }, { "pixel_index": 713, "pixX": 147, "pixY": 214, "ToA": 13341360.9375, "ToT": 22, "TriggerNo": 0 }, { "pixel_index": 714, "pixX": 148, "pixY": 212, "ToA": 13341360.9375, "ToT": 32, "TriggerNo": 0 }, { "pixel_index": 715, "pixX": 151, "pixY": 217, "ToA": 13341360.9375, "ToT": 68, "TriggerNo": 0 }, { "pixel_index": 716, "pixX": 152, "pixY": 218, "ToA": 13341360.9375, "ToT": 30, "TriggerNo": 0 }, { "pixel_index": 852, "pixX": 154, "pixY": 222, "ToA": 13341360.9375, "ToT": 201, "TriggerNo": 0 }, { "pixel_index": 976, "pixX": 146, "pixY": 209, "ToA": 13341360.9375, "ToT": 37, "TriggerNo": 0 }, { "pixel_index": 799, "pixX": 156, "pixY": 225, "ToA": 13341360.9375, "ToT": 239, "TriggerNo": 0 }, { "pixel_index": 975, "pixX": 144, "pixY": 208, "ToA": 13341360.9375, "ToT": 204, "TriggerNo": 0 }, { "pixel_index": 797, "pixX": 153, "pixY": 220, "ToA": 13341360.9375, "ToT": 60, "TriggerNo": 0 }, { "pixel_index": 796, "pixX": 150, "pixY": 216, "ToA": 13341360.9375, "ToT": 149, "TriggerNo": 0 }, { "pixel_index": 795, "pixX": 149, "pixY": 214, "ToA": 13341360.9375, "ToT": 84, "TriggerNo": 0 }, { "pixel_index": 794, "pixX": 146, "pixY": 212, "ToA": 13341360.9375, "ToT": 39, "TriggerNo": 0 }, { "pixel_index": 793, "pixX": 145, "pixY": 205, "ToA": 13341360.9375, "ToT": 26, "TriggerNo": 0 }, { "pixel_index": 792, "pixX": 143, "pixY": 209, "ToA": 13341360.9375, "ToT": 25, "TriggerNo": 0 }, { "pixel_index": 870, "pixX": 148, "pixY": 214, "ToA": 13341360.9375, "ToT": 230, "TriggerNo": 0 }, { "pixel_index": 872, "pixX": 152, "pixY": 220, "ToA": 13341360.9375, "ToT": 238, "TriggerNo": 0 }, { "pixel_index": 873, "pixX": 154, "pixY": 223, "ToA": 13341360.9375, "ToT": 251, "TriggerNo": 0 }, { "pixel_index": 741, "pixX": 147, "pixY": 210, "ToA": 13341360.9375, "ToT": 22, "TriggerNo": 0 }, { "pixel_index": 742, "pixX": 149, "pixY": 216, "ToA": 13341360.9375, "ToT": 136, "TriggerNo": 0 }, { "pixel_index": 652, "pixX": 158, "pixY": 228, "ToA": 13341360.9375, "ToT": 143, "TriggerNo": 0 }, { "pixel_index": 647, "pixX": 149, "pixY": 213, "ToA": 13341360.9375, "ToT": 18, "TriggerNo": 0 }, { "pixel_index": 743, "pixX": 150, "pixY": 218, "ToA": 13341360.9375, "ToT": 70, "TriggerNo": 0 }, { "pixel_index": 744, "pixX": 152, "pixY": 221, "ToA": 13341360.9375, "ToT": 67, "TriggerNo": 0 }, { "pixel_index": 886, "pixX": 143, "pixY": 208, "ToA": 13341360.9375, "ToT": 110, "TriggerNo": 0 }, { "pixel_index": 887, "pixX": 145, "pixY": 207, "ToA": 13341360.9375, "ToT": 96, "TriggerNo": 0 }, { "pixel_index": 769, "pixX": 157, "pixY": 227, "ToA": 13341360.9375, "ToT": 334, "TriggerNo": 0 }, { "pixel_index": 768, "pixX": 155, "pixY": 225, "ToA": 13341360.9375, "ToT": 142, "TriggerNo": 0 }, { "pixel_index": 767, "pixX": 152, "pixY": 219, "ToA": 13341360.9375, "ToT": 151, "TriggerNo": 0 }, { "pixel_index": 766, "pixX": 151, "pixY": 219, "ToA": 13341360.9375, "ToT": 164, "TriggerNo": 0 }, { "pixel_index": 765, "pixX": 148, "pixY": 215, "ToA": 13341360.9375, "ToT": 40, "TriggerNo": 0 }, { "pixel_index": 763, "pixX": 145, "pixY": 211, "ToA": 13341360.9375, "ToT": 24, "TriggerNo": 0 }, { "pixel_index": 888, "pixX": 146, "pixY": 208, "ToA": 13341360.9375, "ToT": 21, "TriggerNo": 0 }, { "pixel_index": 905, "pixX": 145, "pixY": 209, "ToA": 13341360.9375, "ToT": 265, "TriggerNo": 0 }, { "pixel_index": 952, "pixX": 147, "pixY": 212, "ToA": 13341360.9375, "ToT": 246, "TriggerNo": 0 }, { "pixel_index": 951, "pixX": 144, "pixY": 206, "ToA": 13341360.9375, "ToT": 326, "TriggerNo": 0 }, { "pixel_index": 920, "pixX": 144, "pixY": 205, "ToA": 13341360.9375, "ToT": 76, "TriggerNo": 0 }, { "pixel_index": 798, "pixX": 155, "pixY": 223, "ToA": 13341360.9375, "ToT": 84, "TriggerNo": 0 }, { "pixel_index": 818, "pixX": 141, "pixY": 204, "ToA": 13341362.5, "ToT": 19, "TriggerNo": 0 }, { "pixel_index": 904, "pixX": 142, "pixY": 205, "ToA": 13341362.5, "ToT": 356, "TriggerNo": 0 }, { "pixel_index": 885, "pixX": 141, "pixY": 207, "ToA": 13341362.5, "ToT": 38, "TriggerNo": 0 }, { "pixel_index": 866, "pixX": 141, "pixY": 206, "ToA": 13341362.5, "ToT": 133, "TriggerNo": 0 }, { "pixel_index": 846, "pixX": 142, "pixY": 208, "ToA": 13341362.5, "ToT": 41, "TriggerNo": 0 }, { "pixel_index": 845, "pixX": 141, "pixY": 205, "ToA": 13341362.5, "ToT": 128, "TriggerNo": 0 }, { "pixel_index": 541, "pixX": 143, "pixY": 204, "ToA": 13341362.5, "ToT": 34, "TriggerNo": 0 }, { "pixel_index": 683, "pixX": 150, "pixY": 215, "ToA": 13341362.5, "ToT": 29, "TriggerNo": 0 }, { "pixel_index": 764, "pixX": 146, "pixY": 206, "ToA": 13341362.5, "ToT": 13, "TriggerNo": 0 }, { "pixel_index": 738, "pixX": 140, "pixY": 206, "ToA": 13341362.5, "ToT": 17, "TriggerNo": 0 }, { "pixel_index": 739, "pixX": 142, "pixY": 207, "ToA": 13341362.5, "ToT": 111, "TriggerNo": 0 }, { "pixel_index": 684, "pixX": 153, "pixY": 223, "ToA": 13341362.5, "ToT": 39, "TriggerNo": 0 }, { "pixel_index": 848, "pixX": 146, "pixY": 207, "ToA": 13341362.5, "ToT": 17, "TriggerNo": 0 }, { "pixel_index": 937, "pixX": 143, "pixY": 206, "ToA": 13341362.5, "ToT": 385, "TriggerNo": 0 }, { "pixel_index": 919, "pixX": 142, "pixY": 206, "ToA": 13341362.5, "ToT": 421, "TriggerNo": 0 }, { "pixel_index": 819, "pixX": 143, "pixY": 205, "ToA": 13341362.5, "ToT": 149, "TriggerNo": 0 }, { "pixel_index": 643, "pixX": 140, "pixY": 205, "ToA": 13341364.0625, "ToT": 15, "TriggerNo": 0 }, { "pixel_index": 651, "pixX": 157, "pixY": 225, "ToA": 13341364.0625, "ToT": 33, "TriggerNo": 0 }, { "pixel_index": 644, "pixX": 142, "pixY": 204, "ToA": 13341364.0625, "ToT": 52, "TriggerNo": 0 }, { "pixel_index": 712, "pixX": 144, "pixY": 204, "ToA": 13341364.0625, "ToT": 16, "TriggerNo": 0 }, { "pixel_index": 718, "pixX": 156, "pixY": 227, "ToA": 13341364.0625, "ToT": 45, "TriggerNo": 0 }, { "pixel_index": 607, "pixX": 141, "pixY": 208, "ToA": 13341364.0625, "ToT": 16, "TriggerNo": 0 }, { "pixel_index": 710, "pixX": 141, "pixY": 209, "ToA": 13341364.0625, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 612, "pixX": 151, "pixY": 220, "ToA": 13341364.0625, "ToT": 28, "TriggerNo": 0 }, { "pixel_index": 938, "pixX": 145, "pixY": 210, "ToA": 13341364.0625, "ToT": 100, "TriggerNo": 0 }, { "pixel_index": 686, "pixX": 156, "pixY": 224, "ToA": 13341364.0625, "ToT": 42, "TriggerNo": 0 }, { "pixel_index": 791, "pixX": 140, "pixY": 207, "ToA": 13341364.0625, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 648, "pixX": 150, "pixY": 219, "ToA": 13341365.625, "ToT": 16, "TriggerNo": 0 }, { "pixel_index": 608, "pixX": 143, "pixY": 210, "ToA": 13341365.625, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 540, "pixX": 140, "pixY": 204, "ToA": 13341365.625, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 650, "pixX": 155, "pixY": 226, "ToA": 13341365.625, "ToT": 28, "TriggerNo": 0 }, { "pixel_index": 545, "pixX": 151, "pixY": 216, "ToA": 13341365.625, "ToT": 15, "TriggerNo": 0 }, { "pixel_index": 761, "pixX": 140, "pixY": 208, "ToA": 13341365.625, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 610, "pixX": 146, "pixY": 213, "ToA": 13341365.625, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 609, "pixX": 145, "pixY": 204, "ToA": 13341365.625, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 613, "pixX": 152, "pixY": 222, "ToA": 13341367.1875, "ToT": 19, "TriggerNo": 0 }, { "pixel_index": 762, "pixX": 143, "pixY": 203, "ToA": 13341367.1875, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 681, "pixX": 146, "pixY": 205, "ToA": 13341367.1875, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 745, "pixX": 155, "pixY": 222, "ToA": 13341367.1875, "ToT": 24, "TriggerNo": 0 }, { "pixel_index": 611, "pixX": 148, "pixY": 216, "ToA": 13341367.1875, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 679, "pixX": 142, "pixY": 203, "ToA": 13341367.1875, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 577, "pixX": 148, "pixY": 211, "ToA": 13341368.75, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 740, "pixX": 144, "pixY": 203, "ToA": 13341368.75, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 573, "pixX": 140, "pixY": 203, "ToA": 13341368.75, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 678, "pixX": 141, "pixY": 203, "ToA": 13341368.75, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 646, "pixX": 147, "pixY": 209, "ToA": 13341368.75, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 616, "pixX": 158, "pixY": 226, "ToA": 13341370.3125, "ToT": 17, "TriggerNo": 0 }, { "pixel_index": 512, "pixX": 140, "pixY": 209, "ToA": 13341370.3125, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 513, "pixX": 142, "pixY": 210, "ToA": 13341370.3125, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 642, "pixX": 139, "pixY": 207, "ToA": 13341370.3125, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 572, "pixX": 139, "pixY": 205, "ToA": 13341370.3125, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 649, "pixX": 153, "pixY": 219, "ToA": 13341370.3125, "ToT": 13, "TriggerNo": 0 }, { "pixel_index": 606, "pixX": 139, "pixY": 206, "ToA": 13341370.3125, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 539, "pixX": 139, "pixY": 204, "ToA": 13341371.875, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 576, "pixX": 146, "pixY": 204, "ToA": 13341371.875, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 580, "pixX": 154, "pixY": 225, "ToA": 13341371.875, "ToT": 14, "TriggerNo": 0 }, { "pixel_index": 1002, "pixX": 147, "pixY": 207, "ToA": 13341371.875, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 615, "pixX": 157, "pixY": 228, "ToA": 13341371.875, "ToT": 32, "TriggerNo": 0 }, { "pixel_index": 574, "pixX": 142, "pixY": 202, "ToA": 13341371.875, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 543, "pixX": 147, "pixY": 208, "ToA": 13341373.4375, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 645, "pixX": 145, "pixY": 203, "ToA": 13341373.4375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 511, "pixX": 139, "pixY": 208, "ToA": 13341375.0, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 578, "pixX": 150, "pixY": 214, "ToA": 13341375.0, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 579, "pixX": 152, "pixY": 217, "ToA": 13341375.0, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 542, "pixX": 145, "pixY": 212, "ToA": 13341376.5625, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 575, "pixX": 144, "pixY": 211, "ToA": 13341376.5625, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 581, "pixX": 156, "pixY": 223, "ToA": 13341378.125, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 548, "pixX": 157, "pixY": 224, "ToA": 13341379.6875, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 614, "pixX": 154, "pixY": 220, "ToA": 13341379.6875, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 518, "pixX": 153, "pixY": 224, "ToA": 13341379.6875, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 516, "pixX": 149, "pixY": 218, "ToA": 13341379.6875, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 687, "pixX": 158, "pixY": 227, "ToA": 13341381.25, "ToT": 28, "TriggerNo": 0 }, { "pixel_index": 517, "pixX": 151, "pixY": 221, "ToA": 13341381.25, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 906, "pixX": 147, "pixY": 206, "ToA": 13341381.25, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 515, "pixX": 147, "pixY": 215, "ToA": 13341381.25, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 519, "pixX": 155, "pixY": 227, "ToA": 13341384.375, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 520, "pixX": 156, "pixY": 228, "ToA": 13341384.375, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 547, "pixX": 155, "pixY": 221, "ToA": 13341385.9375, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 546, "pixX": 152, "pixY": 223, "ToA": 13341390.625, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 521, "pixX": 158, "pixY": 229, "ToA": 13341392.1875, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 549, "pixX": 158, "pixY": 225, "ToA": 13341393.75, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 582, "pixX": 159, "pixY": 228, "ToA": 13341393.75, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 544, "pixX": 149, "pixY": 212, "ToA": 13341395.3125, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 758, "pixX": 218, "pixY": 201, "ToA": 13341670.3125, "ToT": 150, "TriggerNo": 0 }, { "pixel_index": 780, "pixX": 218, "pixY": 202, "ToA": 13341670.3125, "ToT": 82, "TriggerNo": 0 }, { "pixel_index": 590, "pixX": 215, "pixY": 201, "ToA": 13341671.875, "ToT": 28, "TriggerNo": 0 }, { "pixel_index": 809, "pixX": 218, "pixY": 203, "ToA": 13341671.875, "ToT": 26, "TriggerNo": 0 }, { "pixel_index": 730, "pixX": 218, "pixY": 200, "ToA": 13341671.875, "ToT": 28, "TriggerNo": 0 }, { "pixel_index": 808, "pixX": 217, "pixY": 203, "ToA": 13341671.875, "ToT": 72, "TriggerNo": 0 }, { "pixel_index": 729, "pixX": 217, "pixY": 200, "ToA": 13341671.875, "ToT": 41, "TriggerNo": 0 }, { "pixel_index": 835, "pixX": 216, "pixY": 201, "ToA": 13341671.875, "ToT": 174, "TriggerNo": 0 }, { "pixel_index": 860, "pixX": 216, "pixY": 202, "ToA": 13341671.875, "ToT": 225, "TriggerNo": 0 }, { "pixel_index": 624, "pixX": 215, "pixY": 202, "ToA": 13341671.875, "ToT": 35, "TriggerNo": 0 }, { "pixel_index": 779, "pixX": 217, "pixY": 202, "ToA": 13341671.875, "ToT": 254, "TriggerNo": 0 }, { "pixel_index": 664, "pixX": 216, "pixY": 203, "ToA": 13341671.875, "ToT": 41, "TriggerNo": 0 }, { "pixel_index": 757, "pixX": 217, "pixY": 201, "ToA": 13341671.875, "ToT": 74, "TriggerNo": 0 }, { "pixel_index": 559, "pixX": 216, "pixY": 200, "ToA": 13341671.875, "ToT": 29, "TriggerNo": 0 }, { "pixel_index": 665, "pixX": 219, "pixY": 201, "ToA": 13341675.0, "ToT": 21, "TriggerNo": 0 }, { "pixel_index": 699, "pixX": 219, "pixY": 202, "ToA": 13341676.5625, "ToT": 16, "TriggerNo": 0 }, { "pixel_index": 663, "pixX": 215, "pixY": 203, "ToA": 13341681.25, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 558, "pixX": 215, "pixY": 200, "ToA": 13341682.8125, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 625, "pixX": 217, "pixY": 204, "ToA": 13341682.8125, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 698, "pixX": 217, "pixY": 199, "ToA": 13341684.375, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 626, "pixX": 219, "pixY": 200, "ToA": 13341685.9375, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 527, "pixX": 216, "pixY": 204, "ToA": 13341687.5, "ToT": 8, "TriggerNo": 0 }, { "pixel_index": 560, "pixX": 219, "pixY": 203, "ToA": 13341689.0625, "ToT": 8, "TriggerNo": 0 }, { "pixel_index": 591, "pixX": 216, "pixY": 199, "ToA": 13341689.0625, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 592, "pixX": 218, "pixY": 199, "ToA": 13341692.1875, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 528, "pixX": 218, "pixY": 204, "ToA": 13341696.875, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 526, "pixX": 214, "pixY": 202, "ToA": 13341721.875, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 571, "pixX": 122, "pixY": 125, "ToA": 13342128.125, "ToT": 11, "TriggerNo": 0 }, { "pixel_index": 641, "pixX": 123, "pixY": 125, "ToA": 13342128.125, "ToT": 165, "TriggerNo": 0 }, { "pixel_index": 537, "pixX": 123, "pixY": 126, "ToA": 13342128.125, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 605, "pixX": 123, "pixY": 124, "ToA": 13342131.25, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 538, "pixX": 124, "pixY": 125, "ToA": 13342132.8125, "ToT": 8, "TriggerNo": 0 }, { "pixel_index": 601, "pixX": 96, "pixY": 72, "ToA": 13343065.625, "ToT": 27, "TriggerNo": 0 }, { "pixel_index": 536, "pixX": 97, "pixY": 72, "ToA": 13343065.625, "ToT": 16, "TriggerNo": 0 }, { "pixel_index": 565, "pixX": 75, "pixY": 168, "ToA": 13343068.75, "ToT": 90, "TriggerNo": 0 }, { "pixel_index": 708, "pixX": 96, "pixY": 71, "ToA": 13343068.75, "ToT": 17, "TriggerNo": 0 }, { "pixel_index": 674, "pixX": 75, "pixY": 167, "ToA": 13343068.75, "ToT": 27, "TriggerNo": 0 }, { "pixel_index": 636, "pixX": 75, "pixY": 166, "ToA": 13343068.75, "ToT": 27, "TriggerNo": 0 }, { "pixel_index": 599, "pixX": 75, "pixY": 165, "ToA": 13343070.3125, "ToT": 21, "TriggerNo": 0 }, { "pixel_index": 675, "pixX": 96, "pixY": 70, "ToA": 13343070.3125, "ToT": 30, "TriggerNo": 0 }, { "pixel_index": 566, "pixX": 90, "pixY": 76, "ToA": 13343071.875, "ToT": 8, "TriggerNo": 0 }, { "pixel_index": 707, "pixX": 74, "pixY": 165, "ToA": 13343071.875, "ToT": 58, "TriggerNo": 0 }, { "pixel_index": 637, "pixX": 96, "pixY": 69, "ToA": 13343073.4375, "ToT": 18, "TriggerNo": 0 }, { "pixel_index": 736, "pixX": 97, "pixY": 69, "ToA": 13343076.5625, "ToT": 31, "TriggerNo": 0 }, { "pixel_index": 567, "pixX": 97, "pixY": 70, "ToA": 13343078.125, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 534, "pixX": 74, "pixY": 166, "ToA": 13343084.375, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 564, "pixX": 73, "pixY": 165, "ToA": 13343087.5, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 533, "pixX": 73, "pixY": 191, "ToA": 13343096.875, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 535, "pixX": 90, "pixY": 77, "ToA": 13343110.9375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 775, "pixX": 209, "pixY": 254, "ToA": 13352123.4375, "ToT": 131, "TriggerNo": 0 }, { "pixel_index": 754, "pixX": 211, "pixY": 253, "ToA": 13352123.4375, "ToT": 110, "TriggerNo": 0 }, { "pixel_index": 753, "pixX": 209, "pixY": 252, "ToA": 13352123.4375, "ToT": 36, "TriggerNo": 0 }, { "pixel_index": 737, "pixX": 107, "pixY": 127, "ToA": 13352123.4375, "ToT": 184, "TriggerNo": 0 }, { "pixel_index": 726, "pixX": 210, "pixY": 255, "ToA": 13352123.4375, "ToT": 23, "TriggerNo": 0 }, { "pixel_index": 709, "pixX": 107, "pixY": 128, "ToA": 13352123.4375, "ToT": 94, "TriggerNo": 0 }, { "pixel_index": 694, "pixX": 208, "pixY": 252, "ToA": 13352123.4375, "ToT": 34, "TriggerNo": 0 }, { "pixel_index": 676, "pixX": 107, "pixY": 126, "ToA": 13352123.4375, "ToT": 17, "TriggerNo": 0 }, { "pixel_index": 661, "pixX": 210, "pixY": 252, "ToA": 13352123.4375, "ToT": 23, "TriggerNo": 0 }, { "pixel_index": 638, "pixX": 106, "pixY": 128, "ToA": 13352123.4375, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 621, "pixX": 209, "pixY": 255, "ToA": 13352123.4375, "ToT": 15, "TriggerNo": 0 }, { "pixel_index": 602, "pixX": 106, "pixY": 127, "ToA": 13352123.4375, "ToT": 14, "TriggerNo": 0 }, { "pixel_index": 568, "pixX": 107, "pixY": 129, "ToA": 13352123.4375, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 857, "pixX": 210, "pixY": 254, "ToA": 13352123.4375, "ToT": 189, "TriggerNo": 0 }, { "pixel_index": 856, "pixX": 209, "pixY": 253, "ToA": 13352123.4375, "ToT": 274, "TriggerNo": 0 }, { "pixel_index": 832, "pixX": 210, "pixY": 253, "ToA": 13352123.4375, "ToT": 186, "TriggerNo": 0 }, { "pixel_index": 831, "pixX": 208, "pixY": 254, "ToA": 13352123.4375, "ToT": 90, "TriggerNo": 0 }, { "pixel_index": 805, "pixX": 211, "pixY": 255, "ToA": 13352123.4375, "ToT": 31, "TriggerNo": 0 }, { "pixel_index": 804, "pixX": 208, "pixY": 253, "ToA": 13352123.4375, "ToT": 306, "TriggerNo": 0 }, { "pixel_index": 776, "pixX": 211, "pixY": 254, "ToA": 13352123.4375, "ToT": 286, "TriggerNo": 0 }, { "pixel_index": 569, "pixX": 108, "pixY": 128, "ToA": 13352125.0, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 588, "pixX": 211, "pixY": 252, "ToA": 13352125.0, "ToT": 13, "TriggerNo": 0 }, { "pixel_index": 724, "pixX": 207, "pixY": 253, "ToA": 13352125.0, "ToT": 28, "TriggerNo": 0 }, { "pixel_index": 693, "pixX": 207, "pixY": 252, "ToA": 13352125.0, "ToT": 8, "TriggerNo": 0 }, { "pixel_index": 752, "pixX": 207, "pixY": 254, "ToA": 13352125.0, "ToT": 13, "TriggerNo": 0 }, { "pixel_index": 639, "pixX": 108, "pixY": 127, "ToA": 13352125.0, "ToT": 19, "TriggerNo": 0 }, { "pixel_index": 774, "pixX": 207, "pixY": 255, "ToA": 13352125.0, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 555, "pixX": 208, "pixY": 255, "ToA": 13352125.0, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 623, "pixX": 212, "pixY": 252, "ToA": 13352126.5625, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 622, "pixX": 210, "pixY": 251, "ToA": 13352126.5625, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 777, "pixX": 212, "pixY": 254, "ToA": 13352126.5625, "ToT": 290, "TriggerNo": 0 }, { "pixel_index": 620, "pixX": 206, "pixY": 253, "ToA": 13352126.5625, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 727, "pixX": 213, "pixY": 255, "ToA": 13352126.5625, "ToT": 59, "TriggerNo": 0 }, { "pixel_index": 725, "pixX": 209, "pixY": 251, "ToA": 13352126.5625, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 833, "pixX": 213, "pixY": 254, "ToA": 13352126.5625, "ToT": 270, "TriggerNo": 0 }, { "pixel_index": 587, "pixX": 208, "pixY": 251, "ToA": 13352126.5625, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 755, "pixX": 212, "pixY": 253, "ToA": 13352126.5625, "ToT": 56, "TriggerNo": 0 }, { "pixel_index": 806, "pixX": 212, "pixY": 255, "ToA": 13352126.5625, "ToT": 39, "TriggerNo": 0 }, { "pixel_index": 695, "pixX": 211, "pixY": 251, "ToA": 13352126.5625, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 696, "pixX": 213, "pixY": 253, "ToA": 13352126.5625, "ToT": 32, "TriggerNo": 0 }, { "pixel_index": 659, "pixX": 206, "pixY": 254, "ToA": 13352126.5625, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 557, "pixX": 213, "pixY": 252, "ToA": 13352128.125, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 728, "pixX": 214, "pixY": 253, "ToA": 13352129.6875, "ToT": 8, "TriggerNo": 0 }, { "pixel_index": 556, "pixX": 210, "pixY": 250, "ToA": 13352129.6875, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 859, "pixX": 214, "pixY": 254, "ToA": 13352129.6875, "ToT": 72, "TriggerNo": 0 }, { "pixel_index": 586, "pixX": 207, "pixY": 251, "ToA": 13352129.6875, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 554, "pixX": 206, "pixY": 252, "ToA": 13352129.6875, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 589, "pixX": 212, "pixY": 251, "ToA": 13352131.25, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 756, "pixX": 214, "pixY": 255, "ToA": 13352131.25, "ToT": 13, "TriggerNo": 0 }, { "pixel_index": 660, "pixX": 209, "pixY": 250, "ToA": 13352131.25, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 662, "pixX": 213, "pixY": 251, "ToA": 13352131.25, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 697, "pixX": 214, "pixY": 252, "ToA": 13352132.8125, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 603, "pixX": 108, "pixY": 126, "ToA": 13352132.8125, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 807, "pixX": 215, "pixY": 254, "ToA": 13352134.375, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 778, "pixX": 215, "pixY": 253, "ToA": 13352137.5, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 834, "pixX": 215, "pixY": 255, "ToA": 13352137.5, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 604, "pixX": 114, "pixY": 197, "ToA": 13354754.6875, "ToT": 35, "TriggerNo": 0 }, { "pixel_index": 570, "pixX": 114, "pixY": 196, "ToA": 13354754.6875, "ToT": 32, "TriggerNo": 0 }, { "pixel_index": 598, "pixX": 62, "pixY": 146, "ToA": 13355150.0, "ToT": 14, "TriggerNo": 0 }, { "pixel_index": 635, "pixX": 62, "pixY": 147, "ToA": 13355150.0, "ToT": 18, "TriggerNo": 0 }, { "pixel_index": 594, "pixX": 14, "pixY": 167, "ToA": 13355590.625, "ToT": 30, "TriggerNo": 0 }, { "pixel_index": 593, "pixX": 13, "pixY": 167, "ToA": 13355592.1875, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 600, "pixX": 95, "pixY": 0, "ToA": 13359623.4375, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 627, "pixX": 232, "pixY": 139, "ToA": 13362314.0625, "ToT": 23, "TriggerNo": 0 }, { "pixel_index": 617, "pixX": 167, "pixY": 203, "ToA": 13367293.75, "ToT": 21, "TriggerNo": 0 }, { "pixel_index": 653, "pixX": 167, "pixY": 202, "ToA": 13367293.75, "ToT": 29, "TriggerNo": 0 }, { "pixel_index": 634, "pixX": 46, "pixY": 7, "ToA": 13367454.6875, "ToT": 27, "TriggerNo": 0 }, { "pixel_index": 673, "pixX": 46, "pixY": 6, "ToA": 13367454.6875, "ToT": 58, "TriggerNo": 0 }, { "pixel_index": 672, "pixX": 45, "pixY": 6, "ToA": 13367459.375, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 633, "pixX": 45, "pixY": 7, "ToA": 13367460.9375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 701, "pixX": 11, "pixY": 208, "ToA": 13369740.625, "ToT": 84, "TriggerNo": 0 }, { "pixel_index": 759, "pixX": 12, "pixY": 208, "ToA": 13369740.625, "ToT": 38, "TriggerNo": 0 }, { "pixel_index": 630, "pixX": 14, "pixY": 207, "ToA": 13369742.1875, "ToT": 14, "TriggerNo": 0 }, { "pixel_index": 668, "pixX": 13, "pixY": 207, "ToA": 13369742.1875, "ToT": 17, "TriggerNo": 0 }, { "pixel_index": 731, "pixX": 12, "pixY": 207, "ToA": 13369742.1875, "ToT": 18, "TriggerNo": 0 }, { "pixel_index": 667, "pixX": 10, "pixY": 208, "ToA": 13369745.3125, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 702, "pixX": 12, "pixY": 209, "ToA": 13369751.5625, "ToT": 22, "TriggerNo": 0 }, { "pixel_index": 628, "pixX": 11, "pixY": 209, "ToA": 13369751.5625, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 629, "pixX": 13, "pixY": 208, "ToA": 13369753.125, "ToT": 18, "TriggerNo": 0 }, { "pixel_index": 784, "pixX": 13, "pixY": 209, "ToA": 13369753.125, "ToT": 75, "TriggerNo": 0 }, { "pixel_index": 677, "pixX": 117, "pixY": 222, "ToA": 13369901.5625, "ToT": 88, "TriggerNo": 0 }, { "pixel_index": 640, "pixX": 117, "pixY": 223, "ToA": 13369904.6875, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 855, "pixX": 184, "pixY": 126, "ToA": 13375382.8125, "ToT": 295, "TriggerNo": 0 }, { "pixel_index": 803, "pixX": 185, "pixY": 126, "ToA": 13375382.8125, "ToT": 64, "TriggerNo": 0 }, { "pixel_index": 830, "pixX": 184, "pixY": 125, "ToA": 13375382.8125, "ToT": 140, "TriggerNo": 0 }, { "pixel_index": 877, "pixX": 185, "pixY": 125, "ToA": 13375382.8125, "ToT": 272, "TriggerNo": 0 }, { "pixel_index": 751, "pixX": 184, "pixY": 127, "ToA": 13375382.8125, "ToT": 35, "TriggerNo": 0 }, { "pixel_index": 721, "pixX": 183, "pixY": 126, "ToA": 13375384.375, "ToT": 33, "TriggerNo": 0 }, { "pixel_index": 690, "pixX": 183, "pixY": 125, "ToA": 13375384.375, "ToT": 16, "TriggerNo": 0 }, { "pixel_index": 657, "pixX": 185, "pixY": 127, "ToA": 13375385.9375, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 722, "pixX": 184, "pixY": 124, "ToA": 13375387.5, "ToT": 21, "TriggerNo": 0 }, { "pixel_index": 750, "pixX": 183, "pixY": 127, "ToA": 13375387.5, "ToT": 8, "TriggerNo": 0 }, { "pixel_index": 773, "pixX": 185, "pixY": 124, "ToA": 13375389.0625, "ToT": 76, "TriggerNo": 0 }, { "pixel_index": 658, "pixX": 186, "pixY": 126, "ToA": 13375390.625, "ToT": 8, "TriggerNo": 0 }, { "pixel_index": 723, "pixX": 186, "pixY": 125, "ToA": 13375390.625, "ToT": 29, "TriggerNo": 0 }, { "pixel_index": 692, "pixX": 186, "pixY": 124, "ToA": 13375392.1875, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 656, "pixX": 183, "pixY": 124, "ToA": 13375395.3125, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 691, "pixX": 185, "pixY": 123, "ToA": 13375400.0, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 666, "pixX": 255, "pixY": 82, "ToA": 13377056.25, "ToT": 87, "TriggerNo": 0 }, { "pixel_index": 671, "pixX": 43, "pixY": 34, "ToA": 13378843.75, "ToT": 58, "TriggerNo": 0 }, { "pixel_index": 700, "pixX": 4, "pixY": 232, "ToA": 13383142.1875, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 706, "pixX": 48, "pixY": 110, "ToA": 13384225.0, "ToT": 18, "TriggerNo": 0 }, { "pixel_index": 734, "pixX": 48, "pixY": 109, "ToA": 13384225.0, "ToT": 24, "TriggerNo": 0 }, { "pixel_index": 733, "pixX": 47, "pixY": 109, "ToA": 13384226.5625, "ToT": 13, "TriggerNo": 0 }, { "pixel_index": 705, "pixX": 47, "pixY": 110, "ToA": 13384228.125, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 735, "pixX": 70, "pixY": 218, "ToA": 13392317.1875, "ToT": 13, "TriggerNo": 0 }, { "pixel_index": 747, "pixX": 166, "pixY": 247, "ToA": 13395964.0625, "ToT": 68, "TriggerNo": 0 }, { "pixel_index": 770, "pixX": 167, "pixY": 247, "ToA": 13395964.0625, "ToT": 91, "TriggerNo": 0 }, { "pixel_index": 879, "pixX": 2, "pixY": 122, "ToA": 13405675.0, "ToT": 36, "TriggerNo": 0 }, { "pixel_index": 898, "pixX": 3, "pixY": 123, "ToA": 13405675.0, "ToT": 21, "TriggerNo": 0 }, { "pixel_index": 838, "pixX": 4, "pixY": 125, "ToA": 13405676.5625, "ToT": 14, "TriggerNo": 0 }, { "pixel_index": 782, "pixX": 2, "pixY": 131, "ToA": 13405676.5625, "ToT": 15, "TriggerNo": 0 }, { "pixel_index": 813, "pixX": 4, "pixY": 124, "ToA": 13405679.6875, "ToT": 11, "TriggerNo": 0 }, { "pixel_index": 861, "pixX": 2, "pixY": 121, "ToA": 13405679.6875, "ToT": 18, "TriggerNo": 0 }, { "pixel_index": 812, "pixX": 3, "pixY": 124, "ToA": 13405681.25, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 811, "pixX": 1, "pixY": 120, "ToA": 13405685.9375, "ToT": 40, "TriggerNo": 0 }, { "pixel_index": 785, "pixX": 56, "pixY": 151, "ToA": 13405696.875, "ToT": 13, "TriggerNo": 0 }, { "pixel_index": 783, "pixX": 4, "pixY": 126, "ToA": 13405698.4375, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 781, "pixX": 1, "pixY": 121, "ToA": 13405698.4375, "ToT": 3, "TriggerNo": 0 }, { "pixel_index": 837, "pixX": 2, "pixY": 120, "ToA": 13405739.0625, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 815, "pixX": 131, "pixY": 146, "ToA": 13405795.3125, "ToT": 24, "TriggerNo": 0 }, { "pixel_index": 788, "pixX": 131, "pixY": 145, "ToA": 13405806.25, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 878, "pixX": 213, "pixY": 208, "ToA": 13406453.125, "ToT": 103, "TriggerNo": 0 }, { "pixel_index": 858, "pixX": 213, "pixY": 209, "ToA": 13406459.375, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 787, "pixX": 98, "pixY": 194, "ToA": 13407465.625, "ToT": 54, "TriggerNo": 0 }, { "pixel_index": 816, "pixX": 132, "pixY": 200, "ToA": 13407467.1875, "ToT": 21, "TriggerNo": 0 }, { "pixel_index": 865, "pixX": 133, "pixY": 200, "ToA": 13407467.1875, "ToT": 84, "TriggerNo": 0 }, { "pixel_index": 843, "pixX": 132, "pixY": 201, "ToA": 13407467.1875, "ToT": 20, "TriggerNo": 0 }, { "pixel_index": 786, "pixX": 97, "pixY": 194, "ToA": 13407467.1875, "ToT": 42, "TriggerNo": 0 }, { "pixel_index": 817, "pixX": 134, "pixY": 199, "ToA": 13407467.1875, "ToT": 29, "TriggerNo": 0 }, { "pixel_index": 844, "pixX": 134, "pixY": 198, "ToA": 13407467.1875, "ToT": 49, "TriggerNo": 0 }, { "pixel_index": 789, "pixX": 133, "pixY": 201, "ToA": 13407467.1875, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 790, "pixX": 134, "pixY": 200, "ToA": 13407471.875, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 829, "pixX": 175, "pixY": 187, "ToA": 13411043.75, "ToT": 26, "TriggerNo": 0 }, { "pixel_index": 854, "pixX": 175, "pixY": 186, "ToA": 13411043.75, "ToT": 54, "TriggerNo": 0 }, { "pixel_index": 802, "pixX": 175, "pixY": 185, "ToA": 13411054.6875, "ToT": 18, "TriggerNo": 0 }, { "pixel_index": 810, "pixX": 251, "pixY": 137, "ToA": 13413071.875, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 1122, "pixX": 151, "pixY": 143, "ToA": 13414773.4375, "ToT": 247, "TriggerNo": 0 }, { "pixel_index": 1111, "pixX": 151, "pixY": 142, "ToA": 13414773.4375, "ToT": 317, "TriggerNo": 0 }, { "pixel_index": 1004, "pixX": 151, "pixY": 141, "ToA": 13414773.4375, "ToT": 63, "TriggerNo": 0 }, { "pixel_index": 1041, "pixX": 150, "pixY": 141, "ToA": 13414773.4375, "ToT": 134, "TriggerNo": 0 }, { "pixel_index": 1076, "pixX": 150, "pixY": 142, "ToA": 13414773.4375, "ToT": 430, "TriggerNo": 0 }, { "pixel_index": 1102, "pixX": 150, "pixY": 143, "ToA": 13414773.4375, "ToT": 423, "TriggerNo": 0 }, { "pixel_index": 941, "pixX": 150, "pixY": 140, "ToA": 13414775.0, "ToT": 17, "TriggerNo": 0 }, { "pixel_index": 1101, "pixX": 149, "pixY": 141, "ToA": 13414775.0, "ToT": 47, "TriggerNo": 0 }, { "pixel_index": 1110, "pixX": 149, "pixY": 142, "ToA": 13414775.0, "ToT": 222, "TriggerNo": 0 }, { "pixel_index": 1075, "pixX": 149, "pixY": 144, "ToA": 13414775.0, "ToT": 31, "TriggerNo": 0 }, { "pixel_index": 1057, "pixX": 148, "pixY": 143, "ToA": 13414775.0, "ToT": 21, "TriggerNo": 0 }, { "pixel_index": 1121, "pixX": 149, "pixY": 143, "ToA": 13414775.0, "ToT": 159, "TriggerNo": 0 }, { "pixel_index": 1112, "pixX": 152, "pixY": 143, "ToA": 13414775.0, "ToT": 33, "TriggerNo": 0 }, { "pixel_index": 1026, "pixX": 150, "pixY": 144, "ToA": 13414775.0, "ToT": 77, "TriggerNo": 0 }, { "pixel_index": 1025, "pixX": 148, "pixY": 142, "ToA": 13414775.0, "ToT": 26, "TriggerNo": 0 }, { "pixel_index": 1040, "pixX": 148, "pixY": 144, "ToA": 13414775.0, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 1091, "pixX": 152, "pixY": 141, "ToA": 13414775.0, "ToT": 14, "TriggerNo": 0 }, { "pixel_index": 1103, "pixX": 152, "pixY": 142, "ToA": 13414775.0, "ToT": 46, "TriggerNo": 0 }, { "pixel_index": 1090, "pixX": 151, "pixY": 144, "ToA": 13414775.0, "ToT": 41, "TriggerNo": 0 }, { "pixel_index": 953, "pixX": 148, "pixY": 141, "ToA": 13414776.5625, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 1077, "pixX": 152, "pixY": 140, "ToA": 13414776.5625, "ToT": 3, "TriggerNo": 0 }, { "pixel_index": 923, "pixX": 151, "pixY": 145, "ToA": 13414776.5625, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 1089, "pixX": 149, "pixY": 140, "ToA": 13414776.5625, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 940, "pixX": 149, "pixY": 145, "ToA": 13414776.5625, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 1042, "pixX": 153, "pixY": 143, "ToA": 13414776.5625, "ToT": 3, "TriggerNo": 0 }, { "pixel_index": 890, "pixX": 151, "pixY": 140, "ToA": 13414776.5625, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 1058, "pixX": 150, "pixY": 145, "ToA": 13414776.5625, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 1027, "pixX": 152, "pixY": 144, "ToA": 13414776.5625, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 1059, "pixX": 152, "pixY": 145, "ToA": 13414778.125, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 942, "pixX": 153, "pixY": 144, "ToA": 13414778.125, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 1005, "pixX": 153, "pixY": 142, "ToA": 13414778.125, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 955, "pixX": 153, "pixY": 141, "ToA": 13414779.6875, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 889, "pixX": 148, "pixY": 145, "ToA": 13414779.6875, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 907, "pixX": 148, "pixY": 140, "ToA": 13414779.6875, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 1088, "pixX": 147, "pixY": 143, "ToA": 13414781.25, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 1056, "pixX": 147, "pixY": 142, "ToA": 13414781.25, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 908, "pixX": 150, "pixY": 139, "ToA": 13414781.25, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 954, "pixX": 151, "pixY": 139, "ToA": 13414781.25, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 1024, "pixX": 147, "pixY": 141, "ToA": 13414781.25, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 921, "pixX": 147, "pixY": 144, "ToA": 13414782.8125, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 871, "pixX": 150, "pixY": 146, "ToA": 13414782.8125, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 977, "pixX": 149, "pixY": 139, "ToA": 13414782.8125, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 978, "pixX": 151, "pixY": 146, "ToA": 13414782.8125, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 1003, "pixX": 149, "pixY": 146, "ToA": 13414782.8125, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 909, "pixX": 153, "pixY": 140, "ToA": 13414784.375, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 922, "pixX": 148, "pixY": 139, "ToA": 13414785.9375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 924, "pixX": 152, "pixY": 139, "ToA": 13414785.9375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 979, "pixX": 153, "pixY": 145, "ToA": 13414785.9375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 910, "pixX": 154, "pixY": 142, "ToA": 13414787.5, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 925, "pixX": 154, "pixY": 143, "ToA": 13414787.5, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 891, "pixX": 152, "pixY": 146, "ToA": 13414789.0625, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 939, "pixX": 147, "pixY": 140, "ToA": 13414792.1875, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 892, "pixX": 154, "pixY": 141, "ToA": 13414793.75, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 814, "pixX": 109, "pixY": 6, "ToA": 13416090.625, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 918, "pixX": 95, "pixY": 50, "ToA": 13419990.625, "ToT": 98, "TriggerNo": 0 }, { "pixel_index": 902, "pixX": 94, "pixY": 50, "ToA": 13419992.1875, "ToT": 34, "TriggerNo": 0 }, { "pixel_index": 864, "pixX": 95, "pixY": 49, "ToA": 13419992.1875, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 883, "pixX": 95, "pixY": 51, "ToA": 13419995.3125, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 840, "pixX": 94, "pixY": 51, "ToA": 13420004.6875, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 841, "pixX": 96, "pixY": 50, "ToA": 13420009.375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 836, "pixX": 254, "pixY": 91, "ToA": 13420473.4375, "ToT": 14, "TriggerNo": 0 }, { "pixel_index": 862, "pixX": 39, "pixY": 229, "ToA": 13421965.625, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 839, "pixX": 38, "pixY": 229, "ToA": 13421985.9375, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 842, "pixX": 129, "pixY": 248, "ToA": 13422935.9375, "ToT": 8, "TriggerNo": 0 }, { "pixel_index": 863, "pixX": 72, "pixY": 165, "ToA": 13427828.125, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 931, "pixX": 173, "pixY": 46, "ToA": 13428929.6875, "ToT": 49, "TriggerNo": 0 }, { "pixel_index": 911, "pixX": 173, "pixY": 45, "ToA": 13428929.6875, "ToT": 44, "TriggerNo": 0 }, { "pixel_index": 893, "pixX": 172, "pixY": 46, "ToA": 13428929.6875, "ToT": 20, "TriggerNo": 0 }, { "pixel_index": 875, "pixX": 172, "pixY": 45, "ToA": 13428931.25, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 876, "pixX": 174, "pixY": 27, "ToA": 13429253.125, "ToT": 13, "TriggerNo": 0 }, { "pixel_index": 992, "pixX": 2, "pixY": 133, "ToA": 13430726.5625, "ToT": 48, "TriggerNo": 0 }, { "pixel_index": 949, "pixX": 2, "pixY": 132, "ToA": 13430728.125, "ToT": 32, "TriggerNo": 0 }, { "pixel_index": 966, "pixX": 3, "pixY": 131, "ToA": 13430729.6875, "ToT": 28, "TriggerNo": 0 }, { "pixel_index": 915, "pixX": 3, "pixY": 132, "ToA": 13430729.6875, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1015, "pixX": 3, "pixY": 133, "ToA": 13430729.6875, "ToT": 47, "TriggerNo": 0 }, { "pixel_index": 935, "pixX": 3, "pixY": 130, "ToA": 13430731.25, "ToT": 11, "TriggerNo": 0 }, { "pixel_index": 916, "pixX": 4, "pixY": 130, "ToA": 13430732.8125, "ToT": 19, "TriggerNo": 0 }, { "pixel_index": 899, "pixX": 5, "pixY": 130, "ToA": 13430735.9375, "ToT": 19, "TriggerNo": 0 }, { "pixel_index": 901, "pixX": 8, "pixY": 128, "ToA": 13430735.9375, "ToT": 57, "TriggerNo": 0 }, { "pixel_index": 882, "pixX": 8, "pixY": 129, "ToA": 13430737.5, "ToT": 11, "TriggerNo": 0 }, { "pixel_index": 936, "pixX": 7, "pixY": 128, "ToA": 13430739.0625, "ToT": 66, "TriggerNo": 0 }, { "pixel_index": 917, "pixX": 7, "pixY": 129, "ToA": 13430739.0625, "ToT": 53, "TriggerNo": 0 }, { "pixel_index": 880, "pixX": 5, "pixY": 129, "ToA": 13430740.625, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 881, "pixX": 6, "pixY": 129, "ToA": 13430742.1875, "ToT": 15, "TriggerNo": 0 }, { "pixel_index": 900, "pixX": 6, "pixY": 130, "ToA": 13430743.75, "ToT": 18, "TriggerNo": 0 }, { "pixel_index": 884, "pixX": 138, "pixY": 117, "ToA": 13435389.0625, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 903, "pixX": 139, "pixY": 117, "ToA": 13435390.625, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 1106, "pixX": 240, "pixY": 42, "ToA": 13436223.4375, "ToT": 42, "TriggerNo": 0 }, { "pixel_index": 1094, "pixX": 238, "pixY": 43, "ToA": 13436223.4375, "ToT": 225, "TriggerNo": 0 }, { "pixel_index": 1095, "pixX": 240, "pixY": 41, "ToA": 13436223.4375, "ToT": 56, "TriggerNo": 0 }, { "pixel_index": 1064, "pixX": 238, "pixY": 42, "ToA": 13436223.4375, "ToT": 222, "TriggerNo": 0 }, { "pixel_index": 1013, "pixX": 239, "pixY": 42, "ToA": 13436223.4375, "ToT": 216, "TriggerNo": 0 }, { "pixel_index": 1034, "pixX": 239, "pixY": 39, "ToA": 13436223.4375, "ToT": 50, "TriggerNo": 0 }, { "pixel_index": 947, "pixX": 238, "pixY": 40, "ToA": 13436223.4375, "ToT": 130, "TriggerNo": 0 }, { "pixel_index": 1082, "pixX": 240, "pixY": 40, "ToA": 13436223.4375, "ToT": 51, "TriggerNo": 0 }, { "pixel_index": 1133, "pixX": 238, "pixY": 41, "ToA": 13436223.4375, "ToT": 245, "TriggerNo": 0 }, { "pixel_index": 1124, "pixX": 239, "pixY": 41, "ToA": 13436223.4375, "ToT": 281, "TriggerNo": 0 }, { "pixel_index": 1114, "pixX": 239, "pixY": 40, "ToA": 13436223.4375, "ToT": 315, "TriggerNo": 0 }, { "pixel_index": 964, "pixX": 238, "pixY": 39, "ToA": 13436225.0, "ToT": 20, "TriggerNo": 0 }, { "pixel_index": 1129, "pixX": 239, "pixY": 43, "ToA": 13436225.0, "ToT": 260, "TriggerNo": 0 }, { "pixel_index": 1115, "pixX": 240, "pixY": 43, "ToA": 13436225.0, "ToT": 38, "TriggerNo": 0 }, { "pixel_index": 1047, "pixX": 237, "pixY": 41, "ToA": 13436226.5625, "ToT": 38, "TriggerNo": 0 }, { "pixel_index": 1048, "pixX": 238, "pixY": 45, "ToA": 13436226.5625, "ToT": 64, "TriggerNo": 0 }, { "pixel_index": 1080, "pixX": 237, "pixY": 43, "ToA": 13436226.5625, "ToT": 38, "TriggerNo": 0 }, { "pixel_index": 1081, "pixX": 239, "pixY": 44, "ToA": 13436226.5625, "ToT": 184, "TriggerNo": 0 }, { "pixel_index": 1105, "pixX": 238, "pixY": 44, "ToA": 13436226.5625, "ToT": 188, "TriggerNo": 0 }, { "pixel_index": 1063, "pixX": 237, "pixY": 42, "ToA": 13436226.5625, "ToT": 45, "TriggerNo": 0 }, { "pixel_index": 1033, "pixX": 237, "pixY": 40, "ToA": 13436226.5625, "ToT": 20, "TriggerNo": 0 }, { "pixel_index": 934, "pixX": 240, "pixY": 39, "ToA": 13436228.125, "ToT": 14, "TriggerNo": 0 }, { "pixel_index": 990, "pixX": 239, "pixY": 45, "ToA": 13436228.125, "ToT": 51, "TriggerNo": 0 }, { "pixel_index": 946, "pixX": 237, "pixY": 45, "ToA": 13436228.125, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 989, "pixX": 237, "pixY": 44, "ToA": 13436228.125, "ToT": 26, "TriggerNo": 0 }, { "pixel_index": 1035, "pixX": 240, "pixY": 44, "ToA": 13436228.125, "ToT": 22, "TriggerNo": 0 }, { "pixel_index": 914, "pixX": 241, "pixY": 43, "ToA": 13436229.6875, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 895, "pixX": 236, "pixY": 44, "ToA": 13436229.6875, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 1065, "pixX": 240, "pixY": 45, "ToA": 13436229.6875, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 1014, "pixX": 241, "pixY": 41, "ToA": 13436229.6875, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 963, "pixX": 236, "pixY": 42, "ToA": 13436229.6875, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 1012, "pixX": 236, "pixY": 43, "ToA": 13436229.6875, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 1049, "pixX": 241, "pixY": 42, "ToA": 13436229.6875, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 896, "pixX": 239, "pixY": 46, "ToA": 13436231.25, "ToT": 3, "TriggerNo": 0 }, { "pixel_index": 933, "pixX": 238, "pixY": 46, "ToA": 13436231.25, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 948, "pixX": 241, "pixY": 44, "ToA": 13436231.25, "ToT": 3, "TriggerNo": 0 }, { "pixel_index": 965, "pixX": 241, "pixY": 40, "ToA": 13436234.375, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 991, "pixX": 241, "pixY": 45, "ToA": 13436234.375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 897, "pixX": 240, "pixY": 46, "ToA": 13436235.9375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 912, "pixX": 236, "pixY": 41, "ToA": 13436235.9375, "ToT": 3, "TriggerNo": 0 }, { "pixel_index": 932, "pixX": 237, "pixY": 39, "ToA": 13436240.625, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 913, "pixX": 239, "pixY": 38, "ToA": 13436250.0, "ToT": 3, "TriggerNo": 0 }, { "pixel_index": 894, "pixX": 212, "pixY": 48, "ToA": 13437043.75, "ToT": 11, "TriggerNo": 0 }, { "pixel_index": 982, "pixX": 167, "pixY": 245, "ToA": 13445032.8125, "ToT": 26, "TriggerNo": 0 }, { "pixel_index": 929, "pixX": 169, "pixY": 255, "ToA": 13445035.9375, "ToT": 18, "TriggerNo": 0 }, { "pixel_index": 943, "pixX": 162, "pixY": 250, "ToA": 13445035.9375, "ToT": 37, "TriggerNo": 0 }, { "pixel_index": 945, "pixX": 167, "pixY": 244, "ToA": 13445035.9375, "ToT": 14, "TriggerNo": 0 }, { "pixel_index": 980, "pixX": 163, "pixY": 249, "ToA": 13445035.9375, "ToT": 69, "TriggerNo": 0 }, { "pixel_index": 956, "pixX": 163, "pixY": 250, "ToA": 13445035.9375, "ToT": 45, "TriggerNo": 0 }, { "pixel_index": 930, "pixX": 170, "pixY": 255, "ToA": 13445037.5, "ToT": 16, "TriggerNo": 0 }, { "pixel_index": 981, "pixX": 165, "pixY": 254, "ToA": 13445040.625, "ToT": 34, "TriggerNo": 0 }, { "pixel_index": 957, "pixX": 165, "pixY": 253, "ToA": 13445040.625, "ToT": 19, "TriggerNo": 0 }, { "pixel_index": 928, "pixX": 167, "pixY": 255, "ToA": 13445042.1875, "ToT": 18, "TriggerNo": 0 }, { "pixel_index": 1006, "pixX": 164, "pixY": 252, "ToA": 13445042.1875, "ToT": 30, "TriggerNo": 0 }, { "pixel_index": 1028, "pixX": 164, "pixY": 253, "ToA": 13445042.1875, "ToT": 23, "TriggerNo": 0 }, { "pixel_index": 958, "pixX": 166, "pixY": 255, "ToA": 13445042.1875, "ToT": 28, "TriggerNo": 0 }, { "pixel_index": 926, "pixX": 163, "pixY": 251, "ToA": 13445043.75, "ToT": 35, "TriggerNo": 0 }, { "pixel_index": 944, "pixX": 164, "pixY": 251, "ToA": 13445048.4375, "ToT": 13, "TriggerNo": 0 }, { "pixel_index": 927, "pixX": 165, "pixY": 255, "ToA": 13445060.9375, "ToT": 3, "TriggerNo": 0 }, { "pixel_index": 950, "pixX": 118, "pixY": 154, "ToA": 13448800.0, "ToT": 55, "TriggerNo": 0 }, { "pixel_index": 1010, "pixX": 202, "pixY": 58, "ToA": 13452190.625, "ToT": 25, "TriggerNo": 0 }, { "pixel_index": 1009, "pixX": 201, "pixY": 59, "ToA": 13452190.625, "ToT": 25, "TriggerNo": 0 }, { "pixel_index": 1032, "pixX": 203, "pixY": 56, "ToA": 13452190.625, "ToT": 33, "TriggerNo": 0 }, { "pixel_index": 1046, "pixX": 203, "pixY": 57, "ToA": 13452190.625, "ToT": 69, "TriggerNo": 0 }, { "pixel_index": 987, "pixX": 202, "pixY": 57, "ToA": 13452192.1875, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 1123, "pixX": 201, "pixY": 60, "ToA": 13452192.1875, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 1128, "pixX": 201, "pixY": 61, "ToA": 13452192.1875, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 1079, "pixX": 200, "pixY": 62, "ToA": 13452192.1875, "ToT": 16, "TriggerNo": 0 }, { "pixel_index": 1062, "pixX": 203, "pixY": 58, "ToA": 13452192.1875, "ToT": 51, "TriggerNo": 0 }, { "pixel_index": 1045, "pixX": 200, "pixY": 61, "ToA": 13452192.1875, "ToT": 14, "TriggerNo": 0 }, { "pixel_index": 1104, "pixX": 200, "pixY": 63, "ToA": 13452193.75, "ToT": 15, "TriggerNo": 0 }, { "pixel_index": 962, "pixX": 204, "pixY": 56, "ToA": 13452193.75, "ToT": 8, "TriggerNo": 0 }, { "pixel_index": 988, "pixX": 204, "pixY": 57, "ToA": 13452193.75, "ToT": 16, "TriggerNo": 0 }, { "pixel_index": 1061, "pixX": 200, "pixY": 66, "ToA": 13452196.875, "ToT": 14, "TriggerNo": 0 }, { "pixel_index": 1093, "pixX": 201, "pixY": 66, "ToA": 13452196.875, "ToT": 28, "TriggerNo": 0 }, { "pixel_index": 1113, "pixX": 201, "pixY": 67, "ToA": 13452196.875, "ToT": 44, "TriggerNo": 0 }, { "pixel_index": 1031, "pixX": 200, "pixY": 65, "ToA": 13452196.875, "ToT": 24, "TriggerNo": 0 }, { "pixel_index": 960, "pixX": 200, "pixY": 64, "ToA": 13452196.875, "ToT": 13, "TriggerNo": 0 }, { "pixel_index": 961, "pixX": 202, "pixY": 67, "ToA": 13452198.4375, "ToT": 21, "TriggerNo": 0 }, { "pixel_index": 1011, "pixX": 204, "pixY": 58, "ToA": 13452198.4375, "ToT": 13, "TriggerNo": 0 }, { "pixel_index": 985, "pixX": 199, "pixY": 66, "ToA": 13452200.0, "ToT": 8, "TriggerNo": 0 }, { "pixel_index": 1008, "pixX": 199, "pixY": 67, "ToA": 13452200.0, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 986, "pixX": 200, "pixY": 60, "ToA": 13452201.5625, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 959, "pixX": 199, "pixY": 68, "ToA": 13452203.125, "ToT": 19, "TriggerNo": 0 }, { "pixel_index": 994, "pixX": 49, "pixY": 95, "ToA": 13453979.6875, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 968, "pixX": 49, "pixY": 94, "ToA": 13454037.5, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1022, "pixX": 102, "pixY": 239, "ToA": 13455767.1875, "ToT": 23, "TriggerNo": 0 }, { "pixel_index": 1021, "pixX": 101, "pixY": 239, "ToA": 13455770.3125, "ToT": 22, "TriggerNo": 0 }, { "pixel_index": 973, "pixX": 103, "pixY": 240, "ToA": 13455770.3125, "ToT": 15, "TriggerNo": 0 }, { "pixel_index": 1020, "pixX": 99, "pixY": 240, "ToA": 13455771.875, "ToT": 17, "TriggerNo": 0 }, { "pixel_index": 999, "pixX": 103, "pixY": 239, "ToA": 13455771.875, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 971, "pixX": 99, "pixY": 241, "ToA": 13455773.4375, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 1023, "pixX": 104, "pixY": 240, "ToA": 13455773.4375, "ToT": 13, "TriggerNo": 0 }, { "pixel_index": 997, "pixX": 98, "pixY": 241, "ToA": 13455773.4375, "ToT": 20, "TriggerNo": 0 }, { "pixel_index": 996, "pixX": 97, "pixY": 241, "ToA": 13455776.5625, "ToT": 27, "TriggerNo": 0 }, { "pixel_index": 972, "pixX": 100, "pixY": 240, "ToA": 13455778.125, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 998, "pixX": 100, "pixY": 239, "ToA": 13455781.25, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 970, "pixX": 96, "pixY": 241, "ToA": 13455784.375, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 974, "pixX": 105, "pixY": 240, "ToA": 13455784.375, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 1000, "pixX": 105, "pixY": 239, "ToA": 13455784.375, "ToT": 8, "TriggerNo": 0 }, { "pixel_index": 969, "pixX": 95, "pixY": 241, "ToA": 13455790.625, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 983, "pixX": 185, "pixY": 48, "ToA": 13457551.5625, "ToT": 31, "TriggerNo": 0 }, { "pixel_index": 993, "pixX": 35, "pixY": 165, "ToA": 13457551.5625, "ToT": 50, "TriggerNo": 0 }, { "pixel_index": 1007, "pixX": 186, "pixY": 47, "ToA": 13457557.8125, "ToT": 18, "TriggerNo": 0 }, { "pixel_index": 967, "pixX": 35, "pixY": 166, "ToA": 13457560.9375, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 984, "pixX": 186, "pixY": 46, "ToA": 13457565.625, "ToT": 11, "TriggerNo": 0 }, { "pixel_index": 1019, "pixX": 81, "pixY": 192, "ToA": 13463184.375, "ToT": 59, "TriggerNo": 0 }, { "pixel_index": 995, "pixX": 81, "pixY": 191, "ToA": 13463192.1875, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 1016, "pixX": 36, "pixY": 104, "ToA": 13464834.375, "ToT": 29, "TriggerNo": 0 }, { "pixel_index": 1054, "pixX": 71, "pixY": 130, "ToA": 13468562.5, "ToT": 24, "TriggerNo": 0 }, { "pixel_index": 1037, "pixX": 70, "pixY": 129, "ToA": 13468562.5, "ToT": 14, "TriggerNo": 0 }, { "pixel_index": 1038, "pixX": 72, "pixY": 131, "ToA": 13468571.875, "ToT": 16, "TriggerNo": 0 }, { "pixel_index": 1017, "pixX": 71, "pixY": 131, "ToA": 13468573.4375, "ToT": 3, "TriggerNo": 0 }, { "pixel_index": 1072, "pixX": 72, "pixY": 132, "ToA": 13468573.4375, "ToT": 23, "TriggerNo": 0 }, { "pixel_index": 1086, "pixX": 72, "pixY": 133, "ToA": 13468573.4375, "ToT": 22, "TriggerNo": 0 }, { "pixel_index": 1055, "pixX": 73, "pixY": 134, "ToA": 13468576.5625, "ToT": 24, "TriggerNo": 0 }, { "pixel_index": 1099, "pixX": 72, "pixY": 134, "ToA": 13468578.125, "ToT": 25, "TriggerNo": 0 }, { "pixel_index": 1018, "pixX": 73, "pixY": 132, "ToA": 13468590.625, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 1044, "pixX": 170, "pixY": 0, "ToA": 13470298.4375, "ToT": 61, "TriggerNo": 0 }, { "pixel_index": 1078, "pixX": 169, "pixY": 1, "ToA": 13470298.4375, "ToT": 100, "TriggerNo": 0 }, { "pixel_index": 1030, "pixX": 170, "pixY": 1, "ToA": 13470298.4375, "ToT": 23, "TriggerNo": 0 }, { "pixel_index": 1092, "pixX": 169, "pixY": 0, "ToA": 13470298.4375, "ToT": 193, "TriggerNo": 0 }, { "pixel_index": 1060, "pixX": 168, "pixY": 1, "ToA": 13470300.0, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 1043, "pixX": 168, "pixY": 0, "ToA": 13470300.0, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 1029, "pixX": 169, "pixY": 2, "ToA": 13470301.5625, "ToT": 3, "TriggerNo": 0 }, { "pixel_index": 1036, "pixX": 6, "pixY": 125, "ToA": 13473654.6875, "ToT": 14, "TriggerNo": 0 }, { "pixel_index": 1050, "pixX": 6, "pixY": 126, "ToA": 13473657.8125, "ToT": 20, "TriggerNo": 0 }, { "pixel_index": 1067, "pixX": 7, "pixY": 126, "ToA": 13473662.5, "ToT": 17, "TriggerNo": 0 }, { "pixel_index": 1052, "pixX": 63, "pixY": 97, "ToA": 13477250.0, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1131, "pixX": 62, "pixY": 97, "ToA": 13477250.0, "ToT": 302, "TriggerNo": 0 }, { "pixel_index": 1098, "pixX": 64, "pixY": 97, "ToA": 13477251.5625, "ToT": 31, "TriggerNo": 0 }, { "pixel_index": 1118, "pixX": 63, "pixY": 98, "ToA": 13477251.5625, "ToT": 51, "TriggerNo": 0 }, { "pixel_index": 1126, "pixX": 62, "pixY": 96, "ToA": 13477251.5625, "ToT": 90, "TriggerNo": 0 }, { "pixel_index": 1108, "pixX": 62, "pixY": 98, "ToA": 13477251.5625, "ToT": 50, "TriggerNo": 0 }, { "pixel_index": 1134, "pixX": 63, "pixY": 96, "ToA": 13477251.5625, "ToT": 284, "TriggerNo": 0 }, { "pixel_index": 1130, "pixX": 60, "pixY": 94, "ToA": 13477254.6875, "ToT": 35, "TriggerNo": 0 }, { "pixel_index": 1085, "pixX": 64, "pixY": 96, "ToA": 13477257.8125, "ToT": 29, "TriggerNo": 0 }, { "pixel_index": 1125, "pixX": 61, "pixY": 97, "ToA": 13477257.8125, "ToT": 47, "TriggerNo": 0 }, { "pixel_index": 1117, "pixX": 61, "pixY": 95, "ToA": 13477260.9375, "ToT": 34, "TriggerNo": 0 }, { "pixel_index": 1070, "pixX": 62, "pixY": 95, "ToA": 13477262.5, "ToT": 23, "TriggerNo": 0 }, { "pixel_index": 1069, "pixX": 60, "pixY": 93, "ToA": 13477262.5, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 1096, "pixX": 61, "pixY": 94, "ToA": 13477262.5, "ToT": 18, "TriggerNo": 0 }, { "pixel_index": 1097, "pixX": 63, "pixY": 95, "ToA": 13477262.5, "ToT": 35, "TriggerNo": 0 }, { "pixel_index": 1053, "pixX": 64, "pixY": 98, "ToA": 13477264.0625, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1107, "pixX": 61, "pixY": 98, "ToA": 13477265.625, "ToT": 14, "TriggerNo": 0 }, { "pixel_index": 1083, "pixX": 61, "pixY": 96, "ToA": 13477265.625, "ToT": 18, "TriggerNo": 0 }, { "pixel_index": 1071, "pixX": 64, "pixY": 95, "ToA": 13477270.3125, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 1051, "pixX": 60, "pixY": 97, "ToA": 13477287.5, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 1084, "pixX": 62, "pixY": 99, "ToA": 13477289.0625, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 1066, "pixX": 248, "pixY": 66, "ToA": 13479057.8125, "ToT": 19, "TriggerNo": 0 }, { "pixel_index": 1100, "pixX": 109, "pixY": 32, "ToA": 13480815.625, "ToT": 28, "TriggerNo": 0 }, { "pixel_index": 1073, "pixX": 108, "pixY": 32, "ToA": 13480815.625, "ToT": 19, "TriggerNo": 0 }, { "pixel_index": 1109, "pixX": 109, "pixY": 30, "ToA": 13480820.3125, "ToT": 26, "TriggerNo": 0 }, { "pixel_index": 1120, "pixX": 109, "pixY": 31, "ToA": 13480820.3125, "ToT": 37, "TriggerNo": 0 }, { "pixel_index": 1087, "pixX": 108, "pixY": 31, "ToA": 13480823.4375, "ToT": 3, "TriggerNo": 0 }, { "pixel_index": 1068, "pixX": 42, "pixY": 120, "ToA": 13482598.4375, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 1116, "pixX": 16, "pixY": 88, "ToA": 13492568.75, "ToT": 54, "TriggerNo": 0 }, { "pixel_index": 1119, "pixX": 89, "pixY": 2, "ToA": 13494378.125, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 1127, "pixX": 116, "pixY": 124, "ToA": 13497070.3125, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 1132, "pixX": 87, "pixY": 80, "ToA": 13498973.4375, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 1136, "pixX": 204, "pixY": 175, "ToA": 13506828.125, "ToT": 34, "TriggerNo": 0 }, { "pixel_index": 1135, "pixX": 203, "pixY": 175, "ToA": 13506831.25, "ToT": 13, "TriggerNo": 0 }, { "pixel_index": 1151, "pixX": 179, "pixY": 190, "ToA": 13508082.8125, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 1153, "pixX": 181, "pixY": 190, "ToA": 13508082.8125, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 1142, "pixX": 178, "pixY": 185, "ToA": 13508084.375, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 1138, "pixX": 177, "pixY": 190, "ToA": 13508085.9375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1140, "pixX": 177, "pixY": 186, "ToA": 13508085.9375, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 1141, "pixX": 182, "pixY": 190, "ToA": 13508085.9375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1143, "pixX": 182, "pixY": 185, "ToA": 13508085.9375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1149, "pixX": 177, "pixY": 188, "ToA": 13508085.9375, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 1152, "pixX": 183, "pixY": 188, "ToA": 13508085.9375, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 1139, "pixX": 180, "pixY": 191, "ToA": 13508085.9375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1137, "pixX": 178, "pixY": 191, "ToA": 13508085.9375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1144, "pixX": 180, "pixY": 184, "ToA": 13508089.0625, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1147, "pixX": 133, "pixY": 72, "ToA": 13509437.5, "ToT": 20, "TriggerNo": 0 }, { "pixel_index": 1148, "pixX": 134, "pixY": 71, "ToA": 13509437.5, "ToT": 38, "TriggerNo": 0 }, { "pixel_index": 1150, "pixX": 133, "pixY": 71, "ToA": 13509437.5, "ToT": 37, "TriggerNo": 0 }, { "pixel_index": 1145, "pixX": 134, "pixY": 72, "ToA": 13509440.625, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 1146, "pixX": 71, "pixY": 37, "ToA": 13509585.9375, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 1156, "pixX": 152, "pixY": 99, "ToA": 13511259.375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1155, "pixX": 146, "pixY": 98, "ToA": 13511259.375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1154, "pixX": 150, "pixY": 100, "ToA": 13511260.9375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1166, "pixX": 71, "pixY": 36, "ToA": 13516731.25, "ToT": 16, "TriggerNo": 0 }, { "pixel_index": 1168, "pixX": 133, "pixY": 73, "ToA": 13516737.5, "ToT": 25, "TriggerNo": 0 }, { "pixel_index": 1177, "pixX": 133, "pixY": 74, "ToA": 13516737.5, "ToT": 24, "TriggerNo": 0 }, { "pixel_index": 1206, "pixX": 233, "pixY": 249, "ToA": 13516743.75, "ToT": 29, "TriggerNo": 0 }, { "pixel_index": 1191, "pixX": 232, "pixY": 251, "ToA": 13516743.75, "ToT": 58, "TriggerNo": 0 }, { "pixel_index": 1184, "pixX": 233, "pixY": 251, "ToA": 13516743.75, "ToT": 41, "TriggerNo": 0 }, { "pixel_index": 1175, "pixX": 233, "pixY": 250, "ToA": 13516743.75, "ToT": 26, "TriggerNo": 0 }, { "pixel_index": 1200, "pixX": 148, "pixY": 98, "ToA": 13516751.5625, "ToT": 24, "TriggerNo": 0 }, { "pixel_index": 1245, "pixX": 148, "pixY": 96, "ToA": 13516751.5625, "ToT": 336, "TriggerNo": 0 }, { "pixel_index": 1221, "pixX": 147, "pixY": 96, "ToA": 13516751.5625, "ToT": 58, "TriggerNo": 0 }, { "pixel_index": 1244, "pixX": 149, "pixY": 97, "ToA": 13516751.5625, "ToT": 318, "TriggerNo": 0 }, { "pixel_index": 1240, "pixX": 149, "pixY": 96, "ToA": 13516751.5625, "ToT": 384, "TriggerNo": 0 }, { "pixel_index": 1231, "pixX": 148, "pixY": 97, "ToA": 13516751.5625, "ToT": 91, "TriggerNo": 0 }, { "pixel_index": 1173, "pixX": 189, "pixY": 179, "ToA": 13516751.5625, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 1229, "pixX": 147, "pixY": 97, "ToA": 13516751.5625, "ToT": 22, "TriggerNo": 0 }, { "pixel_index": 1237, "pixX": 151, "pixY": 95, "ToA": 13516753.125, "ToT": 21, "TriggerNo": 0 }, { "pixel_index": 1235, "pixX": 149, "pixY": 95, "ToA": 13516753.125, "ToT": 106, "TriggerNo": 0 }, { "pixel_index": 1201, "pixX": 147, "pixY": 95, "ToA": 13516753.125, "ToT": 22, "TriggerNo": 0 }, { "pixel_index": 1230, "pixX": 151, "pixY": 96, "ToA": 13516753.125, "ToT": 65, "TriggerNo": 0 }, { "pixel_index": 1208, "pixX": 146, "pixY": 96, "ToA": 13516753.125, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 1185, "pixX": 149, "pixY": 99, "ToA": 13516753.125, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 1215, "pixX": 149, "pixY": 98, "ToA": 13516753.125, "ToT": 66, "TriggerNo": 0 }, { "pixel_index": 1195, "pixX": 148, "pixY": 94, "ToA": 13516753.125, "ToT": 16, "TriggerNo": 0 }, { "pixel_index": 1217, "pixX": 150, "pixY": 98, "ToA": 13516753.125, "ToT": 83, "TriggerNo": 0 }, { "pixel_index": 1223, "pixX": 149, "pixY": 94, "ToA": 13516753.125, "ToT": 17, "TriggerNo": 0 }, { "pixel_index": 1247, "pixX": 150, "pixY": 97, "ToA": 13516753.125, "ToT": 363, "TriggerNo": 0 }, { "pixel_index": 1243, "pixX": 148, "pixY": 95, "ToA": 13516753.125, "ToT": 111, "TriggerNo": 0 }, { "pixel_index": 1242, "pixX": 150, "pixY": 95, "ToA": 13516753.125, "ToT": 68, "TriggerNo": 0 }, { "pixel_index": 1239, "pixX": 151, "pixY": 97, "ToA": 13516753.125, "ToT": 85, "TriggerNo": 0 }, { "pixel_index": 1246, "pixX": 150, "pixY": 96, "ToA": 13516753.125, "ToT": 313, "TriggerNo": 0 }, { "pixel_index": 1194, "pixX": 150, "pixY": 94, "ToA": 13516754.6875, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 1186, "pixX": 147, "pixY": 94, "ToA": 13516754.6875, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 1187, "pixX": 150, "pixY": 99, "ToA": 13516754.6875, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 1216, "pixX": 146, "pixY": 97, "ToA": 13516754.6875, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 1163, "pixX": 148, "pixY": 99, "ToA": 13516754.6875, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 1202, "pixX": 151, "pixY": 98, "ToA": 13516754.6875, "ToT": 24, "TriggerNo": 0 }, { "pixel_index": 1176, "pixX": 147, "pixY": 98, "ToA": 13516754.6875, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 1165, "pixX": 151, "pixY": 99, "ToA": 13516756.25, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 1161, "pixX": 146, "pixY": 95, "ToA": 13516756.25, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 1219, "pixX": 152, "pixY": 96, "ToA": 13516756.25, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 1232, "pixX": 152, "pixY": 98, "ToA": 13516756.25, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 1225, "pixX": 151, "pixY": 94, "ToA": 13516756.25, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 1224, "pixX": 152, "pixY": 97, "ToA": 13516756.25, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 1198, "pixX": 179, "pixY": 186, "ToA": 13516757.8125, "ToT": 39, "TriggerNo": 0 }, { "pixel_index": 1203, "pixX": 178, "pixY": 189, "ToA": 13516757.8125, "ToT": 20, "TriggerNo": 0 }, { "pixel_index": 1205, "pixX": 181, "pixY": 188, "ToA": 13516757.8125, "ToT": 93, "TriggerNo": 0 }, { "pixel_index": 1158, "pixX": 178, "pixY": 186, "ToA": 13516757.8125, "ToT": 11, "TriggerNo": 0 }, { "pixel_index": 1212, "pixX": 181, "pixY": 187, "ToA": 13516757.8125, "ToT": 73, "TriggerNo": 0 }, { "pixel_index": 1213, "pixX": 178, "pixY": 187, "ToA": 13516757.8125, "ToT": 37, "TriggerNo": 0 }, { "pixel_index": 1218, "pixX": 178, "pixY": 190, "ToA": 13516757.8125, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 1220, "pixX": 181, "pixY": 189, "ToA": 13516757.8125, "ToT": 31, "TriggerNo": 0 }, { "pixel_index": 1226, "pixX": 179, "pixY": 187, "ToA": 13516757.8125, "ToT": 245, "TriggerNo": 0 }, { "pixel_index": 1228, "pixX": 180, "pixY": 186, "ToA": 13516757.8125, "ToT": 69, "TriggerNo": 0 }, { "pixel_index": 1233, "pixX": 180, "pixY": 188, "ToA": 13516757.8125, "ToT": 281, "TriggerNo": 0 }, { "pixel_index": 1234, "pixX": 179, "pixY": 188, "ToA": 13516757.8125, "ToT": 342, "TriggerNo": 0 }, { "pixel_index": 1236, "pixX": 179, "pixY": 189, "ToA": 13516757.8125, "ToT": 78, "TriggerNo": 0 }, { "pixel_index": 1238, "pixX": 180, "pixY": 187, "ToA": 13516757.8125, "ToT": 370, "TriggerNo": 0 }, { "pixel_index": 1241, "pixX": 180, "pixY": 189, "ToA": 13516757.8125, "ToT": 106, "TriggerNo": 0 }, { "pixel_index": 1181, "pixX": 179, "pixY": 185, "ToA": 13516757.8125, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 1160, "pixX": 180, "pixY": 185, "ToA": 13516757.8125, "ToT": 8, "TriggerNo": 0 }, { "pixel_index": 1188, "pixX": 178, "pixY": 188, "ToA": 13516757.8125, "ToT": 55, "TriggerNo": 0 }, { "pixel_index": 1189, "pixX": 152, "pixY": 95, "ToA": 13516757.8125, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 1190, "pixX": 180, "pixY": 190, "ToA": 13516757.8125, "ToT": 15, "TriggerNo": 0 }, { "pixel_index": 1197, "pixX": 181, "pixY": 186, "ToA": 13516757.8125, "ToT": 21, "TriggerNo": 0 }, { "pixel_index": 1207, "pixX": 182, "pixY": 189, "ToA": 13516759.375, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 1192, "pixX": 182, "pixY": 188, "ToA": 13516759.375, "ToT": 13, "TriggerNo": 0 }, { "pixel_index": 1182, "pixX": 182, "pixY": 187, "ToA": 13516759.375, "ToT": 11, "TriggerNo": 0 }, { "pixel_index": 1169, "pixX": 150, "pixY": 93, "ToA": 13516759.375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1179, "pixX": 153, "pixY": 96, "ToA": 13516759.375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1183, "pixX": 181, "pixY": 185, "ToA": 13516759.375, "ToT": 3, "TriggerNo": 0 }, { "pixel_index": 1167, "pixX": 148, "pixY": 93, "ToA": 13516759.375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1164, "pixX": 152, "pixY": 94, "ToA": 13516759.375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1162, "pixX": 182, "pixY": 186, "ToA": 13516759.375, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 1196, "pixX": 153, "pixY": 97, "ToA": 13516759.375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1214, "pixX": 183, "pixY": 187, "ToA": 13516759.375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1210, "pixX": 149, "pixY": 93, "ToA": 13516759.375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1193, "pixX": 147, "pixY": 99, "ToA": 13516759.375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1172, "pixX": 181, "pixY": 191, "ToA": 13516760.9375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1180, "pixX": 151, "pixY": 100, "ToA": 13516760.9375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1211, "pixX": 153, "pixY": 98, "ToA": 13516760.9375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1178, "pixX": 149, "pixY": 100, "ToA": 13516760.9375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1174, "pixX": 183, "pixY": 189, "ToA": 13516760.9375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1171, "pixX": 177, "pixY": 189, "ToA": 13516760.9375, "ToT": 3, "TriggerNo": 0 }, { "pixel_index": 1170, "pixX": 179, "pixY": 191, "ToA": 13516760.9375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1159, "pixX": 177, "pixY": 187, "ToA": 13516760.9375, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 1204, "pixX": 153, "pixY": 95, "ToA": 13516762.5, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1199, "pixX": 183, "pixY": 186, "ToA": 13516762.5, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1209, "pixX": 151, "pixY": 93, "ToA": 13516762.5, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1157, "pixX": 148, "pixY": 100, "ToA": 13516762.5, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1222, "pixX": 16, "pixY": 217, "ToA": 13525526.5625, "ToT": 21, "TriggerNo": 0 }, { "pixel_index": 1227, "pixX": 137, "pixY": 25, "ToA": 13527317.1875, "ToT": 13, "TriggerNo": 0 }, { "pixel_index": 1250, "pixX": 157, "pixY": 179, "ToA": 13532684.375, "ToT": 49, "TriggerNo": 0 }, { "pixel_index": 1249, "pixX": 157, "pixY": 178, "ToA": 13532684.375, "ToT": 47, "TriggerNo": 0 }, { "pixel_index": 1251, "pixX": 242, "pixY": 68, "ToA": 13534470.3125, "ToT": 22, "TriggerNo": 0 }, { "pixel_index": 1248, "pixX": 243, "pixY": 68, "ToA": 13534476.5625, "ToT": 11, "TriggerNo": 0 }, { "pixel_index": 1254, "pixX": 124, "pixY": 60, "ToA": 13542179.6875, "ToT": 43, "TriggerNo": 0 }, { "pixel_index": 1256, "pixX": 123, "pixY": 61, "ToA": 13542179.6875, "ToT": 24, "TriggerNo": 0 }, { "pixel_index": 1257, "pixX": 123, "pixY": 60, "ToA": 13542179.6875, "ToT": 151, "TriggerNo": 0 }, { "pixel_index": 1252, "pixX": 122, "pixY": 60, "ToA": 13542187.5, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 1253, "pixX": 123, "pixY": 59, "ToA": 13542189.0625, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 1255, "pixX": 161, "pixY": 134, "ToA": 13545010.9375, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 1258, "pixX": 49, "pixY": 79, "ToA": 13545739.0625, "ToT": 67, "TriggerNo": 0 }, { "pixel_index": 1264, "pixX": 242, "pixY": 73, "ToA": 13550160.9375, "ToT": 14, "TriggerNo": 0 }, { "pixel_index": 1261, "pixX": 241, "pixY": 73, "ToA": 13550164.0625, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 1259, "pixX": 24, "pixY": 248, "ToA": 13550451.5625, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 1271, "pixX": 253, "pixY": 194, "ToA": 13550568.75, "ToT": 33, "TriggerNo": 0 }, { "pixel_index": 1274, "pixX": 253, "pixY": 195, "ToA": 13550568.75, "ToT": 26, "TriggerNo": 0 }, { "pixel_index": 1267, "pixX": 252, "pixY": 194, "ToA": 13550576.5625, "ToT": 27, "TriggerNo": 0 }, { "pixel_index": 1263, "pixX": 44, "pixY": 110, "ToA": 13550582.8125, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 1262, "pixX": 252, "pixY": 193, "ToA": 13550585.9375, "ToT": 20, "TriggerNo": 0 }, { "pixel_index": 1273, "pixX": 83, "pixY": 246, "ToA": 13550637.5, "ToT": 106, "TriggerNo": 0 }, { "pixel_index": 1266, "pixX": 82, "pixY": 246, "ToA": 13550640.625, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 1270, "pixX": 83, "pixY": 245, "ToA": 13550646.875, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 1260, "pixX": 83, "pixY": 247, "ToA": 13550656.25, "ToT": 3, "TriggerNo": 0 }, { "pixel_index": 1265, "pixX": 7, "pixY": 210, "ToA": 13552026.5625, "ToT": 36, "TriggerNo": 0 }, { "pixel_index": 1269, "pixX": 92, "pixY": 15, "ToA": 13553431.25, "ToT": 52, "TriggerNo": 0 }, { "pixel_index": 1268, "pixX": 92, "pixY": 16, "ToA": 13553432.8125, "ToT": 11, "TriggerNo": 0 }, { "pixel_index": 1275, "pixX": 180, "pixY": 193, "ToA": 13557750.0, "ToT": 33, "TriggerNo": 0 }, { "pixel_index": 1272, "pixX": 181, "pixY": 193, "ToA": 13557771.875, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 1277, "pixX": 91, "pixY": 172, "ToA": 13560417.1875, "ToT": 41, "TriggerNo": 0 }, { "pixel_index": 1276, "pixX": 90, "pixY": 172, "ToA": 13560429.6875, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 1296, "pixX": 1, "pixY": 189, "ToA": 13566668.75, "ToT": 23, "TriggerNo": 0 }, { "pixel_index": 1283, "pixX": 1, "pixY": 190, "ToA": 13566670.3125, "ToT": 20, "TriggerNo": 0 }, { "pixel_index": 1297, "pixX": 2, "pixY": 188, "ToA": 13566676.5625, "ToT": 24, "TriggerNo": 0 }, { "pixel_index": 1285, "pixX": 2, "pixY": 189, "ToA": 13566679.6875, "ToT": 7, "TriggerNo": 0 }, { "pixel_index": 1289, "pixX": 2, "pixY": 187, "ToA": 13566690.625, "ToT": 8, "TriggerNo": 0 }, { "pixel_index": 1291, "pixX": 1, "pixY": 187, "ToA": 13566693.75, "ToT": 11, "TriggerNo": 0 }, { "pixel_index": 1365, "pixX": 30, "pixY": 239, "ToA": 13567079.6875, "ToT": 101, "TriggerNo": 0 }, { "pixel_index": 1376, "pixX": 30, "pixY": 237, "ToA": 13567079.6875, "ToT": 359, "TriggerNo": 0 }, { "pixel_index": 1316, "pixX": 31, "pixY": 236, "ToA": 13567079.6875, "ToT": 69, "TriggerNo": 0 }, { "pixel_index": 1342, "pixX": 31, "pixY": 239, "ToA": 13567079.6875, "ToT": 94, "TriggerNo": 0 }, { "pixel_index": 1395, "pixX": 31, "pixY": 237, "ToA": 13567079.6875, "ToT": 366, "TriggerNo": 0 }, { "pixel_index": 1300, "pixX": 30, "pixY": 236, "ToA": 13567079.6875, "ToT": 68, "TriggerNo": 0 }, { "pixel_index": 1384, "pixX": 29, "pixY": 238, "ToA": 13567081.25, "ToT": 86, "TriggerNo": 0 }, { "pixel_index": 1386, "pixX": 30, "pixY": 238, "ToA": 13567081.25, "ToT": 408, "TriggerNo": 0 }, { "pixel_index": 1377, "pixX": 32, "pixY": 238, "ToA": 13567081.25, "ToT": 82, "TriggerNo": 0 }, { "pixel_index": 1388, "pixX": 32, "pixY": 239, "ToA": 13567081.25, "ToT": 29, "TriggerNo": 0 }, { "pixel_index": 1295, "pixX": 31, "pixY": 240, "ToA": 13567081.25, "ToT": 13, "TriggerNo": 0 }, { "pixel_index": 1301, "pixX": 28, "pixY": 237, "ToA": 13567081.25, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 1287, "pixX": 33, "pixY": 236, "ToA": 13567081.25, "ToT": 3, "TriggerNo": 0 }, { "pixel_index": 1286, "pixX": 28, "pixY": 236, "ToA": 13567081.25, "ToT": 3, "TriggerNo": 0 }, { "pixel_index": 1322, "pixX": 33, "pixY": 238, "ToA": 13567081.25, "ToT": 11, "TriggerNo": 0 }, { "pixel_index": 1367, "pixX": 32, "pixY": 237, "ToA": 13567081.25, "ToT": 70, "TriggerNo": 0 }, { "pixel_index": 1363, "pixX": 29, "pixY": 236, "ToA": 13567081.25, "ToT": 22, "TriggerNo": 0 }, { "pixel_index": 1353, "pixX": 32, "pixY": 236, "ToA": 13567081.25, "ToT": 21, "TriggerNo": 0 }, { "pixel_index": 1341, "pixX": 33, "pixY": 239, "ToA": 13567081.25, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 1393, "pixX": 29, "pixY": 239, "ToA": 13567081.25, "ToT": 34, "TriggerNo": 0 }, { "pixel_index": 1350, "pixX": 28, "pixY": 239, "ToA": 13567081.25, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 1329, "pixX": 30, "pixY": 240, "ToA": 13567081.25, "ToT": 13, "TriggerNo": 0 }, { "pixel_index": 1374, "pixX": 29, "pixY": 237, "ToA": 13567081.25, "ToT": 78, "TriggerNo": 0 }, { "pixel_index": 1321, "pixX": 28, "pixY": 238, "ToA": 13567081.25, "ToT": 11, "TriggerNo": 0 }, { "pixel_index": 1302, "pixX": 33, "pixY": 237, "ToA": 13567081.25, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 1403, "pixX": 31, "pixY": 238, "ToA": 13567081.25, "ToT": 399, "TriggerNo": 0 }, { "pixel_index": 1280, "pixX": 30, "pixY": 241, "ToA": 13567082.8125, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1288, "pixX": 30, "pixY": 234, "ToA": 13567082.8125, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1299, "pixX": 33, "pixY": 240, "ToA": 13567082.8125, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1318, "pixX": 32, "pixY": 240, "ToA": 13567082.8125, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 1354, "pixX": 31, "pixY": 235, "ToA": 13567082.8125, "ToT": 8, "TriggerNo": 0 }, { "pixel_index": 1303, "pixX": 30, "pixY": 235, "ToA": 13567082.8125, "ToT": 8, "TriggerNo": 0 }, { "pixel_index": 1309, "pixX": 31, "pixY": 241, "ToA": 13567082.8125, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1314, "pixX": 29, "pixY": 240, "ToA": 13567082.8125, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 1331, "pixX": 33, "pixY": 235, "ToA": 13567082.8125, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1278, "pixX": 28, "pixY": 240, "ToA": 13567082.8125, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1290, "pixX": 28, "pixY": 235, "ToA": 13567084.375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1293, "pixX": 27, "pixY": 237, "ToA": 13567084.375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1305, "pixX": 32, "pixY": 235, "ToA": 13567084.375, "ToT": 3, "TriggerNo": 0 }, { "pixel_index": 1330, "pixX": 29, "pixY": 235, "ToA": 13567084.375, "ToT": 3, "TriggerNo": 0 }, { "pixel_index": 1284, "pixX": 27, "pixY": 236, "ToA": 13567084.375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1282, "pixX": 32, "pixY": 241, "ToA": 13567084.375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1281, "pixX": 34, "pixY": 236, "ToA": 13567084.375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1340, "pixX": 29, "pixY": 241, "ToA": 13567084.375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1319, "pixX": 27, "pixY": 239, "ToA": 13567084.375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1320, "pixX": 31, "pixY": 234, "ToA": 13567084.375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1307, "pixX": 27, "pixY": 238, "ToA": 13567084.375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1308, "pixX": 34, "pixY": 239, "ToA": 13567084.375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1304, "pixX": 34, "pixY": 238, "ToA": 13567084.375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1294, "pixX": 34, "pixY": 237, "ToA": 13567084.375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1292, "pixX": 32, "pixY": 234, "ToA": 13567084.375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1306, "pixX": 29, "pixY": 234, "ToA": 13567085.9375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1298, "pixX": 28, "pixY": 241, "ToA": 13567087.5, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1279, "pixX": 27, "pixY": 240, "ToA": 13567089.0625, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1381, "pixX": 186, "pixY": 144, "ToA": 13573881.25, "ToT": 69, "TriggerNo": 0 }, { "pixel_index": 1382, "pixX": 185, "pixY": 145, "ToA": 13573881.25, "ToT": 21, "TriggerNo": 0 }, { "pixel_index": 1383, "pixX": 188, "pixY": 143, "ToA": 13573881.25, "ToT": 280, "TriggerNo": 0 }, { "pixel_index": 1389, "pixX": 180, "pixY": 146, "ToA": 13573881.25, "ToT": 17, "TriggerNo": 0 }, { "pixel_index": 1390, "pixX": 184, "pixY": 142, "ToA": 13573881.25, "ToT": 25, "TriggerNo": 0 }, { "pixel_index": 1391, "pixX": 183, "pixY": 141, "ToA": 13573881.25, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 1392, "pixX": 187, "pixY": 142, "ToA": 13573881.25, "ToT": 43, "TriggerNo": 0 }, { "pixel_index": 1334, "pixX": 183, "pixY": 146, "ToA": 13573881.25, "ToT": 8, "TriggerNo": 0 }, { "pixel_index": 1380, "pixX": 182, "pixY": 145, "ToA": 13573881.25, "ToT": 67, "TriggerNo": 0 }, { "pixel_index": 1398, "pixX": 184, "pixY": 145, "ToA": 13573881.25, "ToT": 37, "TriggerNo": 0 }, { "pixel_index": 1399, "pixX": 182, "pixY": 146, "ToA": 13573881.25, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 1400, "pixX": 186, "pixY": 143, "ToA": 13573881.25, "ToT": 230, "TriggerNo": 0 }, { "pixel_index": 1346, "pixX": 181, "pixY": 146, "ToA": 13573881.25, "ToT": 15, "TriggerNo": 0 }, { "pixel_index": 1404, "pixX": 181, "pixY": 145, "ToA": 13573881.25, "ToT": 110, "TriggerNo": 0 }, { "pixel_index": 1405, "pixX": 184, "pixY": 143, "ToA": 13573881.25, "ToT": 177, "TriggerNo": 0 }, { "pixel_index": 1406, "pixX": 183, "pixY": 142, "ToA": 13573881.25, "ToT": 20, "TriggerNo": 0 }, { "pixel_index": 1407, "pixX": 187, "pixY": 143, "ToA": 13573881.25, "ToT": 295, "TriggerNo": 0 }, { "pixel_index": 1414, "pixX": 183, "pixY": 144, "ToA": 13573881.25, "ToT": 299, "TriggerNo": 0 }, { "pixel_index": 1416, "pixX": 185, "pixY": 144, "ToA": 13573881.25, "ToT": 169, "TriggerNo": 0 }, { "pixel_index": 1348, "pixX": 182, "pixY": 141, "ToA": 13573881.25, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 1422, "pixX": 180, "pixY": 145, "ToA": 13573881.25, "ToT": 143, "TriggerNo": 0 }, { "pixel_index": 1424, "pixX": 183, "pixY": 143, "ToA": 13573881.25, "ToT": 159, "TriggerNo": 0 }, { "pixel_index": 1425, "pixX": 185, "pixY": 143, "ToA": 13573881.25, "ToT": 242, "TriggerNo": 0 }, { "pixel_index": 1432, "pixX": 184, "pixY": 144, "ToA": 13573881.25, "ToT": 252, "TriggerNo": 0 }, { "pixel_index": 1433, "pixX": 183, "pixY": 145, "ToA": 13573881.25, "ToT": 52, "TriggerNo": 0 }, { "pixel_index": 1438, "pixX": 182, "pixY": 143, "ToA": 13573881.25, "ToT": 131, "TriggerNo": 0 }, { "pixel_index": 1439, "pixX": 181, "pixY": 144, "ToA": 13573881.25, "ToT": 374, "TriggerNo": 0 }, { "pixel_index": 1447, "pixX": 182, "pixY": 144, "ToA": 13573881.25, "ToT": 348, "TriggerNo": 0 }, { "pixel_index": 1450, "pixX": 180, "pixY": 144, "ToA": 13573881.25, "ToT": 368, "TriggerNo": 0 }, { "pixel_index": 1361, "pixX": 184, "pixY": 146, "ToA": 13573881.25, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 1362, "pixX": 188, "pixY": 142, "ToA": 13573881.25, "ToT": 34, "TriggerNo": 0 }, { "pixel_index": 1369, "pixX": 182, "pixY": 142, "ToA": 13573881.25, "ToT": 19, "TriggerNo": 0 }, { "pixel_index": 1371, "pixX": 185, "pixY": 142, "ToA": 13573881.25, "ToT": 32, "TriggerNo": 0 }, { "pixel_index": 1372, "pixX": 189, "pixY": 143, "ToA": 13573881.25, "ToT": 158, "TriggerNo": 0 }, { "pixel_index": 1373, "pixX": 186, "pixY": 142, "ToA": 13573881.25, "ToT": 36, "TriggerNo": 0 }, { "pixel_index": 1352, "pixX": 188, "pixY": 144, "ToA": 13573882.8125, "ToT": 34, "TriggerNo": 0 }, { "pixel_index": 1431, "pixX": 181, "pixY": 143, "ToA": 13573882.8125, "ToT": 127, "TriggerNo": 0 }, { "pixel_index": 1360, "pixX": 180, "pixY": 142, "ToA": 13573882.8125, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 1370, "pixX": 181, "pixY": 147, "ToA": 13573882.8125, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 1444, "pixX": 180, "pixY": 143, "ToA": 13573882.8125, "ToT": 83, "TriggerNo": 0 }, { "pixel_index": 1333, "pixX": 179, "pixY": 146, "ToA": 13573882.8125, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 1313, "pixX": 182, "pixY": 147, "ToA": 13573882.8125, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1335, "pixX": 186, "pixY": 145, "ToA": 13573882.8125, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 1379, "pixX": 178, "pixY": 144, "ToA": 13573882.8125, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 1323, "pixX": 180, "pixY": 147, "ToA": 13573882.8125, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 1413, "pixX": 179, "pixY": 144, "ToA": 13573882.8125, "ToT": 64, "TriggerNo": 0 }, { "pixel_index": 1415, "pixX": 181, "pixY": 142, "ToA": 13573882.8125, "ToT": 17, "TriggerNo": 0 }, { "pixel_index": 1423, "pixX": 179, "pixY": 145, "ToA": 13573882.8125, "ToT": 22, "TriggerNo": 0 }, { "pixel_index": 1396, "pixX": 181, "pixY": 141, "ToA": 13573884.375, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 1332, "pixX": 180, "pixY": 141, "ToA": 13573884.375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1387, "pixX": 179, "pixY": 142, "ToA": 13573884.375, "ToT": 3, "TriggerNo": 0 }, { "pixel_index": 1328, "pixX": 184, "pixY": 141, "ToA": 13573884.375, "ToT": 3, "TriggerNo": 0 }, { "pixel_index": 1397, "pixX": 178, "pixY": 145, "ToA": 13573884.375, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 1344, "pixX": 178, "pixY": 143, "ToA": 13573884.375, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 1402, "pixX": 179, "pixY": 143, "ToA": 13573884.375, "ToT": 19, "TriggerNo": 0 }, { "pixel_index": 1358, "pixX": 179, "pixY": 147, "ToA": 13573884.375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1359, "pixX": 183, "pixY": 147, "ToA": 13573884.375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1337, "pixX": 189, "pixY": 142, "ToA": 13573885.9375, "ToT": 18, "TriggerNo": 0 }, { "pixel_index": 1336, "pixX": 185, "pixY": 146, "ToA": 13573885.9375, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 1324, "pixX": 178, "pixY": 142, "ToA": 13573885.9375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1368, "pixX": 179, "pixY": 141, "ToA": 13573885.9375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1312, "pixX": 178, "pixY": 146, "ToA": 13573885.9375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1326, "pixX": 189, "pixY": 144, "ToA": 13573885.9375, "ToT": 17, "TriggerNo": 0 }, { "pixel_index": 1347, "pixX": 185, "pixY": 141, "ToA": 13573887.5, "ToT": 3, "TriggerNo": 0 }, { "pixel_index": 1325, "pixX": 182, "pixY": 140, "ToA": 13573889.0625, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1378, "pixX": 181, "pixY": 140, "ToA": 13573889.0625, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1310, "pixX": 184, "pixY": 147, "ToA": 13573890.625, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1311, "pixX": 181, "pixY": 148, "ToA": 13573890.625, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1317, "pixX": 190, "pixY": 143, "ToA": 13573892.1875, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 1315, "pixX": 187, "pixY": 145, "ToA": 13573901.5625, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 1327, "pixX": 186, "pixY": 141, "ToA": 13573904.6875, "ToT": 3, "TriggerNo": 0 }, { "pixel_index": 1349, "pixX": 187, "pixY": 141, "ToA": 13573921.875, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 1375, "pixX": 226, "pixY": 183, "ToA": 13577400.0, "ToT": 122, "TriggerNo": 0 }, { "pixel_index": 1343, "pixX": 87, "pixY": 60, "ToA": 13577406.25, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 1345, "pixX": 88, "pixY": 60, "ToA": 13577406.25, "ToT": 8, "TriggerNo": 0 }, { "pixel_index": 1366, "pixX": 87, "pixY": 59, "ToA": 13577407.8125, "ToT": 28, "TriggerNo": 0 }, { "pixel_index": 1339, "pixX": 225, "pixY": 183, "ToA": 13577409.375, "ToT": 15, "TriggerNo": 0 }, { "pixel_index": 1357, "pixX": 86, "pixY": 59, "ToA": 13577415.625, "ToT": 19, "TriggerNo": 0 }, { "pixel_index": 1351, "pixX": 226, "pixY": 182, "ToA": 13577435.9375, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 1338, "pixX": 226, "pixY": 184, "ToA": 13577437.5, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 1364, "pixX": 227, "pixY": 183, "ToA": 13577439.0625, "ToT": 3, "TriggerNo": 0 }, { "pixel_index": 1355, "pixX": 70, "pixY": 28, "ToA": 13580978.125, "ToT": 17, "TriggerNo": 0 }, { "pixel_index": 1356, "pixX": 167, "pixY": 37, "ToA": 13581575.0, "ToT": 23, "TriggerNo": 0 }, { "pixel_index": 1394, "pixX": 189, "pixY": 7, "ToA": 13588762.5, "ToT": 18, "TriggerNo": 0 }, { "pixel_index": 1385, "pixX": 190, "pixY": 7, "ToA": 13588773.4375, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 1429, "pixX": 31, "pixY": 21, "ToA": 13591710.9375, "ToT": 18, "TriggerNo": 0 }, { "pixel_index": 1435, "pixX": 30, "pixY": 20, "ToA": 13591710.9375, "ToT": 13, "TriggerNo": 0 }, { "pixel_index": 1401, "pixX": 29, "pixY": 20, "ToA": 13591712.5, "ToT": 21, "TriggerNo": 0 }, { "pixel_index": 1430, "pixX": 29, "pixY": 19, "ToA": 13591714.0625, "ToT": 16, "TriggerNo": 0 }, { "pixel_index": 1421, "pixX": 29, "pixY": 18, "ToA": 13591714.0625, "ToT": 16, "TriggerNo": 0 }, { "pixel_index": 1411, "pixX": 30, "pixY": 21, "ToA": 13591715.625, "ToT": 8, "TriggerNo": 0 }, { "pixel_index": 1412, "pixX": 29, "pixY": 17, "ToA": 13591715.625, "ToT": 11, "TriggerNo": 0 }, { "pixel_index": 1420, "pixX": 30, "pixY": 17, "ToA": 13591717.1875, "ToT": 11, "TriggerNo": 0 }, { "pixel_index": 1445, "pixX": 20, "pixY": 2, "ToA": 13595289.0625, "ToT": 25, "TriggerNo": 0 }, { "pixel_index": 1454, "pixX": 21, "pixY": 1, "ToA": 13595289.0625, "ToT": 21, "TriggerNo": 0 }, { "pixel_index": 1451, "pixX": 21, "pixY": 16, "ToA": 13595289.0625, "ToT": 32, "TriggerNo": 0 }, { "pixel_index": 1428, "pixX": 20, "pixY": 1, "ToA": 13595289.0625, "ToT": 17, "TriggerNo": 0 }, { "pixel_index": 1419, "pixX": 21, "pixY": 15, "ToA": 13595290.625, "ToT": 13, "TriggerNo": 0 }, { "pixel_index": 1426, "pixX": 19, "pixY": 2, "ToA": 13595292.1875, "ToT": 15, "TriggerNo": 0 }, { "pixel_index": 1434, "pixX": 18, "pixY": 8, "ToA": 13595292.1875, "ToT": 14, "TriggerNo": 0 }, { "pixel_index": 1443, "pixX": 18, "pixY": 6, "ToA": 13595292.1875, "ToT": 15, "TriggerNo": 0 }, { "pixel_index": 1462, "pixX": 18, "pixY": 10, "ToA": 13595292.1875, "ToT": 13, "TriggerNo": 0 }, { "pixel_index": 1449, "pixX": 19, "pixY": 3, "ToA": 13595292.1875, "ToT": 23, "TriggerNo": 0 }, { "pixel_index": 1455, "pixX": 18, "pixY": 7, "ToA": 13595292.1875, "ToT": 17, "TriggerNo": 0 }, { "pixel_index": 1457, "pixX": 21, "pixY": 0, "ToA": 13595292.1875, "ToT": 76, "TriggerNo": 0 }, { "pixel_index": 1478, "pixX": 19, "pixY": 5, "ToA": 13595293.75, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 1452, "pixX": 18, "pixY": 9, "ToA": 13595293.75, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 1469, "pixX": 19, "pixY": 4, "ToA": 13595293.75, "ToT": 15, "TriggerNo": 0 }, { "pixel_index": 1436, "pixX": 21, "pixY": 17, "ToA": 13595295.3125, "ToT": 27, "TriggerNo": 0 }, { "pixel_index": 1417, "pixX": 18, "pixY": 5, "ToA": 13595295.3125, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 1410, "pixX": 22, "pixY": 0, "ToA": 13595298.4375, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 1409, "pixX": 19, "pixY": 9, "ToA": 13595300.0, "ToT": 6, "TriggerNo": 0 }, { "pixel_index": 1408, "pixX": 21, "pixY": 18, "ToA": 13595310.9375, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 1418, "pixX": 4, "pixY": 128, "ToA": 13598889.0625, "ToT": 11, "TriggerNo": 0 }, { "pixel_index": 1427, "pixX": 12, "pixY": 225, "ToA": 13600654.6875, "ToT": 32, "TriggerNo": 0 }, { "pixel_index": 1437, "pixX": 129, "pixY": 98, "ToA": 13602762.5, "ToT": 15, "TriggerNo": 0 }, { "pixel_index": 1467, "pixX": 189, "pixY": 123, "ToA": 13604385.9375, "ToT": 66, "TriggerNo": 0 }, { "pixel_index": 1453, "pixX": 188, "pixY": 123, "ToA": 13604385.9375, "ToT": 45, "TriggerNo": 0 }, { "pixel_index": 1472, "pixX": 188, "pixY": 122, "ToA": 13604385.9375, "ToT": 163, "TriggerNo": 0 }, { "pixel_index": 1483, "pixX": 189, "pixY": 122, "ToA": 13604385.9375, "ToT": 224, "TriggerNo": 0 }, { "pixel_index": 1442, "pixX": 188, "pixY": 121, "ToA": 13604385.9375, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 1446, "pixX": 189, "pixY": 121, "ToA": 13604385.9375, "ToT": 17, "TriggerNo": 0 }, { "pixel_index": 1440, "pixX": 187, "pixY": 122, "ToA": 13604387.5, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 1448, "pixX": 190, "pixY": 122, "ToA": 13604387.5, "ToT": 16, "TriggerNo": 0 }, { "pixel_index": 1441, "pixX": 190, "pixY": 123, "ToA": 13604387.5, "ToT": 2, "TriggerNo": 0 }, { "pixel_index": 1471, "pixX": 169, "pixY": 137, "ToA": 13609601.5625, "ToT": 29, "TriggerNo": 0 }, { "pixel_index": 1484, "pixX": 160, "pixY": 126, "ToA": 13609601.5625, "ToT": 22, "TriggerNo": 0 }, { "pixel_index": 1477, "pixX": 160, "pixY": 125, "ToA": 13609601.5625, "ToT": 25, "TriggerNo": 0 }, { "pixel_index": 1466, "pixX": 168, "pixY": 137, "ToA": 13609601.5625, "ToT": 21, "TriggerNo": 0 }, { "pixel_index": 1456, "pixX": 161, "pixY": 132, "ToA": 13609603.125, "ToT": 20, "TriggerNo": 0 }, { "pixel_index": 1470, "pixX": 170, "pixY": 137, "ToA": 13609606.25, "ToT": 21, "TriggerNo": 0 }, { "pixel_index": 1458, "pixX": 167, "pixY": 137, "ToA": 13609607.8125, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 1464, "pixX": 161, "pixY": 131, "ToA": 13609609.375, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 1460, "pixX": 171, "pixY": 137, "ToA": 13609609.375, "ToT": 17, "TriggerNo": 0 }, { "pixel_index": 1468, "pixX": 160, "pixY": 124, "ToA": 13609610.9375, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 1465, "pixX": 172, "pixY": 138, "ToA": 13609615.625, "ToT": 16, "TriggerNo": 0 }, { "pixel_index": 1479, "pixX": 173, "pixY": 138, "ToA": 13609615.625, "ToT": 35, "TriggerNo": 0 }, { "pixel_index": 1459, "pixX": 172, "pixY": 137, "ToA": 13609621.875, "ToT": 4, "TriggerNo": 0 }, { "pixel_index": 1461, "pixX": 174, "pixY": 138, "ToA": 13609631.25, "ToT": 8, "TriggerNo": 0 }, { "pixel_index": 1463, "pixX": 215, "pixY": 76, "ToA": 13609895.3125, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 1473, "pixX": 221, "pixY": 126, "ToA": 13611392.1875, "ToT": 56, "TriggerNo": 0 }, { "pixel_index": 1481, "pixX": 221, "pixY": 125, "ToA": 13611392.1875, "ToT": 72, "TriggerNo": 0 }, { "pixel_index": 1489, "pixX": 241, "pixY": 254, "ToA": 13615037.5, "ToT": 42, "TriggerNo": 0 }, { "pixel_index": 1494, "pixX": 240, "pixY": 255, "ToA": 13615037.5, "ToT": 62, "TriggerNo": 0 }, { "pixel_index": 1498, "pixX": 240, "pixY": 254, "ToA": 13615037.5, "ToT": 189, "TriggerNo": 0 }, { "pixel_index": 1492, "pixX": 243, "pixY": 255, "ToA": 13615039.0625, "ToT": 156, "TriggerNo": 0 }, { "pixel_index": 1495, "pixX": 242, "pixY": 255, "ToA": 13615039.0625, "ToT": 216, "TriggerNo": 0 }, { "pixel_index": 1496, "pixX": 241, "pixY": 255, "ToA": 13615039.0625, "ToT": 180, "TriggerNo": 0 }, { "pixel_index": 1497, "pixX": 239, "pixY": 254, "ToA": 13615039.0625, "ToT": 197, "TriggerNo": 0 }, { "pixel_index": 1487, "pixX": 242, "pixY": 254, "ToA": 13615040.625, "ToT": 17, "TriggerNo": 0 }, { "pixel_index": 1491, "pixX": 239, "pixY": 255, "ToA": 13615040.625, "ToT": 18, "TriggerNo": 0 }, { "pixel_index": 1482, "pixX": 243, "pixY": 254, "ToA": 13615040.625, "ToT": 9, "TriggerNo": 0 }, { "pixel_index": 1486, "pixX": 244, "pixY": 255, "ToA": 13615040.625, "ToT": 18, "TriggerNo": 0 }, { "pixel_index": 1490, "pixX": 245, "pixY": 255, "ToA": 13615042.1875, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1474, "pixX": 244, "pixY": 254, "ToA": 13615042.1875, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1485, "pixX": 240, "pixY": 253, "ToA": 13615042.1875, "ToT": 15, "TriggerNo": 0 }, { "pixel_index": 1488, "pixX": 239, "pixY": 253, "ToA": 13615046.875, "ToT": 12, "TriggerNo": 0 }, { "pixel_index": 1493, "pixX": 238, "pixY": 254, "ToA": 13615046.875, "ToT": 10, "TriggerNo": 0 }, { "pixel_index": 1476, "pixX": 246, "pixY": 255, "ToA": 13615048.4375, "ToT": 1, "TriggerNo": 0 }, { "pixel_index": 1475, "pixX": 241, "pixY": 253, "ToA": 13615056.25, "ToT": 3, "TriggerNo": 0 }, { "pixel_index": 1480, "pixX": 238, "pixY": 255, "ToA": 13615070.3125, "ToT": 5, "TriggerNo": 0 }, { "pixel_index": 1500, "pixX": 2, "pixY": 218, "ToA": 13630195.3125, "ToT": 31, "TriggerNo": 0 }, { "pixel_index": 1499, "pixX": 1, "pixY": 218, "ToA": 13630196.875, "ToT": 20, "TriggerNo": 0 }, { "pixel_index": 1506, "pixX": 162, "pixY": 32, "ToA": 13634640.625, "ToT": 67, "TriggerNo": 0 }, { "pixel_index": 1505, "pixX": 163, "pixY": 32, "ToA": 13634640.625, "ToT": 47, "TriggerNo": 0 }, { "pixel_index": 1503, "pixX": 162, "pixY": 31, "ToA": 13634642.1875, "ToT": 73, "TriggerNo": 0 }, { "pixel_index": 1504, "pixX": 161, "pixY": 31, "ToA": 13634643.75, "ToT": 33, "TriggerNo": 0 }, { "pixel_index": 1502, "pixX": 160, "pixY": 31, "ToA": 13634651.5625, "ToT": 14, "TriggerNo": 0 }, { "pixel_index": 1501, "pixX": 163, "pixY": 33, "ToA": 13634653.125, "ToT": 5, "TriggerNo": 0 },
                { "pixel_index": 1512, "pixX": 79, "pixY": 33, "ToA": 395.3125, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 1514, "pixX": 136, "pixY": 214, "ToA": 18243.75, "ToT": 16, "TriggerNo": 1 }, { "pixel_index": 1515, "pixX": 135, "pixY": 214, "ToA": 18243.75, "ToT": 20, "TriggerNo": 1 }, { "pixel_index": 1518, "pixX": 206, "pixY": 18, "ToA": 23198.4375, "ToT": 25, "TriggerNo": 1 }, { "pixel_index": 1522, "pixX": 207, "pixY": 18, "ToA": 23198.4375, "ToT": 63, "TriggerNo": 1 }, { "pixel_index": 1520, "pixX": 207, "pixY": 17, "ToA": 23198.4375, "ToT": 30, "TriggerNo": 1 }, { "pixel_index": 1517, "pixX": 206, "pixY": 17, "ToA": 23200.0, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 1516, "pixX": 56, "pixY": 151, "ToA": 23534.375, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 1521, "pixX": 170, "pixY": 210, "ToA": 26865.625, "ToT": 17, "TriggerNo": 1 }, { "pixel_index": 1519, "pixX": 171, "pixY": 210, "ToA": 26884.375, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 1523, "pixX": 197, "pixY": 184, "ToA": 32726.5625, "ToT": 10, "TriggerNo": 1 }, { "pixel_index": 1525, "pixX": 21, "pixY": 143, "ToA": 37190.625, "ToT": 16, "TriggerNo": 1 }, { "pixel_index": 1524, "pixX": 20, "pixY": 143, "ToA": 37243.75, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 1527, "pixX": 30, "pixY": 38, "ToA": 40400.0, "ToT": 23, "TriggerNo": 1 }, { "pixel_index": 1526, "pixX": 31, "pixY": 38, "ToA": 40403.125, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 1513, "pixX": 248, "pixY": 0, "ToA": 123268.75, "ToT": 111, "TriggerNo": 1 }, { "pixel_index": 1508, "pixX": 249, "pixY": 0, "ToA": 123270.3125, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 1511, "pixX": 248, "pixY": 1, "ToA": 123270.3125, "ToT": 60, "TriggerNo": 1 }, { "pixel_index": 1510, "pixX": 249, "pixY": 1, "ToA": 123271.875, "ToT": 54, "TriggerNo": 1 }, { "pixel_index": 1509, "pixX": 247, "pixY": 0, "ToA": 123275.0, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 1528, "pixX": 250, "pixY": 166, "ToA": 699209.375, "ToT": 45, "TriggerNo": 1 }, { "pixel_index": 1530, "pixX": 241, "pixY": 221, "ToA": 908404.6875, "ToT": 23, "TriggerNo": 1 }, { "pixel_index": 1529, "pixX": 241, "pixY": 220, "ToA": 908406.25, "ToT": 13, "TriggerNo": 1 }, { "pixel_index": 1535, "pixX": 69, "pixY": 171, "ToA": 1055570.3125, "ToT": 16, "TriggerNo": 1 }, { "pixel_index": 1533, "pixX": 69, "pixY": 172, "ToA": 1055571.875, "ToT": 12, "TriggerNo": 1 }, { "pixel_index": 1537, "pixX": 70, "pixY": 173, "ToA": 1055571.875, "ToT": 12, "TriggerNo": 1 }, { "pixel_index": 1539, "pixX": 70, "pixY": 174, "ToA": 1055571.875, "ToT": 13, "TriggerNo": 1 }, { "pixel_index": 1534, "pixX": 70, "pixY": 172, "ToA": 1055573.4375, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 1540, "pixX": 70, "pixY": 175, "ToA": 1055573.4375, "ToT": 14, "TriggerNo": 1 }, { "pixel_index": 1536, "pixX": 71, "pixY": 176, "ToA": 1055578.125, "ToT": 18, "TriggerNo": 1 }, { "pixel_index": 1531, "pixX": 70, "pixY": 176, "ToA": 1055581.25, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 1538, "pixX": 71, "pixY": 177, "ToA": 1055582.8125, "ToT": 16, "TriggerNo": 1 }, { "pixel_index": 1532, "pixX": 72, "pixY": 177, "ToA": 1055592.1875, "ToT": 12, "TriggerNo": 1 }, { "pixel_index": 1542, "pixX": 215, "pixY": 138, "ToA": 1952459.375, "ToT": 49, "TriggerNo": 1 }, { "pixel_index": 1541, "pixX": 216, "pixY": 138, "ToA": 1952465.625, "ToT": 7, "TriggerNo": 1 },
                { "pixel_index": 1544, "pixX": 249, "pixY": 251, "ToA": 2326992.1875, "ToT": 21, "TriggerNo": 1 }, { "pixel_index": 1543, "pixX": 248, "pixY": 251, "ToA": 2327001.5625, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 1557, "pixX": 135, "pixY": 161, "ToA": 2387539.0625, "ToT": 17, "TriggerNo": 1 }, { "pixel_index": 1561, "pixX": 135, "pixY": 162, "ToA": 2387540.625, "ToT": 11, "TriggerNo": 1 }, { "pixel_index": 1558, "pixX": 135, "pixY": 157, "ToA": 2387540.625, "ToT": 12, "TriggerNo": 1 }, { "pixel_index": 1562, "pixX": 135, "pixY": 158, "ToA": 2387542.1875, "ToT": 16, "TriggerNo": 1 }, { "pixel_index": 1549, "pixX": 135, "pixY": 159, "ToA": 2387542.1875, "ToT": 11, "TriggerNo": 1 }, { "pixel_index": 1554, "pixX": 135, "pixY": 156, "ToA": 2387542.1875, "ToT": 14, "TriggerNo": 1 }, { "pixel_index": 1553, "pixX": 135, "pixY": 160, "ToA": 2387542.1875, "ToT": 10, "TriggerNo": 1 }, { "pixel_index": 1546, "pixX": 135, "pixY": 164, "ToA": 2387543.75, "ToT": 11, "TriggerNo": 1 }, { "pixel_index": 1559, "pixX": 135, "pixY": 155, "ToA": 2387545.3125, "ToT": 25, "TriggerNo": 1 }, { "pixel_index": 1566, "pixX": 135, "pixY": 154, "ToA": 2387545.3125, "ToT": 54, "TriggerNo": 1 }, { "pixel_index": 1548, "pixX": 135, "pixY": 163, "ToA": 2387545.3125, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 1547, "pixX": 136, "pixY": 163, "ToA": 2387546.875, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 1555, "pixX": 135, "pixY": 153, "ToA": 2387548.4375, "ToT": 14, "TriggerNo": 1 }, { "pixel_index": 1563, "pixX": 134, "pixY": 152, "ToA": 2387548.4375, "ToT": 22, "TriggerNo": 1 }, { "pixel_index": 1556, "pixX": 134, "pixY": 150, "ToA": 2387551.5625, "ToT": 18, "TriggerNo": 1 }, { "pixel_index": 1565, "pixX": 134, "pixY": 153, "ToA": 2387551.5625, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 1560, "pixX": 134, "pixY": 151, "ToA": 2387551.5625, "ToT": 14, "TriggerNo": 1 }, { "pixel_index": 1550, "pixX": 134, "pixY": 154, "ToA": 2387551.5625, "ToT": 3, "TriggerNo": 1 }, { "pixel_index": 1551, "pixX": 134, "pixY": 149, "ToA": 2387556.25, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 1564, "pixX": 134, "pixY": 148, "ToA": 2387556.25, "ToT": 13, "TriggerNo": 1 }, { "pixel_index": 1545, "pixX": 136, "pixY": 164, "ToA": 2387567.1875, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 1552, "pixX": 134, "pixY": 147, "ToA": 2387571.875, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 1568, "pixX": 168, "pixY": 210, "ToA": 3441000.0, "ToT": 40, "TriggerNo": 1 }, { "pixel_index": 1569, "pixX": 168, "pixY": 211, "ToA": 3441000.0, "ToT": 63, "TriggerNo": 1 }, { "pixel_index": 1570, "pixX": 168, "pixY": 209, "ToA": 3441000.0, "ToT": 70, "TriggerNo": 1 }, { "pixel_index": 1567, "pixX": 169, "pixY": 211, "ToA": 3441017.1875, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 1572, "pixX": 117, "pixY": 172, "ToA": 4014728.125, "ToT": 68, "TriggerNo": 1 }, { "pixel_index": 1571, "pixX": 118, "pixY": 172, "ToA": 4014740.625, "ToT": 34, "TriggerNo": 1 },
                { "pixel_index": 1573, "pixX": 47, "pixY": 204, "ToA": 4649768.75, "ToT": 16, "TriggerNo": 1 },
                { "pixel_index": 1587, "pixX": 191, "pixY": 204, "ToA": 7754204.6875, "ToT": 87, "TriggerNo": 1 }, { "pixel_index": 1589, "pixX": 188, "pixY": 203, "ToA": 7754204.6875, "ToT": 33, "TriggerNo": 1 }, { "pixel_index": 1580, "pixX": 190, "pixY": 204, "ToA": 7754204.6875, "ToT": 64, "TriggerNo": 1 }, { "pixel_index": 1575, "pixX": 191, "pixY": 205, "ToA": 7754204.6875, "ToT": 16, "TriggerNo": 1 }, { "pixel_index": 1582, "pixX": 189, "pixY": 203, "ToA": 7754206.25, "ToT": 12, "TriggerNo": 1 }, { "pixel_index": 1574, "pixX": 189, "pixY": 204, "ToA": 7754206.25, "ToT": 25, "TriggerNo": 1 }, { "pixel_index": 1577, "pixX": 188, "pixY": 202, "ToA": 7754209.375, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 1579, "pixX": 186, "pixY": 202, "ToA": 7754209.375, "ToT": 25, "TriggerNo": 1 }, { "pixel_index": 1586, "pixX": 187, "pixY": 202, "ToA": 7754209.375, "ToT": 14, "TriggerNo": 1 }, { "pixel_index": 1576, "pixX": 187, "pixY": 203, "ToA": 7754210.9375, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 1581, "pixX": 184, "pixY": 201, "ToA": 7754215.625, "ToT": 28, "TriggerNo": 1 }, { "pixel_index": 1578, "pixX": 185, "pixY": 202, "ToA": 7754215.625, "ToT": 26, "TriggerNo": 1 }, { "pixel_index": 1588, "pixX": 184, "pixY": 202, "ToA": 7754215.625, "ToT": 39, "TriggerNo": 1 }, { "pixel_index": 1585, "pixX": 137, "pixY": 16, "ToA": 7757234.375, "ToT": 24, "TriggerNo": 1 }, { "pixel_index": 1593, "pixX": 253, "pixY": 237, "ToA": 7758165.625, "ToT": 182, "TriggerNo": 1 }, { "pixel_index": 1591, "pixX": 253, "pixY": 236, "ToA": 7758168.75, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 1592, "pixX": 253, "pixY": 238, "ToA": 7758173.4375, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 1584, "pixX": 252, "pixY": 237, "ToA": 7758173.4375, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 1583, "pixX": 254, "pixY": 237, "ToA": 7758175.0, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 1590, "pixX": 22, "pixY": 113, "ToA": 7758787.5, "ToT": 32, "TriggerNo": 1 }, { "pixel_index": 1595, "pixX": 147, "pixY": 231, "ToA": 7770434.375, "ToT": 48, "TriggerNo": 1 }, { "pixel_index": 1594, "pixX": 147, "pixY": 232, "ToA": 7770451.5625, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 1603, "pixX": 24, "pixY": 254, "ToA": 7772468.75, "ToT": 61, "TriggerNo": 1 }, { "pixel_index": 1600, "pixX": 26, "pixY": 255, "ToA": 7772470.3125, "ToT": 12, "TriggerNo": 1 }, { "pixel_index": 1599, "pixX": 25, "pixY": 253, "ToA": 7772471.875, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 1598, "pixX": 26, "pixY": 254, "ToA": 7772471.875, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 1596, "pixX": 24, "pixY": 253, "ToA": 7772471.875, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 1601, "pixX": 23, "pixY": 255, "ToA": 7772473.4375, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 1597, "pixX": 23, "pixY": 254, "ToA": 7772473.4375, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 1604, "pixX": 223, "pixY": 94, "ToA": 7775675.0, "ToT": 56, "TriggerNo": 1 }, { "pixel_index": 1602, "pixX": 224, "pixY": 94, "ToA": 7775678.125, "ToT": 17, "TriggerNo": 1 }, { "pixel_index": 1605, "pixX": 152, "pixY": 233, "ToA": 7778564.0625, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 1607, "pixX": 217, "pixY": 149, "ToA": 7782353.125, "ToT": 25, "TriggerNo": 1 }, { "pixel_index": 1608, "pixX": 152, "pixY": 234, "ToA": 7782359.375, "ToT": 109, "TriggerNo": 1 }, { "pixel_index": 1606, "pixX": 25, "pixY": 254, "ToA": 7782368.75, "ToT": 92, "TriggerNo": 1 }, { "pixel_index": 1609, "pixX": 24, "pixY": 255, "ToA": 7782368.75, "ToT": 161, "TriggerNo": 1 }, { "pixel_index": 1610, "pixX": 25, "pixY": 255, "ToA": 7782368.75, "ToT": 234, "TriggerNo": 1 }, { "pixel_index": 1614, "pixX": 128, "pixY": 198, "ToA": 7784615.625, "ToT": 41, "TriggerNo": 1 }, { "pixel_index": 1615, "pixX": 128, "pixY": 199, "ToA": 7784615.625, "ToT": 67, "TriggerNo": 1 }, { "pixel_index": 1613, "pixX": 129, "pixY": 198, "ToA": 7784620.3125, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 1612, "pixX": 110, "pixY": 195, "ToA": 7784626.5625, "ToT": 14, "TriggerNo": 1 }, { "pixel_index": 1611, "pixX": 129, "pixY": 199, "ToA": 7784650.0, "ToT": 3, "TriggerNo": 1 }, { "pixel_index": 1616, "pixX": 120, "pixY": 117, "ToA": 7790003.125, "ToT": 94, "TriggerNo": 1 }, { "pixel_index": 1617, "pixX": 48, "pixY": 50, "ToA": 7795356.25, "ToT": 57, "TriggerNo": 1 }, { "pixel_index": 1621, "pixX": 79, "pixY": 178, "ToA": 7802501.5625, "ToT": 19, "TriggerNo": 1 }, { "pixel_index": 1619, "pixX": 79, "pixY": 179, "ToA": 7802503.125, "ToT": 12, "TriggerNo": 1 }, { "pixel_index": 1620, "pixX": 80, "pixY": 179, "ToA": 7802506.25, "ToT": 67, "TriggerNo": 1 }, { "pixel_index": 1618, "pixX": 80, "pixY": 180, "ToA": 7802535.9375, "ToT": 3, "TriggerNo": 1 }, { "pixel_index": 1623, "pixX": 234, "pixY": 140, "ToA": 7806221.875, "ToT": 28, "TriggerNo": 1 }, { "pixel_index": 1622, "pixX": 233, "pixY": 140, "ToA": 7806223.4375, "ToT": 19, "TriggerNo": 1 }, { "pixel_index": 1661, "pixX": 103, "pixY": 123, "ToA": 7807878.125, "ToT": 27, "TriggerNo": 1 }, { "pixel_index": 1643, "pixX": 102, "pixY": 123, "ToA": 7807878.125, "ToT": 13, "TriggerNo": 1 }, { "pixel_index": 1651, "pixX": 103, "pixY": 122, "ToA": 7807878.125, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 1628, "pixX": 102, "pixY": 122, "ToA": 7807881.25, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 1658, "pixX": 232, "pixY": 91, "ToA": 7807910.9375, "ToT": 97, "TriggerNo": 1 }, { "pixel_index": 1692, "pixX": 235, "pixY": 102, "ToA": 7807912.5, "ToT": 10, "TriggerNo": 1 }, { "pixel_index": 2093, "pixX": 234, "pixY": 103, "ToA": 7807912.5, "ToT": 155, "TriggerNo": 1 }, { "pixel_index": 1897, "pixX": 234, "pixY": 101, "ToA": 7807912.5, "ToT": 122, "TriggerNo": 1 }, { "pixel_index": 1695, "pixX": 233, "pixY": 97, "ToA": 7807912.5, "ToT": 153, "TriggerNo": 1 }, { "pixel_index": 1696, "pixX": 234, "pixY": 98, "ToA": 7807912.5, "ToT": 10, "TriggerNo": 1 }, { "pixel_index": 1714, "pixX": 232, "pixY": 92, "ToA": 7807912.5, "ToT": 127, "TriggerNo": 1 }, { "pixel_index": 1699, "pixX": 233, "pixY": 95, "ToA": 7807912.5, "ToT": 119, "TriggerNo": 1 }, { "pixel_index": 1728, "pixX": 234, "pixY": 99, "ToA": 7807912.5, "ToT": 72, "TriggerNo": 1 }, { "pixel_index": 1722, "pixX": 235, "pixY": 103, "ToA": 7807912.5, "ToT": 14, "TriggerNo": 1 }, { "pixel_index": 1753, "pixX": 234, "pixY": 100, "ToA": 7807912.5, "ToT": 106, "TriggerNo": 1 }, { "pixel_index": 1721, "pixX": 233, "pixY": 99, "ToA": 7807912.5, "ToT": 57, "TriggerNo": 1 }, { "pixel_index": 1683, "pixX": 233, "pixY": 96, "ToA": 7807912.5, "ToT": 108, "TriggerNo": 1 }, { "pixel_index": 1707, "pixX": 233, "pixY": 98, "ToA": 7807912.5, "ToT": 93, "TriggerNo": 1 }, { "pixel_index": 1689, "pixX": 233, "pixY": 94, "ToA": 7807912.5, "ToT": 89, "TriggerNo": 1 }, { "pixel_index": 2031, "pixX": 234, "pixY": 102, "ToA": 7807912.5, "ToT": 111, "TriggerNo": 1 }, { "pixel_index": 1727, "pixX": 232, "pixY": 93, "ToA": 7807912.5, "ToT": 87, "TriggerNo": 1 }, { "pixel_index": 1990, "pixX": 235, "pixY": 107, "ToA": 7807914.0625, "ToT": 109, "TriggerNo": 1 }, { "pixel_index": 1629, "pixX": 233, "pixY": 101, "ToA": 7807914.0625, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 1667, "pixX": 233, "pixY": 103, "ToA": 7807914.0625, "ToT": 12, "TriggerNo": 1 }, { "pixel_index": 1665, "pixX": 234, "pixY": 106, "ToA": 7807914.0625, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 1668, "pixX": 235, "pixY": 101, "ToA": 7807914.0625, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 1669, "pixX": 234, "pixY": 97, "ToA": 7807914.0625, "ToT": 10, "TriggerNo": 1 }, { "pixel_index": 1850, "pixX": 235, "pixY": 106, "ToA": 7807914.0625, "ToT": 114, "TriggerNo": 1 }, { "pixel_index": 1734, "pixX": 232, "pixY": 94, "ToA": 7807914.0625, "ToT": 16, "TriggerNo": 1 }, { "pixel_index": 1673, "pixX": 233, "pixY": 93, "ToA": 7807914.0625, "ToT": 16, "TriggerNo": 1 }, { "pixel_index": 1715, "pixX": 235, "pixY": 104, "ToA": 7807914.0625, "ToT": 45, "TriggerNo": 1 }, { "pixel_index": 1680, "pixX": 233, "pixY": 100, "ToA": 7807914.0625, "ToT": 11, "TriggerNo": 1 }, { "pixel_index": 1649, "pixX": 233, "pixY": 102, "ToA": 7807914.0625, "ToT": 10, "TriggerNo": 1 }, { "pixel_index": 1743, "pixX": 235, "pixY": 105, "ToA": 7807914.0625, "ToT": 115, "TriggerNo": 1 }, { "pixel_index": 1638, "pixX": 234, "pixY": 107, "ToA": 7807914.0625, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 2078, "pixX": 234, "pixY": 104, "ToA": 7807914.0625, "ToT": 85, "TriggerNo": 1 }, { "pixel_index": 1690, "pixX": 234, "pixY": 105, "ToA": 7807914.0625, "ToT": 38, "TriggerNo": 1 }, { "pixel_index": 1716, "pixX": 236, "pixY": 112, "ToA": 7807915.625, "ToT": 99, "TriggerNo": 1 }, { "pixel_index": 1803, "pixX": 235, "pixY": 108, "ToA": 7807915.625, "ToT": 103, "TriggerNo": 1 }, { "pixel_index": 1666, "pixX": 236, "pixY": 106, "ToA": 7807915.625, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 1646, "pixX": 236, "pixY": 105, "ToA": 7807915.625, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 1691, "pixX": 236, "pixY": 107, "ToA": 7807915.625, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 1851, "pixX": 236, "pixY": 111, "ToA": 7807915.625, "ToT": 124, "TriggerNo": 1 }, { "pixel_index": 1708, "pixX": 234, "pixY": 108, "ToA": 7807915.625, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 1945, "pixX": 235, "pixY": 109, "ToA": 7807915.625, "ToT": 116, "TriggerNo": 1 }, { "pixel_index": 1723, "pixX": 236, "pixY": 109, "ToA": 7807915.625, "ToT": 27, "TriggerNo": 1 }, { "pixel_index": 1804, "pixX": 236, "pixY": 114, "ToA": 7807915.625, "ToT": 131, "TriggerNo": 1 }, { "pixel_index": 1701, "pixX": 236, "pixY": 108, "ToA": 7807915.625, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 1736, "pixX": 236, "pixY": 113, "ToA": 7807915.625, "ToT": 116, "TriggerNo": 1 }, { "pixel_index": 2058, "pixX": 235, "pixY": 110, "ToA": 7807915.625, "ToT": 74, "TriggerNo": 1 }, { "pixel_index": 1682, "pixX": 237, "pixY": 114, "ToA": 7807915.625, "ToT": 28, "TriggerNo": 1 }, { "pixel_index": 1684, "pixX": 235, "pixY": 111, "ToA": 7807915.625, "ToT": 25, "TriggerNo": 1 }, { "pixel_index": 1637, "pixX": 237, "pixY": 112, "ToA": 7807915.625, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 1660, "pixX": 237, "pixY": 113, "ToA": 7807915.625, "ToT": 10, "TriggerNo": 1 }, { "pixel_index": 1685, "pixX": 237, "pixY": 111, "ToA": 7807915.625, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 1663, "pixX": 237, "pixY": 110, "ToA": 7807915.625, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 1744, "pixX": 236, "pixY": 110, "ToA": 7807915.625, "ToT": 85, "TriggerNo": 1 }, { "pixel_index": 1700, "pixX": 235, "pixY": 114, "ToA": 7807917.1875, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 1641, "pixX": 237, "pixY": 109, "ToA": 7807917.1875, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 1681, "pixX": 235, "pixY": 113, "ToA": 7807917.1875, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 1659, "pixX": 235, "pixY": 112, "ToA": 7807917.1875, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 1735, "pixX": 234, "pixY": 109, "ToA": 7807917.1875, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 1635, "pixX": 234, "pixY": 110, "ToA": 7807917.1875, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 1624, "pixX": 233, "pixY": 104, "ToA": 7807917.1875, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 1662, "pixX": 234, "pixY": 111, "ToA": 7807917.1875, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 1946, "pixX": 236, "pixY": 115, "ToA": 7807917.1875, "ToT": 83, "TriggerNo": 1 }, { "pixel_index": 1697, "pixX": 237, "pixY": 115, "ToA": 7807917.1875, "ToT": 61, "TriggerNo": 1 }, { "pixel_index": 1670, "pixX": 232, "pixY": 97, "ToA": 7807918.75, "ToT": 11, "TriggerNo": 1 }, { "pixel_index": 1631, "pixX": 235, "pixY": 115, "ToA": 7807918.75, "ToT": 3, "TriggerNo": 1 }, { "pixel_index": 1657, "pixX": 236, "pixY": 117, "ToA": 7807920.3125, "ToT": 10, "TriggerNo": 1 }, { "pixel_index": 1639, "pixX": 238, "pixY": 114, "ToA": 7807920.3125, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 1898, "pixX": 236, "pixY": 116, "ToA": 7807920.3125, "ToT": 36, "TriggerNo": 1 }, { "pixel_index": 1709, "pixX": 237, "pixY": 117, "ToA": 7807920.3125, "ToT": 111, "TriggerNo": 1 }, { "pixel_index": 1642, "pixX": 235, "pixY": 100, "ToA": 7807920.3125, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 1645, "pixX": 234, "pixY": 96, "ToA": 7807920.3125, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 1693, "pixX": 237, "pixY": 116, "ToA": 7807920.3125, "ToT": 101, "TriggerNo": 1 }, { "pixel_index": 1652, "pixX": 232, "pixY": 96, "ToA": 7807920.3125, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 1654, "pixX": 238, "pixY": 115, "ToA": 7807920.3125, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 1729, "pixX": 237, "pixY": 118, "ToA": 7807921.875, "ToT": 127, "TriggerNo": 1 }, { "pixel_index": 1678, "pixX": 236, "pixY": 118, "ToA": 7807923.4375, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 1634, "pixX": 238, "pixY": 116, "ToA": 7807923.4375, "ToT": 3, "TriggerNo": 1 }, { "pixel_index": 1664, "pixX": 238, "pixY": 118, "ToA": 7807923.4375, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 1636, "pixX": 232, "pixY": 95, "ToA": 7807923.4375, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 1644, "pixX": 231, "pixY": 92, "ToA": 7807923.4375, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 1650, "pixX": 238, "pixY": 117, "ToA": 7807923.4375, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 1655, "pixX": 233, "pixY": 92, "ToA": 7807923.4375, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 1625, "pixX": 235, "pixY": 116, "ToA": 7807923.4375, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 1632, "pixX": 232, "pixY": 98, "ToA": 7807925.0, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 1656, "pixX": 235, "pixY": 117, "ToA": 7807925.0, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 1754, "pixX": 237, "pixY": 119, "ToA": 7807925.0, "ToT": 104, "TriggerNo": 1 }, { "pixel_index": 1671, "pixX": 234, "pixY": 95, "ToA": 7807926.5625, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 1633, "pixX": 236, "pixY": 119, "ToA": 7807926.5625, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 1675, "pixX": 238, "pixY": 119, "ToA": 7807928.125, "ToT": 18, "TriggerNo": 1 }, { "pixel_index": 1648, "pixX": 238, "pixY": 120, "ToA": 7807929.6875, "ToT": 50, "TriggerNo": 1 }, { "pixel_index": 1674, "pixX": 237, "pixY": 120, "ToA": 7807929.6875, "ToT": 86, "TriggerNo": 1 }, { "pixel_index": 1626, "pixX": 236, "pixY": 120, "ToA": 7807932.8125, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 1640, "pixX": 233, "pixY": 91, "ToA": 7807932.8125, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 1653, "pixX": 237, "pixY": 121, "ToA": 7807935.9375, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 1630, "pixX": 231, "pixY": 93, "ToA": 7807937.5, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 1627, "pixX": 238, "pixY": 121, "ToA": 7807937.5, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 1647, "pixX": 234, "pixY": 94, "ToA": 7807945.3125, "ToT": 3, "TriggerNo": 1 }, { "pixel_index": 1672, "pixX": 102, "pixY": 59, "ToA": 7816600.0, "ToT": 14, "TriggerNo": 1 }, { "pixel_index": 1679, "pixX": 209, "pixY": 248, "ToA": 7818600.0, "ToT": 41, "TriggerNo": 1 }, { "pixel_index": 1688, "pixX": 208, "pixY": 249, "ToA": 7818600.0, "ToT": 66, "TriggerNo": 1 }, { "pixel_index": 1698, "pixX": 209, "pixY": 249, "ToA": 7818600.0, "ToT": 110, "TriggerNo": 1 }, { "pixel_index": 1694, "pixX": 206, "pixY": 250, "ToA": 7818615.625, "ToT": 18, "TriggerNo": 1 }, { "pixel_index": 1687, "pixX": 207, "pixY": 250, "ToA": 7818615.625, "ToT": 18, "TriggerNo": 1 }, { "pixel_index": 1706, "pixX": 206, "pixY": 251, "ToA": 7818615.625, "ToT": 13, "TriggerNo": 1 }, { "pixel_index": 1686, "pixX": 205, "pixY": 251, "ToA": 7818617.1875, "ToT": 22, "TriggerNo": 1 }, { "pixel_index": 1676, "pixX": 204, "pixY": 251, "ToA": 7818620.3125, "ToT": 13, "TriggerNo": 1 }, { "pixel_index": 1677, "pixX": 207, "pixY": 249, "ToA": 7818623.4375, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 1738, "pixX": 12, "pixY": 44, "ToA": 7824193.75, "ToT": 25, "TriggerNo": 1 }, { "pixel_index": 1755, "pixX": 11, "pixY": 44, "ToA": 7824193.75, "ToT": 52, "TriggerNo": 1 }, { "pixel_index": 1899, "pixX": 11, "pixY": 47, "ToA": 7824193.75, "ToT": 61, "TriggerNo": 1 }, { "pixel_index": 1745, "pixX": 10, "pixY": 46, "ToA": 7824193.75, "ToT": 54, "TriggerNo": 1 }, { "pixel_index": 1746, "pixX": 12, "pixY": 45, "ToA": 7824193.75, "ToT": 125, "TriggerNo": 1 }, { "pixel_index": 1805, "pixX": 11, "pixY": 45, "ToA": 7824193.75, "ToT": 291, "TriggerNo": 1 }, { "pixel_index": 1852, "pixX": 11, "pixY": 46, "ToA": 7824193.75, "ToT": 271, "TriggerNo": 1 }, { "pixel_index": 1806, "pixX": 13, "pixY": 45, "ToA": 7824193.75, "ToT": 28, "TriggerNo": 1 }, { "pixel_index": 1737, "pixX": 10, "pixY": 45, "ToA": 7824193.75, "ToT": 52, "TriggerNo": 1 }, { "pixel_index": 1731, "pixX": 13, "pixY": 47, "ToA": 7824195.3125, "ToT": 23, "TriggerNo": 1 }, { "pixel_index": 1900, "pixX": 12, "pixY": 46, "ToA": 7824195.3125, "ToT": 292, "TriggerNo": 1 }, { "pixel_index": 1756, "pixX": 12, "pixY": 47, "ToA": 7824195.3125, "ToT": 73, "TriggerNo": 1 }, { "pixel_index": 1730, "pixX": 10, "pixY": 47, "ToA": 7824195.3125, "ToT": 18, "TriggerNo": 1 }, { "pixel_index": 1853, "pixX": 13, "pixY": 46, "ToA": 7824195.3125, "ToT": 62, "TriggerNo": 1 }, { "pixel_index": 1711, "pixX": 10, "pixY": 44, "ToA": 7824198.4375, "ToT": 16, "TriggerNo": 1 }, { "pixel_index": 1712, "pixX": 13, "pixY": 44, "ToA": 7824206.25, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 1725, "pixX": 12, "pixY": 48, "ToA": 7824206.25, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 1724, "pixX": 11, "pixY": 48, "ToA": 7824207.8125, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 1720, "pixX": 14, "pixY": 46, "ToA": 7824212.5, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 1717, "pixX": 9, "pixY": 46, "ToA": 7824212.5, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 1710, "pixX": 9, "pixY": 45, "ToA": 7824212.5, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 1718, "pixX": 11, "pixY": 43, "ToA": 7824218.75, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 1713, "pixX": 14, "pixY": 45, "ToA": 7824223.4375, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 1703, "pixX": 10, "pixY": 48, "ToA": 7824228.125, "ToT": 3, "TriggerNo": 1 }, { "pixel_index": 1704, "pixX": 13, "pixY": 48, "ToA": 7824229.6875, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 1702, "pixX": 9, "pixY": 47, "ToA": 7824240.625, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 1705, "pixX": 14, "pixY": 47, "ToA": 7824254.6875, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 1719, "pixX": 12, "pixY": 43, "ToA": 7824257.8125, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 1739, "pixX": 34, "pixY": 241, "ToA": 7831123.4375, "ToT": 77, "TriggerNo": 1 }, { "pixel_index": 1726, "pixX": 35, "pixY": 241, "ToA": 7831129.6875, "ToT": 18, "TriggerNo": 1 }, { "pixel_index": 1733, "pixX": 202, "pixY": 249, "ToA": 7831965.625, "ToT": 52, "TriggerNo": 1 }, { "pixel_index": 1742, "pixX": 203, "pixY": 249, "ToA": 7831965.625, "ToT": 91, "TriggerNo": 1 }, { "pixel_index": 1741, "pixX": 138, "pixY": 236, "ToA": 7833025.0, "ToT": 23, "TriggerNo": 1 }, { "pixel_index": 1842, "pixX": 138, "pixY": 237, "ToA": 7833026.5625, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 1740, "pixX": 137, "pixY": 236, "ToA": 7833031.25, "ToT": 23, "TriggerNo": 1 }, { "pixel_index": 1732, "pixX": 139, "pixY": 237, "ToA": 7833031.25, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 1747, "pixX": 137, "pixY": 235, "ToA": 7833037.5, "ToT": 24, "TriggerNo": 1 }, { "pixel_index": 1933, "pixX": 136, "pixY": 33, "ToA": 7836560.9375, "ToT": 87, "TriggerNo": 1 }, { "pixel_index": 1979, "pixX": 136, "pixY": 34, "ToA": 7836560.9375, "ToT": 63, "TriggerNo": 1 }, { "pixel_index": 2022, "pixX": 137, "pixY": 34, "ToA": 7836560.9375, "ToT": 125, "TriggerNo": 1 }, { "pixel_index": 1919, "pixX": 108, "pixY": 18, "ToA": 7836562.5, "ToT": 51, "TriggerNo": 1 }, { "pixel_index": 1918, "pixX": 106, "pixY": 18, "ToA": 7836562.5, "ToT": 64, "TriggerNo": 1 }, { "pixel_index": 1917, "pixX": 104, "pixY": 17, "ToA": 7836562.5, "ToT": 45, "TriggerNo": 1 }, { "pixel_index": 2041, "pixX": 91, "pixY": 9, "ToA": 7836562.5, "ToT": 106, "TriggerNo": 1 }, { "pixel_index": 1965, "pixX": 108, "pixY": 19, "ToA": 7836562.5, "ToT": 94, "TriggerNo": 1 }, { "pixel_index": 1812, "pixX": 79, "pixY": 1, "ToA": 7836562.5, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 1915, "pixX": 101, "pixY": 14, "ToA": 7836562.5, "ToT": 18, "TriggerNo": 1 }, { "pixel_index": 1796, "pixX": 146, "pixY": 38, "ToA": 7836562.5, "ToT": 35, "TriggerNo": 1 }, { "pixel_index": 1797, "pixX": 151, "pixY": 41, "ToA": 7836562.5, "ToT": 30, "TriggerNo": 1 }, { "pixel_index": 1981, "pixX": 141, "pixY": 36, "ToA": 7836562.5, "ToT": 131, "TriggerNo": 1 }, { "pixel_index": 1980, "pixX": 139, "pixY": 35, "ToA": 7836562.5, "ToT": 103, "TriggerNo": 1 }, { "pixel_index": 1978, "pixX": 134, "pixY": 32, "ToA": 7836562.5, "ToT": 95, "TriggerNo": 1 }, { "pixel_index": 1977, "pixX": 133, "pixY": 32, "ToA": 7836562.5, "ToT": 155, "TriggerNo": 1 }, { "pixel_index": 1976, "pixX": 131, "pixY": 31, "ToA": 7836562.5, "ToT": 101, "TriggerNo": 1 }, { "pixel_index": 2056, "pixX": 213, "pixY": 72, "ToA": 7836562.5, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 2057, "pixX": 214, "pixY": 72, "ToA": 7836562.5, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 1975, "pixX": 128, "pixY": 30, "ToA": 7836562.5, "ToT": 42, "TriggerNo": 1 }, { "pixel_index": 1974, "pixX": 127, "pixY": 29, "ToA": 7836562.5, "ToT": 126, "TriggerNo": 1 }, { "pixel_index": 1972, "pixX": 123, "pixY": 27, "ToA": 7836562.5, "ToT": 106, "TriggerNo": 1 }, { "pixel_index": 1971, "pixX": 120, "pixY": 25, "ToA": 7836562.5, "ToT": 122, "TriggerNo": 1 }, { "pixel_index": 1970, "pixX": 119, "pixY": 25, "ToA": 7836562.5, "ToT": 93, "TriggerNo": 1 }, { "pixel_index": 1969, "pixX": 116, "pixY": 23, "ToA": 7836562.5, "ToT": 100, "TriggerNo": 1 }, { "pixel_index": 1968, "pixX": 115, "pixY": 22, "ToA": 7836562.5, "ToT": 71, "TriggerNo": 1 }, { "pixel_index": 2076, "pixX": 213, "pixY": 73, "ToA": 7836562.5, "ToT": 53, "TriggerNo": 1 }, { "pixel_index": 2077, "pixX": 214, "pixY": 73, "ToA": 7836562.5, "ToT": 50, "TriggerNo": 1 }, { "pixel_index": 1967, "pixX": 113, "pixY": 22, "ToA": 7836562.5, "ToT": 41, "TriggerNo": 1 }, { "pixel_index": 1966, "pixX": 110, "pixY": 20, "ToA": 7836562.5, "ToT": 93, "TriggerNo": 1 }, { "pixel_index": 1964, "pixX": 107, "pixY": 18, "ToA": 7836562.5, "ToT": 125, "TriggerNo": 1 }, { "pixel_index": 1963, "pixX": 104, "pixY": 16, "ToA": 7836562.5, "ToT": 66, "TriggerNo": 1 }, { "pixel_index": 2091, "pixX": 213, "pixY": 74, "ToA": 7836562.5, "ToT": 313, "TriggerNo": 1 }, { "pixel_index": 2092, "pixX": 214, "pixY": 74, "ToA": 7836562.5, "ToT": 103, "TriggerNo": 1 }, { "pixel_index": 1962, "pixX": 103, "pixY": 16, "ToA": 7836562.5, "ToT": 117, "TriggerNo": 1 }, { "pixel_index": 1961, "pixX": 101, "pixY": 15, "ToA": 7836562.5, "ToT": 97, "TriggerNo": 1 }, { "pixel_index": 1956, "pixX": 90, "pixY": 8, "ToA": 7836562.5, "ToT": 49, "TriggerNo": 1 }, { "pixel_index": 1912, "pixX": 94, "pixY": 10, "ToA": 7836562.5, "ToT": 22, "TriggerNo": 1 }, { "pixel_index": 1910, "pixX": 91, "pixY": 10, "ToA": 7836562.5, "ToT": 19, "TriggerNo": 1 }, { "pixel_index": 1904, "pixX": 78, "pixY": 3, "ToA": 7836562.5, "ToT": 16, "TriggerNo": 1 }, { "pixel_index": 1953, "pixX": 84, "pixY": 6, "ToA": 7836562.5, "ToT": 29, "TriggerNo": 1 }, { "pixel_index": 2104, "pixX": 213, "pixY": 75, "ToA": 7836562.5, "ToT": 212, "TriggerNo": 1 }, { "pixel_index": 2105, "pixX": 214, "pixY": 75, "ToA": 7836562.5, "ToT": 156, "TriggerNo": 1 }, { "pixel_index": 1950, "pixX": 79, "pixY": 2, "ToA": 7836562.5, "ToT": 76, "TriggerNo": 1 }, { "pixel_index": 1949, "pixX": 77, "pixY": 1, "ToA": 7836562.5, "ToT": 97, "TriggerNo": 1 }, { "pixel_index": 2115, "pixX": 212, "pixY": 74, "ToA": 7836562.5, "ToT": 44, "TriggerNo": 1 }, { "pixel_index": 2116, "pixX": 215, "pixY": 74, "ToA": 7836562.5, "ToT": 41, "TriggerNo": 1 }, { "pixel_index": 1936, "pixX": 143, "pixY": 37, "ToA": 7836562.5, "ToT": 121, "TriggerNo": 1 }, { "pixel_index": 1811, "pixX": 76, "pixY": 0, "ToA": 7836562.5, "ToT": 13, "TriggerNo": 1 }, { "pixel_index": 1844, "pixX": 142, "pixY": 36, "ToA": 7836562.5, "ToT": 44, "TriggerNo": 1 }, { "pixel_index": 1761, "pixX": 77, "pixY": 3, "ToA": 7836562.5, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 1935, "pixX": 140, "pixY": 35, "ToA": 7836562.5, "ToT": 59, "TriggerNo": 1 }, { "pixel_index": 1934, "pixX": 138, "pixY": 35, "ToA": 7836562.5, "ToT": 78, "TriggerNo": 1 }, { "pixel_index": 1764, "pixX": 83, "pixY": 6, "ToA": 7836562.5, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 1765, "pixX": 84, "pixY": 4, "ToA": 7836562.5, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 1895, "pixX": 215, "pixY": 73, "ToA": 7836562.5, "ToT": 15, "TriggerNo": 1 }, { "pixel_index": 1932, "pixX": 134, "pixY": 33, "ToA": 7836562.5, "ToT": 42, "TriggerNo": 1 }, { "pixel_index": 1768, "pixX": 90, "pixY": 10, "ToA": 7836562.5, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 1930, "pixX": 130, "pixY": 31, "ToA": 7836562.5, "ToT": 77, "TriggerNo": 1 }, { "pixel_index": 1929, "pixX": 128, "pixY": 29, "ToA": 7836562.5, "ToT": 106, "TriggerNo": 1 }, { "pixel_index": 1926, "pixX": 123, "pixY": 26, "ToA": 7836562.5, "ToT": 41, "TriggerNo": 1 }, { "pixel_index": 1894, "pixX": 212, "pixY": 73, "ToA": 7836562.5, "ToT": 14, "TriggerNo": 1 }, { "pixel_index": 1826, "pixX": 107, "pixY": 19, "ToA": 7836562.5, "ToT": 14, "TriggerNo": 1 }, { "pixel_index": 1774, "pixX": 103, "pixY": 17, "ToA": 7836562.5, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 1775, "pixX": 105, "pixY": 16, "ToA": 7836562.5, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 1776, "pixX": 107, "pixY": 17, "ToA": 7836562.5, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 1891, "pixX": 145, "pixY": 38, "ToA": 7836562.5, "ToT": 95, "TriggerNo": 1 }, { "pixel_index": 1890, "pixX": 142, "pixY": 37, "ToA": 7836562.5, "ToT": 65, "TriggerNo": 1 }, { "pixel_index": 1889, "pixX": 140, "pixY": 36, "ToA": 7836562.5, "ToT": 68, "TriggerNo": 1 }, { "pixel_index": 1885, "pixX": 132, "pixY": 32, "ToA": 7836562.5, "ToT": 45, "TriggerNo": 1 }, { "pixel_index": 1879, "pixX": 121, "pixY": 25, "ToA": 7836562.5, "ToT": 57, "TriggerNo": 1 }, { "pixel_index": 1878, "pixX": 118, "pixY": 25, "ToA": 7836562.5, "ToT": 20, "TriggerNo": 1 }, { "pixel_index": 1876, "pixX": 115, "pixY": 23, "ToA": 7836562.5, "ToT": 50, "TriggerNo": 1 }, { "pixel_index": 1875, "pixX": 112, "pixY": 20, "ToA": 7836562.5, "ToT": 30, "TriggerNo": 1 }, { "pixel_index": 1874, "pixX": 111, "pixY": 21, "ToA": 7836562.5, "ToT": 49, "TriggerNo": 1 }, { "pixel_index": 1872, "pixX": 106, "pixY": 17, "ToA": 7836562.5, "ToT": 51, "TriggerNo": 1 }, { "pixel_index": 1871, "pixX": 105, "pixY": 18, "ToA": 7836562.5, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 1870, "pixX": 102, "pixY": 16, "ToA": 7836562.5, "ToT": 37, "TriggerNo": 1 }, { "pixel_index": 1864, "pixX": 91, "pixY": 8, "ToA": 7836562.5, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 1858, "pixX": 78, "pixY": 1, "ToA": 7836562.5, "ToT": 12, "TriggerNo": 1 }, { "pixel_index": 1857, "pixX": 76, "pixY": 2, "ToA": 7836562.5, "ToT": 16, "TriggerNo": 1 }, { "pixel_index": 1925, "pixX": 121, "pixY": 26, "ToA": 7836562.5, "ToT": 81, "TriggerNo": 1 }, { "pixel_index": 1869, "pixX": 100, "pixY": 15, "ToA": 7836562.5, "ToT": 21, "TriggerNo": 1 }, { "pixel_index": 1845, "pixX": 144, "pixY": 38, "ToA": 7836562.5, "ToT": 50, "TriggerNo": 1 }, { "pixel_index": 1988, "pixX": 212, "pixY": 75, "ToA": 7836562.5, "ToT": 30, "TriggerNo": 1 }, { "pixel_index": 1989, "pixX": 215, "pixY": 75, "ToA": 7836562.5, "ToT": 27, "TriggerNo": 1 }, { "pixel_index": 1924, "pixX": 119, "pixY": 24, "ToA": 7836562.5, "ToT": 55, "TriggerNo": 1 }, { "pixel_index": 1995, "pixX": 77, "pixY": 2, "ToA": 7836562.5, "ToT": 100, "TriggerNo": 1 }, { "pixel_index": 1996, "pixX": 79, "pixY": 3, "ToA": 7836562.5, "ToT": 127, "TriggerNo": 1 }, { "pixel_index": 1998, "pixX": 82, "pixY": 4, "ToA": 7836562.5, "ToT": 115, "TriggerNo": 1 }, { "pixel_index": 1999, "pixX": 85, "pixY": 6, "ToA": 7836562.5, "ToT": 138, "TriggerNo": 1 }, { "pixel_index": 2002, "pixX": 90, "pixY": 9, "ToA": 7836562.5, "ToT": 101, "TriggerNo": 1 }, { "pixel_index": 2009, "pixX": 105, "pixY": 17, "ToA": 7836562.5, "ToT": 121, "TriggerNo": 1 }, { "pixel_index": 2010, "pixX": 109, "pixY": 19, "ToA": 7836562.5, "ToT": 102, "TriggerNo": 1 }, { "pixel_index": 2011, "pixX": 111, "pixY": 20, "ToA": 7836562.5, "ToT": 129, "TriggerNo": 1 }, { "pixel_index": 2013, "pixX": 118, "pixY": 24, "ToA": 7836562.5, "ToT": 109, "TriggerNo": 1 }, { "pixel_index": 2015, "pixX": 122, "pixY": 26, "ToA": 7836562.5, "ToT": 110, "TriggerNo": 1 }, { "pixel_index": 2017, "pixX": 126, "pixY": 28, "ToA": 7836562.5, "ToT": 130, "TriggerNo": 1 }, { "pixel_index": 2018, "pixX": 129, "pixY": 30, "ToA": 7836562.5, "ToT": 171, "TriggerNo": 1 }, { "pixel_index": 2019, "pixX": 130, "pixY": 30, "ToA": 7836562.5, "ToT": 111, "TriggerNo": 1 }, { "pixel_index": 2020, "pixX": 132, "pixY": 31, "ToA": 7836562.5, "ToT": 112, "TriggerNo": 1 }, { "pixel_index": 2021, "pixX": 135, "pixY": 33, "ToA": 7836562.5, "ToT": 122, "TriggerNo": 1 }, { "pixel_index": 1923, "pixX": 117, "pixY": 23, "ToA": 7836562.5, "ToT": 83, "TriggerNo": 1 }, { "pixel_index": 2023, "pixX": 138, "pixY": 34, "ToA": 7836562.5, "ToT": 101, "TriggerNo": 1 }, { "pixel_index": 1922, "pixX": 114, "pixY": 22, "ToA": 7836562.5, "ToT": 104, "TriggerNo": 1 }, { "pixel_index": 2035, "pixX": 76, "pixY": 1, "ToA": 7836562.5, "ToT": 132, "TriggerNo": 1 }, { "pixel_index": 2036, "pixX": 78, "pixY": 2, "ToA": 7836562.5, "ToT": 116, "TriggerNo": 1 }, { "pixel_index": 1817, "pixX": 88, "pixY": 6, "ToA": 7836564.0625, "ToT": 3, "TriggerNo": 1 }, { "pixel_index": 1973, "pixX": 125, "pixY": 28, "ToA": 7836564.0625, "ToT": 109, "TriggerNo": 1 }, { "pixel_index": 1820, "pixX": 95, "pixY": 10, "ToA": 7836564.0625, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 1821, "pixX": 97, "pixY": 11, "ToA": 7836564.0625, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 1822, "pixX": 99, "pixY": 15, "ToA": 7836564.0625, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 1823, "pixX": 100, "pixY": 13, "ToA": 7836564.0625, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 1829, "pixX": 112, "pixY": 22, "ToA": 7836564.0625, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 1837, "pixX": 129, "pixY": 29, "ToA": 7836564.0625, "ToT": 24, "TriggerNo": 1 }, { "pixel_index": 1848, "pixX": 215, "pixY": 76, "ToA": 7836564.0625, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 1849, "pixX": 216, "pixY": 73, "ToA": 7836564.0625, "ToT": 3, "TriggerNo": 1 }, { "pixel_index": 1855, "pixX": 73, "pixY": 0, "ToA": 7836564.0625, "ToT": 67, "TriggerNo": 1 }, { "pixel_index": 1856, "pixX": 75, "pixY": 1, "ToA": 7836564.0625, "ToT": 72, "TriggerNo": 1 }, { "pixel_index": 1859, "pixX": 80, "pixY": 4, "ToA": 7836564.0625, "ToT": 13, "TriggerNo": 1 }, { "pixel_index": 1861, "pixX": 85, "pixY": 7, "ToA": 7836564.0625, "ToT": 10, "TriggerNo": 1 }, { "pixel_index": 1862, "pixX": 87, "pixY": 8, "ToA": 7836564.0625, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 1863, "pixX": 89, "pixY": 9, "ToA": 7836564.0625, "ToT": 16, "TriggerNo": 1 }, { "pixel_index": 1865, "pixX": 92, "pixY": 11, "ToA": 7836564.0625, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 1866, "pixX": 94, "pixY": 12, "ToA": 7836564.0625, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 1867, "pixX": 96, "pixY": 13, "ToA": 7836564.0625, "ToT": 12, "TriggerNo": 1 }, { "pixel_index": 1868, "pixX": 98, "pixY": 12, "ToA": 7836564.0625, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 1873, "pixX": 109, "pixY": 20, "ToA": 7836564.0625, "ToT": 11, "TriggerNo": 1 }, { "pixel_index": 1877, "pixX": 117, "pixY": 24, "ToA": 7836564.0625, "ToT": 82, "TriggerNo": 1 }, { "pixel_index": 1882, "pixX": 126, "pixY": 29, "ToA": 7836564.0625, "ToT": 18, "TriggerNo": 1 }, { "pixel_index": 1896, "pixX": 216, "pixY": 74, "ToA": 7836564.0625, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 1902, "pixX": 75, "pixY": 0, "ToA": 7836564.0625, "ToT": 80, "TriggerNo": 1 }, { "pixel_index": 1903, "pixX": 77, "pixY": 0, "ToA": 7836564.0625, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 1905, "pixX": 81, "pixY": 3, "ToA": 7836564.0625, "ToT": 34, "TriggerNo": 1 }, { "pixel_index": 1906, "pixX": 82, "pixY": 5, "ToA": 7836564.0625, "ToT": 25, "TriggerNo": 1 }, { "pixel_index": 1907, "pixX": 84, "pixY": 5, "ToA": 7836564.0625, "ToT": 105, "TriggerNo": 1 }, { "pixel_index": 1908, "pixX": 87, "pixY": 6, "ToA": 7836564.0625, "ToT": 11, "TriggerNo": 1 }, { "pixel_index": 1909, "pixX": 89, "pixY": 7, "ToA": 7836564.0625, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 1911, "pixX": 93, "pixY": 9, "ToA": 7836564.0625, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 1913, "pixX": 96, "pixY": 11, "ToA": 7836564.0625, "ToT": 10, "TriggerNo": 1 }, { "pixel_index": 1914, "pixX": 98, "pixY": 14, "ToA": 7836564.0625, "ToT": 15, "TriggerNo": 1 }, { "pixel_index": 1916, "pixX": 103, "pixY": 15, "ToA": 7836564.0625, "ToT": 17, "TriggerNo": 1 }, { "pixel_index": 1920, "pixX": 110, "pixY": 19, "ToA": 7836564.0625, "ToT": 21, "TriggerNo": 1 }, { "pixel_index": 1921, "pixX": 113, "pixY": 21, "ToA": 7836564.0625, "ToT": 92, "TriggerNo": 1 }, { "pixel_index": 1927, "pixX": 125, "pixY": 27, "ToA": 7836564.0625, "ToT": 28, "TriggerNo": 1 }, { "pixel_index": 1942, "pixX": 216, "pixY": 75, "ToA": 7836564.0625, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 1948, "pixX": 74, "pixY": 0, "ToA": 7836564.0625, "ToT": 118, "TriggerNo": 1 }, { "pixel_index": 1951, "pixX": 81, "pixY": 4, "ToA": 7836564.0625, "ToT": 80, "TriggerNo": 1 }, { "pixel_index": 1952, "pixX": 83, "pixY": 4, "ToA": 7836564.0625, "ToT": 36, "TriggerNo": 1 }, { "pixel_index": 1954, "pixX": 86, "pixY": 7, "ToA": 7836564.0625, "ToT": 53, "TriggerNo": 1 }, { "pixel_index": 1955, "pixX": 88, "pixY": 8, "ToA": 7836564.0625, "ToT": 75, "TriggerNo": 1 }, { "pixel_index": 1957, "pixX": 93, "pixY": 10, "ToA": 7836564.0625, "ToT": 98, "TriggerNo": 1 }, { "pixel_index": 1958, "pixX": 95, "pixY": 12, "ToA": 7836564.0625, "ToT": 74, "TriggerNo": 1 }, { "pixel_index": 1959, "pixX": 97, "pixY": 12, "ToA": 7836564.0625, "ToT": 55, "TriggerNo": 1 }, { "pixel_index": 1960, "pixX": 99, "pixY": 13, "ToA": 7836564.0625, "ToT": 35, "TriggerNo": 1 }, { "pixel_index": 2043, "pixX": 94, "pixY": 11, "ToA": 7836564.0625, "ToT": 120, "TriggerNo": 1 }, { "pixel_index": 2044, "pixX": 96, "pixY": 12, "ToA": 7836564.0625, "ToT": 112, "TriggerNo": 1 }, { "pixel_index": 2045, "pixX": 98, "pixY": 13, "ToA": 7836564.0625, "ToT": 109, "TriggerNo": 1 }, { "pixel_index": 2066, "pixX": 92, "pixY": 10, "ToA": 7836564.0625, "ToT": 98, "TriggerNo": 1 }, { "pixel_index": 2042, "pixX": 92, "pixY": 9, "ToA": 7836564.0625, "ToT": 20, "TriggerNo": 1 }, { "pixel_index": 1997, "pixX": 80, "pixY": 3, "ToA": 7836564.0625, "ToT": 126, "TriggerNo": 1 }, { "pixel_index": 1824, "pixX": 102, "pixY": 14, "ToA": 7836564.0625, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 1759, "pixX": 72, "pixY": 0, "ToA": 7836564.0625, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2000, "pixX": 87, "pixY": 7, "ToA": 7836564.0625, "ToT": 101, "TriggerNo": 1 }, { "pixel_index": 2001, "pixX": 88, "pixY": 7, "ToA": 7836564.0625, "ToT": 68, "TriggerNo": 1 }, { "pixel_index": 1760, "pixX": 75, "pixY": 2, "ToA": 7836564.0625, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 2003, "pixX": 93, "pixY": 11, "ToA": 7836564.0625, "ToT": 54, "TriggerNo": 1 }, { "pixel_index": 2004, "pixX": 95, "pixY": 11, "ToA": 7836564.0625, "ToT": 70, "TriggerNo": 1 }, { "pixel_index": 2005, "pixX": 97, "pixY": 13, "ToA": 7836564.0625, "ToT": 99, "TriggerNo": 1 }, { "pixel_index": 2006, "pixX": 99, "pixY": 14, "ToA": 7836564.0625, "ToT": 99, "TriggerNo": 1 }, { "pixel_index": 2007, "pixX": 100, "pixY": 14, "ToA": 7836564.0625, "ToT": 98, "TriggerNo": 1 }, { "pixel_index": 2008, "pixX": 102, "pixY": 15, "ToA": 7836564.0625, "ToT": 98, "TriggerNo": 1 }, { "pixel_index": 1762, "pixX": 79, "pixY": 4, "ToA": 7836564.0625, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 1763, "pixX": 81, "pixY": 5, "ToA": 7836564.0625, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 1767, "pixX": 88, "pixY": 9, "ToA": 7836564.0625, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 2012, "pixX": 112, "pixY": 21, "ToA": 7836564.0625, "ToT": 145, "TriggerNo": 1 }, { "pixel_index": 1770, "pixX": 95, "pixY": 13, "ToA": 7836564.0625, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 1771, "pixX": 97, "pixY": 14, "ToA": 7836564.0625, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 2016, "pixX": 124, "pixY": 27, "ToA": 7836564.0625, "ToT": 85, "TriggerNo": 1 }, { "pixel_index": 1773, "pixX": 101, "pixY": 16, "ToA": 7836564.0625, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 1795, "pixX": 144, "pixY": 37, "ToA": 7836564.0625, "ToT": 39, "TriggerNo": 1 }, { "pixel_index": 1800, "pixX": 212, "pixY": 72, "ToA": 7836564.0625, "ToT": 3, "TriggerNo": 1 }, { "pixel_index": 1801, "pixX": 215, "pixY": 72, "ToA": 7836564.0625, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 1809, "pixX": 73, "pixY": 1, "ToA": 7836564.0625, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 1810, "pixX": 74, "pixY": 1, "ToA": 7836564.0625, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 1813, "pixX": 80, "pixY": 2, "ToA": 7836564.0625, "ToT": 10, "TriggerNo": 1 }, { "pixel_index": 2029, "pixX": 213, "pixY": 76, "ToA": 7836564.0625, "ToT": 27, "TriggerNo": 1 }, { "pixel_index": 2030, "pixX": 214, "pixY": 76, "ToA": 7836564.0625, "ToT": 24, "TriggerNo": 1 }, { "pixel_index": 1814, "pixX": 82, "pixY": 3, "ToA": 7836564.0625, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 1815, "pixX": 85, "pixY": 5, "ToA": 7836564.0625, "ToT": 22, "TriggerNo": 1 }, { "pixel_index": 1816, "pixX": 86, "pixY": 5, "ToA": 7836564.0625, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 2038, "pixX": 83, "pixY": 5, "ToA": 7836564.0625, "ToT": 121, "TriggerNo": 1 }, { "pixel_index": 2039, "pixX": 86, "pixY": 6, "ToA": 7836564.0625, "ToT": 106, "TriggerNo": 1 }, { "pixel_index": 2040, "pixX": 89, "pixY": 8, "ToA": 7836564.0625, "ToT": 119, "TriggerNo": 1 }, { "pixel_index": 1827, "pixX": 109, "pixY": 18, "ToA": 7836565.625, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 1766, "pixX": 86, "pixY": 8, "ToA": 7836565.625, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 1818, "pixX": 90, "pixY": 7, "ToA": 7836565.625, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 1778, "pixX": 110, "pixY": 21, "ToA": 7836565.625, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 1830, "pixX": 114, "pixY": 21, "ToA": 7836565.625, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 1928, "pixX": 127, "pixY": 28, "ToA": 7836565.625, "ToT": 19, "TriggerNo": 1 }, { "pixel_index": 1880, "pixX": 122, "pixY": 27, "ToA": 7836565.625, "ToT": 13, "TriggerNo": 1 }, { "pixel_index": 1802, "pixX": 216, "pixY": 72, "ToA": 7836567.1875, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 1799, "pixX": 211, "pixY": 73, "ToA": 7836567.1875, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 1846, "pixX": 211, "pixY": 74, "ToA": 7836567.1875, "ToT": 3, "TriggerNo": 1 }, { "pixel_index": 1847, "pixX": 213, "pixY": 77, "ToA": 7836567.1875, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 1828, "pixX": 111, "pixY": 19, "ToA": 7836567.1875, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 1769, "pixX": 93, "pixY": 12, "ToA": 7836567.1875, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 1752, "pixX": 216, "pixY": 76, "ToA": 7836567.1875, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 1893, "pixX": 211, "pixY": 75, "ToA": 7836567.1875, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 1940, "pixX": 212, "pixY": 76, "ToA": 7836567.1875, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 1751, "pixX": 214, "pixY": 77, "ToA": 7836568.75, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 1884, "pixX": 130, "pixY": 29, "ToA": 7836568.75, "ToT": 15, "TriggerNo": 1 }, { "pixel_index": 1883, "pixX": 129, "pixY": 31, "ToA": 7836568.75, "ToT": 14, "TriggerNo": 1 }, { "pixel_index": 1881, "pixX": 124, "pixY": 28, "ToA": 7836568.75, "ToT": 10, "TriggerNo": 1 }, { "pixel_index": 1941, "pixX": 215, "pixY": 77, "ToA": 7836568.75, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 1931, "pixX": 133, "pixY": 31, "ToA": 7836568.75, "ToT": 14, "TriggerNo": 1 }, { "pixel_index": 1819, "pixX": 92, "pixY": 8, "ToA": 7836568.75, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 1779, "pixX": 113, "pixY": 20, "ToA": 7836568.75, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 1772, "pixX": 99, "pixY": 12, "ToA": 7836568.75, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 1780, "pixX": 114, "pixY": 23, "ToA": 7836570.3125, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 1825, "pixX": 104, "pixY": 15, "ToA": 7836570.3125, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 1777, "pixX": 108, "pixY": 20, "ToA": 7836570.3125, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 1838, "pixX": 131, "pixY": 30, "ToA": 7836570.3125, "ToT": 11, "TriggerNo": 1 }, { "pixel_index": 1833, "pixX": 120, "pixY": 26, "ToA": 7836570.3125, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 1831, "pixX": 116, "pixY": 22, "ToA": 7836570.3125, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 1749, "pixX": 211, "pixY": 76, "ToA": 7836570.3125, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 1750, "pixX": 212, "pixY": 77, "ToA": 7836570.3125, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 1783, "pixX": 120, "pixY": 24, "ToA": 7836570.3125, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 1832, "pixX": 118, "pixY": 23, "ToA": 7836570.3125, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 1841, "pixX": 137, "pixY": 33, "ToA": 7836571.875, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 1781, "pixX": 116, "pixY": 24, "ToA": 7836571.875, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 1789, "pixX": 133, "pixY": 33, "ToA": 7836571.875, "ToT": 10, "TriggerNo": 1 }, { "pixel_index": 1834, "pixX": 122, "pixY": 25, "ToA": 7836571.875, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 1886, "pixX": 135, "pixY": 32, "ToA": 7836573.4375, "ToT": 10, "TriggerNo": 1 }, { "pixel_index": 1887, "pixX": 137, "pixY": 35, "ToA": 7836573.4375, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 1888, "pixX": 139, "pixY": 34, "ToA": 7836573.4375, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 1786, "pixX": 127, "pixY": 30, "ToA": 7836575.0, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 1798, "pixX": 156, "pixY": 44, "ToA": 7836575.0, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 1836, "pixX": 126, "pixY": 27, "ToA": 7836575.0, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 1793, "pixX": 141, "pixY": 37, "ToA": 7836576.5625, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 1790, "pixX": 135, "pixY": 34, "ToA": 7836576.5625, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 1787, "pixX": 128, "pixY": 28, "ToA": 7836576.5625, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 1843, "pixX": 141, "pixY": 35, "ToA": 7836576.5625, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 1794, "pixX": 143, "pixY": 36, "ToA": 7836579.6875, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 1785, "pixX": 125, "pixY": 29, "ToA": 7836579.6875, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 1839, "pixX": 132, "pixY": 30, "ToA": 7836579.6875, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 1792, "pixX": 138, "pixY": 33, "ToA": 7836581.25, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 1748, "pixX": 139, "pixY": 36, "ToA": 7836581.25, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 1784, "pixX": 123, "pixY": 28, "ToA": 7836581.25, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 1788, "pixX": 131, "pixY": 32, "ToA": 7836582.8125, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 1782, "pixX": 119, "pixY": 26, "ToA": 7836582.8125, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 1835, "pixX": 124, "pixY": 26, "ToA": 7836584.375, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 1840, "pixX": 134, "pixY": 31, "ToA": 7836585.9375, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 1791, "pixX": 136, "pixY": 32, "ToA": 7836606.25, "ToT": 3, "TriggerNo": 1 }, { "pixel_index": 1901, "pixX": 30, "pixY": 60, "ToA": 7837171.875, "ToT": 113, "TriggerNo": 1 }, { "pixel_index": 1758, "pixX": 30, "pixY": 61, "ToA": 7837175.0, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 1854, "pixX": 31, "pixY": 60, "ToA": 7837179.6875, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 1757, "pixX": 29, "pixY": 60, "ToA": 7837181.25, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 1807, "pixX": 30, "pixY": 59, "ToA": 7837182.8125, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 1860, "pixX": 82, "pixY": 68, "ToA": 7838471.875, "ToT": 3, "TriggerNo": 1 }, { "pixel_index": 1808, "pixX": 44, "pixY": 233, "ToA": 7847221.875, "ToT": 60, "TriggerNo": 1 }, { "pixel_index": 2073, "pixX": 134, "pixY": 253, "ToA": 7857956.25, "ToT": 41, "TriggerNo": 1 }, { "pixel_index": 2051, "pixX": 133, "pixY": 253, "ToA": 7857960.9375, "ToT": 17, "TriggerNo": 1 }, { "pixel_index": 2052, "pixX": 134, "pixY": 254, "ToA": 7857973.4375, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 2098, "pixX": 123, "pixY": 50, "ToA": 7858040.625, "ToT": 23, "TriggerNo": 1 }, { "pixel_index": 2125, "pixX": 123, "pixY": 49, "ToA": 7858040.625, "ToT": 197, "TriggerNo": 1 }, { "pixel_index": 2085, "pixX": 123, "pixY": 48, "ToA": 7858040.625, "ToT": 20, "TriggerNo": 1 }, { "pixel_index": 2118, "pixX": 122, "pixY": 49, "ToA": 7858040.625, "ToT": 46, "TriggerNo": 1 }, { "pixel_index": 2109, "pixX": 122, "pixY": 48, "ToA": 7858040.625, "ToT": 107, "TriggerNo": 1 }, { "pixel_index": 2086, "pixX": 124, "pixY": 51, "ToA": 7858042.1875, "ToT": 15, "TriggerNo": 1 }, { "pixel_index": 2142, "pixX": 124, "pixY": 50, "ToA": 7858042.1875, "ToT": 158, "TriggerNo": 1 }, { "pixel_index": 2119, "pixX": 125, "pixY": 49, "ToA": 7858042.1875, "ToT": 19, "TriggerNo": 1 }, { "pixel_index": 2126, "pixX": 125, "pixY": 50, "ToA": 7858042.1875, "ToT": 150, "TriggerNo": 1 }, { "pixel_index": 2132, "pixX": 125, "pixY": 51, "ToA": 7858042.1875, "ToT": 60, "TriggerNo": 1 }, { "pixel_index": 2138, "pixX": 124, "pixY": 49, "ToA": 7858042.1875, "ToT": 109, "TriggerNo": 1 }, { "pixel_index": 2143, "pixX": 126, "pixY": 51, "ToA": 7858043.75, "ToT": 194, "TriggerNo": 1 }, { "pixel_index": 2127, "pixX": 126, "pixY": 50, "ToA": 7858043.75, "ToT": 32, "TriggerNo": 1 }, { "pixel_index": 2100, "pixX": 127, "pixY": 50, "ToA": 7858045.3125, "ToT": 10, "TriggerNo": 1 }, { "pixel_index": 2139, "pixX": 127, "pixY": 52, "ToA": 7858045.3125, "ToT": 128, "TriggerNo": 1 }, { "pixel_index": 2120, "pixX": 126, "pixY": 52, "ToA": 7858045.3125, "ToT": 25, "TriggerNo": 1 }, { "pixel_index": 2146, "pixX": 127, "pixY": 51, "ToA": 7858045.3125, "ToT": 120, "TriggerNo": 1 }, { "pixel_index": 2070, "pixX": 124, "pixY": 48, "ToA": 7858045.3125, "ToT": 10, "TriggerNo": 1 }, { "pixel_index": 2071, "pixX": 126, "pixY": 49, "ToA": 7858045.3125, "ToT": 3, "TriggerNo": 1 }, { "pixel_index": 2048, "pixX": 125, "pixY": 52, "ToA": 7858046.875, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 2101, "pixX": 128, "pixY": 51, "ToA": 7858048.4375, "ToT": 17, "TriggerNo": 1 }, { "pixel_index": 2134, "pixX": 128, "pixY": 52, "ToA": 7858050.0, "ToT": 139, "TriggerNo": 1 }, { "pixel_index": 2087, "pixX": 127, "pixY": 53, "ToA": 7858050.0, "ToT": 11, "TriggerNo": 1 }, { "pixel_index": 2049, "pixX": 126, "pixY": 53, "ToA": 7858050.0, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2072, "pixX": 128, "pixY": 50, "ToA": 7858051.5625, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2121, "pixX": 128, "pixY": 53, "ToA": 7858051.5625, "ToT": 33, "TriggerNo": 1 }, { "pixel_index": 2088, "pixX": 129, "pixY": 52, "ToA": 7858053.125, "ToT": 14, "TriggerNo": 1 }, { "pixel_index": 2047, "pixX": 123, "pixY": 51, "ToA": 7858053.125, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2128, "pixX": 129, "pixY": 51, "ToA": 7858054.6875, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2069, "pixX": 122, "pixY": 47, "ToA": 7858059.375, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 2050, "pixX": 129, "pixY": 53, "ToA": 7858059.375, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 2014, "pixX": 121, "pixY": 48, "ToA": 7858064.0625, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 1939, "pixX": 169, "pixY": 45, "ToA": 7870482.8125, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 1986, "pixX": 168, "pixY": 44, "ToA": 7870484.375, "ToT": 13, "TriggerNo": 1 }, { "pixel_index": 1892, "pixX": 169, "pixY": 44, "ToA": 7870496.875, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 2110, "pixX": 125, "pixY": 7, "ToA": 7874053.125, "ToT": 34, "TriggerNo": 1 }, { "pixel_index": 2111, "pixX": 127, "pixY": 9, "ToA": 7874053.125, "ToT": 23, "TriggerNo": 1 }, { "pixel_index": 2145, "pixX": 127, "pixY": 8, "ToA": 7874053.125, "ToT": 26, "TriggerNo": 1 }, { "pixel_index": 2099, "pixX": 125, "pixY": 8, "ToA": 7874053.125, "ToT": 68, "TriggerNo": 1 }, { "pixel_index": 2133, "pixX": 126, "pixY": 8, "ToA": 7874053.125, "ToT": 32, "TriggerNo": 1 }, { "pixel_index": 2112, "pixX": 128, "pixY": 9, "ToA": 7874056.25, "ToT": 13, "TriggerNo": 1 }, { "pixel_index": 2084, "pixX": 96, "pixY": 70, "ToA": 7875846.875, "ToT": 56, "TriggerNo": 1 }, { "pixel_index": 2067, "pixX": 95, "pixY": 70, "ToA": 7875846.875, "ToT": 52, "TriggerNo": 1 }, { "pixel_index": 2068, "pixX": 96, "pixY": 69, "ToA": 7875859.375, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 2027, "pixX": 162, "pixY": 170, "ToA": 7877828.125, "ToT": 16, "TriggerNo": 1 }, { "pixel_index": 1938, "pixX": 162, "pixY": 172, "ToA": 7877829.6875, "ToT": 15, "TriggerNo": 1 }, { "pixel_index": 1985, "pixX": 163, "pixY": 171, "ToA": 7877829.6875, "ToT": 15, "TriggerNo": 1 }, { "pixel_index": 2055, "pixX": 162, "pixY": 171, "ToA": 7877829.6875, "ToT": 193, "TriggerNo": 1 }, { "pixel_index": 1937, "pixX": 161, "pixY": 171, "ToA": 7877831.25, "ToT": 13, "TriggerNo": 1 }, { "pixel_index": 1943, "pixX": 218, "pixY": 221, "ToA": 7879481.25, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 1944, "pixX": 231, "pixY": 55, "ToA": 7882284.375, "ToT": 67, "TriggerNo": 1 }, { "pixel_index": 2037, "pixX": 80, "pixY": 149, "ToA": 7883618.75, "ToT": 40, "TriggerNo": 1 }, { "pixel_index": 2065, "pixX": 79, "pixY": 149, "ToA": 7883618.75, "ToT": 39, "TriggerNo": 1 }, { "pixel_index": 1947, "pixX": 240, "pixY": 48, "ToA": 7886281.25, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 2108, "pixX": 45, "pixY": 39, "ToA": 7890489.0625, "ToT": 284, "TriggerNo": 1 }, { "pixel_index": 2097, "pixX": 45, "pixY": 38, "ToA": 7890489.0625, "ToT": 34, "TriggerNo": 1 }, { "pixel_index": 2083, "pixX": 44, "pixY": 39, "ToA": 7890489.0625, "ToT": 32, "TriggerNo": 1 }, { "pixel_index": 2062, "pixX": 45, "pixY": 40, "ToA": 7890490.625, "ToT": 34, "TriggerNo": 1 }, { "pixel_index": 2063, "pixX": 46, "pixY": 39, "ToA": 7890490.625, "ToT": 30, "TriggerNo": 1 }, { "pixel_index": 2034, "pixX": 46, "pixY": 38, "ToA": 7890496.875, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 2033, "pixX": 44, "pixY": 38, "ToA": 7890498.4375, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 1993, "pixX": 44, "pixY": 40, "ToA": 7890500.0, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 1994, "pixX": 46, "pixY": 40, "ToA": 7890501.5625, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 2024, "pixX": 152, "pixY": 139, "ToA": 7891081.25, "ToT": 37, "TriggerNo": 1 }, { "pixel_index": 1982, "pixX": 153, "pixY": 139, "ToA": 7891084.375, "ToT": 13, "TriggerNo": 1 }, { "pixel_index": 2129, "pixX": 160, "pixY": 156, "ToA": 7895520.3125, "ToT": 25, "TriggerNo": 1 }, { "pixel_index": 2074, "pixX": 160, "pixY": 154, "ToA": 7895520.3125, "ToT": 33, "TriggerNo": 1 }, { "pixel_index": 2089, "pixX": 161, "pixY": 157, "ToA": 7895520.3125, "ToT": 22, "TriggerNo": 1 }, { "pixel_index": 2140, "pixX": 160, "pixY": 158, "ToA": 7895521.875, "ToT": 27, "TriggerNo": 1 }, { "pixel_index": 2113, "pixX": 161, "pixY": 158, "ToA": 7895521.875, "ToT": 17, "TriggerNo": 1 }, { "pixel_index": 2102, "pixX": 160, "pixY": 155, "ToA": 7895521.875, "ToT": 23, "TriggerNo": 1 }, { "pixel_index": 2028, "pixX": 185, "pixY": 231, "ToA": 7895521.875, "ToT": 66, "TriggerNo": 1 }, { "pixel_index": 2054, "pixX": 160, "pixY": 159, "ToA": 7895523.4375, "ToT": 13, "TriggerNo": 1 }, { "pixel_index": 2025, "pixX": 159, "pixY": 158, "ToA": 7895526.5625, "ToT": 96, "TriggerNo": 1 }, { "pixel_index": 2026, "pixX": 160, "pixY": 153, "ToA": 7895526.5625, "ToT": 18, "TriggerNo": 1 }, { "pixel_index": 1984, "pixX": 160, "pixY": 157, "ToA": 7895526.5625, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 1987, "pixX": 185, "pixY": 230, "ToA": 7895528.125, "ToT": 23, "TriggerNo": 1 }, { "pixel_index": 2053, "pixX": 159, "pixY": 153, "ToA": 7895532.8125, "ToT": 21, "TriggerNo": 1 }, { "pixel_index": 1983, "pixX": 159, "pixY": 152, "ToA": 7895539.0625, "ToT": 15, "TriggerNo": 1 }, { "pixel_index": 2032, "pixX": 3, "pixY": 156, "ToA": 7897514.0625, "ToT": 11, "TriggerNo": 1 }, { "pixel_index": 1992, "pixX": 4, "pixY": 156, "ToA": 7897518.75, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 1991, "pixX": 3, "pixY": 157, "ToA": 7897518.75, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 2046, "pixX": 120, "pixY": 38, "ToA": 7899995.3125, "ToT": 24, "TriggerNo": 1 }, { "pixel_index": 2082, "pixX": 13, "pixY": 75, "ToA": 7909828.125, "ToT": 26, "TriggerNo": 1 }, { "pixel_index": 2061, "pixX": 13, "pixY": 74, "ToA": 7909845.3125, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 2135, "pixX": 161, "pixY": 166, "ToA": 7911618.75, "ToT": 30, "TriggerNo": 1 }, { "pixel_index": 2103, "pixX": 162, "pixY": 165, "ToA": 7911623.4375, "ToT": 15, "TriggerNo": 1 }, { "pixel_index": 2123, "pixX": 162, "pixY": 166, "ToA": 7911623.4375, "ToT": 15, "TriggerNo": 1 }, { "pixel_index": 2075, "pixX": 162, "pixY": 164, "ToA": 7911628.125, "ToT": 14, "TriggerNo": 1 }, { "pixel_index": 2130, "pixX": 163, "pixY": 163, "ToA": 7911629.6875, "ToT": 18, "TriggerNo": 1 }, { "pixel_index": 2114, "pixX": 163, "pixY": 162, "ToA": 7911631.25, "ToT": 21, "TriggerNo": 1 }, { "pixel_index": 2136, "pixX": 163, "pixY": 161, "ToA": 7911632.8125, "ToT": 36, "TriggerNo": 1 }, { "pixel_index": 2090, "pixX": 162, "pixY": 163, "ToA": 7911637.5, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 2122, "pixX": 161, "pixY": 167, "ToA": 7911648.4375, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 2064, "pixX": 58, "pixY": 32, "ToA": 7913785.9375, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 2059, "pixX": 239, "pixY": 230, "ToA": 7915198.4375, "ToT": 28, "TriggerNo": 1 }, { "pixel_index": 2107, "pixX": 12, "pixY": 161, "ToA": 7915203.125, "ToT": 58, "TriggerNo": 1 }, { "pixel_index": 2096, "pixX": 12, "pixY": 162, "ToA": 7915206.25, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 2080, "pixX": 255, "pixY": 255, "ToA": 7915207.8125, "ToT": 16, "TriggerNo": 1 }, { "pixel_index": 2060, "pixX": 255, "pixY": 254, "ToA": 7915209.375, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 2094, "pixX": 254, "pixY": 255, "ToA": 7915210.9375, "ToT": 11, "TriggerNo": 1 }, { "pixel_index": 2095, "pixX": 0, "pixY": 83, "ToA": 7916984.375, "ToT": 52, "TriggerNo": 1 }, { "pixel_index": 2081, "pixX": 0, "pixY": 84, "ToA": 7916996.875, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 2117, "pixX": 235, "pixY": 27, "ToA": 7920079.6875, "ToT": 49, "TriggerNo": 1 }, { "pixel_index": 2106, "pixX": 235, "pixY": 232, "ToA": 7921007.8125, "ToT": 18, "TriggerNo": 1 }, { "pixel_index": 2079, "pixX": 236, "pixY": 232, "ToA": 7921040.625, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2141, "pixX": 191, "pixY": 55, "ToA": 7931296.875, "ToT": 63, "TriggerNo": 1 }, { "pixel_index": 2147, "pixX": 191, "pixY": 56, "ToA": 7931296.875, "ToT": 33, "TriggerNo": 1 }, { "pixel_index": 2144, "pixX": 190, "pixY": 56, "ToA": 7931300.0, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 2137, "pixX": 191, "pixY": 57, "ToA": 7931300.0, "ToT": 11, "TriggerNo": 1 }, { "pixel_index": 2131, "pixX": 190, "pixY": 55, "ToA": 7931300.0, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 2124, "pixX": 190, "pixY": 57, "ToA": 7931307.8125, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 2153, "pixX": 85, "pixY": 222, "ToA": 7942029.6875, "ToT": 123, "TriggerNo": 1 }, { "pixel_index": 2149, "pixX": 86, "pixY": 222, "ToA": 7942039.0625, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 2151, "pixX": 85, "pixY": 223, "ToA": 7942039.0625, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 2148, "pixX": 84, "pixY": 222, "ToA": 7942043.75, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 2150, "pixX": 85, "pixY": 221, "ToA": 7942043.75, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 2152, "pixX": 193, "pixY": 173, "ToA": 7946556.25, "ToT": 25, "TriggerNo": 1 }, { "pixel_index": 2155, "pixX": 235, "pixY": 15, "ToA": 7949182.8125, "ToT": 24, "TriggerNo": 1 }, { "pixel_index": 2156, "pixX": 234, "pixY": 15, "ToA": 7949184.375, "ToT": 36, "TriggerNo": 1 }, { "pixel_index": 2154, "pixX": 183, "pixY": 3, "ToA": 7949192.1875, "ToT": 13, "TriggerNo": 1 }, { "pixel_index": 2158, "pixX": 209, "pixY": 69, "ToA": 7952776.5625, "ToT": 27, "TriggerNo": 1 }, { "pixel_index": 2157, "pixX": 210, "pixY": 69, "ToA": 7952778.125, "ToT": 16, "TriggerNo": 1 }, { "pixel_index": 2159, "pixX": 255, "pixY": 225, "ToA": 7958135.9375, "ToT": 18, "TriggerNo": 1 }, { "pixel_index": 2168, "pixX": 135, "pixY": 47, "ToA": 7965785.9375, "ToT": 60, "TriggerNo": 1 }, { "pixel_index": 2162, "pixX": 135, "pixY": 46, "ToA": 7965785.9375, "ToT": 14, "TriggerNo": 1 }, { "pixel_index": 2166, "pixX": 134, "pixY": 47, "ToA": 7965785.9375, "ToT": 42, "TriggerNo": 1 }, { "pixel_index": 2164, "pixX": 134, "pixY": 46, "ToA": 7965787.5, "ToT": 21, "TriggerNo": 1 }, { "pixel_index": 2160, "pixX": 135, "pixY": 48, "ToA": 7965787.5, "ToT": 14, "TriggerNo": 1 }, { "pixel_index": 2163, "pixX": 133, "pixY": 43, "ToA": 7965787.5, "ToT": 17, "TriggerNo": 1 }, { "pixel_index": 2161, "pixX": 132, "pixY": 44, "ToA": 7965789.0625, "ToT": 16, "TriggerNo": 1 }, { "pixel_index": 2165, "pixX": 133, "pixY": 44, "ToA": 7965789.0625, "ToT": 16, "TriggerNo": 1 }, { "pixel_index": 2167, "pixX": 133, "pixY": 45, "ToA": 7965789.0625, "ToT": 16, "TriggerNo": 1 }, { "pixel_index": 2169, "pixX": 133, "pixY": 46, "ToA": 7965789.0625, "ToT": 17, "TriggerNo": 1 }, { "pixel_index": 2172, "pixX": 205, "pixY": 144, "ToA": 7979593.75, "ToT": 18, "TriggerNo": 1 }, { "pixel_index": 2174, "pixX": 204, "pixY": 144, "ToA": 7979593.75, "ToT": 61, "TriggerNo": 1 }, { "pixel_index": 2171, "pixX": 204, "pixY": 145, "ToA": 7979595.3125, "ToT": 16, "TriggerNo": 1 }, { "pixel_index": 2170, "pixX": 203, "pixY": 145, "ToA": 7979603.125, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 2173, "pixX": 196, "pixY": 86, "ToA": 7983193.75, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 2177, "pixX": 97, "pixY": 104, "ToA": 7992114.0625, "ToT": 68, "TriggerNo": 1 }, { "pixel_index": 2178, "pixX": 99, "pixY": 103, "ToA": 7992114.0625, "ToT": 80, "TriggerNo": 1 }, { "pixel_index": 2176, "pixX": 99, "pixY": 102, "ToA": 7992114.0625, "ToT": 16, "TriggerNo": 1 }, { "pixel_index": 2175, "pixX": 98, "pixY": 104, "ToA": 7992128.125, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 2180, "pixX": 16, "pixY": 165, "ToA": 7995690.625, "ToT": 29, "TriggerNo": 1 }, { "pixel_index": 2179, "pixX": 14, "pixY": 162, "ToA": 7995728.125, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2184, "pixX": 246, "pixY": 119, "ToA": 7997482.8125, "ToT": 21, "TriggerNo": 1 }, { "pixel_index": 2182, "pixX": 247, "pixY": 119, "ToA": 7997484.375, "ToT": 18, "TriggerNo": 1 }, { "pixel_index": 2181, "pixX": 248, "pixY": 118, "ToA": 7997490.625, "ToT": 11, "TriggerNo": 1 }, { "pixel_index": 2183, "pixX": 248, "pixY": 119, "ToA": 7997490.625, "ToT": 12, "TriggerNo": 1 }, { "pixel_index": 2187, "pixX": 197, "pixY": 255, "ToA": 8011870.3125, "ToT": 68, "TriggerNo": 1 }, { "pixel_index": 2198, "pixX": 225, "pixY": 222, "ToA": 8012417.1875, "ToT": 65, "TriggerNo": 1 }, { "pixel_index": 2210, "pixX": 224, "pixY": 221, "ToA": 8012417.1875, "ToT": 76, "TriggerNo": 1 }, { "pixel_index": 2216, "pixX": 225, "pixY": 221, "ToA": 8012417.1875, "ToT": 161, "TriggerNo": 1 }, { "pixel_index": 2190, "pixX": 224, "pixY": 222, "ToA": 8012417.1875, "ToT": 31, "TriggerNo": 1 }, { "pixel_index": 2188, "pixX": 225, "pixY": 220, "ToA": 8012418.75, "ToT": 10, "TriggerNo": 1 }, { "pixel_index": 2185, "pixX": 224, "pixY": 220, "ToA": 8012420.3125, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2189, "pixX": 226, "pixY": 221, "ToA": 8012423.4375, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 2186, "pixX": 226, "pixY": 222, "ToA": 8012425.0, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2227, "pixX": 252, "pixY": 145, "ToA": 8017610.9375, "ToT": 19, "TriggerNo": 1 }, { "pixel_index": 2246, "pixX": 252, "pixY": 147, "ToA": 8017610.9375, "ToT": 310, "TriggerNo": 1 }, { "pixel_index": 2236, "pixX": 252, "pixY": 146, "ToA": 8017610.9375, "ToT": 118, "TriggerNo": 1 }, { "pixel_index": 2263, "pixX": 253, "pixY": 147, "ToA": 8017610.9375, "ToT": 169, "TriggerNo": 1 }, { "pixel_index": 2256, "pixX": 253, "pixY": 146, "ToA": 8017610.9375, "ToT": 44, "TriggerNo": 1 }, { "pixel_index": 2208, "pixX": 253, "pixY": 145, "ToA": 8017610.9375, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 2244, "pixX": 251, "pixY": 145, "ToA": 8017612.5, "ToT": 13, "TriggerNo": 1 }, { "pixel_index": 2219, "pixX": 254, "pixY": 146, "ToA": 8017612.5, "ToT": 10, "TriggerNo": 1 }, { "pixel_index": 2233, "pixX": 253, "pixY": 148, "ToA": 8017612.5, "ToT": 139, "TriggerNo": 1 }, { "pixel_index": 2231, "pixX": 250, "pixY": 149, "ToA": 8017612.5, "ToT": 13, "TriggerNo": 1 }, { "pixel_index": 2229, "pixX": 254, "pixY": 147, "ToA": 8017612.5, "ToT": 23, "TriggerNo": 1 }, { "pixel_index": 2202, "pixX": 251, "pixY": 150, "ToA": 8017612.5, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 2207, "pixX": 254, "pixY": 145, "ToA": 8017612.5, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2217, "pixX": 250, "pixY": 146, "ToA": 8017612.5, "ToT": 21, "TriggerNo": 1 }, { "pixel_index": 2222, "pixX": 252, "pixY": 150, "ToA": 8017612.5, "ToT": 10, "TriggerNo": 1 }, { "pixel_index": 2223, "pixX": 250, "pixY": 148, "ToA": 8017612.5, "ToT": 43, "TriggerNo": 1 }, { "pixel_index": 2225, "pixX": 250, "pixY": 147, "ToA": 8017612.5, "ToT": 56, "TriggerNo": 1 }, { "pixel_index": 2238, "pixX": 253, "pixY": 149, "ToA": 8017612.5, "ToT": 34, "TriggerNo": 1 }, { "pixel_index": 2262, "pixX": 251, "pixY": 147, "ToA": 8017612.5, "ToT": 357, "TriggerNo": 1 }, { "pixel_index": 2261, "pixX": 252, "pixY": 149, "ToA": 8017612.5, "ToT": 85, "TriggerNo": 1 }, { "pixel_index": 2260, "pixX": 251, "pixY": 149, "ToA": 8017612.5, "ToT": 50, "TriggerNo": 1 }, { "pixel_index": 2258, "pixX": 255, "pixY": 147, "ToA": 8017612.5, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2257, "pixX": 251, "pixY": 146, "ToA": 8017612.5, "ToT": 89, "TriggerNo": 1 }, { "pixel_index": 2254, "pixX": 252, "pixY": 148, "ToA": 8017612.5, "ToT": 395, "TriggerNo": 1 }, { "pixel_index": 2252, "pixX": 251, "pixY": 148, "ToA": 8017612.5, "ToT": 251, "TriggerNo": 1 }, { "pixel_index": 2218, "pixX": 252, "pixY": 144, "ToA": 8017614.0625, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2253, "pixX": 254, "pixY": 149, "ToA": 8017614.0625, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 2200, "pixX": 249, "pixY": 148, "ToA": 8017614.0625, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 2201, "pixX": 253, "pixY": 150, "ToA": 8017614.0625, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 2248, "pixX": 255, "pixY": 146, "ToA": 8017614.0625, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2240, "pixX": 254, "pixY": 148, "ToA": 8017614.0625, "ToT": 21, "TriggerNo": 1 }, { "pixel_index": 2220, "pixX": 249, "pixY": 149, "ToA": 8017614.0625, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2221, "pixX": 249, "pixY": 146, "ToA": 8017614.0625, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2239, "pixX": 250, "pixY": 150, "ToA": 8017614.0625, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2230, "pixX": 249, "pixY": 147, "ToA": 8017614.0625, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 2206, "pixX": 250, "pixY": 145, "ToA": 8017614.0625, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 2197, "pixX": 253, "pixY": 144, "ToA": 8017615.625, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2235, "pixX": 255, "pixY": 145, "ToA": 8017615.625, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2194, "pixX": 254, "pixY": 150, "ToA": 8017615.625, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2215, "pixX": 255, "pixY": 148, "ToA": 8017615.625, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2234, "pixX": 251, "pixY": 144, "ToA": 8017615.625, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2204, "pixX": 249, "pixY": 145, "ToA": 8017615.625, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2224, "pixX": 255, "pixY": 149, "ToA": 8017615.625, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2195, "pixX": 250, "pixY": 144, "ToA": 8017617.1875, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2199, "pixX": 254, "pixY": 144, "ToA": 8017617.1875, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2196, "pixX": 248, "pixY": 147, "ToA": 8017617.1875, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2214, "pixX": 253, "pixY": 151, "ToA": 8017617.1875, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2213, "pixX": 251, "pixY": 151, "ToA": 8017617.1875, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2192, "pixX": 252, "pixY": 151, "ToA": 8017617.1875, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2191, "pixX": 248, "pixY": 148, "ToA": 8017617.1875, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2228, "pixX": 249, "pixY": 150, "ToA": 8017617.1875, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2193, "pixX": 250, "pixY": 151, "ToA": 8017618.75, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2232, "pixX": 255, "pixY": 150, "ToA": 8017620.3125, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2203, "pixX": 254, "pixY": 151, "ToA": 8017621.875, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2212, "pixX": 111, "pixY": 123, "ToA": 8020029.6875, "ToT": 40, "TriggerNo": 1 }, { "pixel_index": 2205, "pixX": 110, "pixY": 123, "ToA": 8020029.6875, "ToT": 13, "TriggerNo": 1 }, { "pixel_index": 2209, "pixX": 208, "pixY": 146, "ToA": 8020739.0625, "ToT": 13, "TriggerNo": 1 }, { "pixel_index": 2226, "pixX": 248, "pixY": 232, "ToA": 8022528.125, "ToT": 43, "TriggerNo": 1 }, { "pixel_index": 2211, "pixX": 249, "pixY": 232, "ToA": 8022528.125, "ToT": 13, "TriggerNo": 1 }, { "pixel_index": 2249, "pixX": 163, "pixY": 141, "ToA": 8030735.9375, "ToT": 10, "TriggerNo": 1 }, { "pixel_index": 2237, "pixX": 163, "pixY": 140, "ToA": 8030739.0625, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 2250, "pixX": 99, "pixY": 81, "ToA": 8031468.75, "ToT": 23, "TriggerNo": 1 }, { "pixel_index": 2259, "pixX": 88, "pixY": 87, "ToA": 8031468.75, "ToT": 66, "TriggerNo": 1 }, { "pixel_index": 2245, "pixX": 99, "pixY": 82, "ToA": 8031473.4375, "ToT": 22, "TriggerNo": 1 }, { "pixel_index": 2247, "pixX": 88, "pixY": 86, "ToA": 8031475.0, "ToT": 18, "TriggerNo": 1 }, { "pixel_index": 2241, "pixX": 88, "pixY": 88, "ToA": 8031476.5625, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 2251, "pixX": 100, "pixY": 82, "ToA": 8031481.25, "ToT": 26, "TriggerNo": 1 }, { "pixel_index": 2255, "pixX": 89, "pixY": 86, "ToA": 8031481.25, "ToT": 35, "TriggerNo": 1 }, { "pixel_index": 2243, "pixX": 100, "pixY": 81, "ToA": 8031484.375, "ToT": 15, "TriggerNo": 1 }, { "pixel_index": 2242, "pixX": 87, "pixY": 87, "ToA": 8031485.9375, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2268, "pixX": 112, "pixY": 183, "ToA": 8038625.0, "ToT": 19, "TriggerNo": 1 }, { "pixel_index": 2273, "pixX": 113, "pixY": 183, "ToA": 8038625.0, "ToT": 18, "TriggerNo": 1 }, { "pixel_index": 2271, "pixX": 111, "pixY": 181, "ToA": 8038626.5625, "ToT": 18, "TriggerNo": 1 }, { "pixel_index": 2265, "pixX": 112, "pixY": 182, "ToA": 8038626.5625, "ToT": 14, "TriggerNo": 1 }, { "pixel_index": 2266, "pixX": 114, "pixY": 182, "ToA": 8038626.5625, "ToT": 27, "TriggerNo": 1 }, { "pixel_index": 2269, "pixX": 115, "pixY": 182, "ToA": 8038629.6875, "ToT": 30, "TriggerNo": 1 }, { "pixel_index": 2274, "pixX": 111, "pixY": 182, "ToA": 8038629.6875, "ToT": 10, "TriggerNo": 1 }, { "pixel_index": 2267, "pixX": 110, "pixY": 181, "ToA": 8038637.5, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 2264, "pixX": 110, "pixY": 180, "ToA": 8038656.25, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 2270, "pixX": 38, "pixY": 234, "ToA": 8042201.5625, "ToT": 32, "TriggerNo": 1 }, { "pixel_index": 2272, "pixX": 184, "pixY": 203, "ToA": 8043671.875, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2275, "pixX": 168, "pixY": 217, "ToA": 8045781.25, "ToT": 21, "TriggerNo": 1 }, { "pixel_index": 2285, "pixX": 201, "pixY": 216, "ToA": 8047667.1875, "ToT": 116, "TriggerNo": 1 }, { "pixel_index": 2280, "pixX": 202, "pixY": 214, "ToA": 8047668.75, "ToT": 27, "TriggerNo": 1 }, { "pixel_index": 2281, "pixX": 200, "pixY": 216, "ToA": 8047668.75, "ToT": 11, "TriggerNo": 1 }, { "pixel_index": 2282, "pixX": 200, "pixY": 215, "ToA": 8047668.75, "ToT": 28, "TriggerNo": 1 }, { "pixel_index": 2284, "pixX": 202, "pixY": 215, "ToA": 8047668.75, "ToT": 29, "TriggerNo": 1 }, { "pixel_index": 2296, "pixX": 201, "pixY": 214, "ToA": 8047668.75, "ToT": 270, "TriggerNo": 1 }, { "pixel_index": 2299, "pixX": 201, "pixY": 215, "ToA": 8047668.75, "ToT": 311, "TriggerNo": 1 }, { "pixel_index": 2287, "pixX": 200, "pixY": 214, "ToA": 8047670.3125, "ToT": 41, "TriggerNo": 1 }, { "pixel_index": 2276, "pixX": 202, "pixY": 216, "ToA": 8047671.875, "ToT": 10, "TriggerNo": 1 }, { "pixel_index": 2291, "pixX": 201, "pixY": 213, "ToA": 8047671.875, "ToT": 68, "TriggerNo": 1 }, { "pixel_index": 2278, "pixX": 200, "pixY": 213, "ToA": 8047673.4375, "ToT": 13, "TriggerNo": 1 }, { "pixel_index": 2277, "pixX": 201, "pixY": 217, "ToA": 8047675.0, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 2279, "pixX": 202, "pixY": 213, "ToA": 8047676.5625, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 2283, "pixX": 150, "pixY": 195, "ToA": 8051273.4375, "ToT": 20, "TriggerNo": 1 }, { "pixel_index": 2286, "pixX": 150, "pixY": 194, "ToA": 8051273.4375, "ToT": 21, "TriggerNo": 1 }, { "pixel_index": 2290, "pixX": 151, "pixY": 194, "ToA": 8051273.4375, "ToT": 52, "TriggerNo": 1 }, { "pixel_index": 2295, "pixX": 151, "pixY": 195, "ToA": 8051273.4375, "ToT": 42, "TriggerNo": 1 }, { "pixel_index": 2288, "pixX": 120, "pixY": 165, "ToA": 8053904.6875, "ToT": 33, "TriggerNo": 1 }, { "pixel_index": 2292, "pixX": 120, "pixY": 166, "ToA": 8053904.6875, "ToT": 38, "TriggerNo": 1 }, { "pixel_index": 2294, "pixX": 11, "pixY": 236, "ToA": 8055985.9375, "ToT": 18, "TriggerNo": 1 }, { "pixel_index": 2289, "pixX": 11, "pixY": 237, "ToA": 8055985.9375, "ToT": 16, "TriggerNo": 1 }, { "pixel_index": 2303, "pixX": 200, "pixY": 156, "ToA": 8056968.75, "ToT": 13, "TriggerNo": 1 }, { "pixel_index": 2297, "pixX": 201, "pixY": 157, "ToA": 8056968.75, "ToT": 11, "TriggerNo": 1 }, { "pixel_index": 2304, "pixX": 200, "pixY": 157, "ToA": 8056968.75, "ToT": 47, "TriggerNo": 1 }, { "pixel_index": 2293, "pixX": 201, "pixY": 156, "ToA": 8056971.875, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 2301, "pixX": 135, "pixY": 19, "ToA": 8060312.5, "ToT": 66, "TriggerNo": 1 }, { "pixel_index": 2298, "pixX": 135, "pixY": 18, "ToA": 8060331.25, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2302, "pixX": 123, "pixY": 0, "ToA": 8062200.0, "ToT": 15, "TriggerNo": 1 }, { "pixel_index": 2300, "pixX": 124, "pixY": 0, "ToA": 8062209.375, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 2305, "pixX": 106, "pixY": 6, "ToA": 8067920.3125, "ToT": 12, "TriggerNo": 1 }, { "pixel_index": 2307, "pixX": 11, "pixY": 222, "ToA": 8076203.125, "ToT": 17, "TriggerNo": 1 }, { "pixel_index": 2306, "pixX": 10, "pixY": 222, "ToA": 8076237.5, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2308, "pixX": 189, "pixY": 155, "ToA": 8079770.3125, "ToT": 32, "TriggerNo": 1 }, { "pixel_index": 2309, "pixX": 7, "pixY": 150, "ToA": 8083342.1875, "ToT": 74, "TriggerNo": 1 }, { "pixel_index": 2312, "pixX": 18, "pixY": 111, "ToA": 8088793.75, "ToT": 15, "TriggerNo": 1 }, { "pixel_index": 2310, "pixX": 19, "pixY": 111, "ToA": 8088796.875, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 2313, "pixX": 81, "pixY": 177, "ToA": 8089681.25, "ToT": 49, "TriggerNo": 1 }, { "pixel_index": 2311, "pixX": 81, "pixY": 176, "ToA": 8089684.375, "ToT": 11, "TriggerNo": 1 }, { "pixel_index": 2315, "pixX": 16, "pixY": 228, "ToA": 8093803.125, "ToT": 13, "TriggerNo": 1 }, { "pixel_index": 2314, "pixX": 17, "pixY": 228, "ToA": 8093807.8125, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 2316, "pixX": 66, "pixY": 106, "ToA": 8096434.375, "ToT": 44, "TriggerNo": 1 }, { "pixel_index": 2319, "pixX": 204, "pixY": 117, "ToA": 8097659.375, "ToT": 11, "TriggerNo": 1 }, { "pixel_index": 2317, "pixX": 204, "pixY": 116, "ToA": 8097665.625, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 2318, "pixX": 84, "pixY": 191, "ToA": 8099479.6875, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 2322, "pixX": 197, "pixY": 73, "ToA": 8105440.625, "ToT": 10, "TriggerNo": 1 }, { "pixel_index": 2320, "pixX": 197, "pixY": 72, "ToA": 8105443.75, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 2324, "pixX": 59, "pixY": 184, "ToA": 8105575.0, "ToT": 56, "TriggerNo": 1 }, { "pixel_index": 2321, "pixX": 58, "pixY": 184, "ToA": 8105575.0, "ToT": 49, "TriggerNo": 1 }, { "pixel_index": 2325, "pixX": 86, "pixY": 232, "ToA": 8108834.375, "ToT": 20, "TriggerNo": 1 }, { "pixel_index": 2329, "pixX": 86, "pixY": 231, "ToA": 8108834.375, "ToT": 114, "TriggerNo": 1 }, { "pixel_index": 2326, "pixX": 86, "pixY": 230, "ToA": 8108859.375, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 2323, "pixX": 87, "pixY": 231, "ToA": 8108879.6875, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2336, "pixX": 236, "pixY": 239, "ToA": 8113753.125, "ToT": 112, "TriggerNo": 1 }, { "pixel_index": 2338, "pixX": 247, "pixY": 215, "ToA": 8113754.6875, "ToT": 125, "TriggerNo": 1 }, { "pixel_index": 2331, "pixX": 237, "pixY": 239, "ToA": 8113757.8125, "ToT": 26, "TriggerNo": 1 }, { "pixel_index": 2328, "pixX": 247, "pixY": 216, "ToA": 8113759.375, "ToT": 17, "TriggerNo": 1 }, { "pixel_index": 2335, "pixX": 247, "pixY": 214, "ToA": 8113770.3125, "ToT": 19, "TriggerNo": 1 }, { "pixel_index": 2327, "pixX": 236, "pixY": 238, "ToA": 8113771.875, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 2330, "pixX": 246, "pixY": 215, "ToA": 8113771.875, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 2337, "pixX": 243, "pixY": 37, "ToA": 8115556.25, "ToT": 58, "TriggerNo": 1 }, { "pixel_index": 2333, "pixX": 243, "pixY": 38, "ToA": 8115556.25, "ToT": 16, "TriggerNo": 1 }, { "pixel_index": 2334, "pixX": 124, "pixY": 77, "ToA": 8115985.9375, "ToT": 11, "TriggerNo": 1 }, { "pixel_index": 2332, "pixX": 123, "pixY": 77, "ToA": 8115992.1875, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 2341, "pixX": 112, "pixY": 70, "ToA": 8119681.25, "ToT": 19, "TriggerNo": 1 }, { "pixel_index": 2347, "pixX": 113, "pixY": 71, "ToA": 8119681.25, "ToT": 25, "TriggerNo": 1 }, { "pixel_index": 2340, "pixX": 111, "pixY": 70, "ToA": 8119682.8125, "ToT": 17, "TriggerNo": 1 }, { "pixel_index": 2339, "pixX": 112, "pixY": 71, "ToA": 8119696.875, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 2344, "pixX": 119, "pixY": 63, "ToA": 8122867.1875, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 2348, "pixX": 69, "pixY": 133, "ToA": 8123270.3125, "ToT": 10, "TriggerNo": 1 }, { "pixel_index": 2349, "pixX": 68, "pixY": 131, "ToA": 8123270.3125, "ToT": 17, "TriggerNo": 1 }, { "pixel_index": 2353, "pixX": 68, "pixY": 132, "ToA": 8123270.3125, "ToT": 33, "TriggerNo": 1 }, { "pixel_index": 2354, "pixX": 69, "pixY": 131, "ToA": 8123270.3125, "ToT": 96, "TriggerNo": 1 }, { "pixel_index": 2358, "pixX": 69, "pixY": 132, "ToA": 8123270.3125, "ToT": 137, "TriggerNo": 1 }, { "pixel_index": 2343, "pixX": 70, "pixY": 132, "ToA": 8123271.875, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 2345, "pixX": 69, "pixY": 130, "ToA": 8123271.875, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 2346, "pixX": 70, "pixY": 131, "ToA": 8123271.875, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 2342, "pixX": 68, "pixY": 133, "ToA": 8123275.0, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2355, "pixX": 192, "pixY": 93, "ToA": 8128067.1875, "ToT": 13, "TriggerNo": 1 }, { "pixel_index": 2359, "pixX": 193, "pixY": 92, "ToA": 8128070.3125, "ToT": 23, "TriggerNo": 1 }, { "pixel_index": 2352, "pixX": 193, "pixY": 93, "ToA": 8128076.5625, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 2363, "pixX": 180, "pixY": 222, "ToA": 8128465.625, "ToT": 168, "TriggerNo": 1 }, { "pixel_index": 2367, "pixX": 180, "pixY": 221, "ToA": 8128465.625, "ToT": 198, "TriggerNo": 1 }, { "pixel_index": 2357, "pixX": 179, "pixY": 221, "ToA": 8128467.1875, "ToT": 20, "TriggerNo": 1 }, { "pixel_index": 2360, "pixX": 181, "pixY": 221, "ToA": 8128467.1875, "ToT": 11, "TriggerNo": 1 }, { "pixel_index": 2361, "pixX": 181, "pixY": 222, "ToA": 8128467.1875, "ToT": 12, "TriggerNo": 1 }, { "pixel_index": 2356, "pixX": 180, "pixY": 223, "ToA": 8128467.1875, "ToT": 11, "TriggerNo": 1 }, { "pixel_index": 2351, "pixX": 179, "pixY": 222, "ToA": 8128467.1875, "ToT": 17, "TriggerNo": 1 }, { "pixel_index": 2350, "pixX": 180, "pixY": 220, "ToA": 8128468.75, "ToT": 11, "TriggerNo": 1 }, { "pixel_index": 2364, "pixX": 84, "pixY": 172, "ToA": 8137217.1875, "ToT": 30, "TriggerNo": 1 }, { "pixel_index": 2362, "pixX": 84, "pixY": 173, "ToA": 8137217.1875, "ToT": 21, "TriggerNo": 1 }, { "pixel_index": 2380, "pixX": 90, "pixY": 161, "ToA": 8140587.5, "ToT": 40, "TriggerNo": 1 }, { "pixel_index": 2379, "pixX": 89, "pixY": 161, "ToA": 8140589.0625, "ToT": 21, "TriggerNo": 1 }, { "pixel_index": 2373, "pixX": 88, "pixY": 163, "ToA": 8140589.0625, "ToT": 14, "TriggerNo": 1 }, { "pixel_index": 2385, "pixX": 89, "pixY": 162, "ToA": 8140589.0625, "ToT": 51, "TriggerNo": 1 }, { "pixel_index": 2386, "pixX": 91, "pixY": 161, "ToA": 8140589.0625, "ToT": 21, "TriggerNo": 1 }, { "pixel_index": 2389, "pixX": 89, "pixY": 163, "ToA": 8140589.0625, "ToT": 49, "TriggerNo": 1 }, { "pixel_index": 2366, "pixX": 88, "pixY": 162, "ToA": 8140590.625, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 2368, "pixX": 91, "pixY": 162, "ToA": 8140592.1875, "ToT": 16, "TriggerNo": 1 }, { "pixel_index": 2374, "pixX": 91, "pixY": 163, "ToA": 8140601.5625, "ToT": 19, "TriggerNo": 1 }, { "pixel_index": 2365, "pixX": 92, "pixY": 162, "ToA": 8140610.9375, "ToT": 3, "TriggerNo": 1 }, { "pixel_index": 2388, "pixX": 102, "pixY": 183, "ToA": 8141073.4375, "ToT": 257, "TriggerNo": 1 }, { "pixel_index": 2378, "pixX": 102, "pixY": 182, "ToA": 8141075.0, "ToT": 25, "TriggerNo": 1 }, { "pixel_index": 2384, "pixX": 103, "pixY": 183, "ToA": 8141075.0, "ToT": 25, "TriggerNo": 1 }, { "pixel_index": 2376, "pixX": 101, "pixY": 183, "ToA": 8141076.5625, "ToT": 25, "TriggerNo": 1 }, { "pixel_index": 2377, "pixX": 102, "pixY": 184, "ToA": 8141076.5625, "ToT": 27, "TriggerNo": 1 }, { "pixel_index": 2372, "pixX": 103, "pixY": 182, "ToA": 8141096.875, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 2369, "pixX": 101, "pixY": 184, "ToA": 8141096.875, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 2371, "pixX": 101, "pixY": 182, "ToA": 8141098.4375, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 2370, "pixX": 103, "pixY": 184, "ToA": 8141100.0, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 2375, "pixX": 134, "pixY": 21, "ToA": 8142376.5625, "ToT": 25, "TriggerNo": 1 }, { "pixel_index": 2412, "pixX": 40, "pixY": 127, "ToA": 8146259.375, "ToT": 158, "TriggerNo": 1 }, { "pixel_index": 2403, "pixX": 40, "pixY": 126, "ToA": 8146259.375, "ToT": 75, "TriggerNo": 1 }, { "pixel_index": 2387, "pixX": 41, "pixY": 127, "ToA": 8146264.0625, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 2381, "pixX": 40, "pixY": 128, "ToA": 8146265.625, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 2382, "pixX": 39, "pixY": 127, "ToA": 8146267.1875, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 2383, "pixX": 41, "pixY": 126, "ToA": 8146284.375, "ToT": 3, "TriggerNo": 1 }, { "pixel_index": 2501, "pixX": 120, "pixY": 137, "ToA": 8149904.6875, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 2465, "pixX": 123, "pixY": 133, "ToA": 8149904.6875, "ToT": 22, "TriggerNo": 1 }, { "pixel_index": 2502, "pixX": 123, "pixY": 135, "ToA": 8149904.6875, "ToT": 204, "TriggerNo": 1 }, { "pixel_index": 2503, "pixX": 124, "pixY": 137, "ToA": 8149904.6875, "ToT": 28, "TriggerNo": 1 }, { "pixel_index": 2423, "pixX": 121, "pixY": 137, "ToA": 8149904.6875, "ToT": 22, "TriggerNo": 1 }, { "pixel_index": 2517, "pixX": 124, "pixY": 135, "ToA": 8149904.6875, "ToT": 188, "TriggerNo": 1 }, { "pixel_index": 2420, "pixX": 122, "pixY": 133, "ToA": 8149904.6875, "ToT": 15, "TriggerNo": 1 }, { "pixel_index": 2418, "pixX": 123, "pixY": 138, "ToA": 8149904.6875, "ToT": 12, "TriggerNo": 1 }, { "pixel_index": 2506, "pixX": 121, "pixY": 134, "ToA": 8149904.6875, "ToT": 24, "TriggerNo": 1 }, { "pixel_index": 2516, "pixX": 122, "pixY": 136, "ToA": 8149904.6875, "ToT": 351, "TriggerNo": 1 }, { "pixel_index": 2475, "pixX": 122, "pixY": 138, "ToA": 8149904.6875, "ToT": 10, "TriggerNo": 1 }, { "pixel_index": 2451, "pixX": 120, "pixY": 135, "ToA": 8149904.6875, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 2508, "pixX": 124, "pixY": 134, "ToA": 8149904.6875, "ToT": 48, "TriggerNo": 1 }, { "pixel_index": 2439, "pixX": 121, "pixY": 138, "ToA": 8149904.6875, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 2409, "pixX": 125, "pixY": 136, "ToA": 8149904.6875, "ToT": 17, "TriggerNo": 1 }, { "pixel_index": 2511, "pixX": 121, "pixY": 136, "ToA": 8149904.6875, "ToT": 68, "TriggerNo": 1 }, { "pixel_index": 2482, "pixX": 120, "pixY": 136, "ToA": 8149904.6875, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 2483, "pixX": 123, "pixY": 134, "ToA": 8149904.6875, "ToT": 161, "TriggerNo": 1 }, { "pixel_index": 2484, "pixX": 124, "pixY": 136, "ToA": 8149904.6875, "ToT": 96, "TriggerNo": 1 }, { "pixel_index": 2429, "pixX": 125, "pixY": 134, "ToA": 8149904.6875, "ToT": 10, "TriggerNo": 1 }, { "pixel_index": 2518, "pixX": 122, "pixY": 135, "ToA": 8149904.6875, "ToT": 339, "TriggerNo": 1 }, { "pixel_index": 2428, "pixX": 120, "pixY": 134, "ToA": 8149904.6875, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 2512, "pixX": 122, "pixY": 134, "ToA": 8149904.6875, "ToT": 87, "TriggerNo": 1 }, { "pixel_index": 2515, "pixX": 121, "pixY": 135, "ToA": 8149904.6875, "ToT": 69, "TriggerNo": 1 }, { "pixel_index": 2425, "pixX": 125, "pixY": 137, "ToA": 8149904.6875, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 2453, "pixX": 125, "pixY": 135, "ToA": 8149904.6875, "ToT": 25, "TriggerNo": 1 }, { "pixel_index": 2452, "pixX": 122, "pixY": 137, "ToA": 8149904.6875, "ToT": 67, "TriggerNo": 1 }, { "pixel_index": 2493, "pixX": 121, "pixY": 133, "ToA": 8149904.6875, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 2494, "pixX": 123, "pixY": 136, "ToA": 8149904.6875, "ToT": 376, "TriggerNo": 1 }, { "pixel_index": 2507, "pixX": 123, "pixY": 137, "ToA": 8149904.6875, "ToT": 78, "TriggerNo": 1 }, { "pixel_index": 2441, "pixX": 125, "pixY": 138, "ToA": 8149906.25, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2476, "pixX": 124, "pixY": 132, "ToA": 8149906.25, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2495, "pixX": 124, "pixY": 133, "ToA": 8149906.25, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 2513, "pixX": 124, "pixY": 138, "ToA": 8149906.25, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 2474, "pixX": 121, "pixY": 132, "ToA": 8149906.25, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2464, "pixX": 121, "pixY": 139, "ToA": 8149906.25, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2390, "pixX": 120, "pixY": 138, "ToA": 8149906.25, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2396, "pixX": 122, "pixY": 139, "ToA": 8149906.25, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2401, "pixX": 122, "pixY": 132, "ToA": 8149906.25, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2414, "pixX": 120, "pixY": 133, "ToA": 8149906.25, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2416, "pixX": 125, "pixY": 133, "ToA": 8149906.25, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2431, "pixX": 123, "pixY": 139, "ToA": 8149906.25, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2440, "pixX": 123, "pixY": 132, "ToA": 8149906.25, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2407, "pixX": 119, "pixY": 136, "ToA": 8149907.8125, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2392, "pixX": 124, "pixY": 139, "ToA": 8149907.8125, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2454, "pixX": 126, "pixY": 136, "ToA": 8149907.8125, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2393, "pixX": 126, "pixY": 137, "ToA": 8149907.8125, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2442, "pixX": 126, "pixY": 135, "ToA": 8149907.8125, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2426, "pixX": 126, "pixY": 134, "ToA": 8149907.8125, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2422, "pixX": 119, "pixY": 137, "ToA": 8149907.8125, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2410, "pixX": 126, "pixY": 133, "ToA": 8149907.8125, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2408, "pixX": 120, "pixY": 139, "ToA": 8149909.375, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2406, "pixX": 126, "pixY": 138, "ToA": 8149909.375, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2463, "pixX": 119, "pixY": 135, "ToA": 8149909.375, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2466, "pixX": 125, "pixY": 139, "ToA": 8149909.375, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2450, "pixX": 119, "pixY": 134, "ToA": 8149909.375, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2427, "pixX": 119, "pixY": 133, "ToA": 8149909.375, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2397, "pixX": 125, "pixY": 132, "ToA": 8149909.375, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2477, "pixX": 127, "pixY": 134, "ToA": 8149909.375, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2438, "pixX": 119, "pixY": 138, "ToA": 8149909.375, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2395, "pixX": 120, "pixY": 132, "ToA": 8149909.375, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2421, "pixX": 127, "pixY": 136, "ToA": 8149909.375, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2485, "pixX": 127, "pixY": 135, "ToA": 8149909.375, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2424, "pixX": 123, "pixY": 131, "ToA": 8149912.5, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2402, "pixX": 124, "pixY": 131, "ToA": 8149912.5, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2405, "pixX": 122, "pixY": 131, "ToA": 8149912.5, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2432, "pixX": 127, "pixY": 137, "ToA": 8149912.5, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2398, "pixX": 126, "pixY": 132, "ToA": 8149912.5, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2399, "pixX": 118, "pixY": 134, "ToA": 8149914.0625, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2400, "pixX": 121, "pixY": 131, "ToA": 8149914.0625, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2411, "pixX": 118, "pixY": 135, "ToA": 8149914.0625, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2467, "pixX": 127, "pixY": 133, "ToA": 8149914.0625, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2391, "pixX": 122, "pixY": 140, "ToA": 8149914.0625, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2415, "pixX": 123, "pixY": 140, "ToA": 8149914.0625, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2394, "pixX": 118, "pixY": 136, "ToA": 8149915.625, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2419, "pixX": 125, "pixY": 131, "ToA": 8149917.1875, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2413, "pixX": 70, "pixY": 193, "ToA": 8151318.75, "ToT": 65, "TriggerNo": 1 }, { "pixel_index": 2404, "pixX": 70, "pixY": 194, "ToA": 8151318.75, "ToT": 20, "TriggerNo": 1 }, { "pixel_index": 2417, "pixX": 190, "pixY": 47, "ToA": 8153615.625, "ToT": 24, "TriggerNo": 1 }, { "pixel_index": 2430, "pixX": 244, "pixY": 46, "ToA": 8156692.1875, "ToT": 26, "TriggerNo": 1 }, { "pixel_index": 2433, "pixX": 243, "pixY": 46, "ToA": 8156692.1875, "ToT": 49, "TriggerNo": 1 }, { "pixel_index": 2489, "pixX": 16, "pixY": 33, "ToA": 8158543.75, "ToT": 169, "TriggerNo": 1 }, { "pixel_index": 2480, "pixX": 16, "pixY": 32, "ToA": 8158543.75, "ToT": 23, "TriggerNo": 1 }, { "pixel_index": 2499, "pixX": 16, "pixY": 34, "ToA": 8158543.75, "ToT": 23, "TriggerNo": 1 }, { "pixel_index": 2498, "pixX": 15, "pixY": 34, "ToA": 8158545.3125, "ToT": 45, "TriggerNo": 1 }, { "pixel_index": 2514, "pixX": 15, "pixY": 33, "ToA": 8158545.3125, "ToT": 313, "TriggerNo": 1 }, { "pixel_index": 2488, "pixX": 15, "pixY": 32, "ToA": 8158545.3125, "ToT": 49, "TriggerNo": 1 }, { "pixel_index": 2510, "pixX": 14, "pixY": 33, "ToA": 8158545.3125, "ToT": 199, "TriggerNo": 1 }, { "pixel_index": 2479, "pixX": 14, "pixY": 34, "ToA": 8158545.3125, "ToT": 28, "TriggerNo": 1 }, { "pixel_index": 2471, "pixX": 17, "pixY": 33, "ToA": 8158545.3125, "ToT": 14, "TriggerNo": 1 }, { "pixel_index": 2505, "pixX": 14, "pixY": 32, "ToA": 8158546.875, "ToT": 41, "TriggerNo": 1 }, { "pixel_index": 2459, "pixX": 15, "pixY": 35, "ToA": 8158548.4375, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2460, "pixX": 17, "pixY": 32, "ToA": 8158550.0, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 2435, "pixX": 17, "pixY": 34, "ToA": 8158550.0, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 2445, "pixX": 13, "pixY": 32, "ToA": 8158550.0, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 2469, "pixX": 13, "pixY": 34, "ToA": 8158550.0, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 2458, "pixX": 13, "pixY": 33, "ToA": 8158550.0, "ToT": 22, "TriggerNo": 1 }, { "pixel_index": 2434, "pixX": 14, "pixY": 35, "ToA": 8158551.5625, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2446, "pixX": 14, "pixY": 31, "ToA": 8158551.5625, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2470, "pixX": 15, "pixY": 31, "ToA": 8158551.5625, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2447, "pixX": 16, "pixY": 31, "ToA": 8158556.25, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2461, "pixX": 28, "pixY": 124, "ToA": 8159040.625, "ToT": 20, "TriggerNo": 1 }, { "pixel_index": 2436, "pixX": 28, "pixY": 125, "ToA": 8159040.625, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 2481, "pixX": 29, "pixY": 125, "ToA": 8159040.625, "ToT": 35, "TriggerNo": 1 }, { "pixel_index": 2462, "pixX": 30, "pixY": 124, "ToA": 8159042.1875, "ToT": 24, "TriggerNo": 1 }, { "pixel_index": 2490, "pixX": 29, "pixY": 124, "ToA": 8159042.1875, "ToT": 204, "TriggerNo": 1 }, { "pixel_index": 2437, "pixX": 30, "pixY": 125, "ToA": 8159042.1875, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2472, "pixX": 29, "pixY": 123, "ToA": 8159043.75, "ToT": 18, "TriggerNo": 1 }, { "pixel_index": 2448, "pixX": 28, "pixY": 123, "ToA": 8159045.3125, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2449, "pixX": 30, "pixY": 123, "ToA": 8159045.3125, "ToT": 3, "TriggerNo": 1 }, { "pixel_index": 2443, "pixX": 239, "pixY": 33, "ToA": 8160265.625, "ToT": 13, "TriggerNo": 1 }, { "pixel_index": 2456, "pixX": 239, "pixY": 34, "ToA": 8160265.625, "ToT": 13, "TriggerNo": 1 }, { "pixel_index": 2468, "pixX": 240, "pixY": 30, "ToA": 8160267.1875, "ToT": 17, "TriggerNo": 1 }, { "pixel_index": 2478, "pixX": 240, "pixY": 31, "ToA": 8160267.1875, "ToT": 23, "TriggerNo": 1 }, { "pixel_index": 2457, "pixX": 241, "pixY": 30, "ToA": 8160270.3125, "ToT": 11, "TriggerNo": 1 }, { "pixel_index": 2497, "pixX": 241, "pixY": 29, "ToA": 8160271.875, "ToT": 20, "TriggerNo": 1 }, { "pixel_index": 2444, "pixX": 240, "pixY": 32, "ToA": 8160271.875, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 2487, "pixX": 241, "pixY": 28, "ToA": 8160275.0, "ToT": 28, "TriggerNo": 1 }, { "pixel_index": 2455, "pixX": 163, "pixY": 166, "ToA": 8161364.0625, "ToT": 13, "TriggerNo": 1 }, { "pixel_index": 2473, "pixX": 100, "pixY": 189, "ToA": 8165639.0625, "ToT": 52, "TriggerNo": 1 }, { "pixel_index": 2486, "pixX": 221, "pixY": 123, "ToA": 8170992.1875, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 2492, "pixX": 50, "pixY": 56, "ToA": 8171006.25, "ToT": 40, "TriggerNo": 1 }, { "pixel_index": 2491, "pixX": 49, "pixY": 56, "ToA": 8171010.9375, "ToT": 14, "TriggerNo": 1 }, { "pixel_index": 2500, "pixX": 50, "pixY": 55, "ToA": 8171031.25, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 2496, "pixX": 156, "pixY": 196, "ToA": 8174420.3125, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 2509, "pixX": 175, "pixY": 96, "ToA": 8174573.4375, "ToT": 99, "TriggerNo": 1 }, { "pixel_index": 2504, "pixX": 174, "pixY": 96, "ToA": 8174576.5625, "ToT": 26, "TriggerNo": 1 }, { "pixel_index": 2519, "pixX": 228, "pixY": 14, "ToA": 8183523.4375, "ToT": 12, "TriggerNo": 1 }, { "pixel_index": 2520, "pixX": 228, "pixY": 15, "ToA": 8183531.25, "ToT": 18, "TriggerNo": 1 }, { "pixel_index": 2522, "pixX": 12, "pixY": 235, "ToA": 8191942.1875, "ToT": 50, "TriggerNo": 1 }, { "pixel_index": 2521, "pixX": 229, "pixY": 14, "ToA": 8191942.1875, "ToT": 46, "TriggerNo": 1 }, { "pixel_index": 2523, "pixX": 166, "pixY": 33, "ToA": 8194798.4375, "ToT": 18, "TriggerNo": 1 }, { "pixel_index": 2532, "pixX": 210, "pixY": 143, "ToA": 8196042.1875, "ToT": 58, "TriggerNo": 1 }, { "pixel_index": 2531, "pixX": 210, "pixY": 142, "ToA": 8196042.1875, "ToT": 26, "TriggerNo": 1 }, { "pixel_index": 2529, "pixX": 211, "pixY": 143, "ToA": 8196043.75, "ToT": 11, "TriggerNo": 1 }, { "pixel_index": 2527, "pixX": 211, "pixY": 142, "ToA": 8196046.875, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 2528, "pixX": 211, "pixY": 206, "ToA": 8196134.375, "ToT": 25, "TriggerNo": 1 }, { "pixel_index": 2526, "pixX": 210, "pixY": 207, "ToA": 8196134.375, "ToT": 14, "TriggerNo": 1 }, { "pixel_index": 2530, "pixX": 211, "pixY": 207, "ToA": 8196134.375, "ToT": 217, "TriggerNo": 1 }, { "pixel_index": 2525, "pixX": 212, "pixY": 207, "ToA": 8196135.9375, "ToT": 14, "TriggerNo": 1 }, { "pixel_index": 2524, "pixX": 211, "pixY": 208, "ToA": 8196137.5, "ToT": 16, "TriggerNo": 1 }, { "pixel_index": 2533, "pixX": 41, "pixY": 102, "ToA": 8206885.9375, "ToT": 66, "TriggerNo": 1 }, { "pixel_index": 2535, "pixX": 255, "pixY": 179, "ToA": 8220356.25, "ToT": 25, "TriggerNo": 1 }, { "pixel_index": 2534, "pixX": 255, "pixY": 180, "ToA": 8220360.9375, "ToT": 10, "TriggerNo": 1 }, { "pixel_index": 2539, "pixX": 73, "pixY": 234, "ToA": 8224664.0625, "ToT": 43, "TriggerNo": 1 }, { "pixel_index": 2537, "pixX": 73, "pixY": 235, "ToA": 8224664.0625, "ToT": 17, "TriggerNo": 1 }, { "pixel_index": 2536, "pixX": 72, "pixY": 234, "ToA": 8224678.125, "ToT": 17, "TriggerNo": 1 }, { "pixel_index": 2538, "pixX": 167, "pixY": 32, "ToA": 8226548.4375, "ToT": 23, "TriggerNo": 1 }, { "pixel_index": 2541, "pixX": 134, "pixY": 41, "ToA": 8229640.625, "ToT": 30, "TriggerNo": 1 }, { "pixel_index": 2540, "pixX": 134, "pixY": 40, "ToA": 8229643.75, "ToT": 11, "TriggerNo": 1 }, { "pixel_index": 2542, "pixX": 23, "pixY": 13, "ToA": 8240035.9375, "ToT": 13, "TriggerNo": 1 }, { "pixel_index": 2547, "pixX": 18, "pixY": 184, "ToA": 8247912.5, "ToT": 33, "TriggerNo": 1 }, { "pixel_index": 2559, "pixX": 19, "pixY": 184, "ToA": 8247912.5, "ToT": 57, "TriggerNo": 1 }, { "pixel_index": 2578, "pixX": 17, "pixY": 184, "ToA": 8247914.0625, "ToT": 27, "TriggerNo": 1 }, { "pixel_index": 2557, "pixX": 15, "pixY": 184, "ToA": 8247914.0625, "ToT": 35, "TriggerNo": 1 }, { "pixel_index": 2546, "pixX": 17, "pixY": 183, "ToA": 8247914.0625, "ToT": 45, "TriggerNo": 1 }, { "pixel_index": 2558, "pixX": 16, "pixY": 184, "ToA": 8247914.0625, "ToT": 14, "TriggerNo": 1 }, { "pixel_index": 2560, "pixX": 15, "pixY": 183, "ToA": 8247915.625, "ToT": 45, "TriggerNo": 1 }, { "pixel_index": 2570, "pixX": 16, "pixY": 185, "ToA": 8247918.75, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 2545, "pixX": 48, "pixY": 94, "ToA": 8247921.875, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 2544, "pixX": 14, "pixY": 183, "ToA": 8247929.6875, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 2543, "pixX": 17, "pixY": 185, "ToA": 8247935.9375, "ToT": 3, "TriggerNo": 1 }, { "pixel_index": 2585, "pixX": 132, "pixY": 54, "ToA": 8248220.3125, "ToT": 18, "TriggerNo": 1 }, { "pixel_index": 2592, "pixX": 132, "pixY": 55, "ToA": 8248220.3125, "ToT": 19, "TriggerNo": 1 }, { "pixel_index": 2548, "pixX": 132, "pixY": 56, "ToA": 8248225.0, "ToT": 14, "TriggerNo": 1 }, { "pixel_index": 2565, "pixX": 131, "pixY": 56, "ToA": 8248226.5625, "ToT": 13, "TriggerNo": 1 }, { "pixel_index": 2575, "pixX": 131, "pixY": 57, "ToA": 8248228.125, "ToT": 14, "TriggerNo": 1 }, { "pixel_index": 2597, "pixX": 130, "pixY": 58, "ToA": 8248229.6875, "ToT": 20, "TriggerNo": 1 }, { "pixel_index": 2552, "pixX": 130, "pixY": 59, "ToA": 8248232.8125, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 2564, "pixX": 129, "pixY": 60, "ToA": 8248235.9375, "ToT": 20, "TriggerNo": 1 }, { "pixel_index": 2574, "pixX": 128, "pixY": 61, "ToA": 8248235.9375, "ToT": 24, "TriggerNo": 1 }, { "pixel_index": 2561, "pixX": 127, "pixY": 62, "ToA": 8248235.9375, "ToT": 23, "TriggerNo": 1 }, { "pixel_index": 2567, "pixX": 129, "pixY": 59, "ToA": 8248237.5, "ToT": 20, "TriggerNo": 1 }, { "pixel_index": 2553, "pixX": 133, "pixY": 54, "ToA": 8248242.1875, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2550, "pixX": 128, "pixY": 60, "ToA": 8248242.1875, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 2590, "pixX": 130, "pixY": 57, "ToA": 8248242.1875, "ToT": 13, "TriggerNo": 1 }, { "pixel_index": 2583, "pixX": 130, "pixY": 56, "ToA": 8248242.1875, "ToT": 15, "TriggerNo": 1 }, { "pixel_index": 2554, "pixX": 129, "pixY": 58, "ToA": 8248245.3125, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 2549, "pixX": 127, "pixY": 61, "ToA": 8248245.3125, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 2569, "pixX": 133, "pixY": 50, "ToA": 8248245.3125, "ToT": 20, "TriggerNo": 1 }, { "pixel_index": 2577, "pixX": 132, "pixY": 53, "ToA": 8248245.3125, "ToT": 13, "TriggerNo": 1 }, { "pixel_index": 2566, "pixX": 132, "pixY": 52, "ToA": 8248245.3125, "ToT": 12, "TriggerNo": 1 }, { "pixel_index": 2555, "pixX": 131, "pixY": 54, "ToA": 8248246.875, "ToT": 11, "TriggerNo": 1 }, { "pixel_index": 2568, "pixX": 131, "pixY": 55, "ToA": 8248246.875, "ToT": 11, "TriggerNo": 1 }, { "pixel_index": 2579, "pixX": 133, "pixY": 51, "ToA": 8248246.875, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 2586, "pixX": 132, "pixY": 51, "ToA": 8248248.4375, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 2556, "pixX": 133, "pixY": 49, "ToA": 8248251.5625, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 2551, "pixX": 134, "pixY": 49, "ToA": 8248253.125, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 2571, "pixX": 145, "pixY": 178, "ToA": 8251493.75, "ToT": 77, "TriggerNo": 1 }, { "pixel_index": 2563, "pixX": 146, "pixY": 178, "ToA": 8251503.125, "ToT": 13, "TriggerNo": 1 }, { "pixel_index": 2562, "pixX": 145, "pixY": 177, "ToA": 8251507.8125, "ToT": 12, "TriggerNo": 1 }, { "pixel_index": 2588, "pixX": 6, "pixY": 1, "ToA": 8253606.25, "ToT": 38, "TriggerNo": 1 }, { "pixel_index": 2600, "pixX": 5, "pixY": 1, "ToA": 8253606.25, "ToT": 280, "TriggerNo": 1 }, { "pixel_index": 2599, "pixX": 4, "pixY": 2, "ToA": 8253606.25, "ToT": 114, "TriggerNo": 1 }, { "pixel_index": 2598, "pixX": 3, "pixY": 2, "ToA": 8253606.25, "ToT": 24, "TriggerNo": 1 }, { "pixel_index": 2595, "pixX": 4, "pixY": 0, "ToA": 8253606.25, "ToT": 69, "TriggerNo": 1 }, { "pixel_index": 2594, "pixX": 6, "pixY": 2, "ToA": 8253606.25, "ToT": 22, "TriggerNo": 1 }, { "pixel_index": 2587, "pixX": 3, "pixY": 0, "ToA": 8253606.25, "ToT": 20, "TriggerNo": 1 }, { "pixel_index": 2622, "pixX": 4, "pixY": 1, "ToA": 8253606.25, "ToT": 364, "TriggerNo": 1 }, { "pixel_index": 2589, "pixX": 5, "pixY": 0, "ToA": 8253606.25, "ToT": 51, "TriggerNo": 1 }, { "pixel_index": 2593, "pixX": 3, "pixY": 1, "ToA": 8253606.25, "ToT": 60, "TriggerNo": 1 }, { "pixel_index": 2576, "pixX": 4, "pixY": 3, "ToA": 8253606.25, "ToT": 14, "TriggerNo": 1 }, { "pixel_index": 2612, "pixX": 5, "pixY": 2, "ToA": 8253606.25, "ToT": 172, "TriggerNo": 1 }, { "pixel_index": 2581, "pixX": 6, "pixY": 0, "ToA": 8253606.25, "ToT": 12, "TriggerNo": 1 }, { "pixel_index": 2582, "pixX": 5, "pixY": 3, "ToA": 8253606.25, "ToT": 21, "TriggerNo": 1 }, { "pixel_index": 2580, "pixX": 2, "pixY": 1, "ToA": 8253610.9375, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 2572, "pixX": 3, "pixY": 3, "ToA": 8253612.5, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 2573, "pixX": 6, "pixY": 3, "ToA": 8253614.0625, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 2584, "pixX": 127, "pixY": 176, "ToA": 8256857.8125, "ToT": 37, "TriggerNo": 1 }, { "pixel_index": 2591, "pixX": 127, "pixY": 177, "ToA": 8256857.8125, "ToT": 68, "TriggerNo": 1 }, { "pixel_index": 2596, "pixX": 142, "pixY": 134, "ToA": 8260435.9375, "ToT": 32, "TriggerNo": 1 }, { "pixel_index": 2606, "pixX": 71, "pixY": 107, "ToA": 8265812.5, "ToT": 10, "TriggerNo": 1 }, { "pixel_index": 2639, "pixX": 87, "pixY": 159, "ToA": 8265860.9375, "ToT": 61, "TriggerNo": 1 }, { "pixel_index": 2648, "pixX": 89, "pixY": 159, "ToA": 8265860.9375, "ToT": 285, "TriggerNo": 1 }, { "pixel_index": 2641, "pixX": 89, "pixY": 160, "ToA": 8265860.9375, "ToT": 67, "TriggerNo": 1 }, { "pixel_index": 2624, "pixX": 89, "pixY": 161, "ToA": 8265860.9375, "ToT": 13, "TriggerNo": 1 }, { "pixel_index": 2632, "pixX": 87, "pixY": 157, "ToA": 8265860.9375, "ToT": 23, "TriggerNo": 1 }, { "pixel_index": 2629, "pixX": 89, "pixY": 158, "ToA": 8265860.9375, "ToT": 53, "TriggerNo": 1 }, { "pixel_index": 2637, "pixX": 88, "pixY": 158, "ToA": 8265860.9375, "ToT": 126, "TriggerNo": 1 }, { "pixel_index": 2644, "pixX": 88, "pixY": 159, "ToA": 8265860.9375, "ToT": 251, "TriggerNo": 1 }, { "pixel_index": 2616, "pixX": 86, "pixY": 158, "ToA": 8265860.9375, "ToT": 22, "TriggerNo": 1 }, { "pixel_index": 2646, "pixX": 87, "pixY": 158, "ToA": 8265860.9375, "ToT": 231, "TriggerNo": 1 }, { "pixel_index": 2640, "pixX": 91, "pixY": 161, "ToA": 8265862.5, "ToT": 53, "TriggerNo": 1 }, { "pixel_index": 2608, "pixX": 89, "pixY": 157, "ToA": 8265862.5, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 2614, "pixX": 88, "pixY": 161, "ToA": 8265862.5, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 2618, "pixX": 88, "pixY": 157, "ToA": 8265862.5, "ToT": 15, "TriggerNo": 1 }, { "pixel_index": 2636, "pixX": 90, "pixY": 158, "ToA": 8265862.5, "ToT": 40, "TriggerNo": 1 }, { "pixel_index": 2633, "pixX": 90, "pixY": 161, "ToA": 8265862.5, "ToT": 30, "TriggerNo": 1 }, { "pixel_index": 2643, "pixX": 91, "pixY": 159, "ToA": 8265862.5, "ToT": 161, "TriggerNo": 1 }, { "pixel_index": 2634, "pixX": 88, "pixY": 160, "ToA": 8265862.5, "ToT": 40, "TriggerNo": 1 }, { "pixel_index": 2630, "pixX": 91, "pixY": 158, "ToA": 8265862.5, "ToT": 25, "TriggerNo": 1 }, { "pixel_index": 2647, "pixX": 90, "pixY": 160, "ToA": 8265862.5, "ToT": 189, "TriggerNo": 1 }, { "pixel_index": 2650, "pixX": 90, "pixY": 159, "ToA": 8265862.5, "ToT": 241, "TriggerNo": 1 }, { "pixel_index": 2654, "pixX": 91, "pixY": 160, "ToA": 8265862.5, "ToT": 332, "TriggerNo": 1 }, { "pixel_index": 2642, "pixX": 93, "pixY": 160, "ToA": 8265864.0625, "ToT": 164, "TriggerNo": 1 }, { "pixel_index": 2619, "pixX": 92, "pixY": 158, "ToA": 8265864.0625, "ToT": 13, "TriggerNo": 1 }, { "pixel_index": 2613, "pixX": 90, "pixY": 162, "ToA": 8265864.0625, "ToT": 3, "TriggerNo": 1 }, { "pixel_index": 2620, "pixX": 90, "pixY": 157, "ToA": 8265864.0625, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 2625, "pixX": 91, "pixY": 162, "ToA": 8265864.0625, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 2638, "pixX": 93, "pixY": 159, "ToA": 8265864.0625, "ToT": 22, "TriggerNo": 1 }, { "pixel_index": 2645, "pixX": 92, "pixY": 159, "ToA": 8265864.0625, "ToT": 80, "TriggerNo": 1 }, { "pixel_index": 2635, "pixX": 92, "pixY": 161, "ToA": 8265864.0625, "ToT": 70, "TriggerNo": 1 }, { "pixel_index": 2649, "pixX": 92, "pixY": 160, "ToA": 8265864.0625, "ToT": 299, "TriggerNo": 1 }, { "pixel_index": 2626, "pixX": 93, "pixY": 161, "ToA": 8265865.625, "ToT": 33, "TriggerNo": 1 }, { "pixel_index": 2615, "pixX": 92, "pixY": 162, "ToA": 8265865.625, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 2607, "pixX": 91, "pixY": 157, "ToA": 8265865.625, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2604, "pixX": 87, "pixY": 160, "ToA": 8265865.625, "ToT": 13, "TriggerNo": 1 }, { "pixel_index": 2602, "pixX": 89, "pixY": 162, "ToA": 8265865.625, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2631, "pixX": 93, "pixY": 158, "ToA": 8265867.1875, "ToT": 3, "TriggerNo": 1 }, { "pixel_index": 2603, "pixX": 93, "pixY": 162, "ToA": 8265867.1875, "ToT": 3, "TriggerNo": 1 }, { "pixel_index": 2609, "pixX": 92, "pixY": 157, "ToA": 8265867.1875, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2623, "pixX": 86, "pixY": 159, "ToA": 8265868.75, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 2628, "pixX": 94, "pixY": 161, "ToA": 8265868.75, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 2601, "pixX": 91, "pixY": 163, "ToA": 8265868.75, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2621, "pixX": 94, "pixY": 159, "ToA": 8265868.75, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 2617, "pixX": 94, "pixY": 160, "ToA": 8265868.75, "ToT": 13, "TriggerNo": 1 }, { "pixel_index": 2611, "pixX": 94, "pixY": 158, "ToA": 8265870.3125, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2605, "pixX": 94, "pixY": 162, "ToA": 8265871.875, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2610, "pixX": 86, "pixY": 157, "ToA": 8265875.0, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 2627, "pixX": 138, "pixY": 237, "ToA": 8270328.125, "ToT": 63, "TriggerNo": 1 }, { "pixel_index": 2672, "pixX": 48, "pixY": 114, "ToA": 8280412.5, "ToT": 205, "TriggerNo": 1 }, { "pixel_index": 2670, "pixX": 48, "pixY": 113, "ToA": 8280412.5, "ToT": 59, "TriggerNo": 1 }, { "pixel_index": 2667, "pixX": 49, "pixY": 114, "ToA": 8280414.0625, "ToT": 16, "TriggerNo": 1 }, { "pixel_index": 2662, "pixX": 47, "pixY": 114, "ToA": 8280414.0625, "ToT": 24, "TriggerNo": 1 }, { "pixel_index": 2663, "pixX": 49, "pixY": 113, "ToA": 8280414.0625, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 2655, "pixX": 47, "pixY": 113, "ToA": 8280414.0625, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 2656, "pixX": 48, "pixY": 115, "ToA": 8280414.0625, "ToT": 16, "TriggerNo": 1 }, { "pixel_index": 2651, "pixX": 49, "pixY": 115, "ToA": 8280415.625, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2652, "pixX": 47, "pixY": 115, "ToA": 8280415.625, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2653, "pixX": 145, "pixY": 154, "ToA": 8281903.125, "ToT": 26, "TriggerNo": 1 }, { "pixel_index": 2657, "pixX": 74, "pixY": 161, "ToA": 8283071.875, "ToT": 13, "TriggerNo": 1 }, { "pixel_index": 2665, "pixX": 89, "pixY": 108, "ToA": 8283690.625, "ToT": 28, "TriggerNo": 1 }, { "pixel_index": 2658, "pixX": 88, "pixY": 108, "ToA": 8283690.625, "ToT": 11, "TriggerNo": 1 }, { "pixel_index": 2669, "pixX": 89, "pixY": 107, "ToA": 8283693.75, "ToT": 35, "TriggerNo": 1 }, { "pixel_index": 2671, "pixX": 86, "pixY": 106, "ToA": 8283693.75, "ToT": 57, "TriggerNo": 1 }, { "pixel_index": 2668, "pixX": 87, "pixY": 106, "ToA": 8283693.75, "ToT": 56, "TriggerNo": 1 }, { "pixel_index": 2659, "pixX": 87, "pixY": 105, "ToA": 8283695.3125, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 2664, "pixX": 86, "pixY": 105, "ToA": 8283695.3125, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 2666, "pixX": 88, "pixY": 106, "ToA": 8283696.875, "ToT": 22, "TriggerNo": 1 }, { "pixel_index": 2660, "pixX": 89, "pixY": 106, "ToA": 8283712.5, "ToT": 3, "TriggerNo": 1 }, { "pixel_index": 2661, "pixX": 132, "pixY": 231, "ToA": 8285064.0625, "ToT": 29, "TriggerNo": 1 }, { "pixel_index": 2682, "pixX": 235, "pixY": 51, "ToA": 8294704.6875, "ToT": 48, "TriggerNo": 1 }, { "pixel_index": 2676, "pixX": 235, "pixY": 50, "ToA": 8294709.375, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 2724, "pixX": 107, "pixY": 53, "ToA": 8294828.125, "ToT": 55, "TriggerNo": 1 }, { "pixel_index": 2733, "pixX": 106, "pixY": 52, "ToA": 8294828.125, "ToT": 155, "TriggerNo": 1 }, { "pixel_index": 2740, "pixX": 106, "pixY": 53, "ToA": 8294828.125, "ToT": 276, "TriggerNo": 1 }, { "pixel_index": 2713, "pixX": 107, "pixY": 52, "ToA": 8294828.125, "ToT": 63, "TriggerNo": 1 }, { "pixel_index": 2691, "pixX": 107, "pixY": 54, "ToA": 8294828.125, "ToT": 15, "TriggerNo": 1 }, { "pixel_index": 2696, "pixX": 106, "pixY": 54, "ToA": 8294828.125, "ToT": 49, "TriggerNo": 1 }, { "pixel_index": 2723, "pixX": 105, "pixY": 52, "ToA": 8294829.6875, "ToT": 294, "TriggerNo": 1 }, { "pixel_index": 2732, "pixX": 105, "pixY": 53, "ToA": 8294829.6875, "ToT": 181, "TriggerNo": 1 }, { "pixel_index": 2677, "pixX": 108, "pixY": 54, "ToA": 8294829.6875, "ToT": 3, "TriggerNo": 1 }, { "pixel_index": 2679, "pixX": 104, "pixY": 50, "ToA": 8294829.6875, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 2681, "pixX": 107, "pixY": 50, "ToA": 8294829.6875, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 2685, "pixX": 106, "pixY": 55, "ToA": 8294829.6875, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 2687, "pixX": 108, "pixY": 52, "ToA": 8294829.6875, "ToT": 10, "TriggerNo": 1 }, { "pixel_index": 2688, "pixX": 107, "pixY": 51, "ToA": 8294829.6875, "ToT": 20, "TriggerNo": 1 }, { "pixel_index": 2689, "pixX": 104, "pixY": 51, "ToA": 8294829.6875, "ToT": 15, "TriggerNo": 1 }, { "pixel_index": 2690, "pixX": 108, "pixY": 53, "ToA": 8294829.6875, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 2692, "pixX": 105, "pixY": 54, "ToA": 8294829.6875, "ToT": 32, "TriggerNo": 1 }, { "pixel_index": 2686, "pixX": 104, "pixY": 54, "ToA": 8294829.6875, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 2694, "pixX": 106, "pixY": 50, "ToA": 8294829.6875, "ToT": 10, "TriggerNo": 1 }, { "pixel_index": 2695, "pixX": 104, "pixY": 52, "ToA": 8294829.6875, "ToT": 46, "TriggerNo": 1 }, { "pixel_index": 2702, "pixX": 105, "pixY": 51, "ToA": 8294829.6875, "ToT": 57, "TriggerNo": 1 }, { "pixel_index": 2703, "pixX": 106, "pixY": 51, "ToA": 8294829.6875, "ToT": 55, "TriggerNo": 1 }, { "pixel_index": 2712, "pixX": 104, "pixY": 53, "ToA": 8294829.6875, "ToT": 31, "TriggerNo": 1 }, { "pixel_index": 2693, "pixX": 105, "pixY": 50, "ToA": 8294829.6875, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 2675, "pixX": 107, "pixY": 55, "ToA": 8294831.25, "ToT": 3, "TriggerNo": 1 }, { "pixel_index": 2684, "pixX": 103, "pixY": 52, "ToA": 8294831.25, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 2674, "pixX": 103, "pixY": 53, "ToA": 8294831.25, "ToT": 3, "TriggerNo": 1 }, { "pixel_index": 2683, "pixX": 108, "pixY": 51, "ToA": 8294831.25, "ToT": 5, "TriggerNo": 1 }, { "pixel_index": 2673, "pixX": 105, "pixY": 55, "ToA": 8294832.8125, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 2680, "pixX": 103, "pixY": 51, "ToA": 8294832.8125, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2678, "pixX": 247, "pixY": 210, "ToA": 8295239.0625, "ToT": 27, "TriggerNo": 1 }, { "pixel_index": 2720, "pixX": 69, "pixY": 40, "ToA": 8303795.3125, "ToT": 49, "TriggerNo": 1 }, { "pixel_index": 2766, "pixX": 68, "pixY": 41, "ToA": 8303795.3125, "ToT": 200, "TriggerNo": 1 }, { "pixel_index": 2741, "pixX": 68, "pixY": 42, "ToA": 8303795.3125, "ToT": 81, "TriggerNo": 1 }, { "pixel_index": 2749, "pixX": 69, "pixY": 41, "ToA": 8303795.3125, "ToT": 164, "TriggerNo": 1 }, { "pixel_index": 2760, "pixX": 68, "pixY": 40, "ToA": 8303795.3125, "ToT": 218, "TriggerNo": 1 }, { "pixel_index": 2714, "pixX": 65, "pixY": 40, "ToA": 8303796.875, "ToT": 13, "TriggerNo": 1 }, { "pixel_index": 2746, "pixX": 66, "pixY": 41, "ToA": 8303796.875, "ToT": 18, "TriggerNo": 1 }, { "pixel_index": 2747, "pixX": 67, "pixY": 38, "ToA": 8303796.875, "ToT": 83, "TriggerNo": 1 }, { "pixel_index": 2752, "pixX": 66, "pixY": 42, "ToA": 8303796.875, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 2753, "pixX": 67, "pixY": 39, "ToA": 8303796.875, "ToT": 268, "TriggerNo": 1 }, { "pixel_index": 2725, "pixX": 65, "pixY": 41, "ToA": 8303796.875, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 2722, "pixX": 68, "pixY": 37, "ToA": 8303796.875, "ToT": 3, "TriggerNo": 1 }, { "pixel_index": 2761, "pixX": 69, "pixY": 38, "ToA": 8303796.875, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 2735, "pixX": 65, "pixY": 42, "ToA": 8303796.875, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2719, "pixX": 66, "pixY": 37, "ToA": 8303796.875, "ToT": 9, "TriggerNo": 1 }, { "pixel_index": 2755, "pixX": 69, "pixY": 42, "ToA": 8303796.875, "ToT": 227, "TriggerNo": 1 }, { "pixel_index": 2757, "pixX": 67, "pixY": 40, "ToA": 8303796.875, "ToT": 252, "TriggerNo": 1 }, { "pixel_index": 2743, "pixX": 68, "pixY": 39, "ToA": 8303796.875, "ToT": 80, "TriggerNo": 1 }, { "pixel_index": 2759, "pixX": 66, "pixY": 38, "ToA": 8303796.875, "ToT": 91, "TriggerNo": 1 }, { "pixel_index": 2730, "pixX": 69, "pixY": 43, "ToA": 8303796.875, "ToT": 53, "TriggerNo": 1 }, { "pixel_index": 2763, "pixX": 67, "pixY": 41, "ToA": 8303796.875, "ToT": 76, "TriggerNo": 1 }, { "pixel_index": 2734, "pixX": 68, "pixY": 38, "ToA": 8303796.875, "ToT": 19, "TriggerNo": 1 }, { "pixel_index": 2765, "pixX": 66, "pixY": 39, "ToA": 8303796.875, "ToT": 244, "TriggerNo": 1 }, { "pixel_index": 2768, "pixX": 69, "pixY": 39, "ToA": 8303796.875, "ToT": 15, "TriggerNo": 1 }, { "pixel_index": 2737, "pixX": 66, "pixY": 40, "ToA": 8303796.875, "ToT": 85, "TriggerNo": 1 }, { "pixel_index": 2738, "pixX": 67, "pixY": 37, "ToA": 8303796.875, "ToT": 8, "TriggerNo": 1 }, { "pixel_index": 2716, "pixX": 67, "pixY": 42, "ToA": 8303796.875, "ToT": 15, "TriggerNo": 1 }, { "pixel_index": 2706, "pixX": 68, "pixY": 43, "ToA": 8303796.875, "ToT": 12, "TriggerNo": 1 }, { "pixel_index": 2744, "pixX": 64, "pixY": 40, "ToA": 8303796.875, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2721, "pixX": 70, "pixY": 38, "ToA": 8303798.4375, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2727, "pixX": 67, "pixY": 43, "ToA": 8303798.4375, "ToT": 3, "TriggerNo": 1 }, { "pixel_index": 2726, "pixX": 65, "pixY": 37, "ToA": 8303798.4375, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2697, "pixX": 64, "pixY": 41, "ToA": 8303798.4375, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2728, "pixX": 71, "pixY": 42, "ToA": 8303798.4375, "ToT": 4, "TriggerNo": 1 }, { "pixel_index": 2729, "pixX": 67, "pixY": 36, "ToA": 8303798.4375, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2754, "pixX": 70, "pixY": 41, "ToA": 8303798.4375, "ToT": 23, "TriggerNo": 1 }, { "pixel_index": 2736, "pixX": 65, "pixY": 38, "ToA": 8303798.4375, "ToT": 12, "TriggerNo": 1 }, { "pixel_index": 2704, "pixX": 64, "pixY": 38, "ToA": 8303798.4375, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2745, "pixX": 65, "pixY": 39, "ToA": 8303798.4375, "ToT": 31, "TriggerNo": 1 }, { "pixel_index": 2756, "pixX": 69, "pixY": 37, "ToA": 8303798.4375, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2758, "pixX": 70, "pixY": 42, "ToA": 8303798.4375, "ToT": 77, "TriggerNo": 1 }, { "pixel_index": 2748, "pixX": 70, "pixY": 40, "ToA": 8303798.4375, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 2715, "pixX": 64, "pixY": 39, "ToA": 8303798.4375, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2708, "pixX": 66, "pixY": 36, "ToA": 8303800.0, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2707, "pixX": 71, "pixY": 40, "ToA": 8303800.0, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2731, "pixX": 70, "pixY": 39, "ToA": 8303800.0, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2718, "pixX": 71, "pixY": 41, "ToA": 8303800.0, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2705, "pixX": 66, "pixY": 43, "ToA": 8303800.0, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2742, "pixX": 71, "pixY": 38, "ToA": 8303800.0, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2764, "pixX": 70, "pixY": 43, "ToA": 8303800.0, "ToT": 12, "TriggerNo": 1 }, { "pixel_index": 2709, "pixX": 68, "pixY": 36, "ToA": 8303800.0, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2750, "pixX": 71, "pixY": 39, "ToA": 8303801.5625, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2699, "pixX": 68, "pixY": 44, "ToA": 8303801.5625, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2717, "pixX": 69, "pixY": 44, "ToA": 8303801.5625, "ToT": 3, "TriggerNo": 1 }, { "pixel_index": 2710, "pixX": 70, "pixY": 37, "ToA": 8303801.5625, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2751, "pixX": 69, "pixY": 36, "ToA": 8303801.5625, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2739, "pixX": 71, "pixY": 43, "ToA": 8303803.125, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2698, "pixX": 67, "pixY": 44, "ToA": 8303803.125, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2701, "pixX": 72, "pixY": 40, "ToA": 8303804.6875, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2700, "pixX": 70, "pixY": 44, "ToA": 8303804.6875, "ToT": 2, "TriggerNo": 1 }, { "pixel_index": 2711, "pixX": 72, "pixY": 41, "ToA": 8303804.6875, "ToT": 1, "TriggerNo": 1 }, { "pixel_index": 2778, "pixX": 74, "pixY": 143, "ToA": 8323098.4375, "ToT": 97, "TriggerNo": 1 }, { "pixel_index": 2769, "pixX": 74, "pixY": 142, "ToA": 8323100.0, "ToT": 78, "TriggerNo": 1 }, { "pixel_index": 2767, "pixX": 75, "pixY": 142, "ToA": 8323103.125, "ToT": 37, "TriggerNo": 1 }, { "pixel_index": 2762, "pixX": 75, "pixY": 143, "ToA": 8323103.125, "ToT": 14, "TriggerNo": 1 }, { "pixel_index": 2774, "pixX": 244, "pixY": 111, "ToA": 8328707.8125, "ToT": 25, "TriggerNo": 1 }, { "pixel_index": 2776, "pixX": 245, "pixY": 107, "ToA": 8328709.375, "ToT": 16, "TriggerNo": 1 }, { "pixel_index": 2772, "pixX": 242, "pixY": 110, "ToA": 8328717.1875, "ToT": 6, "TriggerNo": 1 }, { "pixel_index": 2770, "pixX": 247, "pixY": 109, "ToA": 8328721.875, "ToT": 7, "TriggerNo": 1 }, { "pixel_index": 2775, "pixX": 242, "pixY": 107, "ToA": 8328729.6875, "ToT": 3, "TriggerNo": 1 }, { "pixel_index": 2773, "pixX": 246, "pixY": 107, "ToA": 8328734.375, "ToT": 3, "TriggerNo": 1 }, { "pixel_index": 2771, "pixX": 245, "pixY": 112, "ToA": 8328743.75, "ToT": 2, "TriggerNo": 1 },
                { "pixel_index": 2786, "pixX": 182, "pixY": 86, "ToA": -31.25, "ToT": 3, "TriggerNo": 2 }, { "pixel_index": 2819, "pixX": 184, "pixY": 84, "ToA": -29.6875, "ToT": 71, "TriggerNo": 2 }, { "pixel_index": 2809, "pixX": 185, "pixY": 85, "ToA": -29.6875, "ToT": 90, "TriggerNo": 2 }, { "pixel_index": 2831, "pixX": 184, "pixY": 86, "ToA": -29.6875, "ToT": 260, "TriggerNo": 2 }, { "pixel_index": 2794, "pixX": 185, "pixY": 84, "ToA": -29.6875, "ToT": 21, "TriggerNo": 2 }, { "pixel_index": 2814, "pixX": 185, "pixY": 86, "ToA": -29.6875, "ToT": 41, "TriggerNo": 2 }, { "pixel_index": 2821, "pixX": 184, "pixY": 85, "ToA": -29.6875, "ToT": 328, "TriggerNo": 2 }, { "pixel_index": 2817, "pixX": 183, "pixY": 86, "ToA": -28.125, "ToT": 49, "TriggerNo": 2 }, { "pixel_index": 2792, "pixX": 183, "pixY": 84, "ToA": -28.125, "ToT": 18, "TriggerNo": 2 }, { "pixel_index": 2800, "pixX": 184, "pixY": 87, "ToA": -28.125, "ToT": 35, "TriggerNo": 2 }, { "pixel_index": 2812, "pixX": 183, "pixY": 85, "ToA": -28.125, "ToT": 67, "TriggerNo": 2 }, { "pixel_index": 2787, "pixX": 186, "pixY": 86, "ToA": -25.0, "ToT": 3, "TriggerNo": 2 }, { "pixel_index": 2785, "pixX": 185, "pixY": 87, "ToA": -21.875, "ToT": 7, "TriggerNo": 2 }, { "pixel_index": 2793, "pixX": 186, "pixY": 85, "ToA": -21.875, "ToT": 8, "TriggerNo": 2 }, { "pixel_index": 2799, "pixX": 183, "pixY": 87, "ToA": -21.875, "ToT": 9, "TriggerNo": 2 }, { "pixel_index": 2806, "pixX": 182, "pixY": 85, "ToA": -17.1875, "ToT": 7, "TriggerNo": 2 }, { "pixel_index": 2788, "pixX": 184, "pixY": 83, "ToA": -12.5, "ToT": 5, "TriggerNo": 2 }, { "pixel_index": 2796, "pixX": 209, "pixY": 86, "ToA": 2978.125, "ToT": 10, "TriggerNo": 2 }, { "pixel_index": 2802, "pixX": 209, "pixY": 87, "ToA": 2978.125, "ToT": 33, "TriggerNo": 2 }, { "pixel_index": 2818, "pixX": 67, "pixY": 163, "ToA": 6571.875, "ToT": 112, "TriggerNo": 2 }, { "pixel_index": 2807, "pixX": 68, "pixY": 163, "ToA": 6582.8125, "ToT": 20, "TriggerNo": 2 }, { "pixel_index": 2813, "pixX": 67, "pixY": 162, "ToA": 6587.5, "ToT": 9, "TriggerNo": 2 }, { "pixel_index": 2805, "pixX": 67, "pixY": 164, "ToA": 6600.0, "ToT": 5, "TriggerNo": 2 }, { "pixel_index": 2808, "pixX": 66, "pixY": 163, "ToA": 6610.9375, "ToT": 3, "TriggerNo": 2 }, { "pixel_index": 2834, "pixX": 220, "pixY": 175, "ToA": 15478.125, "ToT": 18, "TriggerNo": 2 }, { "pixel_index": 2843, "pixX": 221, "pixY": 175, "ToA": 15478.125, "ToT": 45, "TriggerNo": 2 }, { "pixel_index": 2846, "pixX": 220, "pixY": 174, "ToA": 15478.125, "ToT": 61, "TriggerNo": 2 }, { "pixel_index": 2829, "pixX": 221, "pixY": 174, "ToA": 15487.5, "ToT": 1, "TriggerNo": 2 }, { "pixel_index": 2827, "pixX": 222, "pixY": 175, "ToA": 15487.5, "ToT": 9, "TriggerNo": 2 }, { "pixel_index": 2825, "pixX": 221, "pixY": 176, "ToA": 15489.0625, "ToT": 8, "TriggerNo": 2 }, { "pixel_index": 2874, "pixX": 209, "pixY": 231, "ToA": 15507.8125, "ToT": 344, "TriggerNo": 2 }, { "pixel_index": 2873, "pixX": 208, "pixY": 233, "ToA": 15507.8125, "ToT": 354, "TriggerNo": 2 }, { "pixel_index": 2872, "pixX": 207, "pixY": 234, "ToA": 15507.8125, "ToT": 94, "TriggerNo": 2 }, { "pixel_index": 2848, "pixX": 209, "pixY": 234, "ToA": 15507.8125, "ToT": 92, "TriggerNo": 2 }, { "pixel_index": 2869, "pixX": 207, "pixY": 233, "ToA": 15507.8125, "ToT": 68, "TriggerNo": 2 }, { "pixel_index": 2868, "pixX": 209, "pixY": 230, "ToA": 15507.8125, "ToT": 291, "TriggerNo": 2 }, { "pixel_index": 2858, "pixX": 209, "pixY": 233, "ToA": 15507.8125, "ToT": 108, "TriggerNo": 2 }, { "pixel_index": 2836, "pixX": 209, "pixY": 235, "ToA": 15507.8125, "ToT": 82, "TriggerNo": 2 }, { "pixel_index": 2866, "pixX": 208, "pixY": 232, "ToA": 15507.8125, "ToT": 215, "TriggerNo": 2 }, { "pixel_index": 2887, "pixX": 209, "pixY": 232, "ToA": 15507.8125, "ToT": 308, "TriggerNo": 2 }, { "pixel_index": 2878, "pixX": 207, "pixY": 235, "ToA": 15507.8125, "ToT": 132, "TriggerNo": 2 }, { "pixel_index": 2879, "pixX": 208, "pixY": 234, "ToA": 15507.8125, "ToT": 456, "TriggerNo": 2 }, { "pixel_index": 2883, "pixX": 208, "pixY": 235, "ToA": 15507.8125, "ToT": 413, "TriggerNo": 2 }, { "pixel_index": 2861, "pixX": 208, "pixY": 231, "ToA": 15509.375, "ToT": 61, "TriggerNo": 2 }, { "pixel_index": 2857, "pixX": 208, "pixY": 236, "ToA": 15509.375, "ToT": 83, "TriggerNo": 2 }, { "pixel_index": 2847, "pixX": 207, "pixY": 236, "ToA": 15510.9375, "ToT": 34, "TriggerNo": 2 }, { "pixel_index": 2856, "pixX": 210, "pixY": 231, "ToA": 15512.5, "ToT": 49, "TriggerNo": 2 }, { "pixel_index": 2849, "pixX": 210, "pixY": 230, "ToA": 15512.5, "ToT": 34, "TriggerNo": 2 }, { "pixel_index": 2852, "pixX": 210, "pixY": 232, "ToA": 15512.5, "ToT": 43, "TriggerNo": 2 }, { "pixel_index": 2845, "pixX": 209, "pixY": 236, "ToA": 15514.0625, "ToT": 24, "TriggerNo": 2 }, { "pixel_index": 2850, "pixX": 208, "pixY": 230, "ToA": 15514.0625, "ToT": 35, "TriggerNo": 2 }, { "pixel_index": 2863, "pixX": 207, "pixY": 232, "ToA": 15514.0625, "ToT": 35, "TriggerNo": 2 }, { "pixel_index": 2839, "pixX": 209, "pixY": 229, "ToA": 15514.0625, "ToT": 31, "TriggerNo": 2 }, { "pixel_index": 2855, "pixX": 206, "pixY": 235, "ToA": 15517.1875, "ToT": 15, "TriggerNo": 2 }, { "pixel_index": 2862, "pixX": 210, "pixY": 233, "ToA": 15518.75, "ToT": 22, "TriggerNo": 2 }, { "pixel_index": 2838, "pixX": 210, "pixY": 234, "ToA": 15520.3125, "ToT": 14, "TriggerNo": 2 }, { "pixel_index": 2851, "pixX": 206, "pixY": 234, "ToA": 15520.3125, "ToT": 14, "TriggerNo": 2 }, { "pixel_index": 2840, "pixX": 206, "pixY": 233, "ToA": 15521.875, "ToT": 10, "TriggerNo": 2 }, { "pixel_index": 2844, "pixX": 210, "pixY": 235, "ToA": 15521.875, "ToT": 11, "TriggerNo": 2 }, { "pixel_index": 2842, "pixX": 207, "pixY": 231, "ToA": 15523.4375, "ToT": 11, "TriggerNo": 2 }, { "pixel_index": 2835, "pixX": 208, "pixY": 237, "ToA": 15523.4375, "ToT": 9, "TriggerNo": 2 }, { "pixel_index": 2828, "pixX": 208, "pixY": 229, "ToA": 15525.0, "ToT": 9, "TriggerNo": 2 }, { "pixel_index": 2841, "pixX": 210, "pixY": 229, "ToA": 15526.5625, "ToT": 9, "TriggerNo": 2 }, { "pixel_index": 2837, "pixX": 206, "pixY": 236, "ToA": 15531.25, "ToT": 6, "TriggerNo": 2 }, { "pixel_index": 2826, "pixX": 206, "pixY": 232, "ToA": 15539.0625, "ToT": 5, "TriggerNo": 2 }, { "pixel_index": 2832, "pixX": 211, "pixY": 231, "ToA": 15540.625, "ToT": 3, "TriggerNo": 2 }, { "pixel_index": 2823, "pixX": 210, "pixY": 236, "ToA": 15542.1875, "ToT": 4, "TriggerNo": 2 }, { "pixel_index": 2822, "pixX": 207, "pixY": 237, "ToA": 15543.75, "ToT": 4, "TriggerNo": 2 }, { "pixel_index": 2830, "pixX": 211, "pixY": 232, "ToA": 15545.3125, "ToT": 3, "TriggerNo": 2 }, { "pixel_index": 2824, "pixX": 209, "pixY": 237, "ToA": 15550.0, "ToT": 3, "TriggerNo": 2 }, { "pixel_index": 2833, "pixX": 207, "pixY": 230, "ToA": 15553.125, "ToT": 3, "TriggerNo": 2 }, { "pixel_index": 2871, "pixX": 192, "pixY": 215, "ToA": 26210.9375, "ToT": 62, "TriggerNo": 2 }, { "pixel_index": 2885, "pixX": 192, "pixY": 214, "ToA": 26210.9375, "ToT": 73, "TriggerNo": 2 }, { "pixel_index": 2865, "pixX": 192, "pixY": 213, "ToA": 26214.0625, "ToT": 21, "TriggerNo": 2 }, { "pixel_index": 2853, "pixX": 193, "pixY": 215, "ToA": 26214.0625, "ToT": 6, "TriggerNo": 2 }, { "pixel_index": 2860, "pixX": 192, "pixY": 212, "ToA": 26218.75, "ToT": 16, "TriggerNo": 2 }, { "pixel_index": 2875, "pixX": 193, "pixY": 212, "ToA": 26218.75, "ToT": 32, "TriggerNo": 2 }, { "pixel_index": 2881, "pixX": 193, "pixY": 213, "ToA": 26226.5625, "ToT": 7, "TriggerNo": 2 }, { "pixel_index": 2859, "pixX": 194, "pixY": 213, "ToA": 26229.6875, "ToT": 17, "TriggerNo": 2 }, { "pixel_index": 2854, "pixX": 194, "pixY": 212, "ToA": 26242.1875, "ToT": 3, "TriggerNo": 2 }, { "pixel_index": 2864, "pixX": 194, "pixY": 214, "ToA": 26268.75, "ToT": 2, "TriggerNo": 2 }, { "pixel_index": 2870, "pixX": 96, "pixY": 237, "ToA": 31578.125, "ToT": 34, "TriggerNo": 2 }, { "pixel_index": 2867, "pixX": 95, "pixY": 237, "ToA": 31590.625, "ToT": 4, "TriggerNo": 2 }, { "pixel_index": 2890, "pixX": 148, "pixY": 62, "ToA": 35178.125, "ToT": 215, "TriggerNo": 2 }, { "pixel_index": 2891, "pixX": 147, "pixY": 63, "ToA": 35178.125, "ToT": 47, "TriggerNo": 2 }, { "pixel_index": 2892, "pixX": 147, "pixY": 62, "ToA": 35178.125, "ToT": 316, "TriggerNo": 2 }, { "pixel_index": 2886, "pixX": 146, "pixY": 62, "ToA": 35178.125, "ToT": 36, "TriggerNo": 2 }, { "pixel_index": 2888, "pixX": 148, "pixY": 63, "ToA": 35178.125, "ToT": 25, "TriggerNo": 2 }, { "pixel_index": 2889, "pixX": 147, "pixY": 61, "ToA": 35178.125, "ToT": 43, "TriggerNo": 2 }, { "pixel_index": 2884, "pixX": 148, "pixY": 61, "ToA": 35179.6875, "ToT": 26, "TriggerNo": 2 }, { "pixel_index": 2880, "pixX": 149, "pixY": 62, "ToA": 35182.8125, "ToT": 19, "TriggerNo": 2 }, { "pixel_index": 2877, "pixX": 146, "pixY": 61, "ToA": 35190.625, "ToT": 8, "TriggerNo": 2 }, { "pixel_index": 2882, "pixX": 146, "pixY": 63, "ToA": 35190.625, "ToT": 9, "TriggerNo": 2 }, { "pixel_index": 2876, "pixX": 149, "pixY": 61, "ToA": 35214.0625, "ToT": 3, "TriggerNo": 2 }, { "pixel_index": 2798, "pixX": 246, "pixY": 110, "ToA": 136706.25, "ToT": 90, "TriggerNo": 2 }, { "pixel_index": 2791, "pixX": 246, "pixY": 109, "ToA": 136706.25, "ToT": 51, "TriggerNo": 2 }, { "pixel_index": 2810, "pixX": 243, "pixY": 109, "ToA": 136707.8125, "ToT": 135, "TriggerNo": 2 }, { "pixel_index": 2803, "pixX": 244, "pixY": 108, "ToA": 136707.8125, "ToT": 245, "TriggerNo": 2 }, { "pixel_index": 2801, "pixX": 243, "pixY": 108, "ToA": 136707.8125, "ToT": 130, "TriggerNo": 2 }, { "pixel_index": 2797, "pixX": 244, "pixY": 110, "ToA": 136707.8125, "ToT": 102, "TriggerNo": 2 }, { "pixel_index": 2820, "pixX": 244, "pixY": 109, "ToA": 136707.8125, "ToT": 389, "TriggerNo": 2 }, { "pixel_index": 2790, "pixX": 245, "pixY": 108, "ToA": 136707.8125, "ToT": 73, "TriggerNo": 2 }, { "pixel_index": 2816, "pixX": 245, "pixY": 110, "ToA": 136707.8125, "ToT": 323, "TriggerNo": 2 }, { "pixel_index": 2811, "pixX": 245, "pixY": 109, "ToA": 136707.8125, "ToT": 301, "TriggerNo": 2 }, { "pixel_index": 2781, "pixX": 245, "pixY": 111, "ToA": 136707.8125, "ToT": 46, "TriggerNo": 2 }, { "pixel_index": 2784, "pixX": 246, "pixY": 108, "ToA": 136707.8125, "ToT": 18, "TriggerNo": 2 }, { "pixel_index": 2789, "pixX": 242, "pixY": 108, "ToA": 136709.375, "ToT": 15, "TriggerNo": 2 }, { "pixel_index": 2795, "pixX": 242, "pixY": 109, "ToA": 136709.375, "ToT": 15, "TriggerNo": 2 }, { "pixel_index": 2815, "pixX": 243, "pixY": 110, "ToA": 136709.375, "ToT": 39, "TriggerNo": 2 }, { "pixel_index": 2783, "pixX": 244, "pixY": 107, "ToA": 136709.375, "ToT": 34, "TriggerNo": 2 }, { "pixel_index": 2782, "pixX": 243, "pixY": 107, "ToA": 136710.9375, "ToT": 20, "TriggerNo": 2 }, { "pixel_index": 2780, "pixX": 243, "pixY": 111, "ToA": 136717.1875, "ToT": 7, "TriggerNo": 2 }, { "pixel_index": 2804, "pixX": 246, "pixY": 111, "ToA": 136718.75, "ToT": 9, "TriggerNo": 2 }, { "pixel_index": 2779, "pixX": 247, "pixY": 110, "ToA": 136718.75, "ToT": 10, "TriggerNo": 2 }, { "pixel_index": 2895, "pixX": 47, "pixY": 227, "ToA": 1220123.4375, "ToT": 19, "TriggerNo": 2 }, { "pixel_index": 2898, "pixX": 49, "pixY": 228, "ToA": 1220128.125, "ToT": 14, "TriggerNo": 2 }, { "pixel_index": 2893, "pixX": 48, "pixY": 228, "ToA": 1220128.125, "ToT": 12, "TriggerNo": 2 }, { "pixel_index": 2897, "pixX": 50, "pixY": 230, "ToA": 1220129.6875, "ToT": 22, "TriggerNo": 2 }, { "pixel_index": 2894, "pixX": 50, "pixY": 229, "ToA": 1220129.6875, "ToT": 16, "TriggerNo": 2 }, { "pixel_index": 2896, "pixX": 48, "pixY": 227, "ToA": 1220132.8125, "ToT": 6, "TriggerNo": 2 }, { "pixel_index": 2899, "pixX": 49, "pixY": 229, "ToA": 1228753.125, "ToT": 18, "TriggerNo": 2 }, { "pixel_index": 2908, "pixX": 102, "pixY": 203, "ToA": 3093351.5625, "ToT": 17, "TriggerNo": 2 }, { "pixel_index": 2911, "pixX": 102, "pixY": 202, "ToA": 3093351.5625, "ToT": 77, "TriggerNo": 2 }, { "pixel_index": 2909, "pixX": 101, "pixY": 202, "ToA": 3093356.25, "ToT": 10, "TriggerNo": 2 }, { "pixel_index": 2912, "pixX": 100, "pixY": 200, "ToA": 3093356.25, "ToT": 11, "TriggerNo": 2 }, { "pixel_index": 2901, "pixX": 100, "pixY": 201, "ToA": 3093356.25, "ToT": 9, "TriggerNo": 2 }, { "pixel_index": 2906, "pixX": 101, "pixY": 201, "ToA": 3093357.8125, "ToT": 9, "TriggerNo": 2 }, { "pixel_index": 2902, "pixX": 99, "pixY": 199, "ToA": 3093362.5, "ToT": 7, "TriggerNo": 2 }, { "pixel_index": 2904, "pixX": 103, "pixY": 203, "ToA": 3093362.5, "ToT": 11, "TriggerNo": 2 }, { "pixel_index": 2910, "pixX": 104, "pixY": 204, "ToA": 3093364.0625, "ToT": 16, "TriggerNo": 2 }, { "pixel_index": 2903, "pixX": 103, "pixY": 204, "ToA": 3093367.1875, "ToT": 8, "TriggerNo": 2 }, { "pixel_index": 2905, "pixX": 105, "pixY": 205, "ToA": 3093370.3125, "ToT": 9, "TriggerNo": 2 }, { "pixel_index": 2907, "pixX": 105, "pixY": 206, "ToA": 3093373.4375, "ToT": 15, "TriggerNo": 2 }, { "pixel_index": 2900, "pixX": 106, "pixY": 207, "ToA": 3093389.0625, "ToT": 5, "TriggerNo": 2 }] };
    })(mockupdata = tpx3.mockupdata || (tpx3.mockupdata = {}));
})(tpx3 || (tpx3 = {}));
var tpx3;
(function (tpx3) {
    var datafilter;
    (function (datafilter) {
        /**
        Class representing a single filter on the data. Specifies the attribute to be filtered, its desired minimum value and maximum value.
        */
        var DataFilter = (function () {
            /** Creates a new filter for the data. Specifies the desired quantity on which to filter, it's minimum and maximum values. */
            function DataFilter(valueName, minimum, maximum) {
                this._valueName = valueName;
                this._minimum = minimum;
                this._maximum = maximum;
            }
            /** Checks if the inputted value passes this filter's criteria */
            DataFilter.prototype.passed = function (value) {
                return (value >= this._minimum && value < this._maximum) ? true : false;
            };
            return DataFilter;
        }());
        datafilter.DataFilter = DataFilter;
    })(datafilter = tpx3.datafilter || (tpx3.datafilter = {}));
})(tpx3 || (tpx3 = {}));
/// <reference path="mockup_data.ts" />
/// <reference path="baseobject.ts" />
/// <reference path="data-filter.ts" />
var mockup_pixels = tpx3.mockupdata.mockup_pixels;
var DataFilter = tpx3.datafilter.DataFilter;
var tpx3;
(function (tpx3) {
    var dataaccess;
    (function (dataaccess) {
        /**
        Class responsible for handling the data. Receiving from server, providing to other classes, filtering, etc.
        */
        var DataAccess = (function (_super) {
            __extends(DataAccess, _super);
            /** create DataAccess object and bind it to a data source (server address) */
            function DataAccess(serverAddress) {
                if (serverAddress === void 0) { serverAddress = ""; }
                _super.call(this, "DataAccess-" + serverAddress);
                /** allowed tags in the user-specified filter expression */
                this._allowedFilterTags = [];
                /** Marks how many sources were queried for the data within one accumulated call */
                this._calledSources = 0;
                /** Handle of currently running request */
                this._currentRequest = null;
                console.log("Server address is " + serverAddress);
                if (serverAddress) {
                    this._serverAddress = "http://" + serverAddress;
                }
                else {
                    this._serverAddress = "http://localhost:3000";
                }
                this._data = [];
                this._offsets = [];
                this._filters = [];
                this._startIndex = [];
                this._endIndex = [];
                this._ownFilterString = "";
                this._rootFiles = [];
                this._allowedFilterTags = ["TriggerNo", "ToA", "ToT", "fToA", "sToA", "+", "-", "*", "/", "%", "==", "!=", "<", ">", ">=", "<=", "&&", "||", "(", ")"];
            }
            /**
            Queries the server with new request for new set of data.
            */
            DataAccess.prototype.getNewData = function (startTime, timeSlice, source, callback) {
                this._startTime = startTime;
                this._timeSlice = timeSlice;
                if (!callback) {
                    return mockup_pixels;
                }
                if (this._serverAddress === "http://test:3000") {
                    this.setData("mockup", mockup_pixels);
                    callback(mockup_pixels);
                    return;
                }
                if (source === "") {
                    this.callForAllData(startTime, timeSlice, callback);
                    return;
                }
                else {
                    this.callForOneData(startTime, timeSlice, source, callback);
                }
            };
            /** Asynchronous call for data from single (rootFile) source of data */
            DataAccess.prototype.callForOneData = function (startTime, timeSlice, source, callback) {
                var daObj = this;
                //console.log("Ajax call for data from " + source + " started");
                var offset = 0;
                if (this._offsets[source] != undefined) {
                    offset = this._offsets[source];
                }
                offset = +offset;
                startTime = +startTime;
                startTime -= offset;
                this._currentRequest = $.ajax({
                    url: this._serverAddress + "/data/interval/" + startTime + "/" + timeSlice + "/" + source
                }).done(function (data) {
                    //console.log("Ajax call for data from " + source + " done");
                    daObj.setData(source, JSON.parse(data));
                    daObj.checkDataForErrors(source);
                    callback(daObj.getData(source));
                }).fail(function (err) {
                    //console.log("Ajax call for data from " + source + " error");
                    daObj.setData(source, null);
                    daObj.addError(source + ": There was an error contacting the server. No data received.");
                    callback(null);
                });
            };
            /** Calls periodically for new data from all current source files */
            DataAccess.prototype.callForAllData = function (startTime, timeSlice, callback) {
                var daObj = this;
                this._calledSources = 0;
                //console.log("Ajax call for data from all sources started");
                for (var _i = 0, _a = this._rootFiles; _i < _a.length; _i++) {
                    var oneFile = _a[_i];
                    this.callForOneData(startTime, timeSlice, oneFile, function () {
                        daObj._calledSources++;
                        if (daObj._calledSources == daObj._rootFiles.length) {
                            callback();
                        }
                    });
                }
            };
            /** Queries the server with request for list of all root files currently stored on the server */
            DataAccess.prototype.getRootFilesList = function (folders, callback) {
                console.log("Ajax call for file list started");
                var daObj = this;
                this._currentRequest = $.ajax({
                    url: this._serverAddress + "/rootfiles/files/" + folders
                }).done(function (data) {
                    console.log("Ajax call for file list done");
                    data = JSON.parse(data);
                    callback(data);
                }).fail(function (err) {
                    console.log("Ajax call for file list error");
                    daObj.addError("There was an error contacting the server. No data received.");
                    callback(null);
                });
            };
            DataAccess.prototype.callForFolderList = function (folder, callback) {
                if (folder === void 0) { folder = ""; }
                this._currentRequest = $.ajax({
                    url: this._serverAddress + "/rootfiles/folder-list/" + folder
                }).done(function (data) {
                    console.log("Ajax call for ascii folder list done");
                    callback(JSON.parse(data));
                }).fail(function (err) {
                    console.log("Ajax call for ascii folder list error");
                    callback(null);
                });
            };
            /** Specifies current root files on the server, from which to retrieve the data */
            DataAccess.prototype.setRootFile = function (filename) {
                if (this._rootFiles.indexOf(filename) == -1) {
                    this._rootFiles.push(filename);
                }
            };
            /** Kills the currently running request on the server */
            DataAccess.prototype.abortCurrentRequest = function () {
                this._currentRequest.abort();
            };
            DataAccess.prototype.getCurrentRequest = function () {
                return this._currentRequest;
            };
            DataAccess.prototype.removeRootFile = function (filename) {
                var index = this._rootFiles.indexOf(filename);
                if (index > -1) {
                    this._rootFiles.splice(index, 1);
                }
            };
            /** Saves the received data into the class attribute */
            DataAccess.prototype.setData = function (source, data) {
                if (!data) {
                    return;
                }
                this.setRootFile(source);
                this._data[source] = data;
                this._startIndex[source] = 0;
                this._endIndex[source] = data.Size;
            };
            DataAccess.prototype.hasFilter = function () {
                if (this._filters.length > 0 || this._ownFilterString != "") {
                    return true;
                }
                else {
                    return false;
                }
            };
            /** Checks the specified data for any possible errors or inaccuracies and writes down errors/warnings */
            DataAccess.prototype.checkDataForErrors = function (sourceName) {
                var multipleTriggers = false;
                var negativeValues = 0;
                if (this._data[sourceName].TriggerCount > 1) {
                    multipleTriggers = true;
                }
                for (var i = 0; i < this._data[sourceName].Size; i++) {
                    if (this._data[sourceName].Pixels[i].ToA < 0) {
                        negativeValues++;
                    }
                }
                if (multipleTriggers) {
                    var currTime = this.getCurrentTime();
                    this.addWarning(currTime + " Selected interval contains data from multiple triggers. Possible inaccuracies in aquisition time.");
                }
                if (negativeValues > 0) {
                    var currTime = this.getCurrentTime();
                    this.addWarning(currTime + " There are " + negativeValues + " pixels with negative ToA values. Possible error in sensor. Hover pixels to get more info.");
                }
            };
            /** Gets the correct data, checks for possible errors, and returns them. */
            DataAccess.prototype.getData = function (source, original) {
                if (source === void 0) { source = ""; }
                if (original === void 0) { original = false; }
                if (source == "") {
                    source = this._rootFiles[0];
                }
                var data;
                if (original) {
                    data = this._data[source];
                }
                else {
                    data = this.getFilteredData(source);
                }
                if (!data) {
                    return null;
                }
                if (data.Offset == undefined) {
                    data.Offset = 0;
                }
                if (this._offsets[source] != undefined) {
                    data.Offset = this._offsets[source];
                }
                return data;
            };
            DataAccess.prototype.getStartTime = function () {
                return this._startTime;
            };
            DataAccess.prototype.setStartTime = function (newStartTime) {
                this._startTime = newStartTime;
            };
            DataAccess.prototype.getTimeSlice = function () {
                return this._timeSlice;
            };
            DataAccess.prototype.setTimeSlice = function (newTimeSlice) {
                this._timeSlice = newTimeSlice;
            };
            DataAccess.prototype.getAllowedFilterTags = function () {
                return this._allowedFilterTags;
            };
            /** Resets all the filters that have been set up before */
            DataAccess.prototype.reset = function () {
                this._filters = [];
                this._filters.length = 0;
                this._ownFilterString = "";
            };
            /** Sets the interval from getting the data from the _data attribute*/
            DataAccess.prototype.setInterval = function (startIndex, endIndex, source) {
                if (!this._data[source]) {
                    console.log("No data for given source.");
                    return;
                }
                if (startIndex < 0) {
                    startIndex = 0;
                }
                if (endIndex > this._data[source].Size) {
                    endIndex = this._data[source].Size;
                }
                this._startIndex[source] = startIndex;
                this._endIndex[source] = endIndex;
            };
            /**
            Makes sure the specified string is valid expression based on which the data can be filtered. Allows usage only of pre-defined allowed tags.
            Sets the filtering string to filter the data, if it is correct.
            */
            DataAccess.prototype.ownFilter = function (filterString) {
                var termsArray = filterString.split(/\s\s*/);
                for (var i = 0; i < termsArray.length; i++) {
                    termsArray[i] = termsArray[i].replace(new RegExp('[()]', 'g'), "");
                    if (this._allowedFilterTags.indexOf(termsArray[i]) == -1 && isNaN(+termsArray[i])) {
                        this.addError("Term \"" + termsArray[i] + "\" is not allowed for filtering.");
                        return false;
                    }
                }
                var pixel = this._data[this._rootFiles[0]].Pixels[100];
                var ToA = pixel.ToA;
                var TriggerNo = pixel.TriggerNo;
                var ToT = pixel.ToT;
                var sToA = pixel.sToA;
                var fToA = pixel.fToA;
                try {
                    eval(filterString);
                    this._ownFilterString = filterString;
                }
                catch (e) {
                    console.log(e.message);
                    return false;
                }
                return true;
            };
            /**
            Adds a new filter to the collection of filters, based on which the data will be filtered, before they are returned.
            */
            DataAccess.prototype.addFilter = function (what, valueFrom, valueTo) {
                if (isNaN(+valueFrom) || isNaN(+valueTo) || (valueFrom.toString() === "" && valueTo.toString() === "")) {
                    console.log("Values are not correctly set, filtering won't occur");
                    return;
                }
                if (valueFrom.toString() == "") {
                    valueFrom = -Infinity;
                }
                if (valueTo.toString() == "") {
                    valueTo = Infinity;
                }
                this._filters[what] = new DataFilter(what, valueFrom, valueTo);
                this._filters.length++;
            };
            /**
            Getting and returning the current data, taking all the filtering that should take place into consideration.
            */
            DataAccess.prototype.getFilteredData = function (source) {
                if (!this._data[source]) {
                    return null;
                }
                if (this._data[source].Pixels.length == 0) {
                    return this._data[source];
                }
                var newData = {};
                newData.Pixels = [];
                newData.Size = 0;
                newData.SumEnergy = 0;
                newData.TriggerCount = 0;
                newData.StartTime = this._data[source].StartTime;
                var firstTrigger = this._data[source].Pixels[0].TriggerNo;
                var lastTrigger;
                var add;
                for (var i = this._startIndex[source]; i < this._endIndex[source]; i++) {
                    add = true;
                    for (var j in this._filters) {
                        if (this._data[source].Pixels[i][this._filters[j]._valueName] < this._filters[j]._minimum || this._data[source].Pixels[i][this._filters[j]._valueName] >= this._filters[j]._maximum) {
                            add = false;
                        }
                    }
                    if (this._ownFilterString !== "") {
                        var pixel = this._data[source].Pixels[i];
                        var ToA = pixel.ToA;
                        var TriggerNo = pixel.TriggerNo;
                        var ToT = pixel.ToT;
                        var sToA = pixel.sToA;
                        var fToA = pixel.fToA;
                        if (!eval(this._ownFilterString)) {
                            add = false;
                        }
                    }
                    if (add) {
                        newData.Pixels.push(this._data[source].Pixels[i]);
                        newData.Size++;
                        newData.SumEnergy += this._data[source].Pixels[i].ToT;
                        lastTrigger = this._data[source].Pixels[i].TriggerNo;
                    }
                }
                newData.TriggerCount = lastTrigger - firstTrigger + 1;
                return newData;
            };
            DataAccess.prototype.setOffset = function (offset, source) {
                if (offset === void 0) { offset = 0; }
                if (source === void 0) { source = ""; }
                if (!offset || isNaN(offset)) {
                    offset = 0;
                    this.addWarning("Wrong offset value, setting 0.");
                }
                console.log("Setting offset of " + offset);
                //offset = Tools.secondsToNano(offset);
                this._offsets[source] = +offset;
            };
            return DataAccess;
        }(BaseObject));
        dataaccess.DataAccess = DataAccess;
    })(dataaccess = tpx3.dataaccess || (tpx3.dataaccess = {}));
})(tpx3 || (tpx3 = {}));
var tpx3;
(function (tpx3) {
    var tools;
    (function (tools) {
        /** Class with some useful tools and utilities to be used across the program */
        var TPX3Tools = (function () {
            function TPX3Tools() {
            }
            /** Converts number to scientific exponential form (e) with set precision */
            TPX3Tools.toExponentialPrecision = function (value, precision) {
                if (value === 0.0) {
                    return value.toString();
                }
                var correctNumber = value.toExponential().toString();
                var numArray = correctNumber.split(".");
                var intPart = numArray[0];
                if (numArray.length <= 1) {
                    return numArray[0];
                }
                numArray = numArray[1].split("e");
                var ePart = numArray[1];
                var decPart = numArray[0].substr(0, precision);
                correctNumber = intPart + "." + decPart + "e" + ePart;
                return correctNumber;
            };
            /** Trivial */
            TPX3Tools.nanoToSeconds = function (nanoValue) {
                return nanoValue / 1000000000;
            };
            /** Trivial */
            TPX3Tools.secondsToNano = function (secValue) {
                return secValue * 1000000000;
            };
            /** Trivial */
            TPX3Tools.secondsToMilli = function (secValue) {
                return secValue * 1000;
            };
            return TPX3Tools;
        }());
        tools.TPX3Tools = TPX3Tools;
    })(tools = tpx3.tools || (tpx3.tools = {}));
})(tpx3 || (tpx3 = {}));
/// <reference path="data-access.ts" />
/// <reference path="canvas-visualization.ts" />
/// <reference path="tools.ts" />
var CanvasVisualization = tpx3.canvasvisualization.CanvasVisualization;
var DataAccess = tpx3.dataaccess.DataAccess;
var Tools = tpx3.tools.TPX3Tools;
var tpx3;
(function (tpx3) {
    var timeline;
    (function (timeline) {
        /**
        Class representing the interactive 1D timeline visualization.
        */
        var Timeline = (function (_super) {
            __extends(Timeline, _super);
            /** Creates new timeline, attaches all the dependencies, as DataAccess for getting and filtering the pixel data and parent Sensor object for updating the other components of its visualization */
            function Timeline(parent, name, dataAccess, globalControls, startTime, timeSlice, binSize) {
                if (binSize === void 0) { binSize = 0; }
                _super.call(this, name);
                /** Starting indexes of pixels for each of the displayed bins */
                this._pixelIndexes = [];
                this._globalControls = globalControls;
                this.createOverlayCanvas();
                this._parent = parent;
                this._maxBin = 1;
                this._scaleName = this._name + "-scale";
                this._scaleNameVertical = this._name + "-scale-vertical";
                this._timeSlice = timeSlice;
                this._startTime = startTime;
                this._dataAccess = dataAccess;
                this.initParameters();
                var data = dataAccess.getData();
                if (data) {
                }
                else {
                    this.drawNoData();
                }
                this._leftBin = 0;
                this._rightBin = this._numOfBins - 1;
                this.setupHandlers();
                this.setupLabelIds();
                this.updateSize();
            }
            /** Sets up the basic parameters to default values */
            Timeline.prototype.initParameters = function () {
                this._unitDisplayer = new UnitDisplayer();
                this._scale = new LinearScale();
                this._snapToNonZero = this._globalControls.getTimelineSnapping();
                this.setBinSize(this._globalControls.getBinSize());
                this._dataColor = "#333";
                this._filteredDataColor = "#f68735";
                this._triggerColor = "#f68735";
                this._precision = 2;
                this._currentHoveredBin = -1;
            };
            Timeline.prototype.getBinSize = function () {
                return this._binSize;
            };
            /** Sets the bin size, but checks if the value is not too large, or too low to display */
            Timeline.prototype.setBinSize = function (newSize) {
                if (isNaN(+newSize)) {
                    console.log("Invalid value for bin size.");
                    return;
                }
                var currTime = this.getCurrentTime();
                if (this._timeSlice / newSize > this._canvasWidth || newSize < 1.5625) {
                    this.addWarning(currTime + " Wrong value for bin size selected(" + newSize + ") - too small for screen or detector resolution. Value rounded up.");
                    this._binSize = this._timeSlice / this._canvasWidth;
                    if (this._binSize < 1.5625) {
                        this._binSize = 1.5625;
                    }
                }
                else if (this._timeSlice / newSize < 10) {
                    this.addWarning(currTime + " Wrong value for bin size selected(" + newSize + ") - too large for distinct bins. Value rounded down.");
                    this._binSize = this._timeSlice / 10;
                }
                else {
                    this._binSize = newSize;
                }
            };
            /** Initializes the ID of the labels - specifies, where the values should be displayed */
            Timeline.prototype.setupLabelIds = function () {
                this._startTimeLabelId = "timeline-startTime";
                this._endTimeLabelId = "timeline-endTime";
                this._deltaTimeLabelId = "timeline-deltaTime";
                this._binValueLabelId = "timeline-binValue";
                this._cursorTimeLabelId = "timeline-cursorTime";
            };
            /** Returns the number of the bin, which is present on a coordinate specified by the input parameter */
            Timeline.prototype.getCurrentBinNumber = function (input) {
                input += 3;
                var binWidth = this._canvasWidth / this._data.length;
                var binNumber = input / binWidth;
                return Math.floor(binNumber);
            };
            /** For particular bin number, returns the accurate position where it is drawn on the canvas, in canvas coordinates */
            Timeline.prototype.getPositionForBinNumber = function (binNumber) {
                var binWidth = this._canvasWidth / this._data.length;
                var position = binNumber * binWidth;
                return position - 3 + binWidth / 2; // move cursor's center (3px from left of cursor) to the middle of bin (bin width/2)
            };
            /** Returns how many pixels are contained in specified bin */
            Timeline.prototype.getBinValue = function (binNumber) {
                var binDataValue = this._data[Math.floor(binNumber)];
                return Math.abs(binDataValue);
            };
            /** Find first non-empty bin */
            Timeline.prototype.findClosestNonEmpty = function (binNumber) {
                for (var i = Math.floor(binNumber); i < this._data.length; i++) {
                    if (this._data[i] != 0) {
                        return i;
                    }
                }
                console.log("Nothing to snap on.");
                return binNumber;
            };
            /** Updates the labes and filteres all the data in the DataAccess class, based on current position of the draggable handles (that specify the specified interval)*/
            Timeline.prototype.updateData = function () {
                if (this._leftBin == undefined) {
                    this._leftBin = 0;
                }
                if (this._rightBin == undefined) {
                    this._rightBin = this._numOfBins - 1;
                }
                var minBin = Math.min(this._leftBin, this._rightBin);
                var maxBin = Math.max(this._leftBin, this._rightBin);
                var pixCount = 0;
                for (var i = minBin; i <= maxBin; i++) {
                    pixCount += Math.abs(this._data[i]);
                }
                maxBin += 1;
                $("#" + this._binValueLabelId).html(pixCount.toString());
                $("#" + this._deltaTimeLabelId).attr("trueValue", maxBin * this.getBinSize() - minBin * this.getBinSize());
                $("#" + this._deltaTimeLabelId).html(this._unitDisplayer.convert(maxBin * this._binSize - minBin * this._binSize, UnitDisplayer.NANO, this._unitDisplayer.getCurrentUnits()).toFixed(this._precision));
                $("#" + this._endTimeLabelId).html(this._unitDisplayer.convert(maxBin * this._binSize, UnitDisplayer.NANO, this._unitDisplayer.getCurrentUnits()).toFixed(this._precision));
                $("#" + this._startTimeLabelId).attr("trueValue", minBin * this.getBinSize());
                $("#" + this._startTimeLabelId).html(this._unitDisplayer.convert(minBin * this._binSize, UnitDisplayer.NANO, this._unitDisplayer.getCurrentUnits()).toFixed(this._precision));
                this._dataAccess.setInterval(this._pixelIndexes[minBin], this._pixelIndexes[maxBin], this._parent.getSource());
            };
            /** For a given jQuery handle, determines its position, determines the current bin and updates(filters) the data based on the position of the handle */
            Timeline.prototype.handleStop = function (jHandle, which) {
                if (!this._data || this._data.Size == 0) {
                    return;
                }
                var posString = jHandle.css("left");
                var posNumber = +posString.substr(0, posString.length - 2); // remove "px" from end of "left" css attribute
                var binNumber = this.getCurrentBinNumber(+posNumber);
                var binValue = this.getBinValue(binNumber);
                if (this._snapToNonZero && binValue == 0) {
                    binNumber = this.findClosestNonEmpty(binNumber);
                    var newPos = this.getPositionForBinNumber(binNumber);
                    jHandle.css("left", newPos);
                    binValue = this.getBinValue(binNumber);
                }
                if (which === "left") {
                    this._leftBin = binNumber;
                }
                else {
                    this._rightBin = binNumber;
                }
                this.updateData();
                this._parent.redrawHistograms(this._dataAccess);
            };
            /** Handler for stop event of the left handle */
            Timeline.prototype.rightHandleStop = function (jHandle) {
                this.handleStop(jHandle, "right");
            };
            /** Handler for stop event of the right handle */
            Timeline.prototype.leftHandleStop = function (jHandle) {
                this.handleStop(jHandle, "left");
            };
            /** Gets real coordinates of the mouse, respective to the histogram canvas, from the global (screen) coordinates of the mouse. Top left corner of the canvas is 0 0 */
            Timeline.prototype.getRealCanvasCoordX = function (coordinate, jCanvas) {
                var newCoordinate = coordinate - jCanvas.parentNode.offsetLeft;
                return newCoordinate;
            };
            /** Creates another layer */
            Timeline.prototype.createOverlayCanvas = function () {
                var container = this._jCanvas[0].parentNode;
                this._overlayCanvas = document.createElement('canvas');
                this._overlayCanvas.id = this._name + "-over";
                this._overlayCanvas.width = this._canvasWidth;
                this._overlayCanvas.height = this._canvasHeight;
                container.appendChild(this._overlayCanvas);
                this._overlayContext = this._overlayCanvas.getContext('2d');
                this._jOverlayCanvas = $('#' + this._overlayCanvas.id);
                this._jOverlayCanvas.addClass("temp-canvas");
                this._jOverlayCanvas.attr("height", this._canvasHeight);
                this._jOverlayCanvas.attr("width", this._canvasWidth);
                this._jOverlayCanvas.css("max-height", this._canvasHeight);
            };
            /** Registers all the callback functions for interactive elements on the timeline. */
            Timeline.prototype.setupHandlers = function () {
                var timelineObj = this;
                $("#" + this._name + "-slider-left").draggable({
                    scroll: false,
                    axis: "x",
                    containment: ".timeline-canvas-wrapper",
                    drag: function () { },
                    stop: function () {
                        timelineObj.leftHandleStop($(this));
                    }
                });
                $("#" + this._name + "-slider-right").draggable({
                    scroll: false,
                    axis: "x",
                    containment: ".timeline-canvas-wrapper",
                    drag: function () { },
                    stop: function () {
                        timelineObj.rightHandleStop($(this));
                    }
                });
                this._jOverlayCanvas.mousemove(function (e) {
                    if (!timelineObj._globalControls.showCursor()) {
                        return;
                    }
                    var coord = timelineObj.getRealCanvasCoordX(e.pageX, this);
                    var currBin = timelineObj.getCurrentBinNumber(coord);
                    if (currBin != timelineObj._currentHoveredBin) {
                        timelineObj._globalControls.highlightBin(currBin);
                        timelineObj._currentHoveredBin = currBin;
                    }
                    $("#" + timelineObj._cursorTimeLabelId).html(timelineObj._unitDisplayer.convert(currBin * timelineObj._binSize, UnitDisplayer.NANO, timelineObj._unitDisplayer.getCurrentUnits()).toFixed(timelineObj._precision + 1) + " [" + timelineObj._unitDisplayer.getUnitShortcut() + "s]");
                });
            };
            Timeline.prototype.highlightBin = function (binNumber) {
                this._overlayContext.clearRect(0, 0, this._canvasWidth, this._canvasHeight);
                this._overlayContext.fillStyle = "rgba(255,23,23,0.5)";
                this._overlayContext.fillRect(binNumber * this._canvasWidth / this._numOfBins, 0, this._canvasWidth / this._numOfBins, this._canvasHeight);
            };
            /** Sets the "snap to non-zero" functionality to OFF */
            Timeline.prototype.toggleSnappingOff = function () {
                this._snapToNonZero = false;
            };
            /** Sets the "snap to non-zero" functionality to ON */
            Timeline.prototype.toggleSnappingOn = function () {
                this._snapToNonZero = true;
            };
            /** Redraws the data on the Timeline */
            Timeline.prototype.redraw = function (newDataSource) {
                if (newDataSource === void 0) { newDataSource = null; }
                this.drawFilteredData();
                var data = null;
                if (newDataSource) {
                    data = newDataSource.getData(this._parent.getSource(), true);
                    this._startTime = newDataSource.getStartTime();
                    this._timeSlice = newDataSource.getTimeSlice();
                    this.setBinSize(this._globalControls.getBinSize());
                }
                if (data) {
                    if (data == undefined || data == 0) {
                        this.drawNoData();
                        return;
                    }
                    this.getCanvasData(data);
                }
                this.clearTempCanvas();
                this.updateSize();
                this.drawData();
                if (newDataSource && newDataSource.hasFilter()) {
                    this.redrawFiltered(newDataSource);
                }
                $("#" + this._name + "-slider-left").css("left", 0);
                $("#" + this._name + "-slider-right").css("left", "99.6%");
                this._leftBin = 0;
                this._rightBin = this._numOfBins - 1;
                this.previewScales();
                this.updateData();
            };
            /** Redraws only second layer of the visualization, to show current filtered data, while keeping the original for comparison. */
            Timeline.prototype.redrawFiltered = function (newDataSource) {
                if (newDataSource === void 0) { newDataSource = null; }
                var data = null;
                if (newDataSource) {
                    data = newDataSource.getData(this._parent.getSource());
                }
                if (data && data != undefined && data != null) {
                    this.clearTempCanvas();
                    this.getFilteredData(data);
                    this.drawFilteredData();
                }
            };
            /** Drawing all the bins and the trigger lines, where the measurement registered a trigger. */
            Timeline.prototype.drawData = function () {
                if (!this._data) {
                    this.drawNoData();
                    return;
                }
                var triggerWidth = 1;
                var binCount = this._data.length;
                var triggerCount = 0;
                this._context.fillStyle = this._dataColor;
                for (var i = 0; i < binCount; i++) {
                    var currentData = this._data[i];
                    var coord = this._canvasHeight - this._canvasHeight * this._scale.getScaledValue(Math.abs(currentData), 0, this._maxBin) / 100;
                    this._context.fillRect(i * this._canvasWidth / this._numOfBins, this._canvasHeight - this._canvasHeight * this._scale.getScaledValue(Math.abs(currentData), 0, this._maxBin) / 100, this._canvasWidth / this._numOfBins, this._canvasHeight);
                    if (currentData < 0) {
                        this._context.fillStyle = this._triggerColor;
                        var curTrigger = this._startTrigger + triggerCount + 1;
                        this._context.fillText("Trigger " + curTrigger, (i + 1) * this._canvasWidth / binCount + 4, curTrigger % 2 == 0 ? 15 : 30);
                        this._context.fillRect(-triggerWidth + (i + 1) * this._canvasWidth / binCount, 0, triggerWidth, this._canvasHeight);
                        this._context.fillStyle = this._dataColor;
                        triggerCount++;
                    }
                }
            };
            /** Draws only second layer of the visualization, to show current filtered data, while keeping the original for comparison. */
            Timeline.prototype.drawFilteredData = function () {
                if (!this._filteredData) {
                    return;
                }
                this._tempContext.fillStyle = this._filteredDataColor;
                for (var i = 0; i < this._numOfBins; i++) {
                    var currentData = this._filteredData[i];
                    this._tempContext.fillRect(i * this._canvasWidth / this._numOfBins, this._canvasHeight - this._canvasHeight * this._scale.getScaledValue(Math.abs(currentData), 0, this._maxBin) / 100, this._canvasWidth / this._numOfBins, this._canvasHeight);
                }
            };
            /** Draws both horizontal and vertical scales of the timeline */
            Timeline.prototype.previewScales = function () {
                this.previewHorizontalScale();
                this.previewVerticalScale();
            };
            /** Displays the scale under the timeline, to describe the X-axis. To show, how long time period represents a certain distance (number of bins) on the timeline */
            Timeline.prototype.previewHorizontalScale = function () {
                if (!this._data) {
                    return;
                }
                $('#' + this._scaleName).attr("height", 25);
                $('#' + this._scaleName).attr("width", this._canvasWidth);
                var canvas = document.getElementById(this._scaleName);
                var context = canvas.getContext("2d");
                var minWidth = this._canvasWidth / 20;
                var rightPosition = this._canvasWidth / this._data.length;
                var origRightPosition = rightPosition;
                var numberOfBins = 1;
                context.fillStyle = "rgba(255,255,255,0.4)";
                context.fillRect(0, 0, this._canvasWidth, 25);
                while (rightPosition < minWidth) {
                    rightPosition += origRightPosition;
                    numberOfBins++;
                }
                origRightPosition = rightPosition;
                context.fillStyle = "#000000";
                context.fillRect(1, 0, 0.5, 100);
                context.font = "15px Georgia";
                context.fillText("0", 4, 18);
                var whichBin = 1;
                var chosenUnits = false;
                while (rightPosition < this._canvasWidth) {
                    context.fillRect(rightPosition, 0, 0.5, 100);
                    if (!chosenUnits) {
                        this._unitDisplayer.getBestUnits(numberOfBins * whichBin * this._binSize, UnitDisplayer.NANO);
                        chosenUnits = true;
                    }
                    var correctNumber = this._unitDisplayer.convert(numberOfBins * whichBin * this._binSize, UnitDisplayer.NANO, this._unitDisplayer.getCurrentUnits());
                    context.fillText(correctNumber.toFixed(this._precision), rightPosition + 3, 18, origRightPosition - 6);
                    rightPosition += origRightPosition;
                    whichBin++;
                }
                rightPosition = this._canvasWidth - 30;
                context.fillStyle = "rgba(255,255,255,0.4)";
                context.clearRect(rightPosition, 0, 30, 100);
                context.fillRect(rightPosition, 0, 30, 100);
                context.fillStyle = "#000000";
                context.fillText("[" + this._unitDisplayer.getUnitShortcut() + "s]", rightPosition + 3, 18);
            };
            /** Displays the scale next to the timeline, to describe the Y-axis. To show number of hits within bins */
            Timeline.prototype.previewVerticalScale = function () {
                $('#' + this._scaleNameVertical).attr("height", this._canvasHeight);
                $('#' + this._scaleNameVertical).attr("width", this._canvasWidth / 50);
                var width = this._canvasWidth / 50;
                var canvas = document.getElementById(this._scaleNameVertical);
                var context = canvas.getContext("2d");
                context.fillStyle = "rgba(255,255,255,0.8)";
                context.fillRect(0, 0, 50, this._canvasHeight);
                context.fillStyle = "#000000";
                context.font = "20px Georgia";
                context.textAlign = "center";
                context.fillText("#", width / 2, 59);
                context.fillText("h", width / 2, 79);
                context.fillText("i", width / 2, 94);
                context.fillText("t", width / 2, 109);
                context.fillText("s", width / 2, 123);
            };
            /** Takes the new data from argument and saves them into _filteredArray, to be later drawn over the un-filtered, original data */
            Timeline.prototype.getFilteredData = function (data) {
                this._filteredData = [];
                var baseTime = data.StartTime;
                var binHighBound = this._startTime;
                var j = 0;
                var currTrigger = this._startTrigger;
                for (var i = 0; i < this._numOfBins; i++) {
                    this._filteredData[i] = 0;
                    binHighBound += this._binSize;
                    while (j < data.Size && (baseTime + data.Pixels[j].ToA) < binHighBound) {
                        if (data.Pixels[j].TriggerNo != currTrigger) {
                            currTrigger = data.Pixels[j].TriggerNo;
                            baseTime += data.Pixels[j - 1].ToA;
                            break;
                        }
                        this._filteredData[i]++;
                        j++;
                    }
                }
            };
            /** Gets the data and assigns them to bins, according to their ToA values and considering the triggers. Also saves starting pixel index for each bin and maximum bin value, for scaling purposes. */
            Timeline.prototype.getCanvasData = function (data) {
                this._binSize = +this._binSize;
                this._numOfBins = Math.floor(this._timeSlice / this._binSize);
                var baseTime = data.StartTime + data.Offset;
                var binLowBound = this._startTime;
                var binHighBound = binLowBound;
                var j = 0;
                this._data = [];
                this._maxBin = 0;
                var currTrigger;
                // can be left undefined if there are no data, as following loop is for < data.Size
                if (data.Size > 0) {
                    currTrigger = data.Pixels[0].TriggerNo;
                    this._startTrigger = currTrigger;
                }
                // for each bin (fill)
                for (var i = 0; i < this._numOfBins; i++) {
                    this._data[i] = 0;
                    binHighBound += this._binSize;
                    // Save index of first pixel in this bin
                    this._pixelIndexes[i] = j;
                    // Loop through pixels in data
                    while (j < data.Size && (baseTime + data.Pixels[j].ToA) < binHighBound) {
                        // If trigger was found in the data
                        if (data.Pixels[j].TriggerNo != currTrigger) {
                            currTrigger = data.Pixels[j].TriggerNo;
                            baseTime += data.Pixels[j - 1].ToA;
                            this._data[i] = -this._data[i];
                            break;
                        }
                        // Add current pixel into current bin, go to next pixel
                        this._data[i]++;
                        j++;
                    }
                    // Mark trigger in data by negative value
                    if (Math.abs(this._data[i]) > this._maxBin) {
                        this._maxBin = Math.abs(this._data[i]);
                    }
                }
                // Last non-existing bin has no first pixel
                this._pixelIndexes[this._numOfBins] = Infinity;
            };
            Timeline.prototype.changeColormap = function (colormap) {
            };
            return Timeline;
        }(CanvasVisualization));
        timeline.Timeline = Timeline;
    })(timeline = tpx3.timeline || (tpx3.timeline = {}));
})(tpx3 || (tpx3 = {}));
/// <reference path="color-map.ts" />
/// <reference path="timeline.ts" />
var GreyscaleMap = tpx3.colormap.GreyscaleMap;
var JetMap = tpx3.colormap.JetMap;
var HotMap = tpx3.colormap.HotMap;
var tpx3;
(function (tpx3) {
    var histogram;
    (function (histogram) {
        /**
        Abstract class representing the interactive 2D square histogram visualization of the sensor.
        */
        var HistogramCanvas = (function (_super) {
            __extends(HistogramCanvas, _super);
            /** Set name for this visualization, and get the visualization data from the given pixels data */
            function HistogramCanvas(parent, name, data) {
                if (data === void 0) { data = null; }
                _super.call(this, name);
                if (parent) {
                    this._parent = parent;
                }
                this.initParameters();
                if (data) {
                    this._data = this.getCanvasData(data);
                }
                else {
                    this.drawNoData();
                }
                this.setupLabelIds();
                this.setupHandlers();
            }
            /** Initialize the basic parameters, like size of the matrix, default scale and color map, etc. */
            HistogramCanvas.prototype.initParameters = function () {
                this._precision = 2;
                this._showMasked = false;
                this._zooming = false;
                this._pixelCount = 256;
                this._maxPixelCount = 256;
                this._zoom = 1;
                this._startX = 0;
                this._startY = 0;
                this._maxValue = 0;
                this._minValue = 0;
                this._startTime = 0;
                this._scale = new LinearScale();
                this.changeColormap(new GreyscaleMap());
                this.initMaskedMatrix();
                this._maskingColor = "#f68735";
                this._maskedPixelsColor = "#3f547f";
                this._offset = 0;
                this._unitDisplayer = new UnitDisplayer();
            };
            /** Initializes the matrix of masked pixels.
            Uses masked matrix of its parent, to be able to mask across all the histograms of a single sensor.  */
            HistogramCanvas.prototype.initMaskedMatrix = function () {
                this._maskedMatrix = this._parent._maskedMatrix;
            };
            /**
            Initializes the _data array (2D array of the visualization) and process all the pixel data from the data source to store the correct visualization values.
            */
            HistogramCanvas.prototype.getCanvasData = function (data) {
                this._minValue = 0;
                this._maxValue = 0;
                this._data = [];
                this._offset = data.Offset;
                for (var i = 0; i < this._pixelCount; i++) {
                    this._data[i] = [];
                    for (var j = 0; j < this._pixelCount; j++) {
                        this._data[i][j] = [];
                        this._data[i][j][0] = 0;
                        this._data[i][j][1] = 0;
                    }
                }
                for (var i = 0; i < data.Size; i++) {
                    this.processPixel(data.Pixels[i]);
                }
                return this._data;
            };
            /** Shows the data attributes of the pixel on certain coordinate on the canvas */
            HistogramCanvas.prototype.updateLabels = function (mouseX, mouseY) {
                var posX = this.canvasToSensorCoordX(mouseX);
                var posY = this.canvasToSensorCoordY(mouseY);
                if (posX < this._startX || posY < this._startY || posX > this._startX + this._pixelCount || posY > this._startY + this._pixelCount) {
                    console.log("Out of boundaries.");
                    return;
                }
                for (var i = 0; i < this._labelValId.length; i++) {
                    $("#" + this._labelValId[i]).html(this._data[posX][posY][i]);
                    if (this._data[posX][posY][i] == undefined) {
                        $("#" + this._labelValId[i]).html("0");
                    }
                }
                $("#" + this._labelXid).html(posX.toString());
                $("#" + this._labelYid).html(posY.toString());
            };
            /** Finds out for which value would currently selected scale show it is the 50% value */
            HistogramCanvas.prototype.getMidValue = function () {
                return this._scale.getProportionValue(1 / 2, this._minValue, this._maxValue);
            };
            /** Finds out for which value would currently selected scale show it is the 25% value */
            HistogramCanvas.prototype.getQuarterValue = function () {
                return this._scale.getProportionValue(1 / 4, this._minValue, this._maxValue);
            };
            /** Updates the current zooming coordinates according to input 2 points on the canvas. The coordinates will always represent a square and will never exceed the canvas' boundaries.
            Also it always determines the top-most, left-most point of the resulting square as the start coordinate and then the size of the square.
            */
            HistogramCanvas.prototype.updateZoomingCoordinates = function (startX, startY, endX, endY) {
                var sizeX = Math.max(Math.abs(endX - startX), Math.abs(endY - startY));
                var sizeY = sizeX;
                if (startX > endX) {
                    sizeX = -sizeX;
                }
                if (startY > endY) {
                    sizeY = -sizeY;
                }
                if (startX + sizeX >= this._canvasWidth) {
                    sizeX = this._canvasWidth - startX;
                    if (sizeY < 0) {
                        sizeY = -sizeX;
                    }
                    else {
                        sizeY = sizeX;
                    }
                }
                if (startY + sizeY >= this._canvasHeight) {
                    sizeY = this._canvasHeight - startY;
                    if (sizeX < 0) {
                        sizeX = -sizeY;
                    }
                    else {
                        sizeX = sizeY;
                    }
                }
                if (startX + sizeX <= 0) {
                    sizeX = -startX;
                    if (sizeY < 0) {
                        sizeY = sizeX;
                    }
                    else {
                        sizeY = -sizeX;
                    }
                }
                if (startY + sizeY <= 0) {
                    sizeY = -startY;
                    if (sizeX < 0) {
                        sizeX = sizeY;
                    }
                    else {
                        sizeX = -sizeY;
                    }
                }
                this._zoomStartX = startX;
                this._zoomStartY = startY;
                this._zoomWidth = sizeX;
                this._zoomHeight = sizeY;
            };
            /** Draw a semi-transparent square on current zooming coordinates position. Makes the square in proper color to be always clearly visible on the canvas. */
            HistogramCanvas.prototype.drawZoomSquare = function () {
                if (!this._tempCanvas || !this._tempContext) {
                    console.log("No temporary canvas created. Aborting.");
                    return;
                }
                var lowColorValue = this._colorMap.getColor(this._scale.getScaledValue(0, 0, 100));
                if (this._masking) {
                    this._tempContext.fillStyle = this.getMaskingColor();
                }
                else {
                    this._tempContext.fillStyle = "rgba(" + (255 - lowColorValue.r()) + "," + (255 - lowColorValue.r()) + "," + (255 - lowColorValue.r()) + "," + 0.5 + ")";
                }
                this._tempContext.fillRect(this._zoomStartX, this._canvasHeight - this._zoomStartY, this._zoomWidth, -this._zoomHeight);
            };
            /** Updates the visualization to only show values from the selected (zoomed) area */
            HistogramCanvas.prototype.updateViewport = function (startPixelX, startPixelY, zoomSize) {
                if (zoomSize == 0) {
                    this._pixelCount = this._maxPixelCount;
                    this._startX = 0;
                    this._startY = 0;
                }
                else {
                    this._startX = startPixelX;
                    this._startY = startPixelY;
                    this._pixelCount = zoomSize;
                }
            };
            /** Indicates, if some pixel was selected to display its values in labels, or if labels should update on mousemove */
            HistogramCanvas.prototype.togglePixelSelection = function () {
                this._selectedPixel = !this._selectedPixel;
                $('#' + this._name + "-lock").toggle();
            };
            /** Marks pixels on given coordinates as masked */
            HistogramCanvas.prototype.maskPixels = function (startPixelX, startPixelY, zoomSize) {
                if (zoomSize == 0)
                    zoomSize = 1;
                for (var i = startPixelX; i < startPixelX + zoomSize; i++) {
                    for (var j = startPixelY; j < startPixelY + zoomSize; j++) {
                        this._maskedMatrix[i][j] = !this._maskedMatrix[i][j];
                    }
                }
            };
            /** Resets the masking matrix, to set all pixels not to be maskes */
            HistogramCanvas.prototype.unmaskAll = function () {
                for (var i = 0; i < 256; i++) {
                    for (var j = 0; j < 256; j++) {
                        this._maskedMatrix[i][j] = 0;
                    }
                }
            };
            /** Converts a coordinate from the screen coordinate system to corresponding coordinate on the sensor's pixel matrix. */
            HistogramCanvas.prototype.canvasToSensorCoordX = function (coordinate) {
                return this._startX + Math.floor(coordinate * this._pixelCount / this._canvasWidth);
            };
            /** Converts a coordinate from the screen coordinate system to corresponding coordinate on the sensor's pixel matrix. */
            HistogramCanvas.prototype.canvasToSensorCoordY = function (coordinate) {
                return this._startY + Math.floor(coordinate * this._pixelCount / this._canvasHeight);
            };
            /** Gets real coordinates of the mouse, respective to the histogram canvas, from the global (screen) coordinates of the mouse. Top left corner of the canvas is 0 0 */
            HistogramCanvas.prototype.getRealCanvasCoordX = function (coordinate, jCanvas) {
                var newCoordinate = coordinate - $(jCanvas).offset().left;
                return newCoordinate;
            };
            /** Gets real coordinates of the mouse, respective to the histogram canvas, from the global (screen) coordinates of the mouse. Top left corner of the canvas is 0 0 */
            HistogramCanvas.prototype.getRealCanvasCoordY = function (coordinate, jCanvas) {
                var newCoordinate = this._canvasHeight - (coordinate - $(jCanvas).offset().top);
                return newCoordinate;
            };
            HistogramCanvas.prototype.mouseUpHandler = function (e, jCanvas) {
                if (!this._data || this._data.Size == 0) {
                    return;
                }
                if (!this._zooming) {
                    return;
                }
                this._zooming = false;
                var endMouseX = this.getRealCanvasCoordX(e.pageX, jCanvas);
                var endMouseY = this.getRealCanvasCoordY(e.pageY, jCanvas);
                this.updateZoomingCoordinates(this._startMouseX, this._startMouseY, endMouseX, endMouseY);
                var startPixelX = this.canvasToSensorCoordX(Math.min(this._zoomStartX, this._zoomStartX + this._zoomWidth));
                var startPixelY = this.canvasToSensorCoordY(Math.min(this._zoomStartY, this._zoomStartY + this._zoomHeight));
                var endPixelX = this.canvasToSensorCoordX(Math.max(this._zoomStartX, this._zoomStartX + this._zoomWidth));
                var zoomSize = Math.abs(endPixelX - startPixelX);
                if (zoomSize == 0 && !this._masking) {
                    //this.togglePixelSelection();
                    return;
                }
                if (this._masking) {
                    this.maskPixels(startPixelX, startPixelY, zoomSize);
                }
                else {
                    this._parent.updateViewport(startPixelX, startPixelY, zoomSize);
                }
                this.clearTempCanvas();
                this._parent.redrawHistograms();
            };
            /** Sets up all necessary event callbacks to provide the interactivity for the histogram. */
            HistogramCanvas.prototype.setupHandlers = function () {
                var sensorObj = this;
                this._jTempCanvas.mousedown(function (e) {
                    if (!sensorObj._data || sensorObj._data.Size == 0) {
                        return;
                    }
                    sensorObj._startMouseX = sensorObj.getRealCanvasCoordX(e.pageX, this);
                    sensorObj._startMouseY = sensorObj.getRealCanvasCoordY(e.pageY, this);
                    sensorObj._zooming = true;
                });
                this._jTempCanvas.dblclick(function (e) {
                    e.preventDefault();
                    try {
                        window.getSelection().empty();
                    }
                    catch (e) {
                    }
                    if (sensorObj._masking) {
                        sensorObj.unmaskAll();
                        sensorObj._parent.redraw();
                        return;
                    }
                    sensorObj._parent.updateViewport(0, 0, 256);
                    sensorObj._parent.redrawHistograms();
                });
                this._jTempCanvas.contextmenu(function (e) {
                    e.preventDefault();
                    sensorObj.togglePixelSelection();
                });
                this._jTempCanvas.mouseup(function (e) {
                    sensorObj.mouseUpHandler(e, this);
                });
                this._jTempCanvas.mouseleave(function (e) {
                    e.preventDefault();
                    try {
                        window.getSelection().empty();
                    }
                    catch (e) {
                    }
                    sensorObj.mouseUpHandler(e, this);
                });
                this._jTempCanvas.mousemove(function (e) {
                    if (!sensorObj._data || sensorObj._data.Size == 0) {
                        console.log("data null");
                        return;
                    }
                    var currentMouseX = sensorObj.getRealCanvasCoordX(e.pageX, this);
                    var currentMouseY = sensorObj.getRealCanvasCoordY(e.pageY, this);
                    if (sensorObj._zooming) {
                        sensorObj.clearTempCanvas();
                        sensorObj.updateZoomingCoordinates(sensorObj._startMouseX, sensorObj._startMouseY, currentMouseX, currentMouseY);
                        sensorObj.drawZoomSquare();
                    }
                    if (!sensorObj._selectedPixel) {
                        sensorObj.updateLabels(currentMouseX, currentMouseY);
                    }
                });
            };
            /** Gets the corresponding color for input value, according to currently selected scale and color map */
            HistogramCanvas.prototype.getScaledColor = function (value) {
                var scaledValue = this._scale.getScaledValue(value, 0, this._maxValue);
                if (scaledValue == 0 && value != 0) {
                    scaledValue += 1;
                }
                var color = this._colorMap.getColor(scaledValue).css();
                return color;
            };
            /** Returns a color in which all the masked (ignored) bits will be displayed */
            HistogramCanvas.prototype.getMaskedPixelsColor = function () {
                return this._maskedPixelsColor;
            };
            /** Returns a color in which the masked and currently masked pixels will be shown in masking mode */
            HistogramCanvas.prototype.getMaskingColor = function () {
                return this._maskingColor;
            };
            /** Draws a square on specified location. Square is the size of canvas width / number of pixels to be displayed. */
            HistogramCanvas.prototype.drawSquare = function (x_coord, y_coord) {
                y_coord = this._pixelCount - 1 - y_coord;
                var point = this._canvasWidth / this._pixelCount;
                this._context.fillRect(x_coord * point, y_coord * point, point, point);
            };
            /** Redraws all the data on the visualization.  */
            HistogramCanvas.prototype.redraw = function (newDataSource) {
                if (newDataSource === void 0) { newDataSource = null; }
                this.updateSize();
                if (newDataSource) {
                    this.updateViewport(0, 0, 0);
                    this._startTime = newDataSource.getStartTime();
                    this.getCanvasData(newDataSource.getData(this._parent.getSource()));
                }
                this.drawAllPixels();
                this.previewScale(this._previewScaleId);
            };
            /** Draws all the pixels currently saved in the _data array on the canvas. */
            HistogramCanvas.prototype.drawAllPixels = function () {
                if (!this._data || this._data.Size == 0) {
                    return;
                }
                this.updateSize();
                this._context.fillStyle = this.getPixelColor(0);
                this._context.fillRect(0, 0, this._canvasWidth, this._canvasHeight);
                for (var i = 0; i < this._pixelCount; i++) {
                    for (var j = 0; j < this._pixelCount; j++) {
                        /** if this current pixel was masked */
                        if (this._maskedMatrix[this._startX + i][this._startY + j] == 1) {
                            if (this._masking) {
                                this._context.fillStyle = this.getMaskingColor();
                            }
                            else if (this._showMasked) {
                                this._context.fillStyle = this.getMaskedPixelsColor();
                            }
                            else {
                                this._context.fillStyle = this.getPixelColor(0);
                            }
                            this.drawSquare(i, j);
                            continue;
                        }
                        var value = this._data[this._startX + i][this._startY + j][0];
                        if (value != 0) {
                            this._context.fillStyle = this.getPixelColor(value);
                            this.drawSquare(i, j);
                        }
                    }
                }
            };
            /** Update the maximum value currently present in the visualization */
            HistogramCanvas.prototype.updateMaxValue = function (value) {
                if (value > this._maxValue) {
                    this._maxValue = value;
                }
            };
            /** Shows all currently masked pixels */
            HistogramCanvas.prototype.toggleShowMaskedPixels = function () {
                this._showMasked = !this._showMasked;
            };
            /** Switches to the masking mode, if the user is currently selecting pixels to be masked, or no */
            HistogramCanvas.prototype.toggleMaskingOn = function () {
                this._masking = true;
            };
            /** Switches off the masking mode */
            HistogramCanvas.prototype.toggleMaskingOff = function () {
                this._masking = false;
            };
            /** Log some info about the canvas into console */
            HistogramCanvas.prototype.logHistogram = function () {
                console.log(this._name);
                console.log(this._canvas);
                console.log(this._data);
                console.log(this._maxValue);
                console.log(this._colorMap);
            };
            /** Set up elements, where to draw the labels and info about the pixels */
            HistogramCanvas.prototype.setupLabelIds = function () {
                this._labelXid = this._name + "-pixX";
                this._labelYid = this._name + "-pixY";
                this._previewScaleId = this._name + "-scale-histogram";
            };
            HistogramCanvas.prototype.changeColormap = function (colormap) {
                this._colorMap = colormap;
                colormap.preview(this._name + "-colormap-histogram", true);
            };
            HistogramCanvas.prototype.showRelativeValues = function () {
                return this._parent.showRelativeValues();
            };
            return HistogramCanvas;
        }(CanvasVisualization));
        histogram.HistogramCanvas = HistogramCanvas;
        /**
        Histogram displaying the ToT value of the pixel.
        */
        var ToTHistogramCanvas = (function (_super) {
            __extends(ToTHistogramCanvas, _super);
            function ToTHistogramCanvas(parent, name, data) {
                if (data === void 0) { data = null; }
                _super.call(this, parent, name, data);
                this._labelValId = [this._name + "-tot", this._name + "-tot_KeV"];
            }
            ToTHistogramCanvas.prototype.processPixel = function (pixel) {
                this._data[pixel.pixX][pixel.pixY][0] += pixel.ToT;
                this._data[pixel.pixX][pixel.pixY][1] += pixel.ToT_KeV;
                this.updateMaxValue(this._data[pixel.pixX][pixel.pixY][0]);
            };
            ToTHistogramCanvas.prototype.getPixelColor = function (pixel) {
                return this.getScaledColor(pixel);
            };
            /** Previews the color scale used, with the proper corresponding values to show, which displayed color means what. */
            ToTHistogramCanvas.prototype.previewScale = function (divId) {
                var jPreviewCanvas = $("#" + divId);
                jPreviewCanvas.html("");
                jPreviewCanvas.append("<span class=\"scale-min-value\">" + this._minValue.toFixed(0) + "</span>");
                jPreviewCanvas.append("<span class=\"scale-quarter-value\">" + this.getQuarterValue().toFixed(0) + "</span>");
                jPreviewCanvas.append("<span class=\"scale-mid-value\">" + this.getMidValue().toFixed(0) + "</span>");
                jPreviewCanvas.append("<span class=\"scale-max-value\">" + this._maxValue.toFixed(0) + " [cts]</span>");
            };
            return ToTHistogramCanvas;
        }(HistogramCanvas));
        histogram.ToTHistogramCanvas = ToTHistogramCanvas;
        /**
        Histogram displaying the ToA value of the pixel + original raw ToA(as sToA) and fToA
        */
        var ToAHistogramCanvas = (function (_super) {
            __extends(ToAHistogramCanvas, _super);
            function ToAHistogramCanvas(parent, name, data) {
                if (data === void 0) { data = null; }
                _super.call(this, parent, name, data);
                this._labelValId = [this._name + "-toa", this._name + "-stoa", this._name + "-ftoa"];
            }
            ToAHistogramCanvas.prototype.processPixel = function (pixel) {
                this._data[pixel.pixX][pixel.pixY][0] = pixel.ToA + this._offset;
                if (this.showRelativeValues()) {
                    this._data[pixel.pixX][pixel.pixY][0] -= this._startTime;
                }
                this._data[pixel.pixX][pixel.pixY][1] = pixel.sToA;
                this._data[pixel.pixX][pixel.pixY][2] = pixel.fToA;
                this.updateMaxValue(this._data[pixel.pixX][pixel.pixY][0]);
            };
            ToAHistogramCanvas.prototype.getPixelColor = function (pixel) {
                return this.getScaledColor(pixel);
            };
            /** Previews the color scale used, with the proper corresponding values to show, which displayed color means what. */
            ToAHistogramCanvas.prototype.previewScale = function (divId) {
                var jPreviewCanvas = $("#" + divId);
                jPreviewCanvas.html("");
                jPreviewCanvas.append("<span class=\"scale-min-value\">" + this._minValue.toFixed(0) + "</span>");
                jPreviewCanvas.append("<span class=\"scale-quarter-value\">" + this._unitDisplayer.getBestUnits(this.getQuarterValue(), UnitDisplayer.NANO).toFixed(this._precision) + "</span>");
                jPreviewCanvas.append("<span class=\"scale-mid-value\">" + this._unitDisplayer.convert(this.getMidValue(), UnitDisplayer.NANO, this._unitDisplayer.getCurrentUnits()).toFixed(this._precision) + "</span>");
                jPreviewCanvas.append("<span class=\"scale-max-value\">" + this._unitDisplayer.convert(this._maxValue, UnitDisplayer.NANO, this._unitDisplayer.getCurrentUnits()).toFixed(this._precision) + " [" + this._unitDisplayer.getUnitShortcut() + "s]</span>");
            };
            return ToAHistogramCanvas;
        }(HistogramCanvas));
        histogram.ToAHistogramCanvas = ToAHistogramCanvas;
        /**
        Histogram displaying number of pixel hits on each coordinate on the sensor's matrix
        */
        var HitsHistogramCanvas = (function (_super) {
            __extends(HitsHistogramCanvas, _super);
            function HitsHistogramCanvas(parent, name, data) {
                if (data === void 0) { data = null; }
                _super.call(this, parent, name, data);
                this._labelValId = [this._name + "-hits"];
            }
            HitsHistogramCanvas.prototype.processPixel = function (pixel) {
                this._data[pixel.pixX][pixel.pixY][0] += 1;
                this.updateMaxValue(this._data[pixel.pixX][pixel.pixY][0]);
            };
            HitsHistogramCanvas.prototype.getPixelColor = function (pixel) {
                return this.getScaledColor(pixel);
            };
            /** Previews the color scale used, with the proper corresponding values to show, which displayed color means what. */
            HitsHistogramCanvas.prototype.previewScale = function (divId) {
                var jPreviewCanvas = $("#" + divId);
                jPreviewCanvas.html("");
                jPreviewCanvas.append("<span class=\"scale-min-value\">" + this._minValue.toFixed(0) + "</span>");
                jPreviewCanvas.append("<span class=\"scale-quarter-value\">" + this.getQuarterValue().toFixed(0) + "</span>");
                jPreviewCanvas.append("<span class=\"scale-mid-value\">" + this.getMidValue().toFixed(0) + "</span>");
                jPreviewCanvas.append("<span class=\"scale-max-value\">" + this._maxValue.toFixed(0) + " [#hits]</span>");
            };
            return HitsHistogramCanvas;
        }(HistogramCanvas));
        histogram.HitsHistogramCanvas = HitsHistogramCanvas;
    })(histogram = tpx3.histogram || (tpx3.histogram = {}));
})(tpx3 || (tpx3 = {}));
/// <reference path="histogram-canvas.ts" />
var ToAHistogram = tpx3.histogram.ToAHistogramCanvas;
var ToTHistogram = tpx3.histogram.ToTHistogramCanvas;
var HitsHistogram = tpx3.histogram.HitsHistogramCanvas;
var HistogramCanvas = tpx3.histogram.HistogramCanvas;
var tpx3;
(function (tpx3) {
    var histogramfactory;
    (function (histogramfactory) {
        /** Factory class used for creating new instances of individual histograms and placing them correctly on the HTML page */
        var HistogramFactory = (function (_super) {
            __extends(HistogramFactory, _super);
            function HistogramFactory(name) {
                if (name === void 0) { name = ""; }
                _super.call(this, name);
            }
            /** Creates and returns new instance of histogram for displaying ToA values */
            HistogramFactory.prototype.createToAHistogram = function (parent, id, data) {
                if (data === void 0) { data = null; }
                return new ToAHistogram(parent, id, data);
            };
            /** Creates and returns new instance of histogram for displaying ToT values */
            HistogramFactory.prototype.createToTHistogram = function (parent, id, data) {
                if (data === void 0) { data = null; }
                return new ToTHistogram(parent, id, data);
            };
            /** Creates and returns new instance of histogram for displaying #hits */
            HistogramFactory.prototype.createHitsHistogram = function (parent, id, data) {
                if (data === void 0) { data = null; }
                return new HitsHistogram(parent, id, data);
            };
            /** Puts all the contents required for drawing the histogram into the HTML page */
            HistogramFactory.prototype.putHistogramHtmlContent = function (id, containerId, values, header) {
                var jContainer = $('#' + containerId);
                jContainer.append("<div class=\"col-md-4 hist-col\">\
                  " + header + "\
                    <div>\
                      <div class=\"canvas-wrapper\">\
                        <div class=\"canvas-inner-wrapper\">\
                          <canvas class=\"sensor-canvas\" id=\"" + id + "\">\
                          </canvas>\
                        </div>\
                      </div>\
                    </div>\
                  \
                  <canvas class=\"sensor-colormap-histogram\" id=\"" + id + "-colormap-histogram\">\
                  </canvas>\
                  <div class=\"sensor-scale-histogram\" id=\"" + id + "-scale-histogram\">\
                  </div>\
                  <div class=\"sensor-labels\">\
                  <p class=\"canvas-labels\"><span id=\"" + id + "-lock\" class=\"glyphicon glyphicon-lock\" style=\"display:none\"> </span>  X: <span id=\"" + id + "-pixX\">0</span> Y: <span id=\"" + id + "-pixY\">0</span> " + values + "</p>\
                  </div>\
                </div>");
            };
            /** Puts all the contents required for drawing the histogram in its collapsed state into the HTML page */
            HistogramFactory.prototype.putCollapsedHistogramContent = function (id, sensorName, values, header) {
                var jNavContainer = $("#" + sensorName + "-tab-nav");
                var jContentContainer = $("#" + sensorName + "-tab-content");
                jNavContainer.prepend("<li class=\"control-tab-toa active\" id=\"" + id + "-nav-tab\" ><a data-toggle=\"tab\" href=\"#" + id + "-tab\">" + header + "</a></li>");
                jContentContainer.append("<div id=\"" + id + "-tab\" class=\"tab-pane active\">\
                  <div class=\"canvas-wrapper\">\
                    <div class=\"canvas-inner-wrapper\">\
                      <canvas class=\"sensor-canvas\" id=\"" + id + "\">\
                      </canvas>\
                    </div>\
                  </div>\
\
                  <canvas class=\"sensor-colormap-histogram\" id=\"" + id + "-colormap-histogram\">\
                  </canvas>\
\
                  <div class=\"sensor-scale-histogram\" id=\"" + id + "-scale-histogram\">\
                  </div>\
                  \
                  <div class=\"sensor-labels\">\
                    <p class=\"canvas-labels\"><span id=\"" + id + "-lock\" class=\"glyphicon glyphicon-lock\" style=\"display:none\"> </span> X: <span id=\"" + id + "-pixX\">0</span> Y: <span id=\"" + id + "-pixY\">0</span> " + values + "</p>\
                  </div>\
                  </div>");
            };
            /** Adds a new histogram in the HTML page and return its created instance for manipulation */
            HistogramFactory.prototype.addToAHistogram = function (parent, id, containerId) {
                var header = "<h3 class=\"sensor-header\" data-toggle=\"tooltip\" title=\"Time of aquisition in nanoseconds\">ToA [ns]</h3>";
                var values = "ToA: <span id=\"" + id + "-toa\">0</span> sToA: <span id=\"" + id + "-stoa\">0</span> fToA: <span id=\"" + id + "-ftoa\">0</span>";
                this.putHistogramHtmlContent(id, containerId, values, header);
                return this.createToAHistogram(parent, id);
            };
            /** Adds a new histogram in the HTML page and return its created instance for manipulation */
            HistogramFactory.prototype.addToTHistogram = function (parent, id, containerId) {
                var header = "<h3 class=\"sensor-header\" data-toggle=\"tooltip\" title=\"Energy of the particle in clock counts\">ToT [cts]</h3>";
                var values = "ToT: <span id=\"" + id + "-tot\">0</span> ToT_KeV:  <span id=\"" + id + "-tot_KeV\">0</span>";
                this.putHistogramHtmlContent(id, containerId, values, header);
                return this.createToTHistogram(parent, id);
            };
            /** Adds a new histogram in the HTML page and return its created instance for manipulation */
            HistogramFactory.prototype.addHitsHistogram = function (parent, id, containerId) {
                var header = "<h3 class=\"sensor-header\" data-toggle=\"tooltip\" title=\"Number of hits of each pixel\">No. pixel hits</h3>";
                var values = "#hits: <span id=\"" + id + "-hits\">0</span>";
                this.putHistogramHtmlContent(id, containerId, values, header);
                return this.createHitsHistogram(parent, id);
            };
            /** Adds a new histogram in the HTML page in the collapsed state and return its created instance for manipulation */
            HistogramFactory.prototype.addCollapsedToAHistogram = function (parent, id, sensorName) {
                var values = "ToA: <span id=\"" + id + "-toa\">0</span> sToA: <span id=\"" + id + "-stoa\">0</span> fToA: <span id=\"" + id + "-ftoa\">0</span>";
                var header = "ToA";
                this.putCollapsedHistogramContent(id, sensorName, values, header);
                return this.createToAHistogram(parent, id);
            };
            /** Adds a new histogram in the HTML page in the collapsed state and return its created instance for manipulation */
            HistogramFactory.prototype.addCollapsedToTHistogram = function (parent, id, sensorName) {
                var values = "ToT: <span id=\"" + id + "-tot\">0</span> ToT_KeV:  <span id=\"" + id + "-tot_KeV\">0</span>";
                var header = "ToT";
                this.putCollapsedHistogramContent(id, sensorName, values, header);
                return this.createToTHistogram(parent, id);
            };
            /** Adds a new histogram in the HTML page in the collapsed state and return its created instance for manipulation */
            HistogramFactory.prototype.addCollapsedHitsHistogram = function (parent, id, sensorName) {
                var values = "#hits: <span id=\"" + id + "-hits\">0</span>";
                var header = "#hits";
                this.putCollapsedHistogramContent(id, sensorName, values, header);
                return this.createHitsHistogram(parent, id);
            };
            return HistogramFactory;
        }(BaseObject));
        histogramfactory.HistogramFactory = HistogramFactory;
    })(histogramfactory = tpx3.histogramfactory || (tpx3.histogramfactory = {}));
})(tpx3 || (tpx3 = {}));
/// <reference path="timeline.ts" />
var Tline = tpx3.timeline.Timeline;
var tpx3;
(function (tpx3) {
    var timelinefactory;
    (function (timelinefactory) {
        /** Factory class used for creating new instances of Timeline class and placing the timelines correctly on the HTML page */
        var TimelineFactory = (function (_super) {
            __extends(TimelineFactory, _super);
            function TimelineFactory(name) {
                if (name === void 0) { name = ""; }
                _super.call(this, name);
            }
            /** Creates new Timeline object */
            TimelineFactory.prototype.createTimeline = function (parent, name, dataAccess, globalControls, startTime, timeSlice, binSize) {
                if (binSize === void 0) { binSize = 0; }
                return new Tline(parent, name, dataAccess, globalControls, startTime, timeSlice, binSize);
            };
            /** Puts the necessary HTML content onto the page */
            TimelineFactory.prototype.putTimelineHtmlContent = function (container, name) {
                var jContainer = $("." + container);
                jContainer.prepend("\
				<div class=\"row timeline-jumbo\"  id=\"" + name + "-wrapper\">\
	                <canvas id=\"" + name + "-scale-vertical\" class=\"timeline-canvas-scale-vertical\">\
	                </canvas>\
					<div class=\"timeline-canvas-wrapper\">\
		                <canvas id=\"" + name + "\" class=\"timeline-canvas\">\
		                </canvas>\
		                <div class=\"timeline-slider\" id=\"" + name + "-slider-left\">\
		                </div>\
		                <div class=\"timeline-slider\" id=\"" + name + "-slider-right\">\
		                </div>\
		                <canvas id=\"" + name + "-scale\" class=\"timeline-canvas-scale\">\
		                </canvas>\
		        	</div>\
		        </div>\
	        	");
            };
            /** Complete function for adding a new Timeline into the visualization. Creates the HTML content and returns a reference to object representing this timeline, which is used for all the manipulation */
            TimelineFactory.prototype.addTimeline = function (container, parent, name, dataAccess, globalControls, startTime, timeSlice, binSize) {
                if (binSize === void 0) { binSize = 0; }
                this.putTimelineHtmlContent(container, name);
                return this.createTimeline(parent, name, dataAccess, globalControls, startTime, timeSlice, binSize);
            };
            return TimelineFactory;
        }(BaseObject));
        timelinefactory.TimelineFactory = TimelineFactory;
    })(timelinefactory = tpx3.timelinefactory || (tpx3.timelinefactory = {}));
})(tpx3 || (tpx3 = {}));
/// <reference path="histogram-factory.ts" />
/// <reference path="timeline-factory.ts" />
var HistogramFactory = tpx3.histogramfactory.HistogramFactory;
var TimelineFactory = tpx3.timelinefactory.TimelineFactory;
var tpx3;
(function (tpx3) {
    var sensor;
    (function (sensor) {
        /** Class representing (data from) one single sensor/detector - contains a collection of separate histogram for different modes/values saved by the sensor */
        var Sensor = (function (_super) {
            __extends(Sensor, _super);
            /** Creates new sensor with its 3 Histograms (for 3 modes), its Timeline and its own Statistics */
            function Sensor(name, container, source, dataAccess, globalControls, strategy) {
                if (strategy === void 0) { strategy = Sensor.STATE_EXPANDED; }
                _super.call(this, name);
                this._currentState = 0;
                this._currentState = strategy;
                this._globalControls = globalControls;
                this._dataSource = source;
                var hFactory = new HistogramFactory("HistogramFactory");
                var tFactory = new TimelineFactory("TimelineFactory");
                this.initMaskedMatrix();
                this._histograms = [];
                if (strategy == Sensor.STATE_EXPANDED) {
                    var sContainer = this.createExpandedContainer(name, container);
                    this._histograms[0] = hFactory.addToAHistogram(this, this._name + "-toa", sContainer);
                    this._histograms[1] = hFactory.addToTHistogram(this, this._name + "-tot", sContainer);
                    this._histograms[2] = hFactory.addHitsHistogram(this, this._name + "-hits", sContainer);
                }
                else {
                    // collapsed state of the sensor histograms
                    this.createTabFrame(name, container);
                    this._histograms[0] = hFactory.addCollapsedHitsHistogram(this, this._name + "-hits", this._name);
                    this._histograms[1] = hFactory.addCollapsedToTHistogram(this, this._name + "-tot", this._name);
                    this._histograms[2] = hFactory.addCollapsedToAHistogram(this, this._name + "-toa", this._name);
                    $('#' + name + "-tot-nav-tab").removeClass("active");
                    $('#' + name + "-tot-tab").removeClass("active");
                    $('#' + name + "-hits-nav-tab").removeClass("active");
                    $('#' + name + "-hits-tab").removeClass("active");
                }
                this._statistics = new Statistics(name + "-stats", this);
                this._timeline = tFactory.addTimeline("timeline-container", this, this._name + "-timeline", dataAccess, globalControls, 0, 20000000);
                this.changeColormap(globalControls.getColorMap());
            }
            /** Initializes the matrix of masked pixels to 0 value (none of the pixels are masked) */
            Sensor.prototype.initMaskedMatrix = function () {
                this._maskedMatrix = [];
                for (var i = 0; i < 256; i++) {
                    this._maskedMatrix[i] = [];
                    for (var j = 0; j < 256; j++) {
                        this._maskedMatrix[i][j] = 0;
                    }
                }
            };
            /** Getter for the source parameter (name of the file from which the data are taken) */
            Sensor.prototype.getSource = function () {
                return this._dataSource;
            };
            /** Sets the file source for data to display */
            Sensor.prototype.setSource = function (filename) {
                this._dataSource = filename;
            };
            /** Creates a frame needed for putting all the histograms next to each other in expanded state, and adds it to the HTML page */
            Sensor.prototype.createExpandedContainer = function (name, container) {
                var jContainer = $('#' + container);
                var sContainer = name + "-container";
                jContainer.prepend("<div class=\"histogram-container\" id=\"" + sContainer + "\">\
				<h4>" + name + "<a href=\"#\" class=\"sensor-small-link sensor-collapse\" sensorName=\"" + name + "\"><span class=\"glyphicon glyphicon-resize-small\"></span> collapse</a>\
                </h4>\
				</div>\
				");
                return sContainer;
            };
            Sensor.prototype.getTimeline = function () {
                return this._timeline;
            };
            /** Creates a frame needed for putting all the histograms underneath each other in the collapsed state, and adds it to the HTML page */
            Sensor.prototype.createTabFrame = function (name, container) {
                var jContainer = $('#' + container);
                var size = "col-md-4";
                var link = "fullscreen";
                var glyphicon = "fullscreen";
                var expandLink = "<a href=\"#\" class=\"sensor-small-link sensor-expand\" sensorName=\"" + name + "\"> <span class=\"glyphicon glyphicon-new-window\"></span> expand</a></h4>";
                if (this._currentState == Sensor.STATE_FULLSCREEN) {
                    size = "col-md-12";
                    link = "collapse";
                    glyphicon = "resize-small";
                    expandLink = "";
                }
                jContainer.prepend("<div class=\"" + size + " histogram-container\" id=\"" + name + "-container\">\
                <h4>" + name + expandLink +
                    "<h4><a href=\"#\" class=\"sensor-small-link sensor-" + link + "\" sensorName=\"" + name + "\"><span class=\"glyphicon glyphicon-" + glyphicon + "\"></span> " + link + "</a>\
                </h4>\
                  <ul class=\"nav nav-tabs control-tab\" id=\"" + name + "-tab-nav\">\
                  </ul>\
                  <div class=\"tab-content\" id=\"" + name + "-tab-content\">\
                  </div>\
             </div>");
            };
            Sensor.prototype.redraw = function (newDataSource) {
                if (newDataSource === void 0) { newDataSource = null; }
                for (var i = 0; i < this._histograms.length; i++) {
                    this._histograms[i].redraw(newDataSource);
                }
                this._statistics.redraw(newDataSource);
                this._timeline.redraw(newDataSource);
            };
            /** Redraws the histograms, without redrawing the Timeline */
            Sensor.prototype.redrawHistograms = function (newDataSource) {
                if (newDataSource === void 0) { newDataSource = null; }
                for (var i = 0; i < this._histograms.length; i++) {
                    this._histograms[i].redraw(newDataSource);
                }
                this._statistics.redraw(newDataSource);
            };
            /** Changes colormap for each histogram */
            Sensor.prototype.changeColormap = function (colormap) {
                for (var i = 0; i < this._histograms.length; i++) {
                    this._histograms[i].changeColormap(colormap);
                }
            };
            Sensor.prototype.drawNoData = function () {
                for (var i = 0; i < this._histograms.length; i++) {
                    this._histograms[i].drawNoData();
                }
            };
            Sensor.prototype.updateSize = function () {
                for (var i = 0; i < this._histograms.length; i++) {
                    this._histograms[i].drawNoData();
                }
            };
            Sensor.prototype.updateViewport = function (startPixelX, startPixelY, zoomSize) {
                for (var i = 0; i < this._histograms.length; i++) {
                    this._histograms[i].updateViewport(startPixelX, startPixelY, zoomSize);
                }
            };
            Sensor.prototype.changeScale = function (scale) {
                for (var i = 0; i < this._histograms.length; i++) {
                    this._histograms[i].changeScale(scale);
                }
            };
            Sensor.prototype.toggleMaskingOn = function () {
                for (var i = 0; i < this._histograms.length; i++) {
                    this._histograms[i].toggleMaskingOn();
                }
            };
            Sensor.prototype.toggleMaskingOff = function () {
                for (var i = 0; i < this._histograms.length; i++) {
                    this._histograms[i].toggleMaskingOff();
                }
            };
            Sensor.prototype.toggleShowMaskedPixels = function () {
                for (var i = 0; i < this._histograms.length; i++) {
                    this._histograms[i].toggleShowMaskedPixels();
                }
            };
            /** Returns whether relative values should be shown (TRUE) or absolute (FALSE) */
            Sensor.prototype.showRelativeValues = function () {
                return this._globalControls.showRelativeValues();
            };
            /** Removes the HTML elements connected to this instance of sensor from the page and returns NULL.
                Does NOT effectively guarantee the destruction of the object! */
            Sensor.prototype.softDelete = function () {
                $("#" + this._name + "-container").remove();
                $("#" + this._name + "-stats").remove();
                $("#" + this._name + "-timeline-wrapper").remove();
                this._histograms = [];
                this._statistics = null;
                this._timeline = null;
                return null;
            };
            /** Represents collapsed state of the sensor */
            Sensor.STATE_COLLAPSED = 0;
            /** Represents expanded state of the sensor */
            Sensor.STATE_EXPANDED = 1;
            /** Represents fullscreen state of the sensor */
            Sensor.STATE_FULLSCREEN = 2;
            return Sensor;
        }(BaseObject));
        sensor.Sensor = Sensor;
    })(sensor = tpx3.sensor || (tpx3.sensor = {}));
})(tpx3 || (tpx3 = {}));
var tpx3;
(function (tpx3) {
    var statistics;
    (function (statistics) {
        /**
        Class representing the display of the statistic information about the data on the screen. Can be redrawn in event of change of the data.
        */
        var Statistics = (function (_super) {
            __extends(Statistics, _super);
            /** Get the pixel data */
            function Statistics(name, parent, data) {
                if (data === void 0) { data = null; }
                _super.call(this, name);
                this._parent = parent;
                this._data = data;
                this.putHtmlContent();
            }
            /** Show the statistics about the correct actual data */
            Statistics.prototype.redraw = function (newDataSource) {
                if (newDataSource === void 0) { newDataSource = null; }
                if (newDataSource) {
                    var data = newDataSource.getData(this._parent.getSource());
                    if (data) {
                        this._data = data;
                    }
                }
                if (!this._data) {
                    this.drawNoData();
                    return;
                }
                $("#" + this._parent._name + "-statistics-size").html(this._data.Size.toString());
                $("#" + this._parent._name + "-statistics-triggerCnt").html(this._data.TriggerCount.toString());
                $("#" + this._parent._name + "-statistics-sumEnergy").html(this._data.SumEnergy.toString());
                $("#" + this._parent._name + "-statistics-offset").html(this._data.Offset.toString());
            };
            /** Shows all zeroes. Usually when no data were received from the server. */
            Statistics.prototype.drawNoData = function () {
                $("#" + this._parent._name + "-statistics-size").html("0");
                $("#" + this._parent._name + "-statistics-triggerCnt").html("0");
                $("#" + this._parent._name + "-statistics-sumEnergy").html("0");
                $("#" + this._parent._name + "-statistics-offset").html("0");
            };
            /** Do nothing, as resizing has no influence on showing statistics. */
            Statistics.prototype.updateSize = function () {
            };
            /** Puts the necessary HTML content on the page to show the statistics */
            Statistics.prototype.putHtmlContent = function () {
                var jContainer = $('#statistics');
                var filename = this._parent.getSource().substr(0, this._parent.getSource().length - 5) + ".png";
                jContainer.prepend("\
        <table id=\"" + this._name + "\">\
          <tbody>\
            <tr>\
              <td class=\"td-left\" colspan=\"2\">Stats for: " + this._parent._name + "</td>\
            </tr>\
            <tr>\
              <td># of pixel hits</td>\
              <td id=\"" + this._parent._name + "-statistics-size\">0</td>\
            </tr>\
            <tr>\
              <td>Trigger count</td>\
              <td id=\"" + this._parent._name + "-statistics-triggerCnt\">0</td>\
            </tr>\
            <tr>\
              <td>Summed energy</td>\
              <td id=\"" + this._parent._name + "-statistics-sumEnergy\">0</td>\
            </tr>\
            <tr>\
              <td>Offset</td>\
              <td><input id=\"" + this._parent._name + "-statistics-offset\" filename=\"" + this._parent.getSource() + "\" class=\"offset-input\" type=\"text\" value=\"0\"></input><span id=\"offset-units\">[s]</span></td>\
            </tr>\
            <tr>\
              <td>Preview</td>\
              <td><a target=\"_blank\" href=\"rootfiles/preview/" + filename + "\"><img width=\"100%\" src=\"rootfiles/preview/" + filename + "\" /></a></td>\
            </tr>\
          </tbody>\
        </table>");
            };
            Statistics.prototype.changeColormap = function (colormap) {
            };
            return Statistics;
        }(BaseObject));
        statistics.Statistics = Statistics;
    })(statistics = tpx3.statistics || (tpx3.statistics = {}));
})(tpx3 || (tpx3 = {}));
/// <reference path="sensor.ts" />
/// <reference path="timeline.ts" />
/// <reference path="statistics.ts" />
var tpx3;
(function (tpx3) {
    var globalcontrols;
    (function (globalcontrols) {
        /** Class responsible for handling and delegating global settings of the visualization, which is needed to for all the individual parts.
        Contains common settings and controls, like bin size for all timelines, color map for all the histograms, etc. */
        var GlobalControls = (function (_super) {
            __extends(GlobalControls, _super);
            function GlobalControls(name) {
                if (name === void 0) { name = ""; }
                _super.call(this, name);
                this._sensorsList = [];
                this._showCursor = true;
                this._showRelativeValues = true;
                this._binSize = 0;
                this._snapToNonZero = false;
                this._timeSliceHistory = [];
                this._startTimeHistory = [];
                this._historyLimit = 10;
                this._unitDisplayer = new UnitDisplayer();
                this._unitDisplayer.setCurrentUnits(UnitDisplayer.BASE);
            }
            /** Adds a new sensor which was added to the visualization */
            GlobalControls.prototype.addSensor = function (newSensor, name) {
                this._sensorsList[name] = newSensor;
            };
            /** Checks if sensor with specified name is present */
            GlobalControls.prototype.has = function (name) {
                if (this._sensorsList[name] !== undefined && this._sensorsList[name] !== null) {
                    return true;
                }
                else {
                    return false;
                }
            };
            /** Removes sensor from list of current active sensors */
            GlobalControls.prototype.removeSensor = function (name) {
                if (this.has(name)) {
                    delete this._sensorsList[name];
                }
            };
            /** Toggles masking tool for all the sensors of the visualization */
            GlobalControls.prototype.toggleMaskingOn = function () {
                for (var i in this._sensorsList) {
                    this._sensorsList[i].toggleMaskingOn();
                }
            };
            /** Toggles masking tool for all the sensors of the visualization */
            GlobalControls.prototype.toggleMaskingOff = function () {
                for (var i in this._sensorsList) {
                    this._sensorsList[i].toggleMaskingOff();
                }
            };
            /** Toggles the visibility of the coincidency cursor */
            GlobalControls.prototype.toggleShowCursor = function () {
                this._showCursor = !this._showCursor;
                if (!this._showCursor) {
                    this.highlightBin(-1);
                }
            };
            /** Getter for _showCursor attribute */
            GlobalControls.prototype.showCursor = function () {
                return this._showCursor;
            };
            /** Getter for _showRelativeValues attribute*/
            GlobalControls.prototype.showRelativeValues = function () {
                return this._showRelativeValues;
            };
            /** Changes scale for all the sensors of the visualization */
            GlobalControls.prototype.changeScale = function (newScale) {
                this.setScale(newScale);
                for (var i in this._sensorsList) {
                    this._sensorsList[i].changeScale(newScale);
                }
            };
            /** Changes color map for all the sensors of the visualization */
            GlobalControls.prototype.changeColorMap = function (newColorMap) {
                this.setColorMap(newColorMap);
                for (var i in this._sensorsList) {
                    this._sensorsList[i].changeColormap(newColorMap);
                }
            };
            GlobalControls.prototype.setStartTime = function (newTime) {
                if (this._startTimeHistory.length >= this._historyLimit) {
                    this._startTimeHistory.shift();
                }
                this._startTimeHistory.push(this._startTime);
                this._startTime = newTime;
            };
            GlobalControls.prototype.getStartTime = function () {
                return this._startTime;
            };
            GlobalControls.prototype.setTimeSlice = function (newSlice) {
                if (this._timeSliceHistory.length >= this._historyLimit) {
                    this._timeSliceHistory.shift();
                }
                this._timeSliceHistory.push(this._timeSlice);
                this._timeSlice = newSlice;
            };
            /** Returns settings of _starTime and _timeSlice to previousl values */
            GlobalControls.prototype.undoTimeSettings = function () {
                if (this._startTimeHistory.length == 1 || this._timeSliceHistory.length == 1) {
                    return false;
                }
                this._startTime = this._startTimeHistory.pop();
                this._timeSlice = this._timeSliceHistory.pop();
                return true;
            };
            GlobalControls.prototype.getTimeSlice = function () {
                return this._timeSlice;
            };
            GlobalControls.prototype.setBinSize = function (newBinSize) {
                this._binSize = newBinSize;
                for (var i in this._sensorsList) {
                    this._sensorsList[i].getTimeline().setBinSize(this._binSize);
                    if (this._binSize != this._sensorsList[i].getTimeline().getBinSize()) {
                        this._binSize = this._sensorsList[i].getTimeline().getBinSize();
                    }
                }
                $('#bin-size-input').val(this.getBinSize());
            };
            GlobalControls.prototype.getBinSize = function () {
                return this._binSize;
            };
            GlobalControls.prototype.getUnitDisplayer = function () {
                return this._unitDisplayer;
            };
            /** Highlights a single bin on all the timelines */
            GlobalControls.prototype.highlightBin = function (binNumber) {
                for (var i in this._sensorsList) {
                    this._sensorsList[i].getTimeline().highlightBin(binNumber);
                }
            };
            /** Gets current state of timelines' snapping functionality */
            GlobalControls.prototype.getTimelineSnapping = function () {
                return this._snapToNonZero;
            };
            GlobalControls.prototype.toggleSnapping = function () {
                this._snapToNonZero = !this._snapToNonZero;
                if (this._snapToNonZero) {
                    this.toggleSnappingOn();
                }
                else {
                    this.toggleSnappingOff();
                }
            };
            /** Switches off snapping to non-zeroes values functionality for all the timelines */
            GlobalControls.prototype.toggleSnappingOff = function () {
                this._snapToNonZero = false;
                for (var i in this._sensorsList) {
                    this._sensorsList[i].getTimeline().toggleSnappingOff();
                }
            };
            /** Switches on the snapping functionality */
            GlobalControls.prototype.toggleSnappingOn = function () {
                this._snapToNonZero = true;
                for (var i in this._sensorsList) {
                    this._sensorsList[i].getTimeline().toggleSnappingOn();
                }
            };
            /** Selects the option to show the masked pixels for all the sensors */
            GlobalControls.prototype.toggleShowMaskedPixels = function () {
                for (var i in this._sensorsList) {
                    this._sensorsList[i].toggleShowMaskedPixels();
                }
            };
            GlobalControls.prototype.toggleShowRelativeValuesOn = function () {
                this._showRelativeValues = true;
            };
            GlobalControls.prototype.toggleShowRelativeValuesOff = function () {
                this._showRelativeValues = false;
            };
            GlobalControls.prototype.exportSettings = function () {
            };
            GlobalControls.prototype.importSettings = function () {
            };
            GlobalControls.prototype.setColorMap = function (newMap) {
                this._colorMap = newMap;
            };
            GlobalControls.prototype.getColorMap = function () {
                return this._colorMap;
            };
            GlobalControls.prototype.setScale = function (newScale) {
                this._scale = newScale;
            };
            return GlobalControls;
        }(BaseObject));
        globalcontrols.GlobalControls = GlobalControls;
    })(globalcontrols = tpx3.globalcontrols || (tpx3.globalcontrols = {}));
})(tpx3 || (tpx3 = {}));
/// <reference path="tools.ts" />
var tpx3;
(function (tpx3) {
    var unitdisplayer;
    (function (unitdisplayer) {
        var UnitDisplayer = (function (_super) {
            __extends(UnitDisplayer, _super);
            function UnitDisplayer() {
                _super.call(this, "UnitDisplayer");
                this._currentUnits = UnitDisplayer.NANO;
            }
            UnitDisplayer.prototype.getCurrentUnits = function () {
                return this._currentUnits;
            };
            UnitDisplayer.prototype.setCurrentUnits = function (newUnits) {
                this._currentUnits = newUnits;
            };
            UnitDisplayer.prototype.parseValueWithUnits = function (inputString) {
                var arr = inputString.split(" ");
                var units = arr[1].slice(0, -1);
                switch (units) {
                    case "n":
                        this.setCurrentUnits(UnitDisplayer.NANO);
                        break;
                    case "μ":
                        this.setCurrentUnits(UnitDisplayer.MICRO);
                        break;
                    case "m":
                        this.setCurrentUnits(UnitDisplayer.MILLI);
                        break;
                    case "":
                        this.setCurrentUnits(UnitDisplayer.BASE);
                        break;
                }
                return (+arr[0]);
            };
            UnitDisplayer.prototype.getUnitName = function (inputUnits) {
                if (inputUnits === void 0) { inputUnits = 0; }
                if (inputUnits == 0) {
                    inputUnits = this.getCurrentUnits();
                }
                switch (inputUnits) {
                    case UnitDisplayer.NANO:
                        return "nano";
                    case UnitDisplayer.MICRO:
                        return "micro";
                    case UnitDisplayer.MILLI:
                        return "milli";
                    case UnitDisplayer.BASE:
                        return "";
                    default:
                        return "";
                }
            };
            UnitDisplayer.prototype.getUnitShortcut = function (inputUnits) {
                if (inputUnits === void 0) { inputUnits = 0; }
                if (inputUnits == 0) {
                    inputUnits = this.getCurrentUnits();
                }
                switch (inputUnits) {
                    case UnitDisplayer.NANO:
                        return "n";
                    case UnitDisplayer.MICRO:
                        return "μ";
                    case UnitDisplayer.MILLI:
                        return "m";
                    case UnitDisplayer.BASE:
                        return "";
                    default:
                        return "";
                }
            };
            UnitDisplayer.prototype.getBestUnits = function (inputValue, inputFormat) {
                var value = this.convert(inputValue, inputFormat, UnitDisplayer.NANO);
                var negative = false;
                if (value < 0) {
                    value = Math.abs(value);
                    negative = true;
                }
                this._currentUnits = UnitDisplayer.NANO;
                if (value > 99) {
                    value /= 1000;
                    this._currentUnits = UnitDisplayer.MICRO;
                    if (value > 99) {
                        value /= 1000;
                        this._currentUnits = UnitDisplayer.MILLI;
                        if (value > 99) {
                            value /= 1000;
                            this._currentUnits = UnitDisplayer.BASE;
                        }
                    }
                }
                if (negative) {
                    value = 0 - value;
                }
                return value;
            };
            UnitDisplayer.prototype.toSeconds = function (inputNumber, inputFormat) {
                if (inputFormat === void 0) { inputFormat = UnitDisplayer.NANO; }
                return this.convert(inputNumber, inputFormat, UnitDisplayer.BASE);
            };
            UnitDisplayer.prototype.convert = function (inputNumber, inputFormat, outputFormat) {
                return inputNumber * outputFormat / inputFormat;
            };
            UnitDisplayer.GIGA = 0.000000001;
            UnitDisplayer.MEGA = 0.000001;
            UnitDisplayer.KILO = 0.001;
            UnitDisplayer.BASE = 1;
            UnitDisplayer.MILLI = 1000;
            UnitDisplayer.MICRO = 1000000;
            UnitDisplayer.NANO = 1000000000;
            /** Trivial */
            UnitDisplayer.nanoTobase = function (nanoValue) {
                return nanoValue / UnitDisplayer.NANO;
            };
            /** Trivial */
            UnitDisplayer.baseToNano = function (secValue) {
                return secValue * UnitDisplayer.NANO;
            };
            /** Trivial */
            UnitDisplayer.baseToMilli = function (secValue) {
                return secValue * UnitDisplayer.MILLI;
            };
            UnitDisplayer.milliToBase = function (milliValue) {
                return milliValue / UnitDisplayer.MILLI;
            };
            return UnitDisplayer;
        }(BaseObject));
        unitdisplayer.UnitDisplayer = UnitDisplayer;
    })(unitdisplayer = tpx3.unitdisplayer || (tpx3.unitdisplayer = {}));
})(tpx3 || (tpx3 = {}));
/// <reference path="sensor.ts" />
/// <reference path="timeline.ts" />
/// <reference path="statistics.ts" />
/// <reference path="redrawer.ts" />
/// <reference path="unit-displayer.ts" />
var GlobalControls = tpx3.globalcontrols.GlobalControls;
var Sensor = tpx3.sensor.Sensor;
var Timeline = tpx3.timeline.Timeline;
var WhiteDecorator = tpx3.colormap.WhiteBackgroundDecorator;
var Statistics = tpx3.statistics.Statistics;
var CubehelixMap = tpx3.colormap.CubehelixMap;
var Redrawer = tpx3.redrawer.Redrawer;
var TimelineRedrawer = tpx3.redrawer.TimelineRedrawer;
var UnitDisplayer = tpx3.unitdisplayer.UnitDisplayer;
// ====================== DECLARATION, INITIALIZATION =======================
var globalServerAddress;
var white = false;
var animating = false;
var dataAccess = new DataAccess(globalServerAddress);
var pixelsData;
var redrawer = new Redrawer("HistogramsRedrawer");
var timelineRedrawer = new TimelineRedrawer("TimelineRedrawer");
var scale = new LinearScale();
var logScale = new LogarithmicScale();
var colormap;
var greymap = new GreyscaleMap();
var jetmap = new JetMap();
var hotmap = new HotMap();
var cubehelixmap = new CubehelixMap();
var globalControls = new GlobalControls("GlobalControls");
var currFolder = "";
// ===================== END DECLARATION, INITIALIZATION ======================
// set the default initial colormap to all the histograms
colormap = hotmap;
globalControls.setStartTime(0);
globalControls.setTimeSlice(20000000);
globalControls.setColorMap(colormap);
var timeStartUnits = new UnitDisplayer();
timeStartUnits.setCurrentUnits(UnitDisplayer.BASE);
var timeSliceUnits = new UnitDisplayer();
timeSliceUnits.setCurrentUnits(UnitDisplayer.BASE);
var binSizeUnits = new UnitDisplayer();
binSizeUnits.setCurrentUnits(UnitDisplayer.BASE);
var offsetUnits = new UnitDisplayer();
offsetUnits.setCurrentUnits(UnitDisplayer.BASE);
var sensor1 = new Sensor("sensor_neutrons01", "histograms-row", "neutrons01.root", dataAccess, globalControls, Sensor.STATE_COLLAPSED);
/*
Register all observers to a Redrawer object, so they will react on a change in the currently-to-be-displayed data
*/
redrawer.subscribeObserver(sensor1, "sensor_neutrons01");
timelineRedrawer.subscribeObserver(sensor1.getTimeline(), "sensor_neutrons01");
globalControls.addSensor(sensor1, "sensor_neutrons01");
globalControls.setBinSize(0);
var folderName = "";
// get the initial data and draw them
dataAccess.getNewData(globalControls.getStartTime(), globalControls.getTimeSlice(), sensor1.getSource(), function (data) {
    redrawer.redrawAll(dataAccess);
});
// draw previews of the colormaps
greymap.preview("greyscale-canvas");
hotmap.preview("hot-canvas");
jetmap.preview("jet-canvas");
cubehelixmap.preview("cubehelix-canvas");
// display the default values in correct fields
$('#time-start-input').val(timeStartUnits.convert(globalControls.getStartTime(), UnitDisplayer.NANO, timeStartUnits.getCurrentUnits()));
$('#time-slice-input').val(timeSliceUnits.convert(globalControls.getTimeSlice(), UnitDisplayer.NANO, timeSliceUnits.getCurrentUnits()));
$('#bin-size-input').val(binSizeUnits.convert(globalControls.getBinSize(), UnitDisplayer.NANO, binSizeUnits.getCurrentUnits()));
$('.allowed-tags').append(dataAccess.getAllowedFilterTags().join(" "));
function updateRootListByDate(newDate) {
    if (newDate === void 0) { newDate = null; }
    var folders = newDate.format("YYYY") + "/" + newDate.format("MM") + "/" + newDate.format("DD") + "/" + newDate.format("HH");
    // get the data about available root files on the server
    dataAccess.getRootFilesList(folders, function (data) {
        $("#rootfiles").html("");
        // do something with the data
        if (data && data.Files) {
            for (var i = 0; i < data.Files.length; i++) {
                var name_1 = "sensor_" + data.Files[i].split(".root")[0];
                name_1 = name_1.replace(/\./g, '_');
                $("#rootfiles").prepend("<li class=\"root-select-li\"><a class=\"root-select-a\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"" + data.Files[i] + "\" filename=\"" + data.Files[i] + "\" href=\"#\"><input class=\"root-select-check\"  filename=\"" + data.Files[i] + "\" type=\"checkbox\" id=\"" + name_1 + "-checkbox\"></input> " + data.Files[i] + "</a></li>");
                if (redrawer.has(name_1)) {
                    $('#' + name_1 + '-checkbox').prop("checked", "checked");
                }
            }
        }
    });
}
function updateRootListByFolder(folders) {
    if (folders === void 0) { folders = null; }
    // get the data about available root files on the server
    dataAccess.getRootFilesList(folders, function (data) {
        $("#rootfiles").html("");
        // do something with the data
        if (data && data.Files) {
            for (var i = 0; i < data.Files.length; i++) {
                var name_2 = "sensor_" + data.Files[i].split(".root")[0];
                name_2 = name_2.replace(/\./g, '_');
                $("#rootfiles").prepend("<li class=\"root-select-li\"><a class=\"root-select-a\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"" + data.Files[i] + "\" filename=\"" + data.Files[i] + "\" href=\"#\"><input class=\"root-select-check\"  filename=\"" + data.Files[i] + "\" type=\"checkbox\" id=\"" + name_2 + "-checkbox\"></input> " + data.Files[i] + "</a></li>");
                if (redrawer.has(name_2)) {
                    $('#' + name_2 + '-checkbox').prop("checked", "checked");
                }
            }
        }
    });
}
// ===================== START REGISTER ALL EVENT CALLBACKS =========================================
//                        (let's make things interactive)
$(document).on('click', '.root-select-a', function () {
    var filename = $(this).attr("filename");
    $('.ajax-loading').fadeIn(50);
    var name = ("sensor_" + filename.split(".root")[0]);
    name = name.replace(/\./g, "_");
    if (redrawer.has(name)) {
        (redrawer.get(name)).softDelete();
        redrawer.unsubscribeObserver(name);
        timelineRedrawer.unsubscribeObserver(name);
        globalControls.removeSensor(name);
        dataAccess.removeRootFile(filename);
        $('.ajax-loading').fadeOut(50);
        $('#' + name + "-checkbox").removeProp("checked");
        return;
    }
    var newSensor = new Sensor(name, "histograms-row", filename, dataAccess, globalControls, Sensor.STATE_COLLAPSED);
    redrawer.subscribeObserver(newSensor, name);
    timelineRedrawer.subscribeObserver(newSensor.getTimeline(), name);
    globalControls.addSensor(newSensor, name);
    $('#' + name + "-checkbox").prop("checked", "checked");
    dataAccess.getNewData(globalControls.getStartTime(), globalControls.getTimeSlice(), filename, function (data) {
        redrawer.redrawAll(dataAccess);
        $('.ajax-loading').fadeOut(50);
    });
});
$('.theme-select').on('click', function () {
    $('.dropdown-menu input:radio').removeProp("checked");
    var name = $(this).attr("filename");
    $('#css-style').attr("href", "stylesheets/css/" + name + ".css");
    $('#' + name + "-check").prop("checked", "checked");
});
$('#remove-all-a').on('click', function () {
});
$(document).on('click', '.sensor-expand', function () {
    var name = $(this).attr("sensorName");
    var filename = (redrawer.get(name)).getSource();
    (redrawer.get(name)).softDelete();
    redrawer.unsubscribeObserver(name);
    timelineRedrawer.unsubscribeObserver(name);
    globalControls.removeSensor(name);
    var newSensor = new Sensor(name, "histograms-row", filename, dataAccess, globalControls, Sensor.STATE_EXPANDED);
    redrawer.subscribeObserver(newSensor, name);
    timelineRedrawer.subscribeObserver(newSensor.getTimeline(), name);
    globalControls.addSensor(newSensor, name);
    newSensor.redraw(dataAccess);
    return false;
});
$(document).on('click', '.sensor-fullscreen', function () {
    var name = $(this).attr("sensorName");
    var filename = (redrawer.get(name)).getSource();
    (redrawer.get(name)).softDelete();
    redrawer.unsubscribeObserver(name);
    timelineRedrawer.unsubscribeObserver(name);
    globalControls.removeSensor(name);
    var newSensor = new Sensor(name, "histograms-row", filename, dataAccess, globalControls, Sensor.STATE_FULLSCREEN);
    redrawer.subscribeObserver(newSensor, name);
    timelineRedrawer.subscribeObserver(newSensor.getTimeline(), name);
    globalControls.addSensor(newSensor, name);
    newSensor.redraw(dataAccess);
    return false;
});
$(document).on('click', '.sensor-collapse', function () {
    var name = $(this).attr("sensorName");
    var filename = (redrawer.get(name)).getSource();
    (redrawer.get(name)).softDelete();
    redrawer.unsubscribeObserver(name);
    timelineRedrawer.unsubscribeObserver(name);
    globalControls.removeSensor(name);
    var newSensor = new Sensor(name, "histograms-row", filename, dataAccess, globalControls, Sensor.STATE_COLLAPSED);
    redrawer.subscribeObserver(newSensor, name);
    globalControls.addSensor(newSensor, name);
    newSensor.redraw(dataAccess);
    return false;
});
$(document).keydown(function (e) {
    switch (e.which) {
        case 37:
            $('#timeline-prev-button').click();
            break;
        case 38:
            $('#intensity-up').click();
            break;
        case 39:
            $('#timeline-next-button').click();
            break;
        case 40:
            $('#intensity-down').click();
            break;
        default:
            return;
    }
    e.preventDefault();
});
$(document).on("swipeleft", function () {
    $('#timeline-next-button').click();
});
$('#masking-row').on('click', function () {
    globalControls.toggleMaskingOn();
    redrawer.redrawAll();
    $('#masking-radio').prop("checked", "checked");
    $('#zooming-radio').removeProp("checked");
    $('#masking-row').addClass("tool-selected");
    $('#zooming-row').removeClass("tool-selected");
    return false;
});
$('#masking-checkbox').on('click', function () {
    globalControls.toggleShowMaskedPixels();
    redrawer.redrawAll();
});
$('#cursor-checkbox').on('click', function () {
    globalControls.toggleShowCursor();
});
$('#zooming-row').on('click', function () {
    globalControls.toggleMaskingOff();
    redrawer.redrawAll();
    $('#zooming-radio').prop("checked", "checked");
    $('#masking-radio').removeProp("checked");
    $('#zooming-row').addClass("tool-selected");
    $('#masking-row').removeClass("tool-selected");
    return false;
});
$('#values-relative').on('click', function () {
    globalControls.toggleShowRelativeValuesOn();
    $('#values-absolute').removeClass("active-link");
    $(this).addClass("active-link");
    redrawer.redrawAll(dataAccess);
    return false;
});
$('#values-absolute').on('click', function () {
    globalControls.toggleShowRelativeValuesOff();
    $('#values-relative').removeClass("active-link");
    $(this).addClass("active-link");
    redrawer.redrawAll(dataAccess);
    return false;
});
$('#scale-linear').on('click', function () {
    globalControls.changeScale(scale);
    $(this).addClass("active-link");
    $('#scale-logarithmic').removeClass("active-link");
    redrawer.redrawAll();
    return false;
});
$('#scale-logarithmic').on('click', function () {
    globalControls.changeScale(logScale);
    $(this).addClass("active-link");
    $('#scale-linear').removeClass("active-link");
    redrawer.redrawAll();
    return false;
});
$('#intensity-up').on('click', function () {
    colormap.boost();
    redrawer.redrawAll();
    return false;
});
$('#intensity-down').on('click', function () {
    colormap.toneDown();
    redrawer.redrawAll();
    return false;
});
$('#intensity-reset').on('click', function () {
    colormap.boost(0);
    redrawer.redrawAll();
    return false;
});
$('#jet-canvas').on('click', function () {
    white = false;
    $("#colormap-white-bckg").removeClass("active-link");
    colormap = jetmap;
    globalControls.changeColorMap(colormap);
    redrawer.redrawAll();
    return false;
});
$('#cubehelix-canvas').on('click', function () {
    white = false;
    $("#colormap-white-bckg").removeClass("active-link");
    colormap = cubehelixmap;
    globalControls.changeColorMap(colormap);
    redrawer.redrawAll();
    return false;
});
$('#hot-canvas').on('click', function () {
    white = false;
    $("#colormap-white-bckg").removeClass("active-link");
    colormap = hotmap;
    globalControls.changeColorMap(colormap);
    redrawer.redrawAll();
    return false;
});
$('#greyscale-canvas').on('click', function () {
    white = false;
    $("#colormap-white-bckg").removeClass("active-link");
    colormap = greymap;
    globalControls.changeColorMap(colormap);
    redrawer.redrawAll();
    return false;
});
$('#colormap-white-bckg').on('click', function () {
    if (!white) {
        $(this).addClass("active-link");
        white = true;
        var whiteColormap = new WhiteDecorator(colormap);
        globalControls.changeColorMap(whiteColormap);
        redrawer.redrawAll();
    }
    else {
        $(this).removeClass("active-link");
        white = false;
        globalControls.changeColorMap(colormap);
        redrawer.redrawAll();
    }
    return false;
});
$('#time-start-input').on('change', function () {
    var numValue = +this.value;
    var setUnits = false;
    if (isNaN(numValue)) {
        numValue = timeStartUnits.parseValueWithUnits(this.value);
        setUnits = true;
    }
    globalControls.setTimeSlice(globalControls.getTimeSlice());
    globalControls.setStartTime(timeStartUnits.convert(numValue, timeStartUnits.getCurrentUnits(), UnitDisplayer.NANO));
    if (!setUnits) {
        this.value = timeStartUnits.getBestUnits(globalControls.getStartTime(), UnitDisplayer.NANO);
    }
    else {
        this.value = timeStartUnits.convert(globalControls.getStartTime(), UnitDisplayer.NANO, timeStartUnits.getCurrentUnits());
    }
    $('#time-start-units').html('[' + timeStartUnits.getUnitShortcut() + 's]');
});
$('#time-slice-input').on('change', function () {
    var numValue = +this.value;
    var setUnits = false;
    if (isNaN(numValue)) {
        numValue = timeSliceUnits.parseValueWithUnits(this.value);
        setUnits = true;
    }
    globalControls.setStartTime(globalControls.getStartTime());
    globalControls.setTimeSlice(timeSliceUnits.convert(numValue, timeSliceUnits.getCurrentUnits(), UnitDisplayer.NANO));
    if (!setUnits) {
        this.value = timeSliceUnits.getBestUnits(globalControls.getTimeSlice(), UnitDisplayer.NANO);
    }
    else {
        this.value = timeSliceUnits.convert(globalControls.getTimeSlice(), UnitDisplayer.NANO, timeSliceUnits.getCurrentUnits());
    }
    $('#time-slice-units').html('[' + timeSliceUnits.getUnitShortcut() + 's]');
});
$('#bin-size-input').on('change', function () {
    var numValue = +this.value;
    var setUnits = false;
    if (isNaN(numValue)) {
        numValue = binSizeUnits.parseValueWithUnits(this.value);
        setUnits = true;
    }
    globalControls.setBinSize(binSizeUnits.convert(numValue, binSizeUnits.getCurrentUnits(), UnitDisplayer.NANO));
    if (!setUnits) {
        this.value = binSizeUnits.getBestUnits(globalControls.getBinSize(), UnitDisplayer.NANO);
    }
    else {
        this.value = binSizeUnits.convert(globalControls.getBinSize(), UnitDisplayer.NANO, binSizeUnits.getCurrentUnits());
    }
    $('#bin-size-units').html('[' + binSizeUnits.getUnitShortcut() + 's]');
    timelineRedrawer.redrawAll(dataAccess);
});
$(document).on('change', '.offset-input', function () {
    var numValue = +this.value;
    var setUnits = false;
    if (isNaN(numValue)) {
        numValue = offsetUnits.parseValueWithUnits(this.value);
        setUnits = true;
    }
    var nanoValue = offsetUnits.convert(numValue, offsetUnits.getCurrentUnits(), UnitDisplayer.NANO);
    dataAccess.setOffset(nanoValue, $(this).attr("filename"));
    if (!setUnits) {
        this.value = offsetUnits.getBestUnits(nanoValue, UnitDisplayer.NANO);
    }
    else {
        this.value = offsetUnits.convert(nanoValue, UnitDisplayer.NANO, offsetUnits.getCurrentUnits());
    }
    $('#offset-units').html('[' + offsetUnits.getUnitShortcut() + 's]');
});
$('#timeline-snap-toggle').on('click', function () {
    globalControls.toggleSnapping();
});
$('#timeline-prev-button').on('click', function () {
    $('.ajax-loading').fadeIn(50);
    $('#time-start-input').val(timeStartUnits.convert(+globalControls.getStartTime(), UnitDisplayer.NANO, timeStartUnits.getCurrentUnits()) - timeSliceUnits.convert(+$('#time-slice-input').val(), timeSliceUnits.getCurrentUnits(), timeStartUnits.getCurrentUnits())).change();
    dataAccess.getNewData(globalControls.getStartTime(), globalControls.getTimeSlice(), "", function (data) {
        redrawer.redrawAll(dataAccess);
        $('.ajax-loading').fadeOut(50);
    });
    return false;
});
$('#timeline-next-button').on('click', function () {
    $('.ajax-loading').fadeIn(50);
    $('#time-start-input').val(timeStartUnits.convert(+globalControls.getStartTime(), UnitDisplayer.NANO, timeStartUnits.getCurrentUnits()) + timeSliceUnits.convert(+$('#time-slice-input').val(), timeSliceUnits.getCurrentUnits(), timeStartUnits.getCurrentUnits())).change();
    dataAccess.getNewData(globalControls.getStartTime(), globalControls.getTimeSlice(), "", function (data) {
        redrawer.redrawAll(dataAccess);
        $('.ajax-loading').fadeOut(50);
    });
    return false;
});
$('#timeline-update-button').on('click', function () {
    $('.ajax-loading').fadeIn(50);
    var longRequest = true;
    setTimeout(function () {
        if (longRequest) {
            $('#abort-request-span').fadeIn(50);
        }
    }, 2500);
    dataAccess.getNewData(globalControls.getStartTime(), globalControls.getTimeSlice(), "", function (data) {
        if (dataAccess.getCurrentRequest().statusText == "abort") {
            $('#abort-request-span').fadeOut(50);
            $('.ajax-loading').fadeOut(50);
            return false;
        }
        redrawer.redrawAll(dataAccess);
        longRequest = false;
        $("#bin-size-input").val(0);
        $('#bin-size-input').change();
        $('#abort-request-span').fadeOut(50);
        $('.ajax-loading').fadeOut(50);
    });
    return false;
});
$('#abort-request').on('click', function () {
    globalControls.undoTimeSettings();
    $('#time-start-input').val(timeStartUnits.convert(globalControls.getStartTime(), UnitDisplayer.NANO, timeStartUnits.getCurrentUnits()));
    $('#time-slice-input').val(timeSliceUnits.convert(globalControls.getTimeSlice(), UnitDisplayer.NANO, timeSliceUnits.getCurrentUnits()));
    dataAccess.abortCurrentRequest();
    $('.ajax-loading').fadeOut(50);
    $('#abort-request-span').fadeOut(50);
});
$('#timeline-zoom-button').on('click', function () {
    globalControls.setStartTime(+$('#timeline-startTime').attr("trueValue") + globalControls.getStartTime());
    globalControls.setTimeSlice(+$('#timeline-deltaTime').attr("trueValue"));
    $('#time-start-input').val(timeStartUnits.convert(globalControls.getStartTime(), UnitDisplayer.NANO, timeStartUnits.getCurrentUnits()));
    $('#time-slice-input').val(timeSliceUnits.convert(globalControls.getTimeSlice(), UnitDisplayer.NANO, timeSliceUnits.getCurrentUnits()));
    dataAccess.getNewData(globalControls.getStartTime(), globalControls.getTimeSlice(), "", function (data) {
        redrawer.redrawAll(dataAccess);
        $("#bin-size-input").val(0);
        $('#bin-size-input').change();
        $('.ajax-loading').fadeOut(50);
    });
    return false;
});
$('#timeline-undo-button').on('click', function () {
    if (!globalControls.undoTimeSettings()) {
        return;
    }
    $('#time-start-input').val(timeStartUnits.convert(globalControls.getStartTime(), UnitDisplayer.NANO, timeStartUnits.getCurrentUnits()));
    $('#time-slice-input').val(timeSliceUnits.convert(globalControls.getTimeSlice(), UnitDisplayer.NANO, timeSliceUnits.getCurrentUnits()));
    dataAccess.getNewData(globalControls.getStartTime(), globalControls.getTimeSlice(), "", function (data) {
        redrawer.redrawAll(dataAccess);
        $('#bin-size-input').change();
        $('.ajax-loading').fadeOut(50);
    });
    return false;
});
function animate() {
    if (!animating) {
        return;
    }
    var shown = false;
    var animationDelay = Tools.secondsToMilli($('#animation-input').val());
    setTimeout(function () {
        if (!shown) {
            $('.ajax-loading').fadeIn(50);
        }
    }, 300);
    $('#time-start-input').val(timeStartUnits.convert(+globalControls.getStartTime(), UnitDisplayer.NANO, timeStartUnits.getCurrentUnits()) + timeSliceUnits.convert(+$('#time-slice-input').val(), timeSliceUnits.getCurrentUnits(), timeStartUnits.getCurrentUnits())).change();
    dataAccess.getNewData(globalControls.getStartTime(), globalControls.getTimeSlice(), "", function (data) {
        redrawer.redrawAll(dataAccess);
        shown = true;
        $('.ajax-loading').fadeOut(50);
        setTimeout(animate, animationDelay);
    });
}
$('#timeline-animate-button').on('click', function () {
    animating = !animating;
    if (animating) {
        $('#animating-glyphicon').removeClass('glyphicon-play');
        $('#animating-glyphicon').addClass('glyphicon-pause');
    }
    else {
        $('#animating-glyphicon').removeClass('glyphicon-pause');
        $('#animating-glyphicon').addClass('glyphicon-play');
    }
    animate();
});
$('#triggers-filter-button').on('click', function () {
    dataAccess.addFilter("TriggerNo", $('#triggers-from').val(), $('#triggers-to').val());
    redrawer.redrawAll(dataAccess);
});
$('#toa-filter-button').on('click', function () {
    dataAccess.addFilter("ToA", $('#toa-from').val(), $('#toa-to').val());
    redrawer.redrawAll(dataAccess);
});
$('#tot-filter-button').on('click', function () {
    dataAccess.addFilter("ToT", $('#tot-from').val(), $('#tot-to').val());
    redrawer.redrawAll(dataAccess);
});
$('#ftoa-filter-button').on('click', function () {
    dataAccess.addFilter("fToA", $('#ftoa-from').val(), $('#ftoa-to').val());
    redrawer.redrawAll(dataAccess);
});
$('#stoa-filter-button').on('click', function () {
    dataAccess.addFilter("sToA", $('#stoa-from').val(), $('#stoa-to').val());
    redrawer.redrawAll(dataAccess);
});
$('#own-filter-button').on('click', function () {
    if (!dataAccess.ownFilter($("#own-filter").val())) {
        return;
    }
    redrawer.redrawAll(dataAccess);
});
$('#reset-filters-button').on('click', function () {
    dataAccess.reset();
    $('.filter-input').val("");
    redrawer.redrawAll(dataAccess);
});
$('#clear-log-button').on('click', function () {
    $('#errors-inner').html("");
    $('#warning-count').html("0");
    $('#error-count').html("0");
    return false;
});
$('#datetimepicker12').on('dp.change', function (e) {
    updateRootListByDate(e.date);
});
$(document).on('click', '.folder-change', function (e) {
    folderName = $(this).attr('folderName') + "/";
    console.log("Folder " + folderName + " clicked.");
    currFolder += folderName;
    updateRootListByFolder(currFolder);
    dataAccess.callForFolderList(currFolder, processFolderList);
    return false;
});
// ==================== END EVENT CALLBACKS ===============================
$('#bin-size-input').change();
$('#time-slice-input').change();
$('#time-start-input').change();
// ==================== AUTOCOMPLETE of the "OWN FILTTER" field ============
function split(val) {
    return val.split(/\s\s*/);
}
function extractLast(term) {
    return split(term).pop();
}
$("#own-filter") // don't navigate away from the field on tab when selecting an item
    .bind("keydown", function (event) {
    if (event.keyCode === $.ui.keyCode.TAB &&
        $(this).autocomplete("instance").menu.active) {
        event.preventDefault();
    }
})
    .autocomplete({
    minLength: 0,
    source: function (request, response) {
        // delegate back to autocomplete, but extract the last term
        response($.ui.autocomplete.filter(dataAccess.getAllowedFilterTags(), extractLast(request.term)));
    },
    focus: function () {
        // prevent value inserted on focus
        return false;
    },
    select: function (event, ui) {
        var terms = split(this.value);
        // remove the current input
        terms.pop();
        // add the selected item
        terms.push(ui.item.value);
        // add placeholder to get the comma-and-space at the end
        terms.push("");
        this.value = terms.join(" ");
        return false;
    }
});
// ====================== END AUTOCOMPLETE ===============================
$(document).tooltip();
$('#datetimepicker12').datetimepicker({
    inline: true,
    sideBySide: true,
    format: "MM/YYYY dd HH",
    defaultDate: Date.now()
});
function processFolderList(data) {
    if (data == null)
        return;
    $('#folder-list').html("");
    var folderList = data.Folders;
    for (var i = 0; i < folderList.length; i++) {
        if (data.Folders[i] == "..") {
            $('#folder-list').append(' <a class="folder-change" folderName="' + data.Folders[i] + '" href="#"><span class="glyphicon glyphicon-level-up"></span></a> ');
        }
        else if (data.Folders[i] == ".") {
            $('#folder-list').append(' <a class="folder-change" id="folder-update" folderName="' + data.Folders[i] + '" href="#"><span class="glyphicon glyphicon-refresh"></span></a> ');
        }
        else {
            $('#folder-list').append(' <a class="folder-change" folderName="' + data.Folders[i] + '" href="#">' + data.Folders[i] + '</a> ');
        }
    }
}
function updateFolders() {
    $('#folder-update').click();
    setTimeout(updateFolders, 5000);
}
$(document).on('ready', function () {
    updateRootListByFolder(currFolder);
    dataAccess.callForFolderList(currFolder, processFolderList);
    updateFolders();
});
