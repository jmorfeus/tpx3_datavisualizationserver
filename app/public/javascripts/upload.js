/// <reference path="../../typings/jquery/jquery.d.ts" />
$(document).on('ready', function () {
    var ok = false;
    $(".non-empty").on('change focus', function () {
        if ($(this).val() == '') {
            ok = false;
            $(this).parent().addClass('has-error');
            $(this).parent().removeClass('has-success');
        }
        else {
            ok = true;
            $(this).parent().addClass('has-success');
            $(this).parent().removeClass('has-error');
        }
    });
    $(".submit-validate").on('click', function (e) {
        if (!ok) {
            e.preventDefault();
            $(".non-empty").change();
            alert("Please choose a file to upload.");
        }
    });
});
/// <reference path="../../typings/jquery/jquery.d.ts" />
/// <reference path="../../typings/jqueryui/jqueryui.d.ts" />
var globalServerAddress;
var serverAddress;
var asciiFilesList = [];
var rootFilesList = [];
var found = [];
var currFolder = $('#folder-list').attr('folder');
if (globalServerAddress) {
    serverAddress = "http://" + globalServerAddress;
}
else {
    serverAddress = "http://localhost:3000";
}
function printFileList() {
    $('#files').append("<h2>List of files: </h2>");
    $('#files').append("<div class=\"container-fluid upload-files\" id=\"files-tbody\"></div>");
    for (var i = 0; i < asciiFilesList.length; i++) {
        if (!found[asciiFilesList[i]]) {
            $('#files-tbody').append("<div class=\"row\"><div class=\"col-md-6 left\">" + asciiFilesList[i] + "</div><div class=\"col-md-6\"><button class=\"btn btn-default convert-file\" filename=\"" + asciiFilesList[i] + "\">Convert to ROOT</button></div></row>");
        }
        else {
            $('#files-tbody').append("<div class=\"row\"><div class=\"col-md-6 left\">" + asciiFilesList[i] + "</div><div class=\"col-md-6\"><button class=\"btn btn-success disabled\">Converted <span class=\"glyphicon glyphicon-ok\"></span> </button></div></row>");
        }
    }
}
function processRootFileList(files) {
    if (files == null)
        return;
    rootFilesList = files.Files;
    for (var i = 0; i < asciiFilesList.length; i++) {
        if (rootFilesList.indexOf(asciiFilesList[i] + ".root") == -1) {
            found[asciiFilesList[i]] = false;
        }
        else {
            found[asciiFilesList[i]] = true;
        }
        if (i == asciiFilesList.length - 1) {
            printFileList();
        }
    }
}
function processFileList(files) {
    if (files == null)
        return;
    asciiFilesList = files.Files;
    $.ajax({
        url: serverAddress + "/rootfiles/files/" + currFolder
    }).done(function (data) {
        console.log("Ajax call for root file list done");
        processRootFileList(JSON.parse(data));
    }).fail(function (err) {
        console.log("Ajax call for root file list error");
        processRootFileList(null);
    });
}
function callForFileList(folder) {
    $.ajax({
        url: serverAddress + "/asciifiles/files/" + folder
    }).done(function (data) {
        console.log("Ajax call for ascii file list done");
        processFileList(JSON.parse(data));
    }).fail(function (err) {
        console.log("Ajax call for ascii file list error");
        processFileList(null);
    });
}
;
function processConvertCallback(data) {
    $('.ajax-loading').fadeOut(50);
    if (data == "ok") {
        window.location.href = "/asciifiles/convert-success" + '/' + currFolder;
    }
    else {
        window.location.href = "/asciifiles/convert-failure" + '/' + currFolder;
    }
}
$(document).on('click', '.convert-file', function () {
    var filename = $(this).attr("filename");
    $('.ajax-loading').fadeIn(50);
    $.ajax({
        url: serverAddress + "/asciifiles/convert/" + filename + '/' + currFolder
    }).done(function (data) {
        console.log("Ajax call converting file done");
        processConvertCallback(data);
    }).fail(function (err) {
        console.log("Ajax call converting list error");
        processConvertCallback(null);
    });
});
$('.theme-select').on('click', function () {
    $('.dropdown-menu input:radio').removeProp("checked");
    var name = $(this).attr("filename");
    $('#css-style').attr("href", "/stylesheets/css/" + name + ".css");
    $('#' + name + "-check").prop("checked", "checked");
});
function processFolderList(data) {
    if (data == null)
        return;
    var asciiFolderList = data.Folders;
    for (var i = 0; i < asciiFolderList.length; i++) {
        if (data.Folders[i] == "..") {
            $('#folder-list').append(' <a href="/asciifiles/upload/' + currFolder + data.Folders[i] + '"><span class="glyphicon glyphicon-level-up"></span></a> ');
        }
        else {
            $('#folder-list').append(' <a href="/asciifiles/upload/' + currFolder + data.Folders[i] + '">' + data.Folders[i] + '</a> ');
        }
    }
}
function callForFolderList() {
    $.ajax({
        url: serverAddress + "/asciifiles/folder-list/" + currFolder
    }).done(function (data) {
        console.log("Ajax call for ascii folder list done");
        processFolderList(JSON.parse(data));
    }).fail(function (err) {
        console.log("Ajax call for ascii folder list error");
        processFolderList(null);
    });
}
callForFolderList();
callForFileList(currFolder);
